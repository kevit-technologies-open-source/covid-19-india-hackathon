importScripts('https://www.gstatic.com/firebasejs/7.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.5.2/firebase-messaging.js');


firebase.initializeApp({
  apiKey: "AIzaSyCJJQacPC9Zu2FTpZ2ueof82zhXaws1h_4",
  authDomain: "fir-project-niti.firebaseapp.com",
  databaseURL: "https://fir-project-niti.firebaseio.com",
  projectId: "fir-project-niti",
  storageBucket: "fir-project-niti.appspot.com",
  messagingSenderId: "65982489994",
  appId: "1:65982489994:web:5c0f03d0b9934365c49473"
});
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true
    })
    .then(windowClients => {
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[0];
        windowClient.postMessage(payload);
      }
    });
  return promiseChain;
});

self.addEventListener('notificationclick', function (event) {
  if (!event.action) {
    // Was a normal notification click
    event.notification.close();
    event.waitUntil(
      clients.matchAll({includeUncontrolled: true, type: 'window'}).then(function (clientList) {
        clientList = clientList.filter(client => {
          return (client.url.includes(event.notification.data.baseUrl));
        });
        if (clientList.length) {
          for (let i = 0; i < clientList.length; ++i) {
            const client = clientList[i];
            if (client.url.includes(event.notification.data.baseUrl) && 'focus' in client) {
              client.focus();
              // return client.focus();
            }
          }
        } else if (!clientList.length) {
          return self.clients.openWindow(event.notification.data.baseUrl)
        }
      }),
    );
    return;
  }

  switch (event.action) {
    case 'start-chat':
      event.notification.close();
      event.waitUntil(clients.matchAll({includeUncontrolled: true, type: 'window'}).then(function (clientList) {
          clientList = clientList.filter(client => {
            return (client.url.includes(event.notification.data.baseUrl));
          });
          if (clientList.length) {
            for (let i = 0; i < clientList.length; ++i) {
              const client = clientList[i];
              if (client.url.includes(event.notification.data.baseUrl) && 'focus' in client) {
                client.postMessage({id: event.notification.data.id, type: 'accept'});
                client.focus();
                // return client.focus();
              }
            }
          } else if (!clientList.length) {
            return self.clients.openWindow(event.notification.data.baseUrl)
          }
        }),
      );
      break;
    case 'leave':
      event.notification.close();
      break;
  }
});

