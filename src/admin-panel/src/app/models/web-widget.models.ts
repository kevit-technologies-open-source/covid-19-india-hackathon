export interface LookAndFeelModel {
  widget: {
    bot: {
      bubbleColour: string,
      fontColour: string,
      fontStyle: string
    },
    user: {
      bubbleColour: string,
      fontColour: string,
      fontStyle: string
    },
    inputBackground: {
      bubbleColour: string,
      fontColour: string
    },
    agent: {
      bubbleColour: string,
      fontColour: string,
      fontStyle: string
    },
    position: string,
    size: number,
    chatBackgroundColour: string,
    chatFontColour: string
    iconUrl: string,
  },
  botName: string
}
