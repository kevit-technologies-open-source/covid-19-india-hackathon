import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "MinuteToHours"
})
export class MinuteToHours implements PipeTransform {
  transform(val: any) {
    return Math.floor(val / 60) + ':' + val % 60;
  }
}
