import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import {Injectable} from "@angular/core";
import {AccountService} from "../services/account.service";
import {Observable, Subject} from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate {
  _userActionOccured: Subject<void> = new Subject();
  get userActionOccured(): Observable<void> {
    return this._userActionOccured.asObservable()
  };

  constructor(private router: Router, private accountService: AccountService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const token = this.accountService.getToken();
    const user = this.accountService.getUserName();
    if (token && user) {
      return true;
    } else {
      this.accountService
        .logout()
        .then(res => {
          this.router.navigate(["login"], {
            queryParams: {returnUrl: state.url}
          });
        })
        .catch(err => {
          this.router.navigate(["login"], {
            queryParams: {returnUrl: state.url}
          });
        });

      return false;
    }
  }

  notifyUserAction() {
    this._userActionOccured.next();
  }
}
