import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'initials'
})
export class GetInitialsPipe implements PipeTransform {
  transform(value: any, ...args): any {
    return value.replace(/[a-z]/g, '').replace(' ', '');
  }

}
