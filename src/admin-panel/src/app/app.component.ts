import {Component, HostListener} from '@angular/core';
import {Router} from "@angular/router";
import {AuthGuard} from "./utils/authGuard";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loading;

  constructor(private authGuard: AuthGuard) {
    this.loading = false;
  }

  @HostListener('document:keyup', ['$event'])
  @HostListener('document:click', ['$event'])
  @HostListener('document:wheel', ['$event'])
  resetTimer() {
    this.authGuard.notifyUserAction();
  }
}
