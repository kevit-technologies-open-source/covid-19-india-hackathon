import { Component, HostListener, OnInit } from '@angular/core';
import { ChartType, ChartDataSets ,Chart} from 'chart.js';
import { Label } from 'ng2-charts';
import { Subject } from "rxjs";
import { AccountService } from "../../services/account.service";
import * as moment from "moment";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import {NotificationService} from '../../services/notification.service';
import {AnalyticsService} from '../../services/analytics.service';
import {ExcelService} from '../../services/excel-file.service';
import * as XLSX from 'xlsx';
import {WorkBook} from 'xlsx';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {

  partOfTimeDate: { startDate: any; endDate: any }={
    startDate:new Date(moment().startOf("month").format()),
    endDate: new Date()
  };
  agentSpecificDate: { startDate: any; endDate: any }={
    startDate:new Date(moment().startOf("month").format()),
    endDate: new Date()
  };
  customerSupportDate: { startDate: any; endDate: any }={
    startDate:new Date(moment().startOf("month").format()),
    endDate: new Date()
  };
  mostCommunicatedDate: { startDate: any; endDate: any }={
    startDate:new Date(moment().startOf("month").format()),
    endDate: new Date()
  };
  totalUserDate: { startDate: any; endDate: any }={
    startDate:new Date(moment().startOf("month").format()),
    endDate: new Date()
  };
  maxDate=moment();
  totalUsersOptions: any = {};
  totalUsersLabels: Label[] = [];
  totalUsersType: ChartType = 'bar';
  totalUsersLegend = true;
  totalUsersPlugins = [];
  totalUsersData: ChartDataSets[] = [
    {
      data: [], label: 'Conversation with Bot',
      barPercentage: 0.8,
    },
    {
      data: [], label: 'Conversation with Customer Support',
      barPercentage: 0.8,
    }
  ];
  totalUsersColors = [
    {
      backgroundColor: 'rgba(23, 191, 193)',
      borderColor: 'rgb(23, 191, 193)',
      pointBackgroundColor: 'rgb(23, 191, 193)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(23, 191, 193, .8)',
    },
    {
      backgroundColor: 'rgba(4, 128, 129)',
      borderColor: 'rgb(4, 128, 129)',
      pointBackgroundColor: 'rgb(4, 128, 129)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(4, 128, 129, .8)',
      borderWidth: '1'
    }
  ];
  hoursData: any = [
    {
      data: [], label: 'Conversation with Bot',
      barPercentage: 0.8,
    },
    {
      data: [], label: 'Conversation with Customer Support',
      barPercentage: 0.8,
    }
  ];
  hoursLabels: Label[] = [];
  hoursOptions: any = {};
  hoursColors = [
    {
      backgroundColor: 'rgba(23, 191, 193)',
      borderColor: 'rgb(23, 191, 193)',
      pointBackgroundColor: 'rgb(23, 191, 193)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(23, 191, 193, .8)',
    },
    {
      backgroundColor: 'rgba(4, 128, 129)',
      borderColor: 'rgb(4, 128, 129)',
      pointBackgroundColor: 'rgb(4, 128, 129)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(4, 128, 129, .8)',
      borderWidth: '1'
    }
    ];
  pieChartType: any = 'pie';
  customerChatLabels: string[] = ['Initiated by Support Agents', 'Initiated by Users'];
  customerChatData: number[] = [];
  customerChatColors: any[] = [
    {
      backgroundColor: ['#048081', '#17BFC1']
    }
  ];
  customerChatOptions: any = {
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          let label = data.labels[tooltipItem.index].split('-');
          return label[0]+': '+data.datasets[0].data[tooltipItem.index];
        }
      }
    },
    responsive: true,
    legend: {
      position: 'left',
      align: 'start',
      labels: {
        fontSize: 14,
        padding: 20,
        fontWeight: 600,
        usePointStyle: true,
      }
    }
  };
  agentRequestsData: number[] = [];
  agentRequestAllColors: any[] = [
    {
      // backgroundColor: ["#e8fcfc","#bbf6f7","#8df0f1","#60eaec","#33e4e6", "#19cacc","#139d9f","#0e7072", "#084344","#031617"]
      backgroundColor: ["#e8f9f9","#baeded","#8df0f1","#5dd4d6","#46ced0", "#16afb0","#0e7476","#0a4e4e", "#052727","#000000"]
    }
  ];
  agentRequestColors: any[] = [
    {
      backgroundColor: []
    }
  ];
  agentRequestLabels: string[] = [];
  agentRequestOption: any = {
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          let label = data.labels[tooltipItem.index].split('-');
          return label[0]+': '+data.datasets[0].data[tooltipItem.index];
        }
      }
    },
    plugins: {
      datalabels: {
        display: function(context) {
          return context.dataset.data[context.dataIndex]!==0; // display labels with an odd index
        },
        clamp:true,
        borderRadius:'50',
        clip:true,
        color: function(context) {
          if(context.dataset.backgroundColor[context.dataIndex]==='#e8f9f9' ||
            context.dataset.backgroundColor[context.dataIndex]==='#baeded' ||
            context.dataset.backgroundColor[context.dataIndex]==='#8df0f1'){
            return '#000';
          }else {
            return '#fff';
          }
        },
        font: {
          weight: 'bold'
        },
      }
    },
    legend: {
      position: 'left',
      align: 'center',
      labels: {
        fontSize: 14,
        padding: 20,
        fontWeight: 600,
        usePointStyle: true
      }
    }
  };
  partOfTimeRequestArrivedOptions: any = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          let label = data.labels[tooltipItem.index].split('-');
          return label[0]+': '+data.datasets[0].data[tooltipItem.index];
        }
      }
    },
    legend: {
      position: 'left',
      align: 'start',
      labels: {
        fontSize: 14,
        padding: 20,
        fontWeight: 600,
        usePointStyle: true,
      }
    }
  };
  partOfTimeRequestArrivedData: number[] = [40, 60];
  partOfTimeRequestArrivedLabel: string[] = ["Arrived during team's Working Hours", "Arrived outside team's Working Hours"];
  scrHeight: any;
  scrWidth: any;
  isiPad = false;
  private unsubscribe: Subject<void> = new Subject();
  topAgentCount:number=3;
  totalAgentCount=0;
  MostCommunicatedHoursTime: any='pm';
  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
  }

  constructor(public _analyticsService: AnalyticsService,
              public _notificationService:NotificationService,
              public _authService:AccountService,
              public _excelFileService:ExcelService) {
    Chart.plugins.register(ChartDataLabels);
    Chart.helpers.merge(Chart.defaults.global.plugins.datalabels, {
      display: function(context) {
        return context.dataset.data[context.dataIndex]!==0; // display labels with an odd index
      },
      align: 'start',
      anchor:'end',
          clamp:true,
      borderRadius:'50',
      clip:true,
          color: '#fff',
          font: {
            weight: 'bold',
            lineHeight:0
          },
    });
    this.getScreenSize();
    if (this.scrWidth <= 1024) {
      this.isiPad = true;
      _authService.isiPad = true;
    } else {
      this.isiPad = false;
      _authService.isiPad = false;
    }

  }

  ngOnInit() {
    this.setDefaultChartConfig();
  }
  setDefaultChartConfig(){
    if(this.isiPad) {
      this.customerChatOptions = {
        responsive: true,
        legend: {
          position: 'top',
          align: 'center',
        },
        labels: {
          fontSize: 14,
          padding: 20,
          fontWeight: 600,
          usePointStyle: true,
        }
      };
      this.partOfTimeRequestArrivedOptions = {
        responsive: true,
        legend: {
          position: 'top',
          align: 'start',
        },
        labels: {
          fontSize: 14,
          padding: 20,
          fontWeight: 600,
          usePointStyle: true,
        }
      };
      this.agentRequestOption = {
        responsive: true,
        legend: {
          position: 'left',
          align: 'start',
        },
        labels: {
          fontSize: 14,
          fontWeight: 600,
          usePointStyle: true
        }
      }
    }
  }
  changeDate(type) {
    let date;
    switch (type) {
       case 'partOfTime':
         date={
           startDate:new Date(JSON.parse(JSON.stringify(this.partOfTimeDate.startDate))),
           endDate:new Date(JSON.parse(JSON.stringify(this.partOfTimeDate.endDate))),
         };
         date.startDate.setHours(0,0,0,0);
         date.endDate.setHours(0,0,0,0);
         date.startDate.setTime(date.startDate.getTime()-
           (date.startDate.getTimezoneOffset()  * 60 * 1000));
         date.endDate.setTime(date.endDate.getTime()-
           (date.endDate.getTimezoneOffset()  * 60 * 1000));
       this.getPartOfTimeRequest(date);
       break;
       case 'agentSpecific':
         date={
           startDate:new Date(JSON.parse(JSON.stringify(this.agentSpecificDate.startDate))),
           endDate:new Date(JSON.parse(JSON.stringify(this.agentSpecificDate.endDate))),
         };
         date.startDate.setHours(0,0,0,0);
         date.endDate.setHours(0,0,0,0);
         date.startDate.setTime(date.startDate.getTime()-
           (date.startDate.getTimezoneOffset()  * 60 * 1000));
         date.endDate.setTime(date.endDate.getTime()-
           (date.endDate.getTimezoneOffset()  * 60 * 1000));
       this.getAgentSpecificRequest(date);
       break;
       case 'customerSupport':
         date={
           startDate:new Date(JSON.parse(JSON.stringify(this.customerSupportDate.startDate))),
           endDate:new Date(JSON.parse(JSON.stringify(this.customerSupportDate.endDate))),
         };
         date.startDate.setHours(0,0,0,0);
         date.endDate.setHours(0,0,0,0);
         date.startDate.setTime(date.startDate.getTime()-
           (date.startDate.getTimezoneOffset()  * 60 * 1000));
         date.endDate.setTime(date.endDate.getTime()-
           (date.endDate.getTimezoneOffset()  * 60 * 1000));
        this.getCustomerSupportRequest(date);
        break;
       case 'mostCommunicated':
         date={
           startDate:new Date(JSON.parse(JSON.stringify(this.mostCommunicatedDate.startDate))),
           endDate:new Date(JSON.parse(JSON.stringify(this.mostCommunicatedDate.endDate))),
         };
         date.startDate.setHours(0,0,0,0);
         date.endDate.setHours(0,0,0,0);
         date.startDate.setTime(date.startDate.getTime()-
           (date.startDate.getTimezoneOffset()  * 60 * 1000));
         date.endDate.setTime(date.endDate.getTime()-
           (date.endDate.getTimezoneOffset()  * 60 * 1000));
        this.getMostCommunicatedRequest(date);
        break;
       case 'totalUser':
         date={
           startDate:new Date(JSON.parse(JSON.stringify(this.totalUserDate.startDate))),
           endDate:new Date(JSON.parse(JSON.stringify(this.totalUserDate.endDate))),
         };
         date.startDate.setHours(0,0,0,0);
         date.endDate.setHours(0,0,0,0);
         date.startDate.setTime(date.startDate.getTime()-
           (date.startDate.getTimezoneOffset()  * 60 * 1000));
         date.endDate.setTime(date.endDate.getTime()-
           (date.endDate.getTimezoneOffset()  * 60 * 1000));
        this.getTotalUserRequest(date);
        break;
   }
  }
  getPartOfTimeRequest(date){
    this._analyticsService.getPartOfTimeRequest(date)
      .then((res:any) => {
        this.partOfTimeRequestArrivedLabel= ["Arrived during team's Working Hours", "Arrived outside team's Working Hours"];
        Object.keys(res).forEach((key,index)=>{
          this.partOfTimeRequestArrivedData[index]=res[key];
          this.partOfTimeRequestArrivedLabel[index]=this.partOfTimeRequestArrivedLabel[index]+' - '+res[key];
        });
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  getAgentSpecificRequest(date){
    this._analyticsService.getAgentSpecificRequest(date,this.topAgentCount)
      .then((res:any) => {
        this.agentRequestLabels=[];
        this.agentRequestColors[0].backgroundColor=[];
        this.agentRequestsData=[];
        this.totalAgentCount=res.totalAgentCount;
        if(res.topAgents.length>0){
          res.topAgents.forEach((agent,index)=>{
            this.agentRequestLabels.push(agent.name+' - '+agent.count);
            this.agentRequestColors[0].backgroundColor.push(this.agentRequestAllColors[0].backgroundColor[index]);
            this.agentRequestsData.push(agent.count);
          });
        }
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  getCustomerSupportRequest(date){
    this._analyticsService.getCustomerSupportRequest(date)
      .then((res:any) => {
        this.customerChatLabels=['Initiated by Support Agents', 'Initiated by Users'];
        if(Object.keys(res).length>0) {
          Object.keys(res).forEach((key, index) => {
            this.customerChatLabels[index]= this.customerChatLabels[index]+' - '+res[key];
            this.customerChatData[index] = res[key];
          });
        }
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  getMostCommunicatedRequest(date){
    this._analyticsService.getMostCommunicatedRequest(date,this.MostCommunicatedHoursTime)
      .then((res:any) => {
        this.hoursData[0].data=[];
        this.hoursData[1].data=[];
        this.hoursLabels=[];
        if(res.length>0) {
          let max=0
          res.forEach((hour, index) => {
            if(hour.chatWithBot>max){
              max=hour.chatWithBot
            }
            if(hour.chatWithAgent>max){
              max=hour.chatWithAgent
            }
            this.hoursData[0].data.push(hour.chatWithBot);
            this.hoursData[1].data.push(hour.chatWithAgent);
           this.hoursLabels.push(hour.hour)
          });
          let maxRoundToOne = Math.ceil(max * 1.1);
          let divider = Math.ceil(maxRoundToOne/5);
          let roundedDiveder = Math.ceil(divider / Math.pow(10, divider.toString().length - 1)) * Math.pow(10, divider.toString().length - 1);
          let finalCount = Math.ceil(maxRoundToOne / roundedDiveder) * roundedDiveder;
          this.hoursOptions={
            plugins: {
              datalabels: {
                anchor:'end',
                display: function(context) {
                  return context.dataset.data[context.dataIndex]!==0; // display labels with an odd index
                },
                clamp:false,
                align: 'end',
                color: '#4d4f5c',
                font: {
                  weight: 'bold',
                  lineHeight:0

                },
              }
            },
            responsive: true,
            scales: {
              xAxes: [{
                categoryPercentage: 0.4,
                gridLines: {
                  display: false
                }
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  suggestedMin: 0,
                  suggestedMax: finalCount
                }
              }]
            },
            legend: {
              align: 'start',
              padding: 20,
            }
          };
        }
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  getTotalUserRequest(date){
    this._analyticsService.getTotalUserRequest(date)
      .then((res:any) => {
        this.totalUsersLabels=[];
        this.totalUsersData[0].data=[];
        this.totalUsersData[1].data=[];
        if(Object.keys(res).length>0) {
          let max=0;
          Object.keys(res).forEach((date, index) => {
            if(res[date].withBot>max){
              max=res[date].withBot
            }
            if(res[date].withAgent>max){
              max=res[date].withAgent
            }
            this.totalUsersLabels.push(moment(date).format('ll'));
            this.totalUsersData[0].data.push(res[date].withBot);
            this.totalUsersData[1].data.push(res[date].withAgent);
          });
          let maxRoundToOne = Math.ceil(max * 1.1);
          let divider = Math.ceil(maxRoundToOne/5);
          let roundedDiveder = Math.ceil(divider / Math.pow(10, divider.toString().length - 1)) * Math.pow(10, divider.toString().length - 1);
          let finalCount = Math.ceil(maxRoundToOne / roundedDiveder) * roundedDiveder;
          this.totalUsersOptions={
            plugins: {
              datalabels: {
                anchor:'end',
                display: function(context) {
                  return context.dataset.data[context.dataIndex]!==0; // display labels with an odd index
                },
                clamp:false,
                align: 'end',
                color: '#4d4f5c',
                font: {
                  weight: 'bold',
                  lineHeight:0

                },
              }
            },
            responsive: true,
            scales: {
              xAxes: [{
                categoryPercentage: 0.4,
                gridLines: {
                  display: false
                }
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  suggestedMin: 0,
                  suggestedMax: finalCount
                }
              }]
            },
            legend: {
              align: 'start',
              padding: 20,
            }
          }
        }
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  downloadExcelFile(type) {
    let data: any = [];
    let worksheet: XLSX.WorkSheet;
    let wscols;
    switch (type) {
      case 'partOfTime':
        data.push({
          'Analytics for':'Start Date',
          'Part of Time Request Arrived':this.partOfTimeDate.startDate
        });
        data.push({
          'Analytics for':'End Date',
          'Part of Time Request Arrived':this.partOfTimeDate.endDate
        });
        worksheet= XLSX.utils.json_to_sheet(data,
          { skipHeader: false});
        data.length=0;
        data.push({
          'Part of Time of Day':'In working Hours',
          'Number of Requests':this.partOfTimeRequestArrivedData[0]
        });
        data.push({
          'Part of Time of Day':'Out of working hours',
          'Number of Requests':this.partOfTimeRequestArrivedData[1]
        });
        wscols = [
          {wch: 15}, // "characters"
          {wch: 20}, // "pixels"
        ];
        worksheet['!cols'] = wscols;
        XLSX.utils.sheet_add_json(worksheet,
          data
          , {skipHeader: false, origin: "A5"});
        this._excelFileService.exportAsExcelFile(worksheet, 'PartOfTimeRequestArrived');
        break;
      case 'agentSpecific':
        data.push({
          'Analytics for':'Start Date',
          'Agent Specific Requests':this.agentSpecificDate.startDate
        });
        data.push({
          'Analytics for':'End Date',
          'Agent Specific Requests':this.agentSpecificDate.endDate
        });
        worksheet= XLSX.utils.json_to_sheet(data,
          { skipHeader: false});
        data.length=0;
        this.agentRequestLabels.forEach((label,index) => {
          data.push({
            'Agent Name':label.split('(')[0],
            'Number of Requests':this.agentRequestsData[index]
          });
        });
        data.sort((a, b) => (a['Number of Requests'] < b['Number of Requests'] ? 1 :
          b['Number of Requests'] < a['Number of Requests'] ? -1 : 0));
        wscols = [
          {wch: 15}, // "characters"
          {wch: 20}, // "pixels"
        ];
        worksheet['!cols'] = wscols;
        XLSX.utils.sheet_add_json(worksheet,
          data
          , {skipHeader: false, origin: "A5"});
        this._excelFileService.exportAsExcelFile(worksheet, 'AgentSpecificRequest');
        break;
      case 'customerSupport':
        data.push({
          'Analytics for':'Start Date',
          'Agent Specific Requests':this.agentSpecificDate.startDate
        });
        data.push({
          'Analytics for':'End Date',
          'Agent Specific Requests':this.agentSpecificDate.endDate
        });
        worksheet= XLSX.utils.json_to_sheet(data,
          { skipHeader: false});
        data.length=0;
        data.push({
          'Initiated By':'Support Agents',
          'Number of Requests':this.customerChatData[0]
        });
        data.push({
          'Initiated By':'Support Agents',
          'Number of Requests':this.customerChatData[1]
        });
        wscols = [
          {wch: 15}, // "characters"
          {wch: 20}, // "pixels"
        ];
        worksheet['!cols'] = wscols;
        XLSX.utils.sheet_add_json(worksheet,
          data
          , {skipHeader: false, origin: "A5"});
        this._excelFileService.exportAsExcelFile(worksheet, 'ChatWithCustomerSupport');
        break;
      case 'mostCommunicated':
        data.push({
          'Analytics for':'Start Date',
          'Most Communicated Hours':this.mostCommunicatedDate.startDate
        });
        data.push({
          'Analytics for':'End Date',
          'Most Communicated Hours':this.mostCommunicatedDate.endDate
        });
        worksheet= XLSX.utils.json_to_sheet(data,
          { skipHeader: false});
        data.length=0;
        this.hoursLabels.forEach((label,index) => {
          data.push({
            'Hour of Day':label,
            'Number of Conversations':this.hoursData[0].data[index]
          });
        });
        wscols = [
          {wch: 15}, // "characters"
          {wch: 20}, // "pixels"
        ];
        worksheet['!cols'] = wscols;
        XLSX.utils.sheet_add_json(worksheet,
          data
          , {skipHeader: false, origin: "A5"});
        this._excelFileService.exportAsExcelFile(worksheet, 'MostCommunicatedHours');
        break;
      case 'totalUser':
        data.push({
          'Analytics for':'Start Date',
          'Total Users':this.partOfTimeDate.startDate
        });
        data.push({
          'Analytics for':'End Date',
          'Total Users':this.partOfTimeDate.endDate
        });
        worksheet= XLSX.utils.json_to_sheet(data,
          { skipHeader: false});
        data.length=0;
        this.totalUsersLabels.forEach((label,index) => {
          data.push({
            'Start Date of Group':label,
            'Conversations with Bot':this.totalUsersData[0].data[index],
            'Conversations with Customer Support':this.totalUsersData[1].data[index]
          });
        });
        wscols = [
          {wch: 15}, // "characters"
          {wch: 20}, // "pixels"
          {wch: 30}, // "pixels"
        ];
        worksheet['!cols'] = wscols;
        XLSX.utils.sheet_add_json(worksheet,
          data
        , {skipHeader: false, origin: "A5"});
        this._excelFileService.exportAsExcelFile(worksheet, 'Total-Users');
        break;
    }
  }
}
