import {
  AfterViewInit,
  Component, ElementRef, HostListener, OnDestroy,
  OnInit, ViewChild,
} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {HandoffService} from "../../services/handoff.service";
import {AccountService} from "../../services/account.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MessagingService} from "../../services/messaging.service";
import {Subject} from "rxjs";
import {environment} from "../../../environments/environment";
import {DirectLine} from 'botframework-directlinejs';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DialogComponent} from '../../components/dialog/dialog.component';
import {NotificationService} from '../../services/notification.service';
import {HttpClient, HttpEventType} from '@angular/common/http';
import { Urls } from "../../utils/urls";
import { takeUntil } from "rxjs/operators";

declare var $: any;

@Component({
  selector: "app-hand-off",
  templateUrl: "./hand-off.component.html",
  styleUrls: ["./hand-off.component.scss"]
})
export class HandOffComponent implements OnInit, AfterViewInit,OnDestroy {
  @ViewChild('input', {static: false}) input: ElementRef;
  @HostListener("window:resize", ["$event"])
  message: any = new Subject();
  private unsubscribe: Subject<any> = new Subject();
  isMobile = false;
  scrHeight: any;
  scrWidth: any;
  dialogRef;
  displaycomingSoonModel = '';
  transcriptOfUser:any='';
  enableLoader = false;
  enableLoaderchatLoader = false;
  listOfUsers=[];
  isCompleted = false;
  fcmToken;
  errorMessages = false;
  directLine;
  isWaitingEnable = false;
  editId;
  directlineBotName=environment.directlineBotName;
  directlineBotName1=environment.directlineBotName;
  mainUserId: string='';
  chatWith: any='chatWithAgent';
  chatType: any='';
  pageNo = 1;
  pageLimit = 15;
  selectedUserIndex: any;
  isScrollDone: boolean=false;
  search: any = "";
  searchIntent='';
  searchIntents=[];
  intents=[];
  selectedIntentIndex:any='';
  searchAgent='';
  searchAgents=[];
  agents=[];
  selectedAgentIndex:any='';
  agentId=this.accountService.getId();
  agentAssigned='';
  isChatSet=true;
  photoUrl = this.accountService.getUserProfile();
  userName = this.accountService.getUserName();
  selectedImage: any='';
  isUserListSet=false;

  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
  }

  constructor(public dialog: MatDialog,
              private http: HttpClient,
              private handoffService: HandoffService,
              private snackBar: MatSnackBar,
              private messagingService: MessagingService,
              private accountService: AccountService,
              private route: ActivatedRoute,
              private router: Router,
              public _notificationService:NotificationService,
              private activated: ActivatedRoute) {
    this.getScreenSize();
    this.isMobile = this.scrWidth <= 1024;
    this.fcmToken = this.accountService.getFCMToken();
    this.mainUserId=this.accountService.getId();
    if(this.chatWith==='chatWithAgent'){
      if(localStorage.getItem(this.accountService.getId()+'-ChatType-Agent')===null){
        localStorage.setItem(this.accountService.getId()+'-ChatType-Agent','myRequest');
        this.chatType='myRequest';
      }else {
        this.chatType=localStorage.getItem(this.accountService.getId()+'-ChatType-Agent');
      }
      if(localStorage.getItem(this.accountService.getId()+'-ChatType-Bot')===null){
        localStorage.setItem(this.accountService.getId()+'-ChatType-Bot','applied');
      }
    }else {
      if(localStorage.getItem(this.accountService.getId()+'-ChatType-Bot')===null){
        localStorage.setItem(this.accountService.getId()+'-ChatType-Bot','applied');
        this.chatType='applied';
      }else {
        this.chatType=localStorage.getItem(this.accountService.getId()+'-ChatType-Bot');
      }
      if(localStorage.getItem(this.accountService.getId()+'-ChatType-Agent')===null){
        localStorage.setItem(this.accountService.getId()+'-ChatType-Agent','myRequest');
      }
    }
  }


  ngOnInit() {
    this.getIntents();
    this.getAgents();
    if(this.activated.snapshot.queryParams.status){
      this.chatType=this.activated.snapshot.queryParams.status;
      if(this.chatType ==='applied' || this.chatType ==='neverApplied'){
        this.chatWith='chatWithBot';
      }
    }
    if (this.activated.snapshot.queryParams.id) {
      this.editId = this.activated.snapshot.queryParams.id;
      // const data = {
      //   handOffId: this.editId,
      //   isAlreadyAssigned: true,
      // };
      // this.selectedUser(data, '', '');
      this.displayUser(this.editId,this.chatWith,this.chatType);
    }else {
      this.displayUser('',this.chatWith,this.chatType);
    }

    this.mediumSelected('wc');

    this.message = this.messagingService.currentMessage.subscribe(value => {
    });
    // this.message.subscribe((res: any) => {
    //   if (res) {
    //     const data = JSON.parse(res.data.payload);
    //     if (this.listOfUsers && data.isWaiting) {
    //       if (!this.isCompleted) {
    //         this.listOfUsers.unshift(data.userData);
    //       }
    //       // this.displayUser(this.isCompleted ? 'completed' : 'pending');
    //     }
    //     if (data.isCompleted) {
    //       this.displayUser(this.isCompleted ? 'completed' : 'pending', '', 'chatCompleted');
    //     }
    //     if (this.listOfUsers && data.isAlreadyAssigned && !this.isCompleted) {
    //       this.listOfUsers.forEach((d, index) => {
    //         if (this.listOfUsers[index].handOffId === res.handOffId) {
    //           this.listOfUsers[index].isAlreadyAssigned = true;
    //         }
    //       })
    //     }
    //   }
    // }, error => {
    //   console.log(error);
    // });


    this.componentDidMount();

  }

  getSearchList(event) {
    if (event.keyCode === 8 || event.keyCode === 46) {
      if (this.search.length === 1) {
        this.search = "";
        this.pageNo=1;
        this.getUserList(false,true);
      }
    }
    if (event.keyCode === 13) {
      this.pageNo=1;
      this.getUserList(false,true);
    }
  }
  getIntentSearchList(event) {
    setTimeout(() =>{
      if (event.target.value.trim() === '') {
        this.searchIntent = "";
        this.searchIntents=this.intents;
      }else {
        this.selectedIntentIndex='';
        this.searchIntents=this.intents.filter((intent)=>intent.displayName.search(new RegExp(event.target.value,"i"))!== -1);
      }
    },0);
  }
  getAgentSearchList(event) {
    setTimeout(() =>{
    if (event.target.value.trim() === '') {
        this.searchAgent = "";
        this.searchAgents=this.agents;
      }else {
        this.selectedAgentIndex='';
        this.searchAgents=this.agents.filter((agent)=>agent.name.search(new RegExp(event.target.value,"i"))!== -1);
      }
    },0);
  }
  doSearchIntents(){
    if (this.searchIntent.trim() === '') {
      this.searchIntent = "";
      this.searchIntents=this.intents;
    }else {
      this.selectedIntentIndex='';
      this.searchIntents=this.intents.filter((intent)=>intent.displayName.search(this.searchIntent)!== -1);
    }
  }
  doSearchAgents(){
    if (this.searchAgent.trim() === '') {
      this.searchAgent = "";
      this.searchAgents=this.agents;
    }else {
      this.selectedAgentIndex='';
      this.searchAgents=this.agents.filter((agent)=>agent.name.search(this.searchAgent)!== -1);
    }
  }

  async componentDidMount() {
    if(navigator.serviceWorker){

    navigator.serviceWorker.addEventListener("message", (message: any) =>
    {
      let isPageOpen='';
      if(this.router.routerState.snapshot.url.split('/').length>0){
         isPageOpen=this.router.routerState.snapshot.url.split('/')
          [this.router.routerState.snapshot.url.split('/').length-1].split('?')[0];
      }

      let messageRecieve;
      if (message.data.hasOwnProperty('firebaseMessagingData')) {
        messageRecieve = JSON.parse(message.data.firebaseMessagingData.data.payload);

      } else {
        messageRecieve = JSON.parse(message.data.data.payload);
      }
      console.log(messageRecieve);
      if(isPageOpen==='hand-off'){
        if (messageRecieve.isWaiting) {
          if (this.chatWith==='chatWithAgent') {
            if(this.chatType==='newRequest'){
              if(messageRecieve.userData){
                this.listOfUsers.unshift(messageRecieve.userData);
              }else if(messageRecieve.handOffData){
                this.listOfUsers.unshift(messageRecieve.handOffData);
              }
              this.getUnreadMessageCount();
            }

          }
        }
        if (messageRecieve.isCompleted) {
          if(messageRecieve.handOffId===this.editId){
            this.displayUser('', '', this.chatType);
          }else {
            this.displayUser(this.chatWith, '', this.chatType);
          }
          if(messageRecieve.message){
            this._notificationService.sendNotification('success',messageRecieve.message);
          }
        }
        if (this.listOfUsers && messageRecieve.isAlreadyAssigned && this.chatWith==='chatWithAgent'
          && this.chatType==='newRequest') {
          this.listOfUsers.forEach((d, index) => {
            if (this.listOfUsers[index].handOffId === messageRecieve.handOffId) {
              this.listOfUsers.splice(index,1);
              this.transcriptOfUser = '';
              this.selectedUserIndex='';
              this.router.navigate(['dashboard/hand-off'], {
                queryParams: {
                  status: this.chatType,
                }
              });
              this.getUnreadMessageCount();
            }
          })
        }
      }else {
        if(messageRecieve.message){
          this._notificationService.sendNotification('success',messageRecieve.message);
        }
      }

    });
  }

  }

  displayUser(getCondition?, fromwhereSelected?, chatType?,hasTranscript?) {
    this.pageNo=1;
    this.search='';
    this.isUserListSet=false;
    this.isChatSet = true;
    this.isScrollDone = true;
    this.chatType = chatType;
    if(fromwhereSelected==='chatWithBot'){
      localStorage.setItem(this.accountService.getId()+'-ChatType-Bot',this.chatType);
    }else if(fromwhereSelected==='chatWithAgent'){
      localStorage.setItem(this.accountService.getId()+'-ChatType-Agent',this.chatType);
    }
    if (!getCondition) {
      this.editId = '';
      this.router.navigate(['dashboard/hand-off'], {
        queryParams: {
          status: chatType,
        }
      });
    } else {
      this.router.navigate(['dashboard/hand-off'], {
        queryParams: {
          status: chatType,
          id: this.editId
        }
      });
    }
    this.listOfUsers.length=0;
    this.enableLoader = false;
    if(hasTranscript){
      this.getUserList(false,'',hasTranscript);
    }else {
      this.transcriptOfUser = '';
      this.getUserList(false,'');
    }

  }

  getUserList(isScrollUser?,isSearch?,hasTranscript?){
    this.handoffService.getUser(this.chatWith,this.chatType,this.pageNo,this.pageLimit,this.search).then((res: any) => {
      this.isUserListSet=true;
      this.enableLoader = true;
      if (isSearch) {
        this.listOfUsers = res;
      } else {
        if (res.length > 0) {
          res.forEach((user: any) => {
            this.listOfUsers.push(user);
          })
        }
      }
      if (!isScrollUser){
        this.getUnreadMessageCount();
      if (this.editId && this.listOfUsers.length > 0) {
        if (!hasTranscript) {
          this.isChatSet = false;
          this.handoffService.getTranscriptData(this.chatWith,
            this.editId).then((res: any) => {

            if(!res){
              this.isChatSet=true;
              this.isScrollDone=true;
            }
            this.enableLoaderchatLoader = false;
            this.transcriptOfUser = res ? res : 'displayError';
            if (this.transcriptOfUser !== 'displayError') {
              this.isScrollDone = false;
              setTimeout(() => {
                this.humanPixelScroll(false);
              }, 100);
            }
            this.listOfUsers.forEach((d, index) => {
              if (this.chatWith === 'chatWithAgent') {
                if (d.handOffId === this.editId) {
                  this.selectedUserIndex = index;
                  this.selectActiveuser(index);
                }
              } else {
                if (d.userId === this.editId) {
                  this.selectedUserIndex = index;
                  this.selectActiveuser(index);
                }
              }


            });
            if(res && res.agentAssigned){
              this.errorMessages = res.agentAssigned === this.mainUserId;
            }else {
              this.errorMessages=false;
            }

            if (this.chatWith === 'chatWithBot') {
              this.errorMessages = false;
            } else {
              if (this.chatType === 'newRequest' || this.chatType === 'othersRequest') {
                this.errorMessages = false;
              } else {
                this.errorMessages = true;
                this.initialize();
              }
            }
          }).catch((err: any) => {
            console.log(err);
            this.snackBar.open(err.error.message ? err.error.message : 'Something went wrong',
              "", {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              });
          })
        } else {
          this.isChatSet = true;
          this.listOfUsers.forEach((d, index) => {
            if (d.handOffId === this.editId) {
              setTimeout(() => {
                this.selectActiveuser(index);
              }, 100)
            }
          });
          if (this.chatWith === 'chatWithBot') {
            this.errorMessages = false;
          } else {
            if (this.chatType === 'newRequest' || this.chatType === 'othersRequest') {
              this.errorMessages = false;
            } else {
              this.errorMessages = true;
            }
          }
        }

      }
    }
    }).catch((err => {
      console.log(err);
      this.snackBar.open(err.error.message ? err.error.message : 'Something went wrong', "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
    }))
  }


  selectActiveuser(index) {
    for (let i = 0; i < document.getElementsByClassName('user-detail').length; i++) {
      if (i === index) {
        document.getElementsByClassName('user-detail')[i].classList.add('user-selected');
      } else {
        document.getElementsByClassName('user-detail')[i].classList.remove("user-selected");
      }
    }
  }

  getUnreadMessageCount() {
    if (this.chatWith==='chatWithAgent') {
      this.messagingService.unreadMessage = 0;
      this.listOfUsers.forEach((d) => {
        if (!d.isAlreadyAssigned) {
          this.messagingService.unreadMessage += 1;
        }
      });
      this.messagingService.unreadMessageSubject.next();
    }
  }

  mediumSelected(getText, comingSoon?) {
    if (getText === 'wc') {
      $('.social-platform').css('filter', 'grayscale(1)');
      $('#wc').css('filter', 'grayscale(0)');
    }
    // else if (getText === 'fb') {
    //   // $('.social-platform').css('filter', 'grayscale(1)');
    //   // $('#fb').css('filter', 'grayscale(0)');
    //   this.displaycomingSoonModel = 'fb';
    //   this.openDialog(comingSoon)
    // } else if (getText === 'wa') {
    //   // $('.social-platform').css('filter', 'grayscale(1)');
    //   // $('#wa').css('filter', 'grayscale(0)');
    //   this.displaycomingSoonModel = 'wa';
    //   this.openDialog(comingSoon)
    // } else if (getText === 'sl') {
    //   // $('.social-platform').css('filter', 'grayscale(1)');
    //   // $('#sl').css('filter', 'grayscale(0)');
    //   this.displaycomingSoonModel = 'sl';
    //   this.openDialog(comingSoon)
    // }
  }

  selectedUser(data, index, model) {
    this.selectedUserIndex=index;
    this.editId=this.chatWith==='chatWithBot' ?this.listOfUsers[this.selectedUserIndex].address.user.id:
      this.listOfUsers[this.selectedUserIndex].handOffId;
    this.directLine = '';
    this.isWaitingEnable = false;
    this.enableLoaderchatLoader = true;
    if (this.isMobile) {
      this.showSelectedChat();
    }
    this.isChatSet=false;
    this.handoffService.getTranscriptData(this.chatWith,
      this.chatWith==='chatWithBot' ? data.address.user.id : data.handOffId).then((res: any) => {
        if(!res){
          this.isChatSet=true;
          this.isScrollDone=true;
        }
      this.router.navigate(['dashboard/hand-off'], {
        queryParams: {
          status: this.chatType,
          id: this.chatWith==='chatWithBot' ? data.address.user.id : data.handOffId
        }
      });
      this.enableLoaderchatLoader = false;
      this.transcriptOfUser = res ? res : 'displayError';
      if(this.transcriptOfUser!==''){
        this.isScrollDone=false;
        setTimeout(() => {
          this.humanPixelScroll(false);
        }, 100);
      }
      if (data && data.hasOwnProperty('agentAssigned') || this.chatWith==='chatWithBot') {
        this.selectActiveuser(index);
      } else {
        if (this.listOfUsers) {
          this.listOfUsers.forEach((d, index) => {
            if (d.handOffId === data.handOffId) {
              this.selectActiveuser(index);
            }
          });
        }
      }
      if(this.chatWith==='chatWithBot'){
        this.errorMessages = false;
      }else {
        if (this.chatType==='newRequest' || this.chatType==='othersRequest') {
          this.errorMessages = false;
        }else {
          this.errorMessages = true;
          this.initialize();
        }
      }

    }).catch((err: any) => {
      console.log(err);
      this.snackBar.open(err.error.message ? err.error.message : 'Something went wrong', "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
    })

  }

  backToChatList() {
    $('.selected-chat-main-container').css('display', 'none');
    $('.selected-chat-main-container').css('transition', 'width 0.5s ease-in-out');
    $('.live-chat-list-main-div').css('display', 'block');
  }

  showSelectedChat() {
    $('.live-chat-list-main-div').css('display', 'none');
    $('.live-chat-list-main-div').css('transition', 'width 0.5s ease-in-out');
    $('.selected-chat-main-container').css('display', 'block');
  }

  getInputValue(value, event?) {
    if(event){
      if (event.keyCode == 13) {
        event.preventDefault();
        event.preventDefault();
        this.sendUserMessage(value);
      }
      return false;
    }else {
      this.sendUserMessage(value);
    }
  }

  sendUserMessage(value){
    if (this.directLine) {
      if (value.trim() !== '') {
        this.transcriptOfUser.transcript.push({isFromBot: true, text: value,timestamp:Date.now(),agentId:this.accountService.getId(),
          isFrom:'agent'});
        this.input.nativeElement.value = '';
        this.isWaitingEnable = true;
        setTimeout(() => {
          this.humanPixelScroll(true);
        }, 100);
        this.sendMessage(value, this.transcriptOfUser.handOffId);
      }
    }
  }

  // openDialog(templateRef) {
  //   this.dialogRef = this.dialog.open(templateRef, {
  //     width: "500px"
  //   });
  //   this.dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //     }
  //     this.dialogRef = null;
  //   });
  // }


  initialize(isNew?) {
    this.directLine = new DirectLine({
      webSocket: true,
      secret: environment.secret
    });
    if(isNew){
      this.sendMessage('00INITIATEHANDOFF00',this.transcriptOfUser.handOffId);
    }
    this.directLine.activity$.filter(activity=> {
      return ((activity.from.id === this.directlineBotName) || (activity.from.id === this.directlineBotName1)) ;
    }).subscribe(activity => {
      if (activity.text.indexOf('handOffId') > 0) {
        if (activity.type === 'message') {
          const userData = JSON.parse(activity.text);
          if (this.activated.snapshot.queryParams.id === userData.handOffId) {
            if(!activity.value){
              if(userData && userData.message && userData.message==='00CHATACCEPTEDBYANOTHERAGENT00'){
                if(userData.handOffId===this.editId){
                  this._notificationService.sendNotification('info',
                    'Chat has already been accepted by agent you assigned the chat.');
                  this.displayUser('', '', this.chatType);
                }
              }else if(userData && userData.message && userData.message==='00NORESPONSEINDIALOGFLOW00'){
                this._notificationService.sendNotification('error',
                    'No response set in Dialogflow, please reply to the user.');
              }else if(userData && userData.message && userData.message==='00CHATMARKEDASCOMPLETED00'){
                if(userData.handOffId===this.editId) {
                  this._notificationService.sendNotification('success',
                    'Chat marked as resolved.');
                  this.displayUser('', '', this.chatType);
                }
              }else if(userData && userData.message && userData.message==='00NOCONVERSATIONIDENTIFIED00'){
                if(userData.handOffId===this.editId) {
                  this._notificationService.sendNotification('success',
                    'As there was no conversation, chat is marked as resolved.');
                  this.displayUser('', '', this.chatType);
                }
              }else if(userData && userData.message && userData.message==='00RESPONSEINDIALOGFLOW00'){
                if(userData.handOffId===this.editId) {
                  this._notificationService.sendNotification('success',
                    'Response of the selected intent sent to user.');
                  this.displayUser('', '', this.chatType);
                }
              }else {
                this.transcriptOfUser.transcript.push({
                  isFromBot: false, text: userData.message,timestamp:activity.timestamp,type:'text'
                });
              }
            }else if (activity.value && activity.value.split('##').length > 1) {
              let value = activity.value.split('##');
              let urls = '';
              if (value.length === 2) {
                urls = value[1];
              } else if (value.length > 2) {
                value.forEach(function (url, index) {
                  if (index !== 0) {
                    urls += url;
                  }
                });
              }
              if (value[0] === 'image') {
                this.transcriptOfUser.transcript.push({
                  isFromBot: false, text: userData.message,timestamp:activity.timestamp,url:urls,type:'image'
                });
              }
            }
            setTimeout(() => {
                this.humanPixelScroll(true);
              }, 100);
            }

        }
      }
    });
  }

  sendMessage(message, data,attachment?) {
    let object;
    attachment?object={
        from: {id: 'agent-' + data,agentUserId:this.agentId},
        type: 'message',
        value:attachment.type+'##'+attachment.url,
        text: message
      }:object={
      from: {id: 'agent-' + data,agentUserId:this.agentId},
      type: 'message',
      text: message
    };
    this.directLine.postActivity(object).subscribe(
      id => {

        },
      error => {
        console.error(error);
      }
    );
  }

  humanPixelScroll(isMsg) {
    const container = document.getElementById('chat-area');
    if (container) {
      if(container.scrollHeight>container.offsetHeight){
        if(!isMsg){
          this.setSmoothScroll('unset');
          this.isScrollDone=false;
        }
        container.scrollTop = container.scrollHeight;
        setTimeout(() => {
          this.setScroll(true);
          this.setSmoothScroll('smooth');
          } , 100);
      }else {
        // if(!isMsg) {
        this.setSmoothScroll('smooth');
        this.setScroll(true);
        // }
      }
    }
  };

  setSmoothScroll(value){
    $(".chat-container").css("scroll-behavior", value);
  }

  setScroll(value){
    this.isChatSet = value;
    this.isScrollDone = value;
  }

  sendCompleteActivity(data) {
    var msg="Resolve request";
    this.dialogRef = this.dialog.open(DialogComponent, {
      panelClass: 'addUser',
      data: {title: msg, template: data, isShowSave: false},
      height: 'auto',
      width: '500px'
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
          this.directLine.postActivity({
            from: {id: 'agent-' + this.transcriptOfUser.handOffId},
            type: 'message',
            text: '00ASKUSERABOUTCOMPLETE00'
          }).subscribe(
            id => {
            },
            error => {
              console.error(error);
            }
          );
      }
      this.dialogRef = null;
    });
  }

  ngAfterViewInit(): void {
    $(".img-preview-action-icon-close, .overlay").click( ()=> {
      $(".show").fadeOut('fast');
    });
  }

  openDialog(template,type) {
    if(type==='selectIntent'){
      this.searchIntent='';
      this.searchIntents=this.intents;
      this.dialogRef = this.dialog.open(DialogComponent, {
        panelClass: 'select-intent',
        data: {title: "Select Intent", template: template, isShowSave: true},
        height: 'auto',
        width: '40vw'
      });
      this.dialogRef.afterClosed().subscribe(result => {this.selectedIntentIndex=''});
    }else if(type==='assignAgent'){
      this.searchAgent='';
      this.searchAgents=this.agents;
      this.dialogRef = this.dialog.open(DialogComponent, {
        panelClass: 'select-intent',
        data: {title: "Agent List", template: template, isShowSave: true},
        height: 'auto',
        width: '40vw'
      });
      this.dialogRef.afterClosed().subscribe(result => {
        this.selectedAgentIndex=''
      });
    }

  }

  setChat(type) {
    this.chatWith=type;
      if (type==='chatWithAgent'){
        this.displayUser('','chatWithAgent',localStorage.getItem(this.accountService.getId()+'-ChatType-Agent'));
      }else if(type==='chatWithBot'){
        this.displayUser('','chatWithBot',localStorage.getItem(this.accountService.getId()+'-ChatType-Bot'));
      }
  }

  onScrollDown() {
    this.pageNo++;
    this.getUserList(true);
  }

  onScrollUp() {}

  showUserConfirmation(model) {
    if(this.transcriptOfUser !== 'displayError') {
      this.dialogRef = this.dialog.open(DialogComponent, {
        panelClass: 'addUser',
        data: {title: "Talk with bot user", template: model, isShowSave: false},
        height: 'auto',
        width: '500px'
      });
      this.dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.router.navigate(['dashboard/hand-off'], {
            queryParams: {
              status: this.chatType,
              id: this.listOfUsers[this.selectedUserIndex].handOffId
            }
          });
          if (this.chatType === 'newRequest') {
            this.handoffService.checkUserIsAvailable(this.listOfUsers[this.selectedUserIndex].handOffId).then((res: any) => {
              if (res) {
                this.initialize(true);
                this.displayUser(this.editId, this.chatWith, 'myRequest', true);
              }
            }).catch((err: any) => {
              this._notificationService.sendNotification('error', err.error.message ? err.error.message : 'Something went wrong');
            });
          } else if (this.chatType === 'neverApplied' || this.chatType === 'applied') {
            this.handoffService.initiateHandOff(this.listOfUsers[this.selectedUserIndex].userId).then((res: any) => {
              if (res) {
                this.chatWith = 'chatWithAgent';
                this.editId = res._id;
                this.transcriptOfUser.handOffId = res._id;
                this.initialize(true);
                this.displayUser(this.editId, this.chatWith, 'myRequest', true);
              }
            }).catch((err: any) => {
              this._notificationService.sendNotification('error', err.error.message ? err.error.message : 'Something went wrong');
            });
          }
        }
        this.dialogRef = null;
      });
    }
    }

  setChatVisibility(event) {
    if(!this.isChatSet) {
      if (event.target.scrollTop === event.target.scrollHeight - event.target.offsetHeight) {
        this.isChatSet = true;
        this.isScrollDone = true;
      } else {
        console.log('into else')
      }
    }
  }

  getIntents() {
    this.handoffService.getIntents().then((res: any) => {
          this.intents = res;
          this.searchIntents =res;
    }).catch((err => {
      this._notificationService.sendNotification('error',err.error.message ? err.error.message : 'Something went wrong');
    }))
  }
  getAgents() {
    this.handoffService.getAgents().then((res: any) => {
      this.agents = res;
      this.searchAgents =res;
    }).catch((err => {
      this._notificationService.sendNotification('error',err.error.message ? err.error.message : 'Something went wrong');
    }))
  }

  sendIntent() {
    if(this.selectedIntentIndex!==''){
      this.sendMessage('00INTENTID##'+this.searchIntents[this.selectedIntentIndex].intentId,this.transcriptOfUser.handOffId);
      this.dialogRef.close();
    }else {
      this._notificationService.sendNotification('error','Please select intent.');
    }
  }
  assignAgent() {
    if (this.selectedAgentIndex !== '') {
      this.handoffService.assignAgent(this.transcriptOfUser.handOffId,this.searchAgents[this.selectedAgentIndex]._id).then((res: any) => {
        if(res.message){
          this._notificationService.sendNotification('success',res.message);
        }
        this.dialogRef.close();

      }).catch((err => {
        this._notificationService.sendNotification('error',err.error.message ? err.error.message : 'Something went wrong');
      }))
    }else {
      this._notificationService.sendNotification('error','Please select agent.');
    }
  }
  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  fileUpload(events) {
    if (events.target.files && events.target.files[0]) {
      if (
        events.target.files[0].size >
        10 * 1048576
      ) {
        this._notificationService.sendNotification('error','File is too big. Maximum 10 MB allowed.')
      } else {
        setTimeout(() => {
          this.humanPixelScroll(true);
        }, 100);
        this.transcriptOfUser.transcript.push({isFromBot: true,
          progress:0,
          type:events.target.files[0].name.split('.')[events.target.files[0].name.split('.').length-1]==='png'
            || events.target.files[0].name.split('.')[events.target.files[0].name.split('.').length-1]==='jpg'
            ||events.target.files[0].name.split('.')[events.target.files[0].name.split('.').length-1]==='svg'?'image':
            events.target.files[0].name.split('.')[events.target.files[0].name.split('.').length-1]==='pdf'
            ||events.target.files[0].name.split('.')[events.target.files[0].name.split('.').length-1]==='doc'
            ||events.target.files[0].name.split('.')[events.target.files[0].name.split('.').length-1]==='docx'?'document':
              'audio'
          ,text: events.target.files[0].name,
          isFrom:'agent',
          timestamp:Date.now(),
          agentId:this.agentId,
          isUploaded:false});
        const file = new FormData();
        file.append(
          "file",
          events.target.files[0],
          events.target.files[0].name
        );
        this.http
          .post(
            Urls.fileUpload + "?botUserId=" + this.transcriptOfUser.userId,
            file,
            {
              reportProgress: true,
              observe: "events"
            }
          )
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(
            (res: any) => {
              if (res.type === HttpEventType.UploadProgress) {
                this.transcriptOfUser.transcript[this.transcriptOfUser.transcript.length-1].progress=
                  Math.round((res.loaded / res.total) * 100);
              } else if (res.type === HttpEventType.Response) {
                this.transcriptOfUser.transcript[this.transcriptOfUser.transcript.length-1].isUploaded=true;
                this.transcriptOfUser.transcript[this.transcriptOfUser.transcript.length-1].url=res.body.url;
                this.sendMessage(this.transcriptOfUser.transcript[this.transcriptOfUser.transcript.length-1].text,
                  this.transcriptOfUser.handOffId,
                  this.transcriptOfUser.transcript[this.transcriptOfUser.transcript.length-1])
                this._notificationService.sendNotification(
                  "success",
                  this.transcriptOfUser.transcript[this.transcriptOfUser.transcript.length-1].text +
                  " has been uploaded successfully!"
                );
              }
            },
            err => {

              console.log(err);
            }
          );
      }
    }
  }

  OpenPreview(url: any) {
    this.selectedImage=url;
    $(".show").fadeIn("fast");
  }

  downloadImage() {
    let link = document.createElement("a");
    document.body.appendChild(link);
    link.download = "html_image.png";
    link.href = this.selectedImage;
    link.target = '_blank';
    link.click();
  }
}
