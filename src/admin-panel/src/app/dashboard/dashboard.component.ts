import {Component, OnInit, HostListener, Input, ElementRef, ViewChild} from '@angular/core';
import {AccountService} from "../services/account.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {MessagingService} from "../services/messaging.service";
import {MatDialog} from "@angular/material/dialog";
import {Subject, Subscription, timer} from 'rxjs';
import {take, takeUntil} from "rxjs/operators";
import {AuthGuard} from "../utils/authGuard";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {DialogComponent} from "../components/dialog/dialog.component";
import {UsersService} from "../services/users.service";

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isMobile = false;
  scrHeight: any;
  scrWidth: any;

  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
  }

  message;
  initialLetter;
  isMenuToggle = false;
  isLoading = false;
  endTime = 30;
  timerSubscription: Subscription;
  minutesDisplay = 0;
  unsubscribe$: Subject<void> = new Subject();
  secondsDisplay = 0;
  userName;
  unreadMessageCount;
  userRole;
  headerWidth;
  changePasswordForm;
  updatePasswordForm;
  dialogRef;
  userProfile;
  photoUrl;
  notificationCount;
  @ViewChild("changePass", { static: false }) changePasswordTemp: ElementRef;
  @ViewChild("updateProfile", { static: false }) updateProfileTemp: ElementRef;
  constructor(private messagingService: MessagingService,
              private matDialog: MatDialog,
              private accountService: AccountService,
              private userProfileService: UsersService,
              private router: Router, private snackBar: MatSnackBar, private authGuard: AuthGuard) {
    this.getScreenSize();
    this.isMobile = this.scrWidth <= 1024;
  }

  ngOnInit() {
    this.notificationCount=this.messagingService.notificationCount;
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl("", Validators.required),
      newPassword: new FormControl("", Validators.required),
      confirmPassword: new FormControl("", Validators.required)
    });

    this.updatePasswordForm = new FormGroup({
      name: new FormControl("", Validators.required),
      email: new FormControl(""),
      contactNumber: new FormControl("", Validators.required)
    });
    this.userRole=this.accountService.getUserRole();
    this.userName = this.accountService.getUserName();
    this.photoUrl = this.accountService.getUserProfile();
    this.initialLetter = this.userName.substr(0, 1);
    $('#initial_loader').remove();
    this.resetLogoutTimer();
    this.authGuard.userActionOccured.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.timerSubscription) {
        this.timerSubscription.unsubscribe();
      }
      this.resetLogoutTimer();
    });
    this.messagingService.unreadMessageSubject.subscribe((res) => {
      this.unreadMessageCount = this.messagingService.unreadMessage;
    })
    this.headerWidth = document.getElementById("header").offsetWidth;
  }

  resetLogoutTimer(endTime: number = this.endTime) {
    const interval = 1000;
    const duration = endTime * 60;
    this.timerSubscription = timer(0, interval).pipe(
      take(duration)
    ).subscribe(value => {
        if (((duration - +value) * interval) === 1000) {
          this.userLogout();
        }
        this.render((duration - +value) * interval)
      },
      err => {
      },
      () => {
        this.accountService.logout().then(res => {
          this.router.navigate(['']);
        }).catch(err => {
          console.log(err)
        });
      }
    )
  }

  // opening left sidebar
  navOpen() {
    if (!this.isMobile) {
      $("#menu-effect").css("width", "250px");
      $('.icon-text-div').css('justify-content', 'flex-start');
      setTimeout(() => {
        $(".menu-switcher img")
          .css("margin", "unset")
          .css("transition", "0.5s");
        $(".img-logo")
          .css("display", "block")
          .css("transition", "0.5s");
        $(".icon-text-div").css("border-bottom", "1px solid #d5d5d5");
        $(".icon-class")
          .css("margin", "0")
          .css("transition", "0.5s");
        $(".icon-text").show(0);
        $(".img-menu img").css("width", "31px");
        if (this.scrWidth > 762 && this.scrWidth < 1300) {
          $('.live-chat-list').css('height', 'calc(100% - 150px)');
        }
      }, 300);
    } else {
      $('#menu-effect').css('display', 'flex').css('flex-direction', 'column').css("transition", "0.6s").css('width', '260px');
      $('.icon-set').css('display', 'flex');
      setTimeout(() => {
        $('.icon-text').css('display', 'flex');
      }, 200);
    }
  }

  // closing left sidebar
  navClose() {
    if (!this.isMobile) {
      $(".icon-text")
        .css("transition", "0.5s")
        .hide();
      $(".menu-switcher").css("padding", "20px");
      $(".menu-switcher img")
        .css("margin", "0 auto")
        .css("transition", "0.5s");
      $(".img-logo")
        .css("transition", "0.5s")
        .hide();
      $(".img-menu img").css("width", "auto");
      $(".img-menu")
        .css("margin", "0 auto")
        .css("transition", "0.5s");
      $(".img-menu img").css("margin", "0 auto");
      $(".icon-text-div").css("border-bottom", "0px solid #d5d5d5");
      $("#menu-effect")
        .css("width", "90px")
        .css("transition", "0.6s");
      $('.icon-text-div').css('justify-content', 'center');
    } else {
      $('.icon-text').css('display', 'none');
      $('#menu-effect').css("transition", "0.6s").css('width', '0');
    }
  }


  userLogout() {
    if(this.accountService.isAdminLogin){
      sessionStorage.removeItem("_id");
      sessionStorage.removeItem("role");
      sessionStorage.removeItem("token");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("fcmToken");
      sessionStorage.removeItem("organizationId");
      this.router.navigate(["login"]);
    }else {
      this.accountService
        .logout()
        .then(res => {
          this.router.navigate(["login"]);
        })
        .catch(err => {
          this.snackBar.open(err.error.message ? err.error.message : 'Something went wrong', "", {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          });
        });
    }

  }

  handleMenuClick(event) {
    if (event === "changePass") {
      this.openChangePassDialog(this.changePasswordTemp);
    } else if (event === "updateProfile") {
      this.openUpdateProfileDialog(this.updateProfileTemp);
    } else if (event === "logout") {
      this.userLogout();
    } else {
      this.isMenuToggle = false;
    }
  }
  openChangePassDialog(template) {
    this.changePasswordForm.reset();
    this.isMenuToggle = false;
    this.dialogRef = this.matDialog.open(DialogComponent, {
      data: { title: "Change Password", template: template, isShowSave: true },
      height: "50vh",
      width: "40vw"
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result.isSave) {
        this.changePassword();
      }
    });
  }
  changePassword() {
    if (this.changePasswordForm.valid) {
      if (
          !this.changePasswordForm.controls["oldPassword"].value.trim().length
      ) {
        this.snackBar.open("Please enter valid old password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
          !this.changePasswordForm.controls["newPassword"].value.trim().length
      ) {
        this.snackBar.open("Please enter valid new password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
          !this.changePasswordForm.controls["confirmPassword"].value.trim().length
      ) {
        this.snackBar.open("Please enter valid confirm password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
          this.changePasswordForm.controls["newPassword"].value !==
          this.changePasswordForm.controls["confirmPassword"].value
      ) {
        this.snackBar.open(
            "New password and confirm password does not match.",
            "",
            {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-error"]
            }
        );
        return;
      }
      const data = {
        oldPassword: this.changePasswordForm.controls["oldPassword"].value,
        newPassword: this.changePasswordForm.controls["newPassword"].value
      };
      this.accountService
          .changePassword(data)
          .then((res: any) => {
            this.matDialog.closeAll();
            this.snackBar.open("Password changed successfully.", "", {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-success"]
            });
          })
          .catch(err => {
            console.log(err);
            this.snackBar.open(
                err.error && err.error.message
                    ? err.error.message
                    : "Something went wrong.",
                "",
                {
                  horizontalPosition: "right",
                  duration: 3000,
                  panelClass: ["snack-error"]
                }
            );
          });
    } else {
      this.snackBar.open("Please enter all fields.", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
    }
  }


  openUpdateProfileDialog(updateProfile) {
    this.isMenuToggle = false;
    this.userProfileService.getUser().then((res: any) => {
      this.userProfile = res;
      this.updatePasswordForm.controls.name.setValue(res.name);
      this.updatePasswordForm.controls.email.setValue(res.email);
      this.updatePasswordForm.controls.contactNumber.setValue(
          res.contactNumber
      );
      this.photoUrl = res.profilePicture;
      this.dialogRef = this.matDialog.open(DialogComponent, {
        data: {
          title: "Update Profile",
          template: updateProfile,
          isShowSave: true
        },
        width: "40vw"
      });
    });
  }
  render(count) {
    this.secondsDisplay = this.getSeconds(count);
    this.minutesDisplay = this.getMinutes(count);
  }

  getSeconds(ticks: number) {
    const seconds = ((ticks % 60000) / 1000).toFixed(0);
    return this.pad(seconds);
  }

  getMinutes(ticks: number) {
    const minutes = Math.floor(ticks / 60000);
    return this.pad(minutes);
  }

  pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

  toggleMenu() {
    this.isMenuToggle = !this.isMenuToggle;
    this.headerWidth = document.getElementById("header").offsetWidth;
  }
  deleteProfilePic() {
    let oldImage = this.photoUrl.substr(this.photoUrl.lastIndexOf("/") + 1);
    this.userProfileService
        .deleteProfilePic(oldImage)
        .then(res => {
          this.photoUrl = null;
        })
        .catch(err => {
          console.log(err);
        });
  }

  uploadDisplayPicture(events: any) {
    let pictureName;
    if (this.photoUrl) {
      pictureName = this.photoUrl.substring(this.photoUrl.lastIndexOf("/") + 1);
    }

    if (events.target.files && events.target.files[0]) {
      const fd = new FormData();
      fd.append(
          "profile-picture",
          events.target.files[0],
          events.target.files[0].name
      );

      this.userProfileService
          .updateProfilePicture(pictureName ? pictureName : "", fd)
          .then((res: any) => {
            this.photoUrl = res.profilePicture;
            console.log(this.photoUrl);
            this.accountService.setUserProfile(this.photoUrl);
          });
    }
  }
  onUpdateProfile() {
    let data = {
      name: this.updatePasswordForm.controls["name"].value,
      contactNumber: this.updatePasswordForm.controls["contactNumber"].value,
      profilePicture: this.photoUrl
    };
    if (this.updatePasswordForm.controls["name"].value.trim() === "") {
      this.snackBar.open("Please enter valid name", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
      return;
    }
    if (this.updatePasswordForm.controls["contactNumber"].value.trim() !== "") {
      if (
          this.updatePasswordForm.controls["contactNumber"].value.length < 6 ||
          this.updatePasswordForm.controls["contactNumber"].value.length > 10
      ) {
        this.snackBar.open(
            "Please enter contact number of length 6 to 10.",
            "",
            {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-error"]
            }
        );
        return;
      }
    } else if (
        this.updatePasswordForm.controls["contactNumber"].value &&
        this.updatePasswordForm.controls["contactNumber"].value.trim() === ""
    ) {
      this.snackBar.open("Please enter valid Contact Number", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
      return;
    }

    this.userProfileService
        .updateUser(data)
        .then((res: any) => {
          this.matDialog.closeAll();
          this.accountService.setUserName(res.name);
          this.userName = res.name;
          this.snackBar.open("Profile Updated successfully.", "", {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-success"]
          });
        })
        .catch(err => {
          this.snackBar.open(
              err.error.message ? err.error.message : "Something went wrong",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
          );
        });
  }

  restrictNumberInput(e, value?) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(e.charCode);
    if (e.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if ((value === "phone" && e.target.value.length >= 9) || e.keyCode === 46) {
      event.preventDefault();
    }
  }
}
