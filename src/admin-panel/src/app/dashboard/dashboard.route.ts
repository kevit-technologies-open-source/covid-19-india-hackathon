import {ActivatedRoute, Router, Routes} from '@angular/router';
import {DashboardComponent} from "./dashboard.component";
import {HandOffComponent} from "./hand-off/hand-off.component";
import {BotSettingsComponent} from "./bot-settings/bot-settings.component";
import {AnalyticsComponent} from "./analytics/analytics.component";
import {WebWidgetComponent} from "./web-widget/web-widget.component";

export const dashboardRoutes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    children: [
      {
        path: "",
        redirectTo: "analytics",
        pathMatch: "full"
      },
      {
        path: "analytics",
        component: AnalyticsComponent
      },
      {
        path: "hand-off",
        component: HandOffComponent
      },

      {
        path: "bot-settings",
        component: BotSettingsComponent
      },
      {
        path: "web-widget",
        component: WebWidgetComponent
      }
    ]
  }
];

