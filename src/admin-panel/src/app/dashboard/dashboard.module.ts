import {ModuleWithProviders, NgModule} from "@angular/core";
import {DashboardComponent} from "./dashboard.component";
import {RouterModule} from "@angular/router";
import {dashboardRoutes} from "./dashboard.route";
import {CommonModule, DatePipe} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatListModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTableModule,
  MatTreeModule,
  MatIconModule,
  MatTabsModule,
  MatBadgeModule,
  MatTooltipModule
} from "@angular/material";
import {MaterialModule} from "../material.module";
import {HandOffComponent} from "./hand-off/hand-off.component";
import {SharedModule} from "../utils/shared.module";
import {GetInitialsPipe} from "../utils/get-initials.pipe";
import {NgxPopperModule} from "ngx-popper";
import {AutosizeModule} from "ngx-autosize";
import { GeneralSettingsComponent } from './bot-settings/general-settings/general-settings.component';
import { UserSettingsComponent } from './bot-settings/user-settings/user-settings.component';
import { IntegrationComponent } from './bot-settings/integration/integration.component';
import { MessageConfigurationComponent } from './bot-settings/message-configuration/message-configuration.component';
import { BotSettingsComponent } from './bot-settings/bot-settings.component';
import {DialogComponent} from "../components/dialog/dialog.component";
import {NgxMaterialTimepickerModule} from "ngx-material-timepicker";
import { AnalyticsComponent } from './analytics/analytics.component';
import { WebWidgetComponent } from './web-widget/web-widget.component';
import {ColorPickerModule} from "ngx-color-picker";
import { ChartsModule } from "ng2-charts";
import {SettingsService} from "../services/settings.service";
import {MinuteToHours} from '../utils/pipe/minute-to-hours';
import {NotificationService} from '../services/notification.service';
import {UsersService} from '../services/users.service';
import {IntegrationService} from '../services/integration.service';
import {MessageConfigurationService} from '../services/message-configuration.service';
import {HeaderDropdownComponent} from '../components/header-dropdown/header-dropdown.component';
import {ClickOutsideDirective} from '../utils/click-outside.directive';
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {AnalyticsService} from '../services/analytics.service';
import {ExcelService} from '../services/excel-file.service';


@NgModule({
    imports: [
        SharedModule,
        MaterialModule,
        RouterModule.forChild(dashboardRoutes),
        CommonModule,
        InfiniteScrollModule,
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        MatExpansionModule,
        MatTreeModule,
        MatListModule,
        MatCardModule,
        MatTabsModule,
        MatTableModule,
        MatSelectModule,
        MatCheckboxModule,
        MatBadgeModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatTooltipModule,
        ScrollingModule,
        AutosizeModule,
        NgxPopperModule.forRoot({}),
        NgxMaterialTimepickerModule,
        ColorPickerModule,
        ChartsModule,
       NgxDaterangepickerMd.forRoot()
    ],
  declarations: [
    HandOffComponent,
    DashboardComponent,
    GetInitialsPipe,
    GeneralSettingsComponent,
    UserSettingsComponent,
    IntegrationComponent,
    MinuteToHours,
    MessageConfigurationComponent,
    BotSettingsComponent,

    AnalyticsComponent,
    WebWidgetComponent,
    HeaderDropdownComponent,
    ClickOutsideDirective
  ],
  providers: [
    DatePipe,
    GetInitialsPipe,
    SettingsService,
    NotificationService,
    UsersService,
    IntegrationService,
    MessageConfigurationService,
    AnalyticsService,
    ExcelService

  ],

})
export class DashboardModule {
}
