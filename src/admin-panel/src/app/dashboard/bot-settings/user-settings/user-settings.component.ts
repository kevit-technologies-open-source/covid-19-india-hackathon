import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmailValidator, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from "@angular/material";
import {DialogComponent} from "../../../components/dialog/dialog.component";
import {SettingsService} from '../../../services/settings.service';
import {NotificationService} from '../../../services/notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersService} from '../../../services/users.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AccountService} from '../../../services/account.service';
import {ValidateFn} from 'codelyzer/walkerFactory/walkerFn';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements  OnInit,OnDestroy {
  search: any = "";
  isPagination = true;
  disablePrev = false;
  disableNext = true;
  currentPage: number = 1;
  totalpage: number = 0;
  totalPages = [];
  addUserForm;
  dialogRef;
  limit=10;
  users=[];
  deleteIndex='';
  unSubscribe = new Subject();
  isNotifyUser=false;
  editId='';
  editIndex: any='';
  userRole='';
  isFormSubmit=true;
  mainUserId=this.accountService.getId();
  userRoleTitle={
    supportAgent:'Support Agent',
    admin:'Admin'
  };


  constructor(public dialog: MatDialog,
              public _usersService: UsersService,
              public _notificationService:NotificationService,
              public router: Router,
              public activeRoute: ActivatedRoute,
              public accountService: AccountService,) {
  }

  ngOnInit() {
    this.userRole=this.accountService.getUserRole();
    this.activeRoute.queryParams
      .pipe(takeUntil(this.unSubscribe))
      .subscribe((res: any) => {
        if(res.page !== undefined){
          this.currentPage = res.page;
        }
      });
    this.setUserForm('new');
    this.getUsers(this.currentPage,this.limit);
  }

  openDialog(type,template,index?) {
    if(type==='addUser'){
      if(index !== undefined){
        this.editIndex=index;
        this.editId=this.users[index]._id;
        this.setUserForm(this.users[index]);
        this.dialogRef = this.dialog.open(DialogComponent, {
          panelClass: 'addUser',
          data: {title: "Update User", template: template, isShowSave: true},
          height: 'auto',
          width: '40vw'
        });
      }else {
        this.editId='';
        this.setUserForm('new');
        this.dialogRef = this.dialog.open(DialogComponent, {
          panelClass: 'addUser',
          data: {title: "Add User", template: template, isShowSave: true},
          height: 'auto',
          width: '40vw'
        });
      }

    }else if(type==='deleteUser'){
      this.deleteIndex=index;
      this.dialogRef = this.dialog.open(DialogComponent, {
        panelClass: 'addUser',
        data: {title: "Delete user", template: template, isShowSave: false},
        height: 'auto',
        width: '500px'
      });
      this.dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.deleteUser(this.users[index]._id);
        }
      });
    }


  }

  onlyNumberInput(e, value?) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(e.charCode);
    if (e.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if ((value === "phone" && e.target.value.length >= 9) || e.keyCode === 46) {
      event.preventDefault();
    }
  }

  setUserForm(value) {
    if(this.editId.length===0){
      this.addUserForm = new FormGroup({
        name: new FormControl(value.name ? value.name:'', Validators.required),
        email: new FormControl(value.email ? value.email:'',[Validators.required,Validators.email]),
        contactNumber: new FormControl(value.contactNumber ? value.contactNumber:'', Validators.required),
        role: new FormControl(value.role ? value.role:'admin', Validators.required),
      });
    }else {
      this.addUserForm = new FormGroup({
        name: new FormControl(value.name ? value.name:'', Validators.required),
        email: new FormControl(value.email ? value.email:'',[Validators.required,Validators.email,
         Validators.pattern('(([^<>()\\[\\]\\.,;:\\s@\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})')]),
        contactNumber: new FormControl(value.contactNumber ? value.contactNumber:'', Validators.required),
        role: new FormControl(value.role ? value.role:'admin', Validators.required),
        notifyUser: new FormControl(false),
      });
    }

  }

  getUsers(page,limit) {
    this._usersService.getUsers(page,limit,this.search)
      .then((res:any) => {
        this.users=res.docs;
        this.setPagination(res);
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }

  /*
 Function for changing Page
 @Params {number} - Argument is the page you want to go to
 Can be next or previous
*/
  changePage(page) {
    this.currentPage = page;
    this.getUsers(page, 10);
    this.router.navigate(["dashboard", "bot-settings"], {
      queryParams: {
        tab:'user',
        page: this.currentPage,
      }
    });
  }
  /*
      Function for going to next page
   */
  nextPage() {
    if (!this.disableNext) {
      this.changePage(++this.currentPage);
    }
  }

  /*
     Function for going to prev page
  */


  prevPage() {
    if (!this.disablePrev) {
      this.changePage(--this.currentPage);
    }
  }

  getSearchList(event) {
    if (event.keyCode === 8 || event.keyCode === 46) {
      if (this.search.length === 1) {
        this.search = "";
        this.currentPage=1;
        this.getUsers(1, 10);
      }
    }
    if (event.keyCode === 13) {
      this.currentPage=1;
      this.getUsers(1, 10);
    }
  }

  setPagination(res) {
    this.currentPage = res.page;
    this.totalpage = res.pages;
    this.isPagination = res.total > 1;
    if (res.page > this.totalpage) {
      this.currentPage = 1;
    }
    this.disablePrev = this.currentPage === 1;
    this.disableNext = this.currentPage === this.totalpage;
    let temp = parseInt(this.currentPage / 5 + "", 10);
    if (this.currentPage % 5 === 0) {
      temp--;
    }
    let count = 0;
    this.totalPages = [];
    if (this.totalpage > 5) {
      // @ts-ignore
      for (
        let i =
          this.currentPage + 5 <= this.totalpage
            ? this.currentPage === 1
            ? this.currentPage
            : this.currentPage
            : this.totalpage - 4;
        i <=
        (this.currentPage + 5 <= res.pages
          ? this.currentPage + 5
          : this.totalpage);
        i++
      ) {
        count++;
        if (count > 5 || i > this.totalpage) {
          break;
        }
        this.totalPages.push({
          no: i,
          status: i === res["page"]
        });
      }
    } else {
      for (let i = 1; i <= res["pages"]; i++) {
        this.totalPages.push({
          no: i,
          status: i === res["page"]
        });
      }
    }
  }

  createUser() {
    if(this.editId.length===0){
      this.isFormSubmit=false;
      this._usersService.createUsers(this.addUserForm.value)
        .then((res:any) => {
          this.isFormSubmit=true;
          this.closeDialog();
          this.getUsers(this.currentPage,this.limit);
          this._notificationService.sendNotification('success',res.message);
          this.addUserForm.reset();
        }).catch(err => {
        this.isFormSubmit=true;
        console.log(err);
        this._notificationService.sendNotification('error',err.error.message);
      });
    }else {
      let object={};
      object['notifyUser']=this.addUserForm.value.notifyUser
      if(this.users[this.editIndex].name !== this.addUserForm.value.name){
        object['name']=this.addUserForm.value.name
      }
      if(this.users[this.editIndex].contactNumber !== this.addUserForm.value.contactNumber){
        object['contactNumber']=this.addUserForm.value.contactNumber
      }
      if(this.users[this.editIndex].role !== this.addUserForm.value.role){
        object['role']=this.addUserForm.value.role
      }
      this.isFormSubmit=false;
      this._usersService.updateUsers(object,this.editId)
        .then((res:any) => {
          this.isFormSubmit=true;
          this.closeDialog();
          this.editId='';
          this.getUsers(this.currentPage,this.limit);
          this._notificationService.sendNotification('success',res.message);
          this.addUserForm.reset();
        }).catch(err => {
        this.isFormSubmit=true;
        console.log(err);
        this._notificationService.sendNotification('error',err.error.message);
      });
    }

  }

  deleteUser(id) {
    this._usersService.deleteUsers(id)
      .then((res:any) => {
        this.getUsers(this.currentPage,this.limit);
        this._notificationService.sendNotification('success',res.message);
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unSubscribe.complete();
  }
}
