import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AccountService} from '../../services/account.service';

@Component({
  selector: 'app-bot-settings',
  templateUrl: './bot-settings.component.html',
  styleUrls: ['./bot-settings.component.scss']
})
export class BotSettingsComponent implements OnInit,OnDestroy {
  unSubscribe = new Subject();
  selectedTab = 0;
  userRole='';
  constructor( public router: Router,
               protected route: ActivatedRoute,
               public accountService: AccountService,) { }

  ngOnInit() {
    this.userRole=this.accountService.getUserRole();
    this.route.queryParams
      .pipe(takeUntil(this.unSubscribe))
      .subscribe((res: any) => {
        switch (res.tab) {
          case "general":
            this.selectedTab = 0;
            break;
          case "user":
            if(this.userRole!=='supportAgent'){
              this.selectedTab = 1;
            }else {
              this.selectedTab = 0;
            }
            break;
          case "integration":
            this.selectedTab = 2;
            break;
          case "message-configuration":
            this.selectedTab = 3;
            break;
        }
      });
  }
  tabChanges(event) {
    let type;
    switch (event) {
      case 0:
        type = "general";
        break;
      case 1:
        type = "user";
        break;
      case 2:
        type = "integration";
        break;
      case 3:
        type = "message-configuration";
        break;
    }
    this.router.navigate(["dashboard", "bot-settings"], {
      queryParams: {
        tab: type
      }
    });
  }

  ngOnDestroy(): void {
    this.unSubscribe.complete();
  }

}
