import {Component, OnInit} from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {NotificationService} from '../../../services/notification.service';
import {IntegrationService} from '../../../services/integration.service';
import { environment } from "../../../../environments/environment.prod";
import {AccountService} from '../../../services/account.service';
import {DialogComponent} from '../../../components/dialog/dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-integration',
  templateUrl: './integration.component.html',
  styleUrls: ['./integration.component.scss']
})
export class IntegrationComponent implements OnInit {
  dialogRef;
  deleteIndex='';
  webScriptText =
    '<script>\n' +
    'window.organizationId = "' +
    this._authService.getOrganizationId() +
    '";\n' +
    "var script = document.createElement('script');\n" +
    'script.src = "' +
    environment.apiUrl +
    'webchat/webChat.js";\n' +
    "document.getElementsByTagName('body')[0].appendChild(script);\n"+
    "</script>";
  urls=[];
  constructor(public _integrationService: IntegrationService,public _notificationService:NotificationService,
              public _authService:AccountService,public dialog: MatDialog,) {
  }

  ngOnInit() {
    this.getIntegration(true);
  }
  getIntegration(refresh) {
    this._integrationService.getIntegration()
      .then((res:any) => {
        if(refresh){
          this.urls=[];
          res.siteUrls.forEach(url=>{
            this.urls.push({value:url,isValid:true});
          });
        }else {
          res.siteUrls.forEach((url,index)=>{
            this.urls[index].value=url;
            this.urls[index].isValid=true;
          });
        }

      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  updateIntegration() {
    let flag=true;
    if(this.urls.length > 0){
      this.urls.forEach((url,index)=>{
        if(url.value.trim().length===0){
          flag=false;
          url['isValid']=false;
          this._notificationService.sendNotification('error','Enter url')
        }
      });
    }
    if(flag) {
      let urls=[];
      this.urls.forEach(url=>{
        urls.push(url.value);
      });
      let object={
        siteUrls:urls
      };
      this._integrationService.updateIntegration(object)
        .then((res: any) => {
          if(res.message){
            this.getIntegration(false);
            this._notificationService.sendNotification('success',res.message);
          }
        }).catch(err => {
        if(err.error.message){
          this._notificationService.sendNotification('error',err.error.message);
        }
        if(err.error.urlConflicts.length > 0){
          this.urls.forEach(url=>{
            url['isValid']=true;
          });
          err.error.urlConflicts.forEach(urlConflict=>{
            this.urls[urls.indexOf(urlConflict)]['isValid']=false;
          });
        }
        console.log(err);
      });
    }
  }
  addUrl() {
    if(this.urls.length > 0){
      if(this.urls[this.urls.length-1].value.trim().length > 0){
        this.urls.push({value:'',isValid:true});
        setTimeout(() => {
          document
            .getElementById(
              "integrationUrl" +
              (this.urls.length - 1)
            )
            .focus();
        }, 100);
      }
    }else{
      this.urls.push({value:'',isValid:true});
      setTimeout(() => {
        document
          .getElementById(
            "integrationUrl" +
            (this.urls.length - 1)
          )
          .focus();
      }, 100);
    }
  }
  removeUrl(uIndex: number) {
    if(this.urls.length>1){
      this.urls.splice(uIndex,1);
    }
  }
  copyTextArea(str) {
    const el = document.createElement("textarea");
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    this._notificationService.sendNotification("success", "Copied!");
  }
  openDialog(type,template,index?) {
    if(type==='deleteURL'){
      this.deleteIndex=index;
      this.dialogRef = this.dialog.open(DialogComponent, {
        panelClass: 'addUser',
        data: {title: "Delete URL", template: template, isShowSave: false},
        height: 'auto',
        width: '500px'
      });
      this.dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.removeUrl(index);
        }
      });
    }


  }
}
