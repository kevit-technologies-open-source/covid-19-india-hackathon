import { Component, OnInit } from '@angular/core';
import {timezone} from "../../../models/timezone";
import {SettingsService} from "../../../services/settings.service";
import {NotificationService} from '../../../services/notification.service';
import {DialogComponent} from '../../../components/dialog/dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss']
})
export class GeneralSettingsComponent implements OnInit {
  timezoneList = [];
  generalSettings;
  dialogRef;
  daysWord=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  isPutOnHold=true;
  isNameValid=true;
  constructor(public _settingService: SettingsService,
              public _notificationService:NotificationService,
              public dialog: MatDialog,) { }
  ngOnInit() {
    for (let item in timezone) {
      this.timezoneList.push(timezone[item]);
    }
    this.getGeneralSettings();
  }
  getGeneralSettings() {
    this._settingService.getGeneralSetting()
      .then((res:any) => {
        res.organization.daySpecificWorking.forEach(day=>{
          day.startTime=this.minuteToHours(day.startTime);
          day.endTime=this.minuteToHours(day.endTime);
        });
        this.generalSettings = res;
      }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error',err.error.message);
    });
  }
  minuteToHours(minutes){
    return Math.floor(minutes / 60) + ':' + minutes % 60;
  }
  timeToMinutes(time){
    let hour=time.split(':');
    let minute=hour[1].split(' ');
    if(minute[1] === 'PM'){
      if( hour[0] < 12) {
        hour[0] = (+hour[0]) + 12;
      }
    }
    return ((+hour[0])*60)+(+minute[0]);
  }
  saveGeneralSettings() {
    if(this.generalSettings.bot.botName.trim() !== ''){
      this.isNameValid=true;
      let object=JSON.parse(JSON.stringify(this.generalSettings));
      object.organization.daySpecificWorking.forEach(day=>{
        day.startTime=this.timeToMinutes(day.startTime);
        day.endTime=this.timeToMinutes(day.endTime);
      });
      this.clean(object);
      this._settingService.saveGeneralSetting(object)
        .then((res:any) => {
          this._notificationService.sendNotification('success',res.message);
        }).catch(err => {
        console.log(err);
        this._notificationService.sendNotification('error',err.error.message);
      });
    }else {
      this.isNameValid=false;
      this._notificationService.sendNotification('error','Enter bot name');
    }

  }
  clean(obj) {
   delete obj.organization['_id'];
    delete obj.organization['__v'];
    obj.organization.daySpecificWorking.forEach(day=>{
      delete day["_id"];
    });
  }
  openDialog(type,template) {
    if(type==='putOnHold'){
      this.isPutOnHold=true;
      this.generalSettings.bot.putOnHold=!this.generalSettings.bot.putOnHold;
      this.dialogRef = this.dialog.open(DialogComponent, {
          panelClass: 'addUser',
          data: {title: "Put on hold", template: template, isShowSave: false},
          height: 'auto',
          width: '500px'
        });
      }else if(type==='handoffShutdown'){
        this.isPutOnHold=false;
      this.generalSettings.organization.isHandOffShutDown=!this.generalSettings.organization.isHandOffShutDown;
        this.dialogRef = this.dialog.open(DialogComponent, {
          panelClass: 'addUser',
          data: {title: "Handoff Shutdown", template: template, isShowSave: false},
          height: 'auto',
          width: '500px'
        });
    }
  }
  setChecked() {
    if(this.isPutOnHold){
      this.generalSettings.bot.putOnHold=!this.generalSettings.bot.putOnHold;
    }else {
      this.generalSettings.organization.isHandOffShutDown=!this.generalSettings.organization.isHandOffShutDown;
    }
    this.dialogRef.close();
  }
  unChecked(){
    this.dialogRef.close();
  }

  setDay(day) {
    day.isPublicHoliday=false;
  }
}
