import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../../services/notification.service';
import {AccountService} from '../../../services/account.service';
import {MessageConfigurationService} from '../../../services/message-configuration.service';

declare var $;


@Component({
  selector: 'app-message-configuration',
  templateUrl: './message-configuration.component.html',
  styleUrls: ['./message-configuration.component.scss']
})
export class MessageConfigurationComponent implements OnInit {
  messagesTitles=[{
    title:'Handoff Initiation Message',
    value:'handOffStart',
    isValid:true
  },{
      title:'Agent to Agent Transfer Message',
      value:'handOffTransfer',
    isValid:true
    },{
      title:'Handoff Completion Message',
      value:'handOffEnd',
    isValid:true
    },{
      title:'Request Outside Team\'s Working Hours Message',
      value:'handOffUnavailable',
    isValid:true
    },{
      title:'System Shutdown Message',
      value:'handOffShutDown',
    isValid:true
    },{
      title:'Public Holiday Message',
      value:'publicHoliday',
    isValid:true
    },{
      title:'Bot on Hold Message',
      value:'putOnHold',
    isValid:true
    },{
      title:'Handoff Auto Completion Message',
      value:'fiveMinMessage',
    isValid:true
    },];
  messages={};
  agentSelection=['agent-name', 'old-agent-name'];
  completionSelection=['agent-name'];
  handOffUnavailableSelection=['next-day','start-time'];


  constructor(public _messageConfigurationService: MessageConfigurationService,
              public _notificationService:NotificationService,) {
  }
  ngOnInit() {
    this.getMessageConfiguration();
  }
  getMessageConfiguration() {
    this._messageConfigurationService.getMessageConfiguration()
      .then((res:any) => {
          this.messages=res.messages;
      }).catch(err => {
      if(err.error.message){
        this._notificationService.sendNotification('error',err.error.message);
      }
      console.log(err);
    });
  }
  updateMessageConfiguration() {
    let flag=true;
    if(this.messagesTitles.length > 0){
      this.messagesTitles.forEach((message,index)=>{
        if(this.messages[message.value].trim().length===0){
          flag=false;
          message['isValid']=false;
          this._notificationService.sendNotification('error','Enter message')
        }
      });
    }
    if(flag) {
      this._messageConfigurationService.updateMessageConfiguration({messages:this.messages})
        .then((res: any) => {
          if(res.message){
            this.messagesTitles.forEach(message=>{
              message['isValid']=true;
            });
            this._notificationService.sendNotification('success',res.message);
          }
        }).catch(err => {
        if(err.error.message){
          this._notificationService.sendNotification('error',err.error.message);
        }
        console.log(err);
      });
    }
  }
  createSuggestions(value) {
    if(value === 'handOffTransfer' || value === 'handOffEnd' || value === 'handOffUnavailable'){
      let a = [];
      if(value==='handOffTransfer'){
        a=this.agentSelection;
      }else if(value==='handOffEnd'){
        a=this.completionSelection;
      }else if(value==='handOffUnavailable'){
        a=this.handOffUnavailableSelection;
      }
      $("#"+value).atwho({
        at: "{{",
        data: a,
        limit: 10,
        suffix: "}}"
      });
    }
  }
}
