import {Component, OnInit} from '@angular/core';
import {WebWidgetService} from "../../services/web-widget.service";
import {LookAndFeelModel} from "../../models/web-widget.models";
import {FormControl, FormGroup} from "@angular/forms";
import {Urls} from "../../utils/urls";
import {HttpClient} from "@angular/common/http";
import {AccountService} from "../../services/account.service";
import {NotificationService} from "../../services/notification.service";

@Component({
  selector: 'app-web-widget',
  templateUrl: './web-widget.component.html',
  styleUrls: ['./web-widget.component.scss']
})
export class WebWidgetComponent implements OnInit {
  fontSet = [
    "Roboto",
    "Lato",
    "Open Sans",
    "Montserrat"
  ];
  botSizes = [
    {
      value: 1,
      height: 400,
      width: 300
    },
    {
      value: 2,
      height: 450,
      width: 350
    }
    , {
      value: 3,
      height: 500,
      width: 400
    }, {
      value: 4,
      height: 550,
      width: 450
    },
    {
      value: 5,
      height: 600,
      width: 500
    }
  ];
  botLookAndFeel: LookAndFeelModel = {
    widget: {
      bot: {
        bubbleColour: '#18c2c4',
        fontColour: '#fff',
        fontStyle: 'Lato'
      },
      user: {
        bubbleColour: '#fff',
        fontColour: '#000',
        fontStyle: 'Lato'
      },
      inputBackground: {
        bubbleColour: '#18c2c4',
        fontColour: '#000'
      },
      agent: {
        bubbleColour: '#18c2c4',
        fontColour: '#fff',
        fontStyle: 'Lato'
      },
      position: 'right',
      size: 5,
      chatBackgroundColour: '#F7F7F7',
      chatFontColour: '#fff',
      iconUrl: ''
    },
    botName: 'Human Pixel'
  };
  delayConfig = {
    messageDelayTime: 0,
    faceAppearDelayTime: 0,
    unFoldTime: 0,
    isAutoUnfold:true,
  };
  welcomeMessageConfig = {
    requiredFields: {
      name: true,
      email: false,
      contactNumber: false
    },
    title: "Hey There",
    greetingText: "Welcome to the Smart Bot Solution"
  };


  constructor(private _webWidgetService: WebWidgetService,
              private http: HttpClient,
              private _authService: AccountService,
              private _notificationService: NotificationService) {
  }

  ngOnInit() {
    this._webWidgetService.getLookAndFeelConfig().then(res => {
      this.botLookAndFeel = res;
    });

    this._webWidgetService.getDelayConfig().then(res => {
      this.delayConfig = res.delay;
    });
    this._webWidgetService.getWelcomeConfig().then(res => {
      this.welcomeMessageConfig = res.welcomeMessage;
    });
  }


  profileUpload(events: any) {
    if (events.target.files && events.target.files[0]) {
      if(events.target.files[0].size > (1024.25988700565 * 500)){
        this._notificationService.sendNotification('error','File size must be smaller than 500KB.')
      }else {
        const fd = new FormData();
        fd.append('bot-icon', events.target.files[0], events.target.files[0].name);
        this.http.post(Urls.webWidget + 'upload/bot-icon?organizationId=' +
          this._authService.getOrganizationId(), fd).subscribe(res => {
          const temp: any = res;
          this.botLookAndFeel.widget.iconUrl = temp.file;
        });
      }
    }
  }

  getSelectedSize(size) {
    return this.botSizes.filter(d => d.value === parseInt(size))[0];
  }

  saveLookAndFeelSettings() {
    if (this.validationForLookAndFeel()) {
      this._webWidgetService.setLookAndFeelConfig(this.botLookAndFeel).then(res => {
        this._notificationService.sendNotification('success', 'Saved successfully !');
      }).catch(err => {
        console.log(err);
        this._notificationService.sendNotification('error', err.error.message);
      });
    }
  }

  resetLookAndFeelSettings() {
    this._webWidgetService.resetLookAndFeelConfig().then(res => {
      this.botLookAndFeel = res;
      this._notificationService.sendNotification('success', 'Reverted successfully !');
    }).catch(err => {
      console.log(err);
      this._notificationService.sendNotification('error', err.error.message);
    });
  }

  saveDelaySettings() {
    if (this.validationForDelay()) {
      this._webWidgetService.setDelayConfig({delay: this.delayConfig}).then(res => {
        this._notificationService.sendNotification('success', 'Saved successfully !');
      }).catch(err => {
        console.log(err);
        this._notificationService.sendNotification('error', err.error.message);
      });
    }

  }

  saveWelcomeMessageSettings() {
     if (this.validationForWelcomeMessage()) {
      this._webWidgetService.setWelcomeConfig({welcomeMessage : this.welcomeMessageConfig}).then(res => {
        this._notificationService.sendNotification('success', 'Saved successfully !');
      }).catch(err => {
        console.log(err);
        this._notificationService.sendNotification('error', err.error.message);
      });
    }
  }

  validationForDelay() {
    if (parseInt(this.delayConfig.faceAppearDelayTime.toString()) < 0 || parseInt(this.delayConfig.faceAppearDelayTime.toString()) > 10) {
      this._notificationService.sendNotification('error', 'Delay for bot face to appear must be between 0 and 10');
      return false;
    }
    if (parseInt(this.delayConfig.unFoldTime.toString()) < 0 || parseInt(this.delayConfig.unFoldTime.toString()) > 10) {
      this._notificationService.sendNotification('error', 'Delay for bot to appear must be between 0 and 10');
      return false;
    }
    if (parseInt(this.delayConfig.messageDelayTime.toString()) < 0 || parseInt(this.delayConfig.messageDelayTime.toString()) > 10) {
      this._notificationService.sendNotification('error', 'Delay between two messages must be between 0 and 10');
      return false;
    }
    return true;
  }

  validationForLookAndFeel() {
    if (this.botLookAndFeel.botName === '') {
      this._notificationService.sendNotification('error', 'Please enter bot name');
      return false;
    }
    return true;
  }

  validationForWelcomeMessage() {
    if (this.welcomeMessageConfig.greetingText.trim() === '') {
      this._notificationService.sendNotification('error', 'Please enter greeting message');
      return false;
    }
    if (this.welcomeMessageConfig.title.trim() === '') {
      this._notificationService.sendNotification('error', 'Please enter title');
      return false;
    }
    return true;
  }

  checkLimit(event) {
    if((+event.target.value)>10){
      event.target.value=10;
    }
  }
}
