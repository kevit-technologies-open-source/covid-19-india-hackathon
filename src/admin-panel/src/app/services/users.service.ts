import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Urls} from "../utils/urls";
import {AccountService} from "./account.service";

@Injectable()
export class UsersService {
  constructor(private http: HttpClient,
              private _authService: AccountService) {
  }
  getUsers(page,limit,search){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.user + "?page="+page+"&limit="+limit+"&search="+search+"&organizationId=" +
        this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  createUsers(object){
    object['organizationId']=this._authService.getOrganizationId();
    return new Promise((resolve, reject) => {
      this.http.post(Urls.user,object).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  updateUsers(object,id){
    return new Promise((resolve, reject) => {
      this.http.put(Urls.user+id,object).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  deleteUsers(id){
    return new Promise((resolve, reject) => {
      this.http.delete(Urls.user+id).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  updateUser(data) {
    return new Promise((resolve, reject) => {
      return this.http.put(Urls.userProfile, data).subscribe(
        (res: any) => {
          resolve(res);
          if (this._authService.getRememberMe()) {
            localStorage.setItem("user", res.name);
          } else {
            sessionStorage.setItem("user", res.name);
          }
        },
        err => {
          reject(err);
        }
      );
    });
  }

  updateProfilePicture(oldFileName, fd) {
    return new Promise((resolve, reject) => {
      return this.http
        .post(Urls.profilePicture + "?oldImageName=" + oldFileName, fd)
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  deleteProfilePic(imageName) {
    return new Promise((resolve, reject) => {
      return this.http
        .delete(Urls.baseUrl + "user/profile-picture?oldImageName=" + imageName)
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
  getUser() {
    return new Promise((resolve, reject) => {
      return this.http.get(Urls.userProfile).subscribe(
        (res: any) => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
