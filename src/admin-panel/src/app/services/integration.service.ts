import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Urls} from "../utils/urls";
import {AccountService} from "./account.service";

@Injectable()
export class IntegrationService {
  constructor(private http: HttpClient,
              private _authService: AccountService) {
  }
  getIntegration(){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.settings +"integration?organizationId=" +
        this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  updateIntegration(object){
    return new Promise((resolve, reject) => {
      this.http.put(Urls.settings +"integration?organizationId=" +
        this._authService.getOrganizationId(),object).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

}
