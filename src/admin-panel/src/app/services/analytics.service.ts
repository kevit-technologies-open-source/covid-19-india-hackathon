import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Urls} from "../utils/urls";
import {AccountService} from "./account.service";

@Injectable()
export class AnalyticsService {
  constructor(private http: HttpClient,
              private _authService: AccountService) {
  }
  getPartOfTimeRequest(date){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.analytics +"part-of-time?organizationId=" +
        this._authService.getOrganizationId()+"&startDate="+date.startDate.toISOString()+"&endDate="+date.endDate.toISOString()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  getCustomerSupportRequest(date){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.analytics +"initiated-by?organizationId=" +
        this._authService.getOrganizationId()+"&startDate="+date.startDate.toISOString()+"&endDate="+date.endDate.toISOString()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  getMostCommunicatedRequest(date,time){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.analytics +"most-comm-hours?organizationId=" +
        this._authService.getOrganizationId()+"&startDate="+date.startDate.toISOString()+
        "&endDate="+date.endDate.toISOString()+"&meridiem="+time).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  getAgentSpecificRequest(date,topAgentCount){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.analytics +"top-agents?organizationId=" +
        this._authService.getOrganizationId()+"&startDate="+date.startDate.toISOString()+
        "&endDate="+date.endDate.toISOString()+"&topAgentCount="+topAgentCount).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  getTotalUserRequest(date){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.analytics +"total-users?organizationId=" +
        this._authService.getOrganizationId()+"&startDate="+date.startDate.toISOString()+
        "&endDate="+date.endDate.toISOString()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

}
