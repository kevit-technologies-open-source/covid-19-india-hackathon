import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Urls} from "../utils/urls";
import {AccountService} from "./account.service";
import {LookAndFeelModel} from "../models/web-widget.models";

@Injectable()
export class WebWidgetService {
  constructor(private http: HttpClient,
              private _authService: AccountService) {
  }

  getLookAndFeelConfig(): any {
    return new Promise((resolve, reject) => {
      this.http.get(Urls.lookAndFeel + "?organizationId=" +
        this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  setLookAndFeelConfig(data): any {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.lookAndFeel + "?organizationId=" +
        this._authService.getOrganizationId(), data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  getDelayConfig(): any {
    return new Promise((resolve, reject) => {
      this.http.get(Urls.delay + "?organizationId=" +
        this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  setDelayConfig(data): any {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.delay + "?organizationId=" +
        this._authService.getOrganizationId(), data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  getWelcomeConfig(): any {
    return new Promise((resolve, reject) => {
      this.http.get(Urls.welcomeMessages + "?organizationId=" +
        this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  setWelcomeConfig(data): any {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.welcomeMessages + "?organizationId=" +
        this._authService.getOrganizationId(), data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  resetLookAndFeelConfig(): any {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.lookAndFeel + "/restore-default?organizationId=" +
        this._authService.getOrganizationId(), {}).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
