import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Urls} from "../utils/urls";
import {AccountService} from "./account.service";

@Injectable()
export class SettingsService {
  constructor(private http: HttpClient,
              private _authService: AccountService) {
  }
  getGeneralSetting() {
    return new Promise((resolve, reject) => {
      this.http.get(Urls.settings + "general?organizationId=" + this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  saveGeneralSetting(object) {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.settings + "general?organizationId=" + this._authService.getOrganizationId(),object).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
