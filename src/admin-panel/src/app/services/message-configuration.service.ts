import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Urls} from "../utils/urls";
import {AccountService} from "./account.service";

@Injectable()
export class MessageConfigurationService {
  constructor(private http: HttpClient,
              private _authService: AccountService) {
  }
  getMessageConfiguration(){
    return new Promise((resolve, reject) => {
      this.http.get(Urls.settings +"messages?organizationId=" +
        this._authService.getOrganizationId()).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  updateMessageConfiguration(object){
    return new Promise((resolve, reject) => {
      this.http.put(Urls.settings +"messages?organizationId=" +
        this._authService.getOrganizationId(),object).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

}
