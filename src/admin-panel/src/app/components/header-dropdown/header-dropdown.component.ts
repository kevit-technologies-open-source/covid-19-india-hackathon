import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "app-header-dropdown",
  templateUrl: "./header-dropdown.component.html",
  styleUrls: ["./header-dropdown.component.scss"]
})
export class HeaderDropdownComponent implements OnInit {
  @Output() closeModal = new EventEmitter();
  @Output() menuClick = new EventEmitter();
  @Input() width;

  constructor() {}

  ngOnInit() {
    this.width = this.width + 20;
  }

  close() {
    this.closeModal.emit(true);
  }

  changePassModal() {
    this.menuClick.emit("changePass");
  }

  updateProfileModal() {
    this.menuClick.emit("updateProfile");
  }

  userLogout() {
    this.menuClick.emit("logout");
  }
}
