export const environment = {
  production: true,
  apiUrl: '', // <- Development
  secret: '',
  directlineBotName: '',
  firebase: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: '',
  }
};
