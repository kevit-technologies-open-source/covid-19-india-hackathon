let uploadLocations = {
    user: {
        base: 'user-data/',
        profile: 'profile-picture/',
    },
    bot: {
        base: 'bot/',
        icon: 'icon/',
    },
    media: {
        base: 'media/',
        screenshot: 'screenshot/',
        agentFile: 'agent-file/',
    },
};

let frontEndURLs = {
    superAdminPanelBase: process.env.FRONT_URL_SUPER_ADMIN || 'http://192.168.1.202:4600/',
    adminPanelBase: process.env.FRONT_URL_ADMIN || 'http://192.168.1.202:4600/',
    //TODO ask priyadidi
    setPasswordPage: 'set/',
    resetPasswordPage: 'reset/',
    forgotPasswordPage: 'forgot/',
};

module.exports = {
    jwtSecret: process.env.JWT_SECRET || 'SimpleJWT',
    firebaseDbUrl: process.env.FIREBASE_DB_URL,
    uploadLocations: uploadLocations,
    BaseUrl: process.env.BASE_URL || 'http://192.168.1.43:3200/',
    frontEndURLs: frontEndURLs,
    bot: {
        MicrosoftAppId: process.env.MICROSOFT_APP_ID,
        MicrosoftAppPassword: process.env.MICROSOFT_APP_PASSWORD,
    },
    mailer: {
        fromAddress: process.env.FROM_EMAIL_ADDRESS,
        sendgridApiKey: process.env.SENDGRID_API_KEY,
        send: process.env.SEND_MAIL || true,
    },
    server: {
        port: process.env.PORT || 3200,
        logLevel: process.env.LOG_LEVEL || 'all',
        alertLogLevel: process.env.ALERT_LOG_LEVEL || 'all',
        mongoDBConnectionUrl: process.env.MONGODB_URL || 'mongodb://127.0.0.1:27017/HP',
        webhookUrl: process.env.WEBHOOK_URL,
    },
    environment: process.env.ENVIRONMENT || 'production',
};
