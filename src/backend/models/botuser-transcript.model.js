/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema Definition
 */

const botUserTranscriptSchema = new Schema(
    {
        userId: { type: Schema.Types.String, unique: true },
        isArchived: { type: Schema.Types.Boolean, default: false },
        transcript: Schema.Types.Mixed,
    },
    { timestamps: true },
);

/**
 * Export Schema
 */
module.exports = mongoose.model('botuser-transcript', botUserTranscriptSchema);
