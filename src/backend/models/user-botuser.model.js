/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const pagination = require('mongoose-paginate');

/**
 * Schema Definition
 */

const botUserSchema = new Schema(
    {
        userId: { type: Schema.Types.String, unique: true },
        conversationId: Schema.Types.String,
        name: Schema.Types.String,
        email: Schema.Types.String,
        contactNumber: Schema.Types.String,
        isArchived: { type: Schema.Types.Boolean, default: false },
        address: Schema.Types.Mixed,
        channel: Schema.Types.String,
        isAppliedForHandOff: { type: Schema.Types.Boolean, default: false },
        isHandOffGoingOn: { type: Schema.Types.Boolean, default: false },
        isConversionOver: { type: Schema.Types.Boolean, default: false },
        lastInteraction: { type: Schema.Types.Date, default: Date.now },
        organizationId: { type: Schema.Types.ObjectId, ref: 'organization' },
        dialogflowContext: Schema.Types.Mixed,
    },
    { timestamps: true },
);

botUserSchema.plugin(pagination);
/**
 * Export Schema
 */
module.exports = mongoose.model('user-botuser', botUserSchema);
