/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const pagination = require('mongoose-paginate');
const Schema = mongoose.Schema;
/**
 * Schema Definition
 */

const organizationSchema = new Schema(
    {
        name: Schema.Types.String,
        originAdminId: { type: Schema.Types.ObjectId, ref: 'user-agent' },
        timezone: {
            str: {
                type: Schema.Types.String,
                enum: [
                    '(UTC +14) LINT - Kiritimati',
                    '(UTC +13:45) CHADT - Chatham Islands',
                    '(UTC +13) NZDT - Auckland',
                    '(UTC +12) ANAT - Anadyr',
                    '(UTC +11) AEDT - Melbourne',
                    '(UTC +10:30)	ACDT - Adelaide',
                    '(UTC +10) AEST - Brisbane',
                    '(UTC +9:30) ACST - Darwin',
                    '(UTC +9) JST - Tokyo',
                    '(UTC +8:45) ACWST - Eucla',
                    '(UTC +8) CST - Beijing',
                    '(UTC +7) WIB - Jakarta',
                    '(UTC +6:30) MMT - Yangon',
                    '(UTC +6) BST - Dhaka',
                    '(UTC +5:45) NPT - Kathmandu',
                    '(UTC +5:30) IST - New Delhi',
                    '(UTC +5) UZT - Tashkent',
                    '(UTC +4:30) AFT - Kabul',
                    '(UTC +4) GST - Dubai',
                    '(UTC +3:30) IRST - Tehran',
                    '(UTC +3) MSK - Moscow',
                    '(UTC +2) EET - Cairo',
                    '(UTC +1) CET - Brussels',
                    '(UTC +0) GMT - London',
                    '(UTC -1) CVT - Praia',
                    '(UTC -2) GST - King Edward Point',
                    '(UTC -3) ART - Buenos Aires',
                    "(UTC -3:30) NST - St. John's",
                    '(UTC -4) VET - Caracas',
                    '(UTC -5) EST - New York',
                    '(UTC -6) CST - Mexico City',
                    '(UTC -7) MST - Calgary',
                    '(UTC -8) PST - Los Angeles',
                    '(UTC -9) AKST - Anchorage',
                    '(UTC -9:30) MART - Taiohae',
                    '(UTC -10) HST - Honolulu',
                    '(UTC -11) NUT - Alofi',
                    '(UTC -12) AoE - Baker Island',
                ],
                default: '(UTC +5:30) IST - New Delhi',
            },
            val: {
                type: Schema.Types.Number,
                default: 5.5,
            },
        },
        daylightSavings: { type: Schema.Types.Boolean, default: false },
        work24by7: { type: Schema.Types.Boolean, default: false },
        daySpecificWorking: [
            {
                dayNumber: { type: Schema.Types.Number },
                isEnabled: { type: Schema.Types.Boolean },
                isPublicHoliday: { type: Schema.Types.Boolean },
                startTime: { type: Schema.Types.Number },
                endTime: { type: Schema.Types.Number },
            },
        ],
        isHandOffShutDown: { type: Schema.Types.Boolean, default: false },
        previousBot: {
            name: Schema.Types.String,
            deleteDate: Schema.Types.Date,
        },
        isArchived: { type: Schema.Types.Boolean, default: false },
    },
    { timestamps: true },
);

organizationSchema.plugin(pagination);

/**
 * Export Schema
 */
module.exports = mongoose.model('organization', organizationSchema);
