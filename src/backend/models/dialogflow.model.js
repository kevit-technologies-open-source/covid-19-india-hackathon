const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dialogflowSchema = new Schema({
    accessToken: { type: Schema.Types.String },
    createdAt: { type: Schema.Types.Date, default: Date.now() },
    projectId: { type: Schema.Types.String, required: true },
    dfJson: Schema.Types.Mixed,
    fileName: Schema.Types.String,
    intentList: [Schema.Types.String],
    organizationId: { type: Schema.Types.ObjectId, ref: 'organization' },
    isArchived: { type: Schema.Types.Boolean, default: false },
});

// dialogflowTokenSchema.index({createdAt: 1}, {expireAfterSeconds: 3500});

module.exports = mongoose.model('dialogflow', dialogflowSchema);
