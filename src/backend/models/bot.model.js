/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const pagination = require('mongoose-paginate');
const Schema = mongoose.Schema;
/**
 * Schema Definition
 */

const botSchema = new Schema(
    {
        botName: Schema.Types.String,
        organizationId: { type: Schema.Types.ObjectId, ref: 'organization' },
        dialogflow: { type: Schema.Types.ObjectId, ref: 'dialogflow' },
        putOnHold: { type: Schema.Types.Boolean, default: false },
        messages: {
            handOffStart: Schema.Types.String,
            handOffTransfer: Schema.Types.String,
            handOffEnd: Schema.Types.String,
            handOffUnavailable: Schema.Types.String,
            handOffShutDown: Schema.Types.String,
            publicHoliday: Schema.Types.String,
            putOnHold: Schema.Types.String,
            fiveMinMessage: Schema.Types.String,
        },
        botScript: Schema.Types.String,
        siteUrls: [Schema.Types.String],
        widget: {
            iconUrl: Schema.Types.String,
            position: { type: Schema.Types.String, enum: ['right', 'left'] },
            size: { type: Schema.Types.String },
            chatBackgroundColour: { type: Schema.Types.String },
            chatFontColour: { type: Schema.Types.String },
            bot: {
                bubbleColour: Schema.Types.String,
                fontColour: Schema.Types.String,
                fontStyle: Schema.Types.String,
            },
            user: {
                bubbleColour: Schema.Types.String,
                fontColour: Schema.Types.String,
                fontStyle: Schema.Types.String,
            },
            inputBackground: {
                bubbleColour: Schema.Types.String,
                fontColour: Schema.Types.String,
            },
            //placeholderText: {type: Schema.Types.String},
            agent: {
                bubbleColour: Schema.Types.String,
                fontColour: Schema.Types.String,
                fontStyle: Schema.Types.String,
            },
        },
        welcomeMessage: {
            title: Schema.Types.String,
            greetingText: Schema.Types.String,
            requiredFields: {
                name: { type: Schema.Types.Boolean },
                email: { type: Schema.Types.Boolean },
                contactNumber: { type: Schema.Types.Boolean },
            },
        },
        delay: {
            messageDelayTime: { type: Schema.Types.Number, default: 0 },
            faceAppearDelayTime: { type: Schema.Types.Number, default: 2 },
            unFoldTime: { type: Schema.Types.Number, default: 5 },
            isAutoUnfold: { type: Schema.Types.Boolean, default: true },
        },
        isArchived: { type: Schema.Types.Boolean, default: false },
    },
    { timestamps: true },
);

botSchema.plugin(pagination);
/**
 * Export Schema
 */
module.exports = mongoose.model('bot', botSchema);
