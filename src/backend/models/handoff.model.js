/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema Definition
 */

const handOffSchema = new Schema(
    {
        botUserId: { type: Schema.Types.String },
        agentAddress: [{ type: Schema.Types.Mixed }],
        agentId: Schema.Types.String,
        isWaiting: { type: Schema.Types.Boolean, default: true },
        isFinished: { type: Schema.Types.Boolean, default: false },
        fcmToken: Schema.Types.String,
        organizationId: { type: Schema.Types.ObjectId, ref: 'organization' },
        agentAssigned: [{ type: Schema.Types.ObjectId, ref: 'user-agent' }],
        newAgentId: { type: Schema.Types.ObjectId, ref: 'user-agent' },
        initiatedByAgent: { type: Schema.Types.Boolean, default: false },
        requestArrival: {
            type: Schema.Types.String,
            enum: ['inWorkingHours', 'outOfWorkingHours', 'onOffDay', 'onPublicHoliday', 'handOffShutdown'],
        },
        acceptanceTime: { type: Schema.Types.Date },
    },
    { timestamps: true },
);

/**
 * Export Schema
 */
module.exports = mongoose.model('handoff', handOffSchema);
