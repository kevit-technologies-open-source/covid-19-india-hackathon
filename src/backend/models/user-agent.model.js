/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const pagination = require('mongoose-paginate');
/**
 * Schema Definition
 */
/**
 * Config
 * */
const config = require('../config');
/**
 * Services
 * */
const Logger = require('./../services/logger');

const userSchema = new Schema(
    {
        name: { type: Schema.Types.String, default: 'Web User' },
        email: {
            type: Schema.Types.String,
        },
        password: Schema.Types.String,
        signUpToken: Schema.Types.String,
        contactNumber: Schema.Types.String,
        profilePicture: Schema.Types.String,
        webToken: [Schema.Types.String],
        role: {
            type: Schema.Types.String,
            enum: ['admin', 'supportAgent', 'superAdmin'],
            default: 'admin',
        },
        fcmToken: [Schema.Types.String],
        createdBy: { type: Schema.Types.ObjectId, ref: 'user-agent' },
        forgetToken: Schema.Types.String,
        organizationId: { type: Schema.Types.ObjectId, ref: 'organization' },
        isArchived: { type: Schema.Types.Boolean, default: false },
    },
    { timestamps: true },
);

userSchema.statics.findByEmail = function(email) {
    let user = this;
    return user.findOne({ email, isArchived: false }).then((user) => {
        if (!user) {
            return Promise.reject({
                status: 'USER_NOT_FOUND',
                message: 'Incorrect email or password.',
            });
        } else {
            return Promise.resolve(user);
        }
    });
};

userSchema.methods.getForgetToken = function() {
    let u = this;
    let access = 'auth';
    let token = jwt.sign({ _id: u._id.toHexString(), access }, config.jwtSecret, { expiresIn: '15m' }).toString();
    u.forgetToken = token;
    return u.save().then((usr) => {
        setInterval(() => {
            usr.forgetToken = undefined;
            usr.save();
        }, 60000 * 15);
        return token;
    });
};

userSchema.statics.findByToken = function(token) {
    let user = this;
    let decoded;
    let jwtSecret = config.jwtSecret;
    try {
        decoded = jwt.verify(token, jwtSecret);
    } catch (e) {
        return Promise.reject({ status: 'INVALID_TOKEN', message: 'Cannot decode token' });
    }

    return user.findOne({
        _id: decoded._id,
        // webToken: token,
    });
};

userSchema.statics.findForgotToken = function(token) {
    let user = this;
    let decoded;
    try {
        decoded = jwt.verify(token, config.jwtSecret);
    } catch (e) {
        return Promise.reject('Fail');
    }
    return user.findOne({
        _id: decoded._id,
        forgetToken: token,
    });
};

userSchema.statics.findByCredentials = function(email, password) {
    let user = this;
    return user.findOne({ email, isArchived: false }).then((user) => {
        if (!user) {
            return Promise.reject({
                status: 'USER_NOT_FOUND',
                message: 'Incorrect email or password.',
            });
        }
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                if (res === true) {
                    return resolve(user);
                } else {
                    Logger.log.warn('Wrong Password for email:', user.email);
                    return reject({
                        status: 'USER_NOT_FOUND',
                        message: 'Incorrect email or password.',
                    });
                }
            });
        });
    });
};

userSchema.methods.removeToken = function(token, fcmToken) {
    let u = this;
    return u.update({
        $pull: {
            // webToken: token,
            fcmToken: fcmToken,
        },
    });
};

userSchema.methods.getAuthToken = function() {
    let u = this;
    let jwtSecret = config.jwtSecret;
    let access = 'auth';
    let token = jwt.sign({ _id: u._id.toHexString(), access }, jwtSecret).toString();
    // console.log('Web token generated::', token);
    // u.webToken.push(token);
    // return u.save().then(() => {
    return token;
    // });
};

userSchema.pre('save', function(next) {
    var user = this;
    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});
userSchema.methods.comparePassword = function(oldPassword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(oldPassword, this.password, function(err, isMatch) {
            if (err) {
                return cb(err);
            }
            return resolve(isMatch);
        });
    });
};
userSchema.plugin(pagination);
/**
 * Export Schema
 */
module.exports = mongoose.model('user-agent', userSchema);
