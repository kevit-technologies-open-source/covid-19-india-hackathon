const { MessageFactory, InputHints } = require('botbuilder');
const {
    ComponentDialog,
    DialogSet,
    DialogTurnStatus,
    TextPrompt,
    WaterfallDialog,
    Dialog,
} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const _ = require('lodash');
const axios = require('axios');
const TEXT_PROMPT = 'textPrompt';
const ESSENTIALS_WATERFALL_DIALOG = 'essentialsWaterfallDialog';
const FLOW_BUILDER_TEXT_PROMPT = 'FlowBuilderTextPrompt';
const essentialsHelper = require('../../helper/essentials.helper');
const HandOffHelper = require('./../../helper/handoff.helper');
// const {CancelAndHelpDialog} = require('./cancelAndHelpDialog');

class EssentialsDialog extends ComponentDialog {
    constructor(dialogId, userState, conversationState, conversationInfoPropertyAccessor) {
        super(dialogId);
        this.userState = userState;
        this.conversationState = conversationState;
        this.conversationInfoPropertyAccessor = conversationInfoPropertyAccessor;

        this.addDialog(new TextPrompt(FLOW_BUILDER_TEXT_PROMPT)).addDialog(
            new WaterfallDialog(ESSENTIALS_WATERFALL_DIALOG, [
                this.getStateStep.bind(this),
                this.getCityStep.bind(this),
                this.getServiceStep.bind(this),
                this.finalResultStep.bind(this),
            ]),
        );

        this.initialDialogId = ESSENTIALS_WATERFALL_DIALOG;
    }

    async getStateStep(step) {
        await step.context.sendActivity({
            text: 'Let me show you essential services available in your city',
            value: 'fromBot',
            inputHint: step.context.activity.from.id,
        });
        let organizationId = step.context._activity.from.organizationId || '5e44e3bf499fc022ce378206';
        var messageDelay = await HandOffHelper.getMessageDelay({ organizationId });
        console.log('Sending state list...');
        await step.context.sendActivity({ type: 'typing', inputHint: step.context.activity.from.id });
        await step.context.sendActivity({ type: 'delay', value: messageDelay * 1000 });
        let stateList = await essentialsHelper.getStateName();
        await step.context.sendActivity({
            text: '00STATELIST00' + stateList,
            value: 'fromBot',
            inputHint: step.context.activity.from.id,
        });
        return Dialog.EndOfTurn;
    }

    async getCityStep(step) {
        // Capture the response to the previous step's prompt
        let stateName = step.result;
        console.log('Here we got state name', stateName);
        let organizationId = step.context._activity.from.organizationId || '5e44e3bf499fc022ce378206';
        var messageDelay = await HandOffHelper.getMessageDelay({ organizationId });
        const cityList = await essentialsHelper.getCityNameByState(stateName);
        await step.context.sendActivity({ type: 'typing', inputHint: step.context.activity.from.id });
        await step.context.sendActivity({ type: 'delay', value: messageDelay * 1000 });
        await step.context.sendActivity({
            text: '00CITYLIST00' + cityList,
            value: 'fromBot',
            inputHint: step.context.activity.from.id,
        });
        return Dialog.EndOfTurn;
    }
    async getServiceStep(step) {
        let userDetails = await this.conversationInfoPropertyAccessor.get(step.context, {});
        userDetails.cityName = step.result;
        await this.conversationInfoPropertyAccessor.set(step.context, userDetails);
        let cityName = await essentialsHelper.getEssentialsCategoryByCity(step.result);
        await step.context.sendActivity({
            text: '00CATEGORY00' + cityName,
            value: 'fromBot',
            inputHint: step.context.activity.from.id,
        });
        return Dialog.EndOfTurn;
    }
    async finalResultStep(step) {
        // Capture the response to the previous step's prompt
        let serviceName = step.result;
        let userDetails = await this.conversationInfoPropertyAccessor.get(step.context, {});
        const finalResponse = await essentialsHelper.getEssentialsDetailsByCaregory(serviceName, userDetails.cityName);
        let messagesTosend = '';
        for (let i = 0; i < finalResponse.length; i++) {
            messagesTosend =
                messagesTosend +
                `<b>${finalResponse[i].nameoftheorganisation}</b></br>
            ${finalResponse[i].descriptionandorserviceprovided}</br><b>Mob No -</b>${finalResponse[i].phonenumber}</br><a href="${finalResponse[i].contact}" target="_blank">link</a>
</br>`;
        }
        await step.context.sendActivity({
            text: messagesTosend,
            value: 'fromBot',
            inputHint: step.context.activity.from.id,
        });
        return await step.endDialog();
        // return await this.conversationInfoPropertyAccessor.set(step, conversationStateObj);
    }

    dialogRegister(dialogId) {
        this.addDialog(dialogId);
    }
}

module.exports.EssentialsDialog = EssentialsDialog;
