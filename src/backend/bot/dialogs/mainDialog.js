const { ActivityTypes, ActionTypes, CardFactory } = require('botbuilder');
const { ComponentDialog, DialogSet, WaterfallDialog, DialogTurnStatus } = require('botbuilder-dialogs');
const GREETING_WATERFALL_DIALOG = 'greetingWaterfallDialog';
const axios = require('axios');
const stateHelpLineNumer = require('../../shared/stateHelpLineNumber');
const moment = require('moment');
const mongoose = require('mongoose');
const BotUser = mongoose.model('user-botuser');
const botInformaction = require('../../models/bot.model');
const statisticHelper = require('../../helper/covid-19-Statistics.helper');
const latestNewsHelper = require('../../helper/latestNews.helper');
/**
 * Helpers
 * */
const dialogflowHelper = require('../../helper/nlp.helper');
const ProactiveHelper = require('./../index');
const HandOffHelper = require('./../../helper/handoff.helper');
/**
 * Services
 * */
const Logger = require('./../../services/logger');
const UserHelper = require('../../helper/user.helper');

class MainDialog extends ComponentDialog {
    constructor(dialogId, userState, conversationState, conversationInfoPropertyAccessor, essentialDialog) {
        super(dialogId);

        if (!userState) throw new Error(`[MainDialog]: Missing parameter \'userInfoPropertyAccessor\' is required`);
        this.userState = userState;
        this.conversationState = conversationState;
        this.conversationInfoPropertyAccessor = conversationInfoPropertyAccessor;
        this.essentialDialog = essentialDialog;
        this.addDialog(new WaterfallDialog(GREETING_WATERFALL_DIALOG, [this.messageSend.bind(this)]));

        this.initialDialogId = GREETING_WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     * @param {boolean} handOffFlag
     * @param {boolean} handOffInitialize
     */

    async run({ turnContext, accessor, handOffFlag = false, handOffInitialize = false }) {
        if (handOffFlag) {
            let conversationStateObj = await this.conversationInfoPropertyAccessor.get(turnContext);
            if (!conversationStateObj) conversationStateObj = {};
            conversationStateObj.isHandOff = false;
            await this.conversationInfoPropertyAccessor.set(turnContext, conversationStateObj);
            //TODO Remove Timeout Timer based on handoffId
            Logger.log.info('Removing timer for', turnContext.activity.from.id);
            HandOffHelper.removeTimeout({ userId: turnContext.activity.from.id });
        }
        if (handOffInitialize) {
            let conversationStateObj = await this.conversationInfoPropertyAccessor.get(turnContext);
            if (!conversationStateObj) conversationStateObj = {};
            conversationStateObj.isHandOff = true;
            await this.conversationInfoPropertyAccessor.set(turnContext, conversationStateObj);
            //Handoff initialization flag will be sent to widget to store in Local Storage
            // when the AGENT accepts the request
            await turnContext.sendActivity({
                text: 'start',
                value: 'handOffStatus',
                // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                inputHint: turnContext.activity.from.id,
            });
            //TODO add userId for Timeout
            HandOffHelper.addOrUpdateTimeout({ userId: turnContext.activity.from.id });
        }
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);
        dialogSet.add(this.essentialDialog);
        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        // if (results.status === DialogTurnStatus.empty) {
        //     await dialogContext.beginDialog(this.id);
        // }
        let conversationStateObj = await this.conversationInfoPropertyAccessor.get(turnContext);
        if (!conversationStateObj) conversationStateObj = {};
        if (results.status === DialogTurnStatus.empty) {
            if (
                turnContext.activity.type === 'message' &&
                turnContext.activity.text !== '00CLOSE00' &&
                turnContext.activity.from.id.indexOf('agent') === -1
            ) {
                let organizationId = turnContext.activity.from.organizationId || '5e44e3bf499fc022ce378206';
                // let conversationStateObj = await this.conversationInfoPropertyAccessor.get(turnContext);
                if (!conversationStateObj || !conversationStateObj.isHandOff) {
                    if (turnContext.activity.type === 'message') {
                        //TODO send organizationId dynamically
                        let statusObj = await HandOffHelper.checkBotStatus({
                            organizationId: organizationId,
                        });
                        if (!statusObj.isActive) {
                            Logger.log.info('Bot is kept on hold identified.');
                            await turnContext.sendActivity({
                                text: statusObj.message || "Sorry. I can't help you at this moment.",
                                value: 'detailText',
                                // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                inputHint: turnContext.activity.from.id,
                            });
                        } else {
                            let contextFromDb = await this.getContext({ userId: turnContext.activity.from.id });
                            let obj = await dialogflowHelper.identifyIntent(
                                turnContext.activity.text,
                                turnContext.activity.from.id,
                                organizationId,
                                contextFromDb,
                            );
                            if (obj && obj.queryResult && obj.queryResult.outputContexts)
                                await this.updateContext({
                                    userId: turnContext.activity.from.id,
                                    context: obj.queryResult.outputContexts,
                                });
                            let intentDisplayName;
                            if (obj && obj.queryResult && obj.queryResult.intent && obj.queryResult.intent.displayName)
                                intentDisplayName = obj.queryResult.intent.displayName;
                            if (
                                turnContext.activity.text === '00DISABLETEXTBOX00' ||
                                this.connectToPanelIntent(intentDisplayName, obj)
                            ) {
                                Logger.log.info('00DISABLETEXTBOX00 identified.');
                                await turnContext.sendActivity('00ENABLETEXTBOX00');
                                Logger.log.info('Initial call for the handOff process');
                                let botUserId = turnContext.activity.from.id;
                                let systemStatus = await HandOffHelper.checkSystemHandOffStatus({
                                    organizationId: organizationId,
                                    botUserId: botUserId,
                                });
                                if (!systemStatus.allowHandOff) {
                                    Logger.log.info('Handoff cannot be initiated.', systemStatus.message);
                                    await turnContext.sendActivity({
                                        text: systemStatus.message,
                                        value: 'detailText',
                                        // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                        inputHint: turnContext.activity.from.id,
                                    });
                                    // await turnContext.sendActivity('00ENABLETEXTBOX00');
                                } else {
                                    await HandOffHelper.initializeHandOffProcess({
                                        botUserId,
                                        organizationId: organizationId,
                                        messageToUser: systemStatus.message,
                                        requestArrival: systemStatus.requestArrival,
                                        fromId: turnContext.activity.from.id,
                                    });
                                }
                            } else if (turnContext.activity.text === '00CONTINUEWITHBOT00') {
                                Logger.log.info('00CONTINUEWITHBOT00 identified.');
                                await turnContext.sendActivity({
                                    text: '00ENABLETEXTBOX00',
                                    // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                    inputHint: turnContext.activity.from.id,
                                });
                                await turnContext.sendActivity({
                                    text: 'Fine, how can I help you then?',
                                    value: 'fromBot',
                                    // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                    inputHint: turnContext.activity.from.id,
                                });
                            } else if (
                                // intentDisplayName === 'Default Fallback Intent' ||
                                intentDisplayName === 'Live chat agent' ||
                                // intentDisplayName === 'FAQ#&name=Live Chat' ||
                                turnContext.activity.text.toLowerCase() === 'talk to agent' ||
                                (conversationStateObj &&
                                    conversationStateObj.fallbackCount &&
                                    conversationStateObj.fallbackCount >= 1 &&
                                    intentDisplayName === 'Default Fallback Intent')
                            ) {
                                if (conversationStateObj && conversationStateObj.fallbackCount) {
                                    conversationStateObj.fallbackCount = 0;
                                    await this.conversationInfoPropertyAccessor.set(turnContext, conversationStateObj);
                                }
                                Logger.log.info('Intent to communicate with agent identified.');
                                await turnContext.sendActivity({
                                    text: '00DISABLETEXTBOX00',
                                    // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                    inputHint: turnContext.activity.from.id,
                                });
                                const reply = { type: ActivityTypes.Message };
                                let buttons = [{ type: ActionTypes.ImBack, title: 'Yes', value: '00DISABLETEXTBOX00' }];
                                if (intentDisplayName === 'Default Fallback Intent') {
                                    buttons.push({
                                        type: ActionTypes.ImBack,
                                        title: 'Explore COVID-19',
                                        value: 'Explore COVID-19',
                                    });
                                } else {
                                    buttons.push({
                                        type: ActionTypes.ImBack,
                                        title: 'No',
                                        value: '00CONTINUEWITHBOT00',
                                    });
                                }
                                await turnContext.sendActivity('00DISABLETEXTBOX00');
                                const card = CardFactory.heroCard('', undefined, buttons, {
                                    text: 'Would you mind if I connect our Live Expert to assist you further?',
                                });
                                reply.attachments = [card];
                                reply.value = 'fromBot';
                                // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                (reply.inputHint = turnContext.activity.from.id), await turnContext.sendActivity(reply);
                            } else {
                                if (intentDisplayName === 'Default Fallback Intent') {
                                    if (!conversationStateObj) conversationStateObj = {};
                                    if (conversationStateObj && !conversationStateObj.fallbackCount)
                                        conversationStateObj.fallbackCount = 1;
                                    else if (
                                        conversationStateObj &&
                                        conversationStateObj.fallbackCount &&
                                        conversationStateObj.fallbackCount > 0
                                    )
                                        conversationStateObj.fallbackCount++;
                                    await this.conversationInfoPropertyAccessor.set(turnContext, conversationStateObj);
                                } else {
                                    if (conversationStateObj && conversationStateObj.fallbackCount) {
                                        conversationStateObj.fallbackCount = 0;
                                        await this.conversationInfoPropertyAccessor.set(
                                            turnContext,
                                            conversationStateObj,
                                        );
                                    }
                                }
                                Logger.log.info('Normal message to the bot identified');
                                if (intentDisplayName === 'essentials' || turnContext.activity.text === 'essentials') {
                                    await dialogContext.replaceDialog(this.essentialDialog.id);
                                } else {
                                    await dialogContext.beginDialog(this.id, { nlpObj: obj });
                                }
                            }
                        }
                    }
                } else if (conversationStateObj && conversationStateObj.isHandOff) {
                    Logger.log.info('Continue the proactive messaging with the agent');
                    let botUserId = turnContext.activity.from.id;
                    let message = turnContext.activity.text;
                    if (message === '00DENYFORSCREENSHOT00' || message === '00ALLOWFORSCREENSHOT00') {
                        await turnContext.sendActivity({
                            text: '00ENABLETEXTBOX00',
                            // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                            inputHint: turnContext.activity.from.id,
                        });
                    }
                    if (turnContext.activity.value && turnContext.activity.value.indexOf('##') !== -1)
                        await HandOffHelper.sendMessageToAgent({
                            botUserId,
                            message,
                            value: turnContext.activity.value,
                        });
                    else await HandOffHelper.sendMessageToAgent({ botUserId, message });
                    //TODO update Timeout Timer based on handoffId
                    HandOffHelper.addOrUpdateTimeout({ userId: turnContext.activity.from.id });
                }
            } else if (turnContext.activity.type === 'message' && turnContext.activity.text === '00CLOSE00') {
                let organizationId = turnContext.activity.from.organizationId || '5e44e3bf499fc022ce378206';
                let conversationStateObj = await this.conversationInfoPropertyAccessor.get(turnContext);
                if (conversationStateObj && conversationStateObj.isHandOff) {
                    Logger.log.info('00CLOSE00 detected while handoff');
                    // let objToSave = {};
                    conversationStateObj.isHandOff = false;
                    await this.conversationInfoPropertyAccessor.set(turnContext, conversationStateObj);
                    let botUserId = turnContext.activity.from.id;
                    await HandOffHelper.changeHandOffStatus({ botUserId, organizationId });
                    HandOffHelper.removeTimeout({ userId: botUserId });
                }
            } else if (
                turnContext.activity.type === 'message' &&
                turnContext.activity.from.id.indexOf('agent') !== -1
            ) {
                Logger.log.info('Message from agent identified');
                let agentId = turnContext.activity.from.id;
                let message = turnContext.activity.text;
                if (message === '00INITIATEHANDOFF00') {
                    await HandOffHelper.turnOnHandOffStatusInDirectline({ agentId });
                    //Call for add timeout is done from handOffInitiate condition mentioned above
                } else {
                    let agentUserId;
                    if (turnContext.activity.from && turnContext.activity.from.agentUserId)
                        agentUserId = turnContext.activity.from.agentUserId;
                    HandOffHelper.addOrUpdateTimeout({ agentId });
                    if (turnContext.activity.value && turnContext.activity.value.indexOf('##') !== -1)
                        await HandOffHelper.sendMessageToBotUser({
                            agentId,
                            message,
                            value: turnContext.activity.value,
                            agentUserId,
                        });
                    else await HandOffHelper.sendMessageToBotUser({ agentId, message, agentUserId });
                    //TODO update Timeout Timer based on handoffId
                }
            }
        }

        await this.conversationState.saveChanges(turnContext);
        await this.userState.saveChanges(turnContext);
    }

    /**
     * Sends Static message if there is no response from dialogflow
     * */
    connectToPanelIntent(intentName, intentObj) {
        let connectToPanelFlag = true;
        let connectToPanelIntentsArr = [
            'user-handoff',
            'AFP Check Take - Now - Now',
            'Already Applied - yes - yes',
            'Check Status Of IPC - Now - Now',
            'Check Status of Police Check - Now - Now',
            'Expedite International Checks - Now - Now',
            'IPC Take - Now - Now',
            'Police Check MP - Applied - Now - Now',
            'Police Check Result - Now - Now',
            'Police Check Take-AlreadyApplied - Now - Now',
            'Process Police Check Urgently - no - Now - Now',
            'Status Check Of Police Check - now - Now',
            'Track Status AFP Applicants - yes - yes',
            'Track Status IPC - For You - Now - Now',
            'Track Status IPC - For Your Business - Now - Now',
            'Track Status IPC Applicants - yes - yes',
            'Track Status of AFP Check_FB - Now - Now',
            'Track Status of AFP Check_FM - Now - Now',
            'Track the status of Police Check_FM - Now - Now',
            'Track the status of Police Check_FB - Now - Now',
        ];
        if (connectToPanelIntentsArr.includes(intentName)) {
            if (intentObj && intentObj.queryResult && intentObj.queryResult.parameters) {
                for (let i = 0; i <= Object.keys(intentObj.queryResult.parameters).length; i++) {
                    if (intentObj.queryResult.parameters[Object.keys(intentObj.queryResult.parameters)[i]] === '') {
                        connectToPanelFlag = false;
                        break;
                    }
                }
                if (
                    intentObj &&
                    intentObj.queryResult &&
                    intentObj.queryResult.fulfillmentMessages &&
                    intentObj.queryResult.fulfillmentMessages.length > 0 &&
                    intentObj.queryResult.fulfillmentMessages[0].text.text[0].indexOf('connect') !== -1 &&
                    intentObj.queryResult.fulfillmentMessages[0].text.text[0].indexOf('fit2work') !== -1
                ) {
                    connectToPanelFlag = true;
                }
            }
            return connectToPanelFlag;
        } else {
            return false;
        }
    }

    async messageSend(stepContext) {
        let nlpObj = stepContext.options.nlpObj;
        console.log('Intent Name---------', nlpObj.queryResult.intent.displayName);
        console.log('Geo-city----------', nlpObj.queryResult.parameters['geo-city']);
        console.log('Geo-country-------', nlpObj.queryResult.parameters['geo-country']);
        console.log('Geo-state--------', nlpObj.queryResult.parameters['geo-state']);
        let atleastOneText = false;
        let isFirstMessage = true;
        let organizationId = stepContext.context._activity.from.organizationId || '5e44e3bf499fc022ce378206';
        let messageDelay = await HandOffHelper.getMessageDelay({ organizationId });
        if (
            nlpObj &&
            nlpObj.queryResult &&
            nlpObj.queryResult.fulfillmentMessages &&
            nlpObj.queryResult.fulfillmentMessages.length > 0
        ) {
            const reply = { type: ActivityTypes.Message };
            let buttons = [];
            let userName = await UserHelper.getBotUserNameById(stepContext.context.activity.from.id);
            await stepContext.context.sendActivity({
                type: 'typing',
                inputHint: stepContext.context.activity.from.id,
            });
            await stepContext.context.sendActivity({ type: 'delay', value: messageDelay * 1000 });
            if (nlpObj.queryResult.intent.displayName == 'Latest news') {
                let latestNews = await latestNewsHelper.getLatestNews();
                await stepContext.context.sendActivity({
                    text: latestNews,
                    value: 'fromBot',
                    inputHint: stepContext.context.activity.from.id,
                });
            } // for latest news
            if (nlpObj.queryResult.parameters['geo-city']) {
                let cityStatus = await statisticHelper.getCityReport(
                    nlpObj.queryResult.parameters['geo-city'],
                    nlpObj.queryResult.parameters['date-time'] || nlpObj.queryResult.parameters['recent'],
                );

                await stepContext.context.sendActivity({
                    text: cityStatus,
                    value: 'fromBot',
                    inputHint: stepContext.context.activity.from.id,
                });
            } else if (
                (nlpObj.queryResult.intent.displayName != 'Other country' &&
                    nlpObj.queryResult.intent.displayName == 'COVID-19 Statistics') ||
                nlpObj.queryResult.parameters['geo-country']
            ) {
                let indiaStatus = await statisticHelper.getIndaiReport(
                    nlpObj.queryResult.parameters['date-time'] || nlpObj.queryResult.parameters['recent'],
                );
                if (nlpObj.queryResult.intent.displayName == 'COVID-19 Statistics') {
                    await stepContext.context.sendActivity({
                        text: 'As we know ' + userName + ', Active cases of COVID-19 is still increasing.',
                        value: 'fromBot',
                        inputHint: stepContext.context.activity.from.id,
                    });
                }
                await stepContext.context.sendActivity({
                    text: indiaStatus,
                    value: 'fromBot',
                    inputHint: stepContext.context.activity.from.id,
                });
            } else if (
                nlpObj.queryResult.intent.displayName != 'State' &&
                nlpObj.queryResult.parameters['geo-state'] &&
                nlpObj.queryResult.intent.displayName != 'selected-helpline-number-state'
            ) {
                console.log(
                    'here-------------',
                    nlpObj.queryResult.parameters['geo-state'] &&
                        (nlpObj.queryResult.intent.displayName != 'selected-helpline-number-state' ||
                            nlpObj.queryResult.intent.displayName != 'State'),
                );
                let stateStatus = await statisticHelper.getStateReport(
                    nlpObj.queryResult.parameters['geo-state'],
                    nlpObj.queryResult.parameters['date-time'] || nlpObj.queryResult.parameters['recent'],
                );
                console.log('state data', stateStatus);
                await stepContext.context.sendActivity({
                    text: stateStatus,
                    value: 'fromBot',
                    inputHint: stepContext.context.activity.from.id,
                });
            } else if (
                !nlpObj.queryResult.parameters['geo-state'] &&
                nlpObj.queryResult.parameters['geo-city'] &&
                nlpObj.queryResult.parameters['geo-country']
            ) {
                await stepContext.context.sendActivity({
                    text: 'Sorry we are unable to find your Data',
                    value: 'fromBot',
                    inputHint: stepContext.context.activity.from.id,
                });
            }
            for (let i = 0; i < nlpObj.queryResult.fulfillmentMessages.length; i++) {
                if (nlpObj.queryResult.fulfillmentMessages[i].hasOwnProperty('payload')) {
                    for (let j = 0; j < nlpObj.queryResult.fulfillmentMessages[i].payload.buttons.length; j++) {
                        buttons.push({
                            type: ActionTypes.ImBack,
                            title: nlpObj.queryResult.fulfillmentMessages[i].payload.buttons[j].title,
                            value: nlpObj.queryResult.fulfillmentMessages[i].payload.buttons[j].title,
                        });
                    }
                }

                if (nlpObj.queryResult.fulfillmentMessages[i].hasOwnProperty('text')) {
                    atleastOneText = true;

                    let textResponses = nlpObj.queryResult.fulfillmentMessages[i].text.text
                        .toString()
                        .replace('{name}', userName);
                    if (nlpObj.queryResult.intent.displayName === 'selected-helpline-number-state') {
                        if (textResponses.indexOf('{number}') != -1) {
                            if (stateHelpLineNumer[nlpObj.queryResult.parameters['geo-state']]) {
                                textResponses = textResponses
                                    .toString()
                                    .replace(
                                        '{number}',
                                        stateHelpLineNumer[nlpObj.queryResult.parameters['geo-state']],
                                    );
                            } else {
                                textResponses =
                                    "Sorry, we didn't found helpline number for your state, You can always contact to general helpline number 1075.";
                            }
                        }
                    }
                    if (nlpObj.queryResult.intent.displayName === 'Default Welcome Intent') {
                        let botName = await botInformaction.findOne({
                            organizationId: stepContext.context.activity.from.organizationId,
                        });
                        textResponses = textResponses.toString().replace('{chatbot-name}', botName.botName);
                        // nlpObj.queryResult.parameters["geo-state"]
                    }
                    try {
                        if (!isFirstMessage) {
                            await stepContext.context.sendActivity({
                                type: 'typing',
                                inputHint: stepContext.context.activity.from.id,
                            });
                            await stepContext.context.sendActivity({ type: 'delay', value: messageDelay * 1000 });
                            await stepContext.context.sendActivity({
                                text: textResponses,
                                value: 'fromBot',
                                inputHint: stepContext.context.activity.from.id,
                            });
                        } else {
                            if (textResponses.length > 0) {
                                isFirstMessage = false;
                                // [Math.floor(Math.random() * textResponses.length)]
                                // await stepContext.context.sendActivity({ type: 'typing' });
                                await stepContext.context.sendActivity({
                                    text: textResponses,
                                    value: 'fromBot',
                                    // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                                    inputHint: stepContext.context.activity.from.id,
                                });
                            }
                        }
                    } catch (err) {
                        Logger.log.error('Error sending response of intent.');
                        Logger.log.error(err.message || err);
                        return reject(err);
                    }
                }
            }

            if (buttons.length > 0) {
                // await stepContext.context.sendActivity({ type: 'typing', inputHint: stepContext.context.activity.from.id });
                // await stepContext.context.sendActivity({ type: 'delay', value: messageDelay * 1000 });
                const card = CardFactory.heroCard('', undefined, buttons, {
                    text: '',
                });
                reply.attachments = [card];
                reply.value = 'fromBot';
                // // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                (reply.inputHint = stepContext.context.activity.from.id), await stepContext.context.sendActivity(reply);
            }
        }
        if (!atleastOneText) {
            //:todo if response is not available then handle based on asked
            let response = [
                'I am young bot. I am still learning.',
                'I cannot help you with us for now.',
                'OOPS! I did not understand that.',
                "I didn't get that. Can you say it again?",
                'I missed what you said. What was that?',
                'Sorry, could you say that again?',
                'Sorry, can you say that again?',
                'Can you say that again?',
                "Sorry, I didn't get that. Can you rephrase?",
                'Sorry, what was that?',
                'One more time?',
                'What was that?',
                'Say that one more time?',
                "I didn't get that. Can you repeat?",
                'I missed that, say that again?',
            ];
            await stepContext.context.sendActivity({
                text: response[Math.floor(Math.random() * response.length)],
                value: 'fromBot',
                // Note: As no custom fields are allowed to send, we are sending receipt.id as inputHint
                inputHint: stepContext.context.activity.from.id,
            });
        }
        return await stepContext.endDialog();
    }

    async updateContext({ userId, context }) {
        return BotUser.findOneAndUpdate({ userId: userId }, { dialogflowContext: context }, { new: true })
            .then((updatedBotUser) => {
                return;
            })
            .catch((err) => {
                Logger.log.error('Error updating dialogflow context in db');
                Logger.log.error(err.message || err);
                return;
            });
    }
    async getContext({ userId }) {
        return BotUser.findOne({ userId: userId })
            .then((botUser) => {
                if (botUser && botUser.dialogflowContext) return botUser.dialogflowContext;
                else return;
            })
            .catch((err) => {
                Logger.log.error('Error getting dialogflow context from db');
                Logger.log.error(err.message || err);
                return;
            });
    }
}

module.exports.MainDialog = MainDialog;
