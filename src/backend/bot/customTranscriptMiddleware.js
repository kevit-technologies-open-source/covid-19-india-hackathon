// const { TurnContext } = require('botbuilder');
// const { Activity, ResourceResponse } = require('botframework-schema');
const mongoose = require('mongoose');
const BotUser = mongoose.model('user-botuser');
const HandOff = mongoose.model('handoff');
const BotUserTranscript = mongoose.model('botuser-transcript');
const Logger = require('../services/logger');

/**
 * Middleware for logging incoming, outgoing, updated or deleted Activity messages.
 * Uses the botTelemetryClient interface.
 */
class CustomTranscriptMiddleware {
    constructor(conversationInfoPropertyAccessor) {
        this.conversationInfoPropertyAccessor = conversationInfoPropertyAccessor;
        // Logger.log.info('In constructor of custom middleware for logging transcript');
    }

    async onTurn(context, next) {
        let conversationStateObj = await this.conversationInfoPropertyAccessor.get(context);
        if (context === null) {
            throw new Error('context is null');
        }
        //When message from user is found
        if (context.activity !== null && context.activity.text) {
            const activity = context.activity;
            await this.onReceiveActivity(activity, conversationStateObj);
        }

        //When sending message to a user from bot
        context.onSendActivities(async (ctx, activities, nextSend) => {
            const responses = await nextSend();
            activities.forEach(async (act) => {
                if (act.text || (act.attachments && act.attachments.length > 0))
                    await this.onSendActivity(act, conversationStateObj);
            });

            return responses;
        });
        if (next !== null) {
            await next();
        }
    }

    /**
     * Invoked when a message is received from the user.
     * @param activity Current activity sent from user.
     */
    async onReceiveActivity(activity) {
        if (activity.from.id.indexOf('agent') !== -1) return;
        Logger.log.info('Message received from User', activity.text);
        let displayMessage = '';
        if (activity && activity.text && activity.text.indexOf('00') !== -1) {
            switch (activity.text) {
                case '00DISABLETEXTBOX00':
                    displayMessage = 'Yes';
                    break;
                case '00CONTINUEWITHBOT00':
                    displayMessage = 'No';
                    break;
                case '00HANDOFFOVER00':
                    displayMessage = 'Yes';
                    break;
                case '00CONTINUEWITHAGENT00':
                    displayMessage = 'No';
                    break;
                case '00HANDOFFOVERBYUSER00':
                    displayMessage = 'Yes';
                    break;
                case '00HANDOFFDENYBYUSER00':
                    displayMessage = 'No';
                    break;
                case '00CLOSE00':
                    displayMessage = 'Closed the conversation';
                    break;
            }
        }
        let obj;
        let userId = activity.from.id;
        if (activity.text) {
            obj = {
                isFromBot: false,
                text: activity.text,
                timestamp: new Date(),
                isFrom: 'user',
                type: 'text',
            };
            // if (activity.from.id.indexOf('agent') !== -1) obj.isFrom = 'agent';
            if (activity.text.indexOf('00') !== -1) obj.text = displayMessage;
            if (activity.value && activity.value.indexOf('##') !== -1) {
                obj.type = activity.value.split('##').shift();
                let url = activity.value.split('##').splice(1);
                let urlStr = '';
                url.forEach((urlSubStr) => {
                    urlStr += urlSubStr;
                });
                obj.url = urlStr;
            }
        }
        if (obj) {
            let promiseArr = [];
            if (activity.text === '00CLOSE00') {
                BotUserTranscript.findOne({ userId: activity.from.id })
                    .then((userTranscript) => {
                        if (!userTranscript) {
                            Logger.log.info(
                                'No transcript found for the user, so removing the user.',
                                activity.from.id,
                            );
                            BotUser.deleteOne({ userId: activity.from.id })
                                .then((deletedUser) => {
                                    Logger.log.info('User deleted as no transcript found, userId', activity.from.id);
                                })
                                .catch((err) => {
                                    Logger.log.error('Error deleting user');
                                    Logger.log.error(err.message || err);
                                });
                        } else {
                            Logger.log.info('Transcript on 00CLOSE00 was present, so updating user');
                            BotUser.findOneAndUpdate(
                                { userId: activity.from.id },
                                {
                                    $set: {
                                        isConversionOver: true,
                                        lastInteraction: new Date(),
                                    },
                                    $push: { transcript: obj },
                                },
                                { upsert: true },
                            ).catch((err) => {
                                Logger.log.error('Error in updating transcript');
                                Logger.log.error(err.message || err);
                            });
                        }
                    })
                    .catch((err) => {
                        Logger.log.error('Error in finding transcript');
                        Logger.log.error(err.message || err);
                    });
            } else {
                promiseArr.push(
                    BotUserTranscript.findOneAndUpdate(
                        { userId: userId },
                        {
                            $push: {
                                transcript: obj,
                            },
                        },
                        { upsert: true },
                    ),
                );
                promiseArr.push(
                    BotUser.findOneAndUpdate({ userId: userId }, { $set: { lastInteraction: new Date() } }),
                );
                Promise.all(promiseArr)
                    .then((data) => {
                        Logger.log.info('Transcript Updated');
                    })
                    .catch((err) => {
                        BotUserTranscript.findOneAndUpdate(
                            { userId: userId },
                            { $push: { transcript: obj } },
                            { upsert: true },
                        )
                            .then((data) => {
                                Logger.log.info('Transcript Updated');
                            })
                            .catch((err) => {
                                Logger.log.error('Error in updating transcript');
                                Logger.log.error(err.message || err);
                            });
                    });
            }
        }
    }

    /**
     * Invoked when the bot sends a message to the user.
     * @param activity Last activity sent from user.
     */
    async onSendActivity(activity, conversationStateObj) {
        if (activity.recipient.id.indexOf('agent') !== -1) return;
        if (activity.value === 'handOffStatus') return;
        Logger.log.info('Message sent from Bot');
        let messageNotToStore = false;
        let displayMessage = '';
        if (activity && activity.text && activity.text.indexOf('00') !== -1) {
            switch (activity.text) {
                case '00DISABLETEXTBOX00':
                    messageNotToStore = true;
                    break;
                case '00ENABLETEXTBOX00':
                    messageNotToStore = true;
                    break;
                case '00ASKUSERABOUTCOMPLETE00':
                    messageNotToStore = true;
                    break;
            }
        }
        let obj;
        let userId = activity.recipient.id;
        if (messageNotToStore) {
            Logger.log.info('Message is not to be stored in the transcript.');
            return;
        } else {
            if (activity.text) {
                activity.text = activity.text.split('</br>').join('\n');
                activity.text = activity.text.split('<b>').join('');
                activity.text = activity.text.split('</b>').join('');
                activity.text = activity.text.split('<li>').join('');
                activity.text = activity.text.split('</li>').join('');
                obj = {
                    isFromBot: true,
                    text: activity.text,
                    type: 'text',
                    timestamp: new Date(),
                    isFrom: 'bot',
                };
                if (
                    activity.text.indexOf('00') === 0 &&
                    activity.text.lastIndexOf('00') === activity.text.length - 2 &&
                    displayMessage
                )
                    obj.text = displayMessage;
                if (activity.value && activity.value.indexOf('##') !== -1) {
                    obj.type = activity.value.split('##').shift();
                    let url = activity.value.split('##').splice(1);
                    let urlStr = '';
                    url.forEach((urlSubStr) => {
                        urlStr += urlSubStr;
                    });
                    obj.url = urlStr;
                }
            } else if (activity.attachments && activity.attachments.length > 0) {
                if (activity.attachments[0].content && activity.attachments[0].content.text) {
                    return;
                }
                obj = {
                    isFromBot: true,
                    isFrom: 'bot',
                    type: 'attachment',
                    text: activity.attachments[0].content.text,
                    buttons: JSON.stringify(activity.attachments[0].content.buttons),
                    timestamp: new Date(),
                };
            }
            if (
                (conversationStateObj &&
                    conversationStateObj.hasOwnProperty('isHandOff') &&
                    conversationStateObj.isHandOff) ||
                activity.value === 'fromAgent'
            ) {
                obj.isFrom = 'agent';
                obj.agentId = await this.getAgentId({ userId: activity.recipient.id });
            }
            if (activity.value === 'detailText') obj.type = 'detailText';
        }
        let promiseArr = [];
        if (obj)
            promiseArr.push(
                BotUserTranscript.findOneAndUpdate(
                    { userId: userId },
                    { $push: { transcript: obj } },
                    { upsert: true },
                ),
            );
        promiseArr.push(BotUser.findOneAndUpdate({ userId: userId }, { $set: { lastInteraction: new Date() } }));
        Promise.all(promiseArr)
            .then((data) => {
                Logger.log.info('Transcript Updated');
            })
            .catch((err) => {
                BotUserTranscript.findOneAndUpdate({ userId: userId }, { $push: { transcript: obj } }, { upsert: true })
                    .then((data) => {
                        Logger.log.info('Transcript Updated');
                    })
                    .catch((err) => {
                        Logger.log.error('Error in updating transcript');
                        Logger.log.error(err.message || err);
                    });
            });
    }

    getAgentId({ userId = activity.from.id }) {
        return HandOff.findOne({
            botUserId: userId,
            isWaiting: false,
            isFinished: false,
        })
            .select({ agentAssigned: 1 })
            .then((handOffData) => {
                if (handOffData && handOffData.agentAssigned && handOffData.agentAssigned.length > 0)
                    return handOffData.agentAssigned[handOffData.agentAssigned.length - 1];
                else return '';
            });
    }
}

module.exports = CustomTranscriptMiddleware;
