/**
 * Router Configuration Files
 */

/**
 * System and 3rd party libs
 */
const express = require('express');
const router = express.Router();
/**
 * Config
 * */
const config = require('../config');

// Import required bot services. See https://aka.ms/bot-services to learn more about the different parts of a bot.
const {
    BotFrameworkAdapter,
    ConversationState,
    InputHints,
    InspectionMiddleware,
    InspectionState,
    MemoryStorage,
    UserState,
    TranscriptLoggerMiddleware,
} = require('botbuilder');

//node-module for storing conversation and user data into mongodb database
const { MongoStorage } = require('@kevit/botbuilder-storage-mongo-v4');

// This bot's main dialog.
const { DialogAndWelcomeBot } = require('./dialogAndWelcomeBot');

const { MicrosoftAppCredentials } = require('botframework-connector');

//User and conversation accessor require
const UserStateProperties = require('./../shared/UserStateProperties');
const ConversationStateProperties = require('./../shared/ConversationStateProperties');
const DialogsNameList = require('./../shared/DialogsNameList');

// Create adapter.
const adapter = new BotFrameworkAdapter({
    appId: config.bot.MicrosoftAppId,
    appPassword: config.bot.MicrosoftAppPassword,
});

//Creating a mongo object for storing user and conversation data
let dbUrl = config.server.mongoDBConnectionUrl.substr(0, config.server.mongoDBConnectionUrl.lastIndexOf('/') + 1);
let dbName = config.server.mongoDBConnectionUrl.split('/').pop();
let storage = new MongoStorage(dbUrl || 'mongodb://127.0.0.1:27017/', dbName || 'HP', 'bot-storage');

const { Transcript } = require('../helper/transcriptHelper.js');
const transcriptLogger = new TranscriptLoggerMiddleware(new Transcript());
// console.log('Transcript logger type::', typeof transcriptLogger)
// console.log('Limiter type::', typeof limiter.schedule(() => transcriptLogger.logActivity()));
// adapter.use(transcriptLogger);
const CustomTranscriptMiddleware = require('./customTranscriptMiddleware');

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
    console.error(error);
    console.error(`\n [onTurnError]: ${error}`);
    // Send a message to the user
    const onTurnErrorMessage = `Sorry, it looks like something went wrong!`;
    await context.sendActivity(onTurnErrorMessage, onTurnErrorMessage, InputHints.ExpectingInput);
    // Clear out state
    await conversationState.delete(context);
};

//Memory storage is done in the mongoDB database

const inspectionState = new InspectionState(storage);
const conversationState = new ConversationState(storage);
const userState = new UserState(storage);

adapter.use(
    new InspectionMiddleware(
        inspectionState,
        userState,
        conversationState,
        new MicrosoftAppCredentials(config.bot.MicrosoftAppId, config.bot.MicrosoftAppPassword),
    ),
);

// Create user accessor property and conversation accessor property
let userInfoPropertyAccessor = userState.createProperty(UserStateProperties.USER_INFO_PROPERTY);
let conversationInfoPropertyAccessor = conversationState.createProperty(
    ConversationStateProperties.CONVERSATION_INFO_PROPERTY,
);
adapter.use(new CustomTranscriptMiddleware(conversationInfoPropertyAccessor));

const { MainDialog } = require('./dialogs/mainDialog');
const { EssentialsDialog } = require('./dialogs/essentials.dialog');

// const faqDialog = new FAQDialog(DialogsNameList.FAQ_DIALOG);
// const flowBuilderDialog = new FlowBuilderDialog(DialogsNameList.FLOW_BUILDER_DIALOG, flowNextQuestionAccessor, flowBuilderConversationAccessor);
const essentialsDialog = new EssentialsDialog(
    DialogsNameList.ESSENTIALS_DIALOG,
    userState,
    conversationState,
    conversationInfoPropertyAccessor,
);
const greetingDialog = new MainDialog(
    DialogsNameList.GREETING_DIALOG,
    userState,
    conversationState,
    conversationInfoPropertyAccessor,
    essentialsDialog,
);

// Create and add the InspectionMiddleware to the adapter.
let conversationReferences = {};
const bot = new DialogAndWelcomeBot(
    conversationState,
    userState,
    greetingDialog,
    userInfoPropertyAccessor,
    conversationReferences,
    conversationInfoPropertyAccessor,
);
// const dialogFlowRecognizer = require('./dialogFlow.recognizer');
//

/**
 * Router Definitions
 */
router.get('/messages', function(req, res, next) {
    res.send('Hy');
});

router.post('/messages', (req, res, next) => {
    // Route received a request to adapter for processing
    adapter.processActivity(req, res, async (turnContext) => {
        // route to bot activity handler.
        await bot.run(turnContext);
    });
});

let sendProactiveMessages = ({
    message,
    address,
    handOffOver = false,
    handOffInitialize = false,
    sendMessage = true,
}) => {
    return new Promise(async (resolve, reject) => {
        // console.log('sendProactiveMessages function called...', conversationReferences);
        // console.log("::conversationReference::", address);
        await adapter.continueConversation(address, async (turnContext) => {
            if (sendMessage) await turnContext.sendActivity(message);
            if (handOffOver) {
                // console.log('Set context=>isHandOff=false');
                let accessor = conversationState.createProperty('DialogState');
                await greetingDialog.run({ turnContext, accessor, handOffFlag: true });
            } else if (handOffInitialize) {
                // console.log('Set context=>isHandOff=false');
                let accessor = conversationState.createProperty('DialogState');
                await greetingDialog.run({ turnContext, accessor, handOffFlag: false, handOffInitialize: true });
            }
            return resolve();
        });
    });
};

/**
 * Export Router
 */
//module.exports = router;

module.exports = {
    router: router,
    sendProactiveMessages: sendProactiveMessages,
};
