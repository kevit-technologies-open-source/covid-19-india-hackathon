const { DialogBot } = require('./dialogBot');
const { TurnContext } = require('botbuilder');
const mongoose = require('mongoose');
const BotUsers = mongoose.model('user-botuser');
/**
 * Helpers
 * */
const HandOffHelper = require('./../helper/handoff.helper');
/**
 * Services
 * */
const Logger = require('./../services/logger');

class DialogAndWelcomeBot extends DialogBot {
    constructor(
        conversationState,
        userState,
        dialog,
        userInfoPropertyAccessor,
        conversationReferences,
        conversationInfoPropertyAccessor,
    ) {
        super(conversationState, userState, dialog, userInfoPropertyAccessor);
        this.conversationReferences = conversationReferences;
        this.conversationInfoPropertyAccessor = conversationInfoPropertyAccessor;

        this.onConversationUpdate(async (turnContext, next) => {
            this.addConversationReference(turnContext.activity);

            await next();
        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; cnt++) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    Logger.log.info(`Inside member update`);
                    if (context.activity.from.id.indexOf('agent') === -1) {
                        if (context.activity.from.handOffOn) {
                            let botUser = await BotUsers.findOne({ userId: context.activity.from.id }).select({
                                isHandOffGoingOn: 1,
                            });
                            Logger.log.info('Fetched user details to set the handoff status in directline object');
                            if (botUser.isHandOffGoingOn) {
                                Logger.log.info('Handoff status was already on.');
                                let objToSave = {};
                                objToSave.isHandOff = true;
                                try {
                                    await this.conversationInfoPropertyAccessor.set(context, objToSave);
                                } catch (e) {
                                    console.log(e);
                                    throw new Error(e);
                                }
                            } else {
                                Logger.log.info('Handoff status was set to off.');
                                await context.sendActivity({ text: 'end', value: 'handOffStatus' });
                            }
                        }
                        const conversationReference = TurnContext.getConversationReference(context.activity);
                        // let botuser = new BotUsers({
                        //     userId: context.activity.from.id,
                        //     conversationId: context.activity.conversation.id,
                        //     channel: context.activity.channelId,
                        //     address: conversationReference,
                        //     organizationId: context.activity.from.organizationId,
                        // });
                        // botuser
                        //     .save()
                        BotUsers.findOneAndUpdate(
                            { userId: context.activity.from.id },
                            {
                                userId: context.activity.from.id,
                                conversationId: context.activity.conversation.id,
                                channel: context.activity.channelId,
                                address: conversationReference,
                                organizationId: context.activity.from.organizationId || '5e44e3bf499fc022ce378206',
                                isConversionOver: false,
                            },
                            { new: true, upsert: true, setDefaultsOnInsert: true },
                        )
                            // botuser
                            //     .save()
                            .then((results) => {
                                Logger.log.info('New User Joined..');
                            })
                            .catch((err) => {
                                Logger.log.error('Unable to store new bot user.');
                                Logger.log.error(err.message || err);
                            });
                    } else {
                        const conversationReference = TurnContext.getConversationReference(context.activity);
                        await HandOffHelper.bindAgentAddressToHandOff({
                            agentId: context.activity.from.id,
                            agentAddress: conversationReference,
                        });
                    }
                }
            }

            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });
    }

    addConversationReference(activity) {
        const conversationReference = TurnContext.getConversationReference(activity);
        this.conversationReferences[conversationReference.conversation.id] = conversationReference;
    }
}

module.exports.DialogAndWelcomeBot = DialogAndWelcomeBot;
