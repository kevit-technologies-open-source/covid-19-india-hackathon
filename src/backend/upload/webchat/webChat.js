function HumanPixelConfig() {
    var baseUrl = "";
    var data='';
    this.defaultConfig = {
        loadScript: function (url, callback) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if (script.readyState) {
                //IE
                script.onreadystatechange = function () {
                    if (
                        script.readyState === "loaded" ||
                        script.readyState === "complete"
                    ) {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else {
                //Others
                script.onload = function () {
                    callback();
                };
            }
            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        },
        setConfigs: function () {
        },
        callForBasicConfig: function () {
            this.setConfigs();
        }
    };
    this.init = function () {
        var _this = this;
        try {
            if (!window.jQuery) {
                _this.defaultConfig.loadScript(
                    "https://code.jquery.com/jquery-1.12.4.min.js",
                    function () {
                        _this.defaultConfig.loadScript(
                            "https://html2canvas.hertzen.com/dist/html2canvas.js",
                            function () {
                        var myRequest = new XMLHttpRequest();
                                        myRequest.open('GET', baseUrl + "bot-widget/configuration?organizationId=" + window['organizationId']);
                                        myRequest.onreadystatechange = function () {
                                            if (myRequest.readyState === 4) {
                                                data = JSON.parse(myRequest.responseText);
                                                if (data.status === undefined) {
                                                    _this.defaultConfig.loadScript(
                                                        "https://static-serves.s3.ap-south-1.amazonaws.com/directline/directline.js",
                                                        function () {
                                                            _this.defaultConfig.callForBasicConfig();
                                                        }
                                                    );
                                                } else {
                                                    if (data.message) {
                                                        if (data.status !== 'bot_put_on_hold') {
                                                            alert(data.message);
                                                        }
                                                    }
                                                }
                                            }
                                        };
                                        myRequest.send();
                            });
                    });
            } else {
                _this.defaultConfig.loadScript(
                    "https://html2canvas.hertzen.com/dist/html2canvas.js",
                    function () {
                var myRequest = new XMLHttpRequest();
                myRequest.open('GET', baseUrl + "bot-widget/configuration?organizationId=" + window['organizationId']);
                myRequest.onreadystatechange = function () {
                                    // 4. check if the request has a readyState of 4, which indicates the server has responded (complete)
                                    if (myRequest.readyState === 4) {
                                        // 5. insert the text sent by the server into the HTML of the 'ajax-content'
                                        data = JSON.parse(myRequest.responseText);
                                        if (data.status === undefined) {
                                            _this.defaultConfig.loadScript(
                                                "https://static-serves.s3.ap-south-1.amazonaws.com/directline/directline.js",
                                                function () {
                                                    _this.defaultConfig.callForBasicConfig();
                                                }
                                            );
                                        } else {
                                            if (data.message) {
                                                if (data.status !== 'bot_put_on_hold') {
                                                    alert(data.message);
                                                }
                                            }
                                        }
                                    }
                                };
                myRequest.send();
                    });
            }
        } catch (e) {
            console.log(e);
        }
    };
    if(data.status === undefined){
        this.defaultConfig.setConfigs = function () {
            window.mobilecheck = function () {
                var check = false;
                (function (a) {
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
                })(navigator.userAgent || navigator.vendor || window.opera);
                return check;
            };
            var sBrowser, sUsrAg = navigator.userAgent;
            if (sUsrAg.indexOf('SamsungBrowser') > -1) {
                sBrowser = 'Samsung Internet';
                // "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G955F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36
            } else if (sUsrAg.indexOf('Opera') > -1 || sUsrAg.indexOf('OPR') > -1) {
                sBrowser = 'Opera';
                // "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106"
            } else if (sUsrAg.indexOf('Trident') > -1) {
                sBrowser = 'Microsoft Internet Explorer';
                // "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0; wbx 1.0.0; rv:11.0) like Gecko"
            } else if (sUsrAg.indexOf('Edge') > -1) {
                sBrowser = 'Microsoft Edge';
                // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299"
            } else if (sUsrAg.indexOf('Chrome') > -1) {
                sBrowser = 'Google Chrome';
                // "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36"
            } else if (sUsrAg.indexOf('Safari') > -1) {
                sBrowser = 'Apple Safari';
                // "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1 980x1306"
            } else if (sUsrAg.includes('Firefox') > -1) {
                sBrowser = 'Mozilla Firefox';
                // "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"
            } else {
                sBrowser = 'unknown';
            }
            var widgetConfig=data.widget;
            jQuery('body').append('<iframe id="'+data._id+'_HumanPixel" name="Human Pixel" data-html2canvas-ignore="true"  src="' +
                baseUrl + 'webchat/index.html" style="z-index: 9999999999;' +
                'border: transparent;position: fixed;bottom: '+
                ((window.mobilecheck() ? 0 : 50))+'px;'+widgetConfig.position+': '+( window.mobilecheck() ? 0+'px;': 50+'px;')+'"></iframe>');
            jQuery('head').append('<style>\n' +
                '               #'+data._id+'_HumanPixel {\n' +
                '                 transform-origin: bottom ' + (widgetConfig.position) + ';\n' +
                '                 ' + widgetConfig.position + ': ' + ((window.mobilecheck() ? 0 : 50)) + 'px;' +
                '               }\n' +
                '               </style>');

                jQuery('#' + data._id + '_HumanPixel').css('width', (75) + 'px');
                jQuery('#' + data._id + '_HumanPixel').css('height', (75) + 'px');
                jQuery('#'+data._id+'_HumanPixel').on('load', (function () {
                var iframeWin = document.getElementById(data._id+'_HumanPixel').contentWindow;
                iframeWin.postMessage(JSON.stringify({
                    Data: data,
                    isMobile: window.mobilecheck(),
                    baseUrl: baseUrl,
                    browser: sBrowser,
                    type: 'message',
                    userConfig:{
                        user:getUser(),
                        messages:getMessages(),
                        handOffStatus:getHandOffStatus(),
                        organizationId:window['organizationId']
                    },
                }), baseUrl + 'webchat/index.html');
                if (window.addEventListener) {
                    window.addEventListener('message', handleMessage);
                } else {
                    window.attachEvent('onmessage', handleMessage);
                }
                    jQuery(document).ready(function () {
                        var image=  "   .popup{\n" +
                            "               width: 900px;\n" +
                            "               margin: auto;\n" +
                            "               text-align: center\n" +
                            "           }\n" +
                            "           .popup img{\n" +
                            "               width: 200px;\n" +
                            "               height: 200px;\n" +
                            "               cursor: pointer\n" +
                            "           }\n" +
                            "           .overlay-preview-show{\n" +
                            "               z-index: 99999999999;\n" +
                            "               display: none;\n" +
                            "           }\n" +
                            "           .overlay-preview-show .overlay{\n" +
                            "               width: 100%;\n" +
                            "               height: 100%;\n" +
                            "               background: rgba(0,0,0,.66);\n" +
                            "               position: fixed;\n" +
                            "               top: 0;\n" +
                            "               z-index: 99999999999;\n" +
                            "               left: 0;\n" +
                            "               display: flex;\n" +
                            "               align-items: center;\n" +
                            "               justify-content: center;\n" +
                            "           }\n" +
                            "           .overlay-preview-show .img-show{\n" +
                            "               width: 80vw;\n" +
                            "               height: 80vh;\n" +
                            "               background: #FFF;\n" +
                            "               position: fixed;\n" +
                            "               top: 50%;\n" +
                            "               z-index: 99999999999;\n" +
                            "               left: 50%;\n" +
                            "               transform: translate(-50%,-50%);\n" +
                            "               overflow: hidden;\n" +
                            "           }\n" +
                            "           .img-show span{\n" +
                            "               position: absolute;\n" +
                            "               top: 10px;\n" +
                            "               right: 10px;\n" +
                            "               z-index: 99;\n" +
                            "               cursor: pointer;\n" +
                            "               color: white;\n" +
                            "               font-weight: 500;\n" +
                            "           }\n" +
                            "           .img-show img{\n" +
                            "               width: 100%;\n" +
                            "               height: 100%;\n" +
                            "               position: absolute;\n" +
                            "               top: 0;\n" +
                            "               left: 0;\n" +
                            "               z-index: 10;\n" +
                            "           }";
                        jQuery("head").append("<style>" + image + "</style>");
                        jQuery('body').append( '<div class="overlay-preview-show">\n' +
                            '  <div class="overlay"></div>\n' +
                            '  <div class="img-show">\n' +
                            '    <span>X</span>\n' +
                            '    <img src="">\n' +
                            '  </div>\n' +
                            '</div><div id="human-pixel-first-screen" class="human-pixel-first-screen">');
                        // close image preview
                        jQuery("span, .overlay").click(function () {
                            jQuery(".overlay-preview-show").fadeOut('fast');
                        });
                    });
                /**
                 * Handle a message that was sent from some window.
                 *
                 * @param {MessageEvent} event The MessageEvent object holding the message/data.
                 */
                function handleMessage(event) {
                    var messageFromSender = event.data;
                    if (messageFromSender.type === 'isBotOpen') {
                        if(messageFromSender.isBotOpen) {
                            if (window.mobilecheck()) {
                                setTimeout(function () {
                                    jQuery('#' + data._id + '_HumanPixel').css('height', '100%');
                                    jQuery('#' + data._id + '_HumanPixel').css('width', '100vw');
                                },0);

                            } else {
                                setTimeout(function () {
                                    jQuery('#' + data._id + '_HumanPixel').css('width', (221 + (widgetConfig.size * 42)) + 'px');
                                    jQuery('#' + data._id + '_HumanPixel').css('height', (320 + (widgetConfig.size * 60)) + 'px');
                                },0);

                            }
                        }else {
                            setTimeout(function () {
                                jQuery('#'+data._id+'_HumanPixel').css('height','75px');
                                jQuery('#'+data._id+'_HumanPixel').css('width','75px');
                            },500);

                        }
                    }
                    if(messageFromSender.type === 'isFormBotOpen'){
                        if (window.mobilecheck()) {
                            setTimeout(function () {
                                jQuery('#' + data._id + '_HumanPixel').css('height', '100%');
                                jQuery('#' + data._id + '_HumanPixel').css('width', '100vw');
                            },0);

                        } else {
                            setTimeout(function(){
                                jQuery('#' + data._id + '_HumanPixel').css('width', (221 + (widgetConfig.size * 42)) + 'px');
                                jQuery('#' + data._id + '_HumanPixel').css('height', (190.4 + (messageFromSender.isFormBotOpen * 90.6)) + 'px');
                            },0);

                        }

                    }
                    if (messageFromSender.type === 'handOffStatus') {
                        storeHandOffStatus(messageFromSender.handOffStatus);
                    }
                    if (messageFromSender.type === 'messages') {
                        storeMessages(messageFromSender.messages);
                    }
                    if (messageFromSender.type === 'user') {
                        storeUser(messageFromSender.user);
                    }
                    if (messageFromSender.type === 'removeStorage') {
                        localStorage.removeItem(data._id+'_HumanPixel_user');
                        localStorage.removeItem(data._id+'_HumanPixel_status');
                        localStorage.removeItem(data._id+'_HumanPixel_messages');
                    }
                    if (messageFromSender.type === 'takeScreenShot') {
                        takeScreenShot();
                    }
                    if(messageFromSender.type==='previewImage'){
                        previewImage(messageFromSender.previewImage);
                    }
                }
                function takeScreenShot() {
                    html2canvas(document.querySelector("body"),
                        {
                            y: 0,
                            useCORS:true,
                        }).then(function(canvas) {
                        var tempCanvas = document.createElement("canvas"),
                            tCtx = tempCanvas.getContext("2d");

                        tempCanvas.width = window.innerWidth;
                        tempCanvas.height = window.innerHeight;

                        tCtx.drawImage(canvas,window.pageXOffset,window.pageYOffset,window.innerWidth,window.innerHeight,0,0,window.innerWidth,window.innerHeight);

                        // write on screen
                        // var img = tempCanvas.toDataURL("image/png");
                        // var link = document.createElement("a");
                        // document.body.appendChild(link);
                        // link.download = "html_image.png";
                        // link.href = canvas.toDataURL('image/png', 1.0);
                        // link.target = '_blank';
                        // link.click();
                        // _this.uploadScreenshot(tempCanvas.toDataURL("image/png", 1.0),value);
                        var iframeWin = document.getElementById(data._id+'_HumanPixel').contentWindow;
                        iframeWin.postMessage(JSON.stringify({
                            screenShot:tempCanvas.toDataURL("image/png",1.0),
                            type: 'screenShot',
                        }), baseUrl + 'webchat/index.html');
                    });
                }
                function previewImage(image) {
                   jQuery(".img-show img").attr("src", image);
                    jQuery(".overlay-preview-show").fadeIn("fast");
                }
                function storeMessages(messages) {
                    localStorage.setItem(data._id+'_HumanPixel_messages',JSON.stringify(messages));
                }
                function getMessages() {
                    if(localStorage.getItem(data._id+'_HumanPixel_messages') !== undefined){
                        return JSON.parse(localStorage.getItem(data._id+'_HumanPixel_messages'));
                    }else {
                        return '';
                    }

                }
                function storeHandOffStatus(status) {
                    localStorage.setItem(data._id+'_HumanPixel_status',status);
                }
                function getHandOffStatus() {
                    if(localStorage.getItem(data._id+'_HumanPixel_status') !== undefined){
                        return JSON.parse(localStorage.getItem(data._id+'_HumanPixel_status'));
                    }else {
                        return '';
                    }

                }
                function storeUser(user) {
                    localStorage.setItem(data._id+'_HumanPixel_user',JSON.stringify(user));
                }
                function getUser() {
                    if(localStorage.getItem(data._id+'_HumanPixel_user') !== undefined){
                        return JSON.parse(localStorage.getItem(data._id+'_HumanPixel_user'));
                    }else {
                        return '';
                    }

                }
            }));
        };
    }else {
        if(data.message){
            alert(data.message);
        }
    }
}
var start = new HumanPixelConfig();
start.init();


