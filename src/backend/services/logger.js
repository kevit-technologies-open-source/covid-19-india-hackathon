const log4js = require('log4js');
const morgan = require('morgan');
/**
 * Config
 * */
const config = require('../config');

/**
 * Declarations & Implementations
 */

log4js.configure({
    appenders: {
        out: { type: 'stdout' },
        allLogs: { type: 'file', filename: 'all.log', maxLogSize: 10485760, backups: 10, compress: true },
        outFilter: {
            type: 'logLevelFilter',
            appender: 'out',
            level: config.server.logLevel || 'all',
        },
       
    },
    categories: { default: { appenders: ['allLogs', 'outFilter'], level: 'all' } },
});

let log = log4js.getLogger();
let morganInstance = morgan('dev', {
    stream: {
        write: (str) => {
            log.debug(str);
        },
    },
});
/**
 * Service Export
 */
module.exports = {
    log: log,
    morgan: morganInstance,
};
