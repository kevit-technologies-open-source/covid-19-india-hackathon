const Logger = require('./logger');
const NlpHelper = require('./../helper/nlp.helper');

let scheduleCrons = () => {
    setTimeout(() => {
        Logger.log.trace(
            'Generating the dialogflow token after 10 seconds of server start at:',
            new Date().toISOString(),
        );
        NlpHelper.cronForUpdatingToken();
    }, 1000 * 10);

    //Cron will run after every 55 mins
    setInterval(() => {
        NlpHelper.cronForUpdatingToken();
    }, 1000 * 60 * 55);
};

module.exports = {
    scheduleCrons: scheduleCrons,
};
