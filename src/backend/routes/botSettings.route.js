/**
 * 3rd Party APIs and Models
 * */
const express = require('express');
const router = express.Router();
const parseDomain = require('parse-domain');
let mongoose = require('mongoose');
// let Organization = mongoose.model('organization');
let Bot = mongoose.model('bot');
const Organization = mongoose.model('organization');
/**
 * Config
 * */
const config = require('../config');
/**
 * Services
 * */
const Logger = require('./../services/logger');
/**
 * Helper
 * */
const TimezoneHelper = require('./../helper/timezone.helper');

/**
 * Gets the message configuration
 */
router.get('/messages', function(req, res) {
    Logger.log.info('In get messages call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOne({
            organizationId: req.query.organizationId,
            isArchived: false,
        })
            .select({ messages: 1 })
            .exec()
            .then((botData) => {
                Logger.log.info('Fetched configurable messages successfully');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error fetching configurable messages.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error fetching configurable messages.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Updates the message Configuration
 */
router.put('/messages', function(req, res) {
    Logger.log.info('In update configurable messages call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        let updateObj = {};
        if (req.body.messages.handOffStart) updateObj['messages.handOffStart'] = req.body.messages.handOffStart;
        if (req.body.messages.handOffTransfer)
            updateObj['messages.handOffTransfer'] = req.body.messages.handOffTransfer;
        if (req.body.messages.handOffEnd) updateObj['messages.handOffEnd'] = req.body.messages.handOffEnd;
        if (req.body.messages.handOffUnavailable)
            updateObj['messages.handOffUnavailable'] = req.body.messages.handOffUnavailable;
        if (req.body.messages.handOffShutDown)
            updateObj['messages.handOffShutDown'] = req.body.messages.handOffShutDown;
        if (req.body.messages.publicHoliday) updateObj['messages.publicHoliday'] = req.body.messages.publicHoliday;
        if (req.body.messages.putOnHold) updateObj['messages.putOnHold'] = req.body.messages.putOnHold;
        if (req.body.messages.fiveMinMessage) updateObj['messages.fiveMinMessage'] = req.body.messages.fiveMinMessage;
        Bot.findOneAndUpdate(
            {
                organizationId: req.query.organizationId,
                isArchived: false,
            },
            updateObj,
            { new: true },
        )
            .select({
                messages: 1,
            })
            .exec()
            .then((botData) => {
                Logger.log.info('Updated configurable messages successfully.');
                res.status(200).send({
                    messages: botData.messages,
                    message: 'Configurable messages updated successfully.',
                });
            })
            .catch((err) => {
                Logger.log.error('Error updating configurable messages.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error updating configurable messages.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Gets the integration configuration
 */
router.get('/integration', function(req, res) {
    Logger.log.info('In get integration configuration call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOne({
            organizationId: req.query.organizationId,
            isArchived: false,
        })
            .select({ siteUrls: 1, botScript: 1 })
            .exec()
            .then((botData) => {
                Logger.log.info('Fetched integration configuration successfully');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error fetching integration configuration.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error fetching integration configuration.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Updates the integration configuration
 */
router.put('/integration', function(req, res) {
    Logger.log.info('In update integration configuration call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        let siteUrls = req.body.siteUrls;
        let urlDomains = [];
        let invalidUrls = [];
        siteUrls.forEach((eachUrl) => {
            let extractedDomain = parseDomain(eachUrl);
            let extractedUrl = '';
            if (extractedDomain) {
                if (extractedDomain.domain) extractedUrl += extractedDomain.domain;
                if (extractedDomain.subdomain) extractedUrl = extractedDomain.subdomain + '.' + extractedUrl;
                if (extractedDomain.tld) extractedUrl = extractedUrl + '.' + extractedDomain.tld;
                urlDomains.push(extractedUrl);
            } else {
                invalidUrls.push(eachUrl);
            }
        });
        if (invalidUrls.length > 0) {
            Logger.log.info('Some invalid urls identified for the organization:', req.query.organizationId);
            Logger.log.info(invalidUrls);
            return res.status(400).send({
                status: 'invalid_url_present',
                urlConflicts: invalidUrls,
                message: 'Invalid Url(s) found.',
            });
        }
        Bot.find({
            isArchived: false,
            organizationId: { $ne: req.query.organizationId },
            siteUrls: { $in: urlDomains },
        })
            .select({ siteUrls: 1 })
            .exec()
            .then((otherBots) => {
                let urlConflicts = [];
                Logger.log.info('Fetched other organization details for checking site url conflicts.');
                if (otherBots.length > 0) {
                    for (let i = 0; i < urlDomains.length; i++) {
                        otherBots.forEach((bot) => {
                            if (bot.siteUrls.indexOf(urlDomains[i]) !== -1) urlConflicts.push(siteUrls[i]);
                        });
                    }
                    Logger.log.info('Other organization having same site url.', urlConflicts);
                    return res.status(400).send({
                        status: 'url_present_with_other_organization',
                        urlConflicts: urlConflicts,
                        message: 'On highlighted URLs other bot is also configured.',
                    });
                }
                Bot.findOneAndUpdate(
                    {
                        organizationId: req.query.organizationId,
                        isArchived: false,
                    },
                    { siteUrls: urlDomains },
                    { new: true },
                )
                    .select({
                        siteUrls: 1,
                        botScript: 1,
                    })
                    .exec()
                    .then((botData) => {
                        Logger.log.info('Updated configurable messages successfully.');
                        res.status(200).send({
                            siteUrls: botData.siteUrls,
                            message: 'Integration settings updated successfully.',
                        });
                    })
                    .catch((err) => {
                        Logger.log.error('Error updating configurable messages.');
                        Logger.log.error(err.message || err);
                        return res.status(500).send({ message: 'Error updating configurable messages.' });
                    });
            })
            .catch((err) => {
                Logger.log.error('Error fetching other organization for updating urls.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error updating configurable messages.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Gets the general settings
 */
router.get('/general', function(req, res) {
    Logger.log.info('In get general settings call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOne({
            organizationId: req.query.organizationId,
            isArchived: false,
        })
            .select({ botName: 1, putOnHold: 1 })
            .populate({
                path: 'organizationId',
                select: { originAdminId: 0, isArchived: 0, name: 0, createdAt: 0, updatedAt: 0 },
            })
            .exec()
            .then((results) => {
                let responseObj = {
                    bot: {
                        botName: results.botName,
                        putOnHold: results.putOnHold,
                    },
                    organization: results.organizationId,
                };
                Logger.log.info('Fetched general settings successfully');
                res.status(200).send(responseObj);
            })
            .catch((err) => {
                Logger.log.error('Error fetching general settings.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error fetching general settings.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Updates the general Settings
 */
router.put('/general', function(req, res) {
    Logger.log.info('In update general settings call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        let promiseArr = [];
        if (req.body.bot) {
            promiseArr.push(
                Bot.findOneAndUpdate(
                    {
                        organizationId: req.query.organizationId,
                        isArchived: false,
                    },
                    req.body.bot,
                    { new: true },
                ),
            );
        }
        if (req.body.organization) {
            let updateOrganization = req.body.organization;
            updateOrganization.timezone.val = TimezoneHelper.extractValue(updateOrganization.timezone.str);
            promiseArr.push(
                Organization.findByIdAndUpdate(req.query.organizationId, updateOrganization, { new: true }),
            );
        }
        Promise.all(promiseArr)
            .then((results) => {
                Logger.log.info('General Settings updated successfully.');
                res.status(200).send({ message: 'General Settings updated successfully.' });
            })
            .catch((err) => {
                Logger.log.error('Error updating general settings.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error updating general settings.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Export Router
 */
module.exports = router;
