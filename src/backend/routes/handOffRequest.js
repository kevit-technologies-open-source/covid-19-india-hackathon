const express = require('express');
const router = express.Router();
const multer = require('multer');
const fs = require('fs');
const path = require('path');
let mongoose = require('mongoose');
let BotUser = mongoose.model('user-botuser');
let Bot = mongoose.model('bot');
let HandOff = mongoose.model('handoff');
const UserAgent = mongoose.model('user-agent');
const BotUserTranscript = mongoose.model('botuser-transcript');

/**
 * Helpers
 * */
let TranscriptHelper = require('./../helper/transcriptHelper');
let HandOffHelper = require('./../helper/handoff.helper');
const FirebaseHelper = require('./../helper/firebaseNotification');
const NlpHelper = require('./../helper/nlp.helper');
/**
 * Config
 * */
const config = require('../config');
/**
 * Services
 * */
const Logger = require('./../services/logger');

const uploadFilePath = path.resolve(__dirname, '../upload/' + getFilePath());
//Custom Multer storage engine
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, uploadFilePath);
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
    },
});
const upload = multer({ dest: uploadFilePath, storage: storage });

/**
 * Get the status availability of user
 */
router.get('/checkAvailability/:handOffId', function(req, res) {
    let handOffId = req.params.handOffId;
    let agentId = req.user._id;
    if (!mongoose.Types.ObjectId.isValid(handOffId)) {
        Logger.log.error('Handoff id not passed');
        return res.status(400).send({ message: 'Handoff id not passed.' });
    }
    if (!mongoose.Types.ObjectId.isValid(agentId)) {
        Logger.log.error('User id not present');
        return res.status(400).send({ message: 'User id not present, please login again and try.' });
    }
    HandOff.findById(handOffId)
        .then((handOffData) => {
            if (!handOffData) {
                Logger.log.error('Handoff details not found');
                return res.status(500).send({ message: 'Handoff details not found.' });
            }
            Logger.log.info('Transcript received successfully.');
            let dataToSend = {};
            if (
                handOffData.isWaiting === false &&
                (!handOffData.newAgentId || handOffData.newAgentId.toString() !== agentId.toString())
            ) {
                Logger.log.info('Chat is already on going with other agent.');
                dataToSend = {
                    isAlreadyAssigned: true,
                    userId: handOffData.botUserId,
                    handOffId: handOffData._id,
                    agentAssigned: handOffData.agentAssigned[handOffData.agentAssigned.length - 1],
                };
                res.status(200).send(dataToSend);
            } else if (
                handOffData.isWaiting === false &&
                handOffData.newAgentId &&
                handOffData.newAgentId.toString() === agentId.toString()
            ) {
                let promiseArr = [];
                let oldAgent = handOffData.agentAssigned[handOffData.agentAssigned.length - 1];
                handOffData.newAgentId = null;
                handOffData.agentAssigned.push(agentId);
                promiseArr.push(handOffData.save());
                promiseArr.push(BotUser.findOne({ userId: handOffData.botUserId }));
                promiseArr.push(Bot.findOne({ organizationId: handOffData.organizationId }));
                promiseArr.push(UserAgent.findById(oldAgent));
                Promise.all(promiseArr)
                    .then((results) => {
                        let updatedHandOffData = results[0];
                        let botUserData = results[1];
                        let agentToAgentMessage =
                            results[2].messages.handOffTransfer ||
                            'Current communicating agent transferred the chat to other agent.';
                        Logger.log.info('Assigning the chat to the agent.', updatedHandOffData);
                        dataToSend = {
                            isAlreadyAssigned: false,
                            userId: handOffData.botUserId,
                            handOffId: handOffData._id,
                            agentAssigned: handOffData.agentAssigned[handOffData.agentAssigned.length - 1],
                        };
                        res.status(200).send(dataToSend);
                        let newPromiseArr = [];
                        if (
                            agentToAgentMessage.indexOf('agent-name') !== -1 ||
                            agentToAgentMessage.indexOf('old-agent-name') !== -1
                        ) {
                            let dynamicFields = {
                                'agent-name': req.user && req.user.name ? req.user.name : 'Another Agent',
                                'old-agent-name': results[3] && results[3].name ? results[3].name : 'Old Agent',
                            };
                            agentToAgentMessage = HandOffHelper.resolveDynamicMessage(
                                agentToAgentMessage,
                                dynamicFields,
                            );
                        }
                        let configMessageObj = {
                            text: agentToAgentMessage,
                            value: 'detailText',
                            inputHint: botUserData.userId,
                        };
                        newPromiseArr.push(
                            HandOffHelper.configMessageToUser({
                                userAddress: botUserData.address,
                                message: configMessageObj,
                            }),
                        );
                        let acceptanceMessage = `Request assigned by you is accepted by ${
                            req.user.name ? req.user.name : 'agent'
                        }`;
                        newPromiseArr.push(
                            HandOffHelper.notifyAcceptRequestToOldAgent({
                                agentId: oldAgent,
                                handOffId: handOffId,
                                acceptanceMessage: acceptanceMessage,
                            }),
                        );
                        Promise.all(newPromiseArr)
                            .then((results) => {
                                Logger.log.info('Notification for acceptance by a user sent successfully.');
                            })
                            .catch((err) => {
                                Logger.log.error('Unable to send notification for acceptance by a user.');
                            });
                    })
                    .catch((err) => {
                        Logger.log.error('Unable to update Handoff details');
                        return res.status(500).send({ message: 'Something went wrong, please try again later.' });
                    });
            } else {
                handOffData.isWaiting = false;
                handOffData.agentAssigned.push(agentId);
                handOffData
                    .save()
                    .then((updatedHandOffData) => {
                        Logger.log.info('Assigning the chat to the agent.', updatedHandOffData);
                        dataToSend = {
                            isAlreadyAssigned: false,
                            userId: handOffData.botUserId,
                            handOffId: handOffData._id,
                            agentAssigned: handOffData.agentAssigned[handOffData.agentAssigned.length - 1],
                        };
                        res.status(200).send(dataToSend);
                        HandOffHelper.notifyAcceptRequestToOtherUsers({
                            agentId: agentId,
                            handOffId: handOffId,
                            organizationId: handOffData.organizationId,
                        })
                            .then((results) => {
                                Logger.log.info('Notification for acceptance by a user sent successfully.');
                            })
                            .catch((err) => {
                                Logger.log.error('Unable to send notification for acceptance by a user.');
                                Logger.log.error(err);
                            });
                    })
                    .catch((err) => {
                        Logger.log.error('Unable to update Handoff details');
                        Logger.log.error(err);
                        return res.status(500).send({ message: 'Something went wrong, please try again later.' });
                    });
            }
        })
        .catch((err) => {
            Logger.log.error('Handoff details not found');
            Logger.log.error(err);
            return res.status(500).send({ message: 'Something went wrong, please try again later.' });
        });
});

/**
 * Assigns user's request to another agent
 */
router.put('/assignNewAgent/:handOffId', function(req, res) {
    let handOffId = req.params.handOffId;
    let agentId = req.user._id;
    let newAgentId = req.query.newAgentId;
    if (!mongoose.Types.ObjectId.isValid(handOffId)) {
        Logger.log.error('Handoff id not passed');
        return res.status(400).send({ message: 'Handoff id not passed.' });
    }
    if (!mongoose.Types.ObjectId.isValid(agentId)) {
        Logger.log.error('User id not present');
        return res.status(400).send({ message: 'User id not present, please login again and try.' });
    }
    if (!mongoose.Types.ObjectId.isValid(newAgentId)) {
        Logger.log.error('New agent id not present in the request query params for handoff id:', handOffId);
        return res.status(400).send({ message: 'New agent id not present in the request.' });
    }
    HandOff.findByIdAndUpdate(handOffId, { newAgentId: newAgentId }, { new: true })
        .populate({
            select: ['name'],
            path: 'agentAssigned',
        })
        .populate({
            select: ['fcmToken', 'name'],
            path: 'newAgentId',
        })

        .then((handOffData) => {
            if (!handOffData) {
                Logger.log.error('Handoff details not found');
                return res.status(500).send({ message: 'Handoff details not found.' });
            }

            if (handOffData.newAgentId && handOffData.newAgentId.fcmToken.length > 0) {
                BotUser.findOne({ userId: handOffData.botUserId })
                    .then((userData) => {
                        let userObj = JSON.parse(JSON.stringify(userData));
                        userObj.handOffId = handOffData._id;
                        let notifyNewAgent = {
                            userData: userObj,
                            isWaiting: true,
                        };
                        notifyNewAgent.userData.isAssignedByOtherAgent = true;
                        notifyNewAgent.userData.oldAgentName =
                            handOffData.agentAssigned &&
                            handOffData.agentAssigned.length > 0 &&
                            handOffData.agentAssigned[handOffData.agentAssigned.length - 1].name
                                ? handOffData.agentAssigned[handOffData.agentAssigned.length - 1].name
                                : null;
                        FirebaseHelper.sendNotification(handOffData.newAgentId.fcmToken, notifyNewAgent).then(
                            (notificationResult) => {
                                Logger.log.info('Firebase notification to new user sent successfully.');
                            },
                        );
                    })
                    .catch((err) => {
                        Logger.log.error('Bot User details not found');
                        Logger.log.error(err);
                        return res.status(500).send({ message: 'Something went wrong, please try again later.' });
                    });
            }
            let newAgentName = handOffData.newAgentId.name ? handOffData.newAgentId.name : 'new agent';
            res.status(200).send({
                message: `Request sent to ${newAgentName}. Until he/she accepts, you can continue with the chat.`,
            });
        })
        .catch((err) => {
            Logger.log.error('Handoff details not found');
            Logger.log.error(err);
            return res.status(500).send({ message: 'Something went wrong, please try again later.' });
        });
});

/**
 * Get intent list
 */
router.get('/getIntents', function(req, res) {
    let organizationId = req.query.organizationId;
    if (!mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization id not passed');
        return res.status(400).send({ message: 'Organisation id not passed.' });
    }
    NlpHelper.getIntentList('en', organizationId)

        .then((intentList) => {
            if (!intentList) {
                Logger.log.error('Intent list not found');
                return res.status(500).send({ message: 'Handoff details not found.' });
            }
            let intentNames = [];
            Logger.log.info('Intent List fetched successfully.');
            intentList.forEach((intent) =>
                intentNames.push({ displayName: intent.displayName, intentId: intent.name.split('/').pop() }),
            );
            res.status(200).send(intentNames);
        })
        .catch(() => {
            Logger.log.error('Error fetching intent list');
            return res.status(500).send({ message: 'Error fetching intent list, please try again later.' });
        });
});

/**
 * Get agent list
 */
router.get('/getAgents', function(req, res) {
    let organizationId = req.query.organizationId;
    if (!mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization id not passed');
        return res.status(400).send({ message: 'Organisation id not passed.' });
    }
    let userId = req.user._id;
    UserAgent.find({ organizationId, isArchived: false, _id: { $nin: userId } })
        .select({ name: 1, email: 1 })
        .then((results) => {
            Logger.log.info('Agent List fetched successfully for assigning to another agent.');
            // console.log(results);
            res.status(200).send(results);
        })
        .catch((err) => {
            Logger.log.error('Error getting list of users.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error getting list of agents.' });
        });
});

/**
 * Upload fle by Agent call
 */
router.post('/upload/file', upload.single('file'), function(req, res) {
    if (req.file.filename) {
        res.status(200).send({ url: getFileUrl(req.file.filename) });
    } else {
        Logger.log.error('Error uploading file.');
        res.status(500).send({ message: 'Error uploading file.' });
    }
});

/**
 * Initiate Handoff by Agent
 */
router.get('/initiate-handoff/:botUserId', function(req, res) {
    let botUserId = req.params.botUserId;
    let organizationId = req.query.organizationId;
    let agentId = req.user._id;
    if (!botUserId) {
        Logger.log.error('Bot User Id not passed');
        return res.status(400).send({ message: 'Bot User id not passed.' });
    }
    if (!mongoose.Types.ObjectId.isValid(agentId)) {
        Logger.log.error('User id not present');
        return res.status(400).send({ message: 'User id not present, please login again and try.' });
    }
    if (!mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization Id not passed');
        return res.status(400).send({ message: 'Organisation id not passed.' });
    }
    let promiseArr = [];
    promiseArr.push(
        BotUser.findOneAndUpdate(
            { userId: botUserId },
            { isAppliedForHandOff: true, isHandOffGoingOn: true },
            { new: true },
        ),
    );
    let handOffObject = {
        botUserId: botUserId,
        organizationId: organizationId || '5e3d6cd6f28d02155c8cfe45',
        isWaiting: false,
        agentAssigned: [agentId],
        initiatedByAgent: true,
        requestArrival: 'inWorkingHours',
    };
    promiseArr.push(HandOff.create(handOffObject));
    promiseArr.push(Bot.findOne({ organizationId: organizationId }));
    Promise.all(promiseArr)
        .then((results) => {
            let userData = results[0];
            let handOffData = results[1];
            let initiationMessage =
                results[2] && results[2].messages && results[2].messages.handOffStart
                    ? results[2].messages.handOffStart
                    : 'You are now communicating with our Live Agent.';
            if (!userData) {
                Logger.log.error('No user data found with the given user id.');
                return reject();
            }
            res.status(200).send(handOffData);
            let newPromiseArr = [];
            let configMessageObj = {
                text: initiationMessage,
                value: 'detailText',
                inputHint: userData.userId,
            };
            newPromiseArr.push(
                HandOffHelper.configMessageToUser({
                    userAddress: userData.address,
                    message: configMessageObj,
                }),
            );
            newPromiseArr.push(
                HandOffHelper.notifyAcceptRequestToOtherUsers({
                    agentId: agentId,
                    handOffObj: handOffData,
                    handOffId: handOffData._id,
                    organizationId: handOffData.organizationId,
                }),
            );
            Promise.all(newPromiseArr)
                .then((results) => {
                    Logger.log.info('Notification for opening a chat by agent sent successfully.');
                })
                .catch((err) => {
                    Logger.log.error('Unable to send notification for opening a chat by agent.');
                });
        })
        .catch((err) => {
            Logger.log.error('Unable to update Handoff details');
            return res.status(500).send({ message: 'Something went wrong, please try again later.' });
        });
});

/**
 * Helper Functions
 */
function getFilePath() {
    return config.uploadLocations.media.base + config.uploadLocations.media.agentFile;
}

function getFileUrl(fileName) {
    if (fileName)
        if (fileName.indexOf(config.BaseUrl + getFilePath()) !== -1) return fileName;
        else return config.BaseUrl + getFilePath() + fileName;
    return '';
}

/**
 * Export Router
 */
module.exports = router;
