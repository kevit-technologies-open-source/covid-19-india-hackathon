const express = require('express');
const router = express.Router();
let mongoose = require('mongoose');
let BotUser = mongoose.model('user-botuser');
let BotUserTranscript = mongoose.model('botuser-transcript');
let HandOff = mongoose.model('handoff');

const Logger = require('./../services/logger');

/**
 * Get the List of Bot Users
 */
router.get('/', function(req, res) {
    BotUser.find({})
        .lean()
        .then((userDetails) => {
            res.send(userDetails);
        })
        .catch((err) => {
            Logger.log.error('Error in getting list of users');
            Logger.log.error(err.message || err);
        });
});

/**
 * Get the list of users based on the status
 */
router.get('/status/:status/:subStatus', function(req, res) {
    let status = req.params.status;
    let subStatus = req.params.subStatus;
    let organizationId = req.query.organizationId;
    let page = parseInt(req.query.page) || 1;
    let limit = parseInt(req.query.limit) || 20;
    let searchValue = req.query.search;
    Logger.log.info(`In the call to get users with status: ${status} and sub-status: ${subStatus}`);
    if (!status) {
        Logger.log.error('No status retrieved in list users call');
        return res.status(400).send({ message: 'No status retrieved in the list users call.' });
    }
    if (!subStatus) {
        Logger.log.error('No sub status retrieved in list users call');
        return res.status(400).send({ message: 'No sub status retrieved in the list users call.' });
    }
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('No organization id found in the request.');
        return res.status(400).send({ message: 'No organisation id found in the request.' });
    }
    if (status === 'chatWithBot') {
        let promiseArr = [];
        if (subStatus === 'neverApplied')
            promiseArr.push(
                getNeverAppliedForHandOffUsers({
                    organizationId,
                    page,
                    limit,
                    searchBy: 'name',
                    searchValue,
                }),
            );
        else if (subStatus === 'applied')
            promiseArr.push(getHandOffFinishedUsers({ organizationId, page, limit, searchBy: 'name', searchValue }));
        Promise.all(promiseArr)
            .then((results) => {
                // results[0].sort((a, b) => (a.updatedAt < b.updatedAt ? 1 : b.updatedAt < a.updatedAt ? -1 : 0));
                res.status(200).send(results[0]);
            })
            .catch((err) => {
                Logger.log.error('Unable to fetch the list of users.', err.message || err);
                res.status(500).send({ message: 'Unable to fetch the list of users.' });
            });
    } else if (status === 'chatWithAgent') {
        getChatWithAgent({
            organizationId,
            subStatus,
            userId: req.user._id,
            page,
            limit,
            searchBy: 'name',
            searchValue,
        })
            .then((pendingUsers) => {
                Logger.log.info('List of pending users generated.', pendingUsers.length);
                // pendingUsers.sort((a, b) => (a.updatedAt < b.updatedAt ? 1 : b.updatedAt < a.updatedAt ? -1 : 0));
                res.status(200).send(pendingUsers);
            })
            .catch((err) => {
                Logger.log.error('Unable to fetch the list of users.', err.message || err);
                res.status(500).send({ message: 'Unable to fetch the list of users.' });
            });
    } else {
        Logger.log.error('Invalid value of status sent.');
        res.status(400).send({ message: 'Invalid value of status sent.' });
    }
});

/**
 * Returns list of users who have never applied for Handoff
 * */
let getNeverAppliedForHandOffUsers = ({ organizationId, page, limit, searchBy, searchValue }) => {
    return new Promise((resolve, reject) => {
        Logger.log.info('getNeverAppliedForHandOffUsers function called.', typeof organizationId);
        let queryFilter = {
            isAppliedForHandOff: false,
            organizationId: mongoose.Types.ObjectId(organizationId),
        };
        if (searchValue) queryFilter[searchBy] = { $regex: searchValue, $options: 'i' };
        let option = {
            page: page,
            limit: limit,
        };
        option.sort = { updatedAt: 'desc' };
        BotUser.paginate(queryFilter, option)
            // BotUser.find({isAppliedForHandOff: false, organizationId: mongoose.Types.ObjectId(organizationId)})
            .then((neverAppliedForHandOffUsers) => {
                Logger.log.info('Users retrieved who have never applied for handoff.');
                return resolve(neverAppliedForHandOffUsers.docs);
            })
            .catch((err) => {
                Logger.log.error('Error fetching the users who have never applied for HandOff.', err.message || err);
                return reject();
            });
    });
};

/**
 * Returns list of users who have whose Handoff is finished
 * */
let getHandOffFinishedUsers = ({ organizationId, page, limit, searchBy, searchValue }) => {
    return new Promise((resolve, reject) => {
        Logger.log.info('getHandOffFinishedUsers function called.');
        let queryFilter = {
            organizationId: mongoose.Types.ObjectId(organizationId),
            isAppliedForHandOff: true,
            isHandOffGoingOn: false,
        };
        if (searchValue) queryFilter[searchBy] = { $regex: searchValue, $options: 'i' };
        let option = {
            page: page,
            limit: limit,
        };
        option.sort = { updatedAt: 'desc' };
        BotUser.paginate(queryFilter, option)
            .then((botUsers) => {
                Logger.log.info('BotUser data retrieved');
                return resolve(botUsers.docs);
            })
            .catch((err) => {
                Logger.log.error('Error fetching the users who have finished HandOff.', err.message || err);
                return reject();
            });
    });
};

/**
 * Returns list of users chatting with Agent
 * */
let getChatWithAgent = ({ organizationId, subStatus, userId, page, limit, searchBy, searchValue }) => {
    return new Promise((resolve, reject) => {
        Logger.log.info('getChatWithAgent function called.');
        let alreadyAssignedUserId = [];
        let assignedToMeUserId = [];
        let notAssignedUserId = [];
        let handOffMap;
        let promiseArr = [];
        switch (subStatus) {
            case 'myRequest':
                promiseArr.push(
                    HandOff.aggregate([
                        {
                            $project: {
                                _id: 1,
                                isWaiting: 1,
                                isFinished: 1,
                                fcmToken: 1,
                                botUserId: 1,
                                organizationId: 1,
                                agentAssigned: { $slice: ['$agentAssigned', -1] },
                            },
                        },
                        {
                            $match: {
                                isWaiting: false,
                                isFinished: false,
                                organizationId: mongoose.Types.ObjectId(organizationId),
                                agentAssigned: { $eq: userId },
                            },
                        },
                    ]),
                );

                break;
            case 'newRequest':
                promiseArr.push(
                    HandOff.find({
                        isFinished: false,
                        isWaiting: true,
                        requestArrival: { $in: ['inWorkingHours', 'outOfWorkingHours'] },
                        organizationId: mongoose.Types.ObjectId(organizationId),
                    }),
                );
                promiseArr.push(
                    HandOff.find({
                        isFinished: false,
                        isWaiting: false,
                        newAgentId: userId,
                        organizationId: mongoose.Types.ObjectId(organizationId),
                    }).populate({
                        path: 'agentAssigned',
                        select: ['name'],
                    }),
                );
                break;
            case 'othersRequest':
                promiseArr.push(
                    HandOff.aggregate([
                        {
                            $project: {
                                _id: 1,
                                isWaiting: 1,
                                isFinished: 1,
                                fcmToken: 1,
                                botUserId: 1,
                                organizationId: 1,
                                agentAssigned: { $slice: ['$agentAssigned', -1] },
                            },
                        },
                        {
                            $match: {
                                isWaiting: false,
                                isFinished: false,
                                organizationId: mongoose.Types.ObjectId(organizationId),
                                agentAssigned: { $ne: userId },
                            },
                        },
                    ]),
                );
                break;
        }
        Promise.all(promiseArr)
            .then((handOffResults) => {
                let newPromiseArr = [];
                if (subStatus === 'newRequest') {
                    handOffResults[0].forEach((handOff) => {
                        notAssignedUserId.push(handOff.botUserId);
                    });
                    handOffMap = handOffResults[0].reduce(function(map, handOff) {
                        map[handOff.botUserId] = {
                            handOffId: handOff._id,
                        };
                        return map;
                    }, {});
                    if (searchValue)
                        newPromiseArr.push(
                            BotUser.find({
                                userId: { $in: notAssignedUserId },
                                [searchBy]: { $regex: searchValue, $options: 'i' },
                            }),
                        );
                    else newPromiseArr.push(BotUser.find({ userId: { $in: notAssignedUserId } }));
                    handOffResults[1].forEach((handOff) => {
                        assignedToMeUserId.push(handOff.botUserId);
                    });
                    let tempHandOffMap = handOffResults[1].reduce(function(map, handOff) {
                        map[handOff.botUserId] = {
                            handOffId: handOff._id,
                            oldAgent: handOff.agentAssigned[handOff.agentAssigned.length - 1],
                        };
                        return map;
                    }, {});
                    handOffMap = { ...handOffMap, ...tempHandOffMap };
                    if (searchValue)
                        newPromiseArr.push(
                            BotUser.find({
                                userId: { $in: assignedToMeUserId },
                                [searchBy]: { $regex: searchValue, $options: 'i' },
                            }),
                        );
                    else newPromiseArr.push(BotUser.find({ userId: { $in: assignedToMeUserId } }));
                } else {
                    handOffResults[0].forEach((handOff) => {
                        alreadyAssignedUserId.push(handOff.botUserId);
                    });
                    handOffMap = handOffResults[0].reduce(function(map, handOff) {
                        map[handOff.botUserId] = {
                            handOffId: handOff._id,
                            agentId: handOff.agentAssigned[handOff.agentAssigned.length - 1],
                        };
                        return map;
                    }, {});
                    let queryFilter = {
                        userId: { $in: alreadyAssignedUserId },
                    };
                    if (searchValue) queryFilter[searchBy] = { $regex: searchValue, $options: 'i' };
                    let option = {
                        page: page,
                        limit: limit,
                    };
                    option.sort = { updatedAt: 'desc' };
                    newPromiseArr.push(BotUser.paginate(queryFilter, option));
                }
                return Promise.all(newPromiseArr);
            })
            .then((botUsers) => {
                Logger.log.info('BotUser data retrieved', botUsers[0].length);
                let usersToSend = [];
                let newUsersToSend = [];
                let assignedToMeUsersToSend = [];
                if (subStatus === 'newRequest') {
                    botUsers[0].forEach((user) => {
                        let tempUser = JSON.parse(JSON.stringify(user));
                        tempUser.handOffId = handOffMap[tempUser.userId]['handOffId'];
                        tempUser.isAlreadyAssigned = false;
                        newUsersToSend.push(tempUser);
                    });
                    newUsersToSend.sort((a, b) => (a.updatedAt < b.updatedAt ? 1 : b.updatedAt < a.updatedAt ? -1 : 0));
                    botUsers[1].forEach((user) => {
                        let tempUser = JSON.parse(JSON.stringify(user));
                        tempUser.handOffId = handOffMap[tempUser.userId]['handOffId'];
                        tempUser.agentId = handOffMap[tempUser.userId]['agentId'];
                        tempUser.oldAgentName = handOffMap[tempUser.userId]['oldAgent'].name;
                        tempUser.isAlreadyAssigned = true;
                        tempUser.isAssignedByOtherAgent = true;
                        assignedToMeUsersToSend.push(tempUser);
                    });
                    assignedToMeUsersToSend.sort((a, b) =>
                        a.updatedAt < b.updatedAt ? 1 : b.updatedAt < a.updatedAt ? -1 : 0,
                    );
                    usersToSend = [...assignedToMeUsersToSend, ...newUsersToSend];
                    usersToSend = usersToSend.slice((page - 1) * limit, page * limit);
                } else {
                    botUsers[0].docs.forEach((user) => {
                        let tempUser = JSON.parse(JSON.stringify(user));
                        tempUser.handOffId = handOffMap[tempUser.userId]['handOffId'];
                        tempUser.agentId = handOffMap[tempUser.userId]['agentId'];
                        tempUser.isAlreadyAssigned = true;
                        usersToSend.push(tempUser);
                    });
                }
                return resolve(usersToSend);
            })
            .catch((err) => {
                Logger.log.error('Error fetching the users who have applied for HandOff.', err.message || err);
                return reject();
            });
    });
};

/**
 * Export Router
 */
module.exports = router;
