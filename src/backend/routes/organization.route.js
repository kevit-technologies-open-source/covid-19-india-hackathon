/**
 * 3rd Party APIs and Models
 * */
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
let mongoose = require('mongoose');
let Organization = mongoose.model('organization');
let Bot = mongoose.model('bot');
let Df = mongoose.model('dialogflow');
const UserAgent = mongoose.model('user-agent');
/**
 * Config
 * */
const config = require('../config');
/**
 * Helper
 * */
const MailHelper = require('./../helper/mailer.helper');
/**
 * Services
 * */
const Logger = require('./../services/logger');
/**
 * Static Data
 * */
const BotMessages = require('./../shared/bot.messages');
const BotWidget = require('./../shared/bot.widget');
const OrganizationDefault = require('./../shared/organization.default');

/**
 * Get details of an Organization
 */
router.get('/:organizationId', function(req, res) {
    Logger.log.info('In get organization details call');
    if (req.params.organizationId && mongoose.Types.ObjectId.isValid(req.params.organizationId)) {
        let objToSend = {
            organization: {},
            bot: {},
        };
        let organizationId = req.params.organizationId;
        Organization.findOne({ isArchived: false, _id: organizationId })
            .populate({
                path: 'originAdminId',
                select: ['name', 'email', 'contactNumber'],
            })
            .then((organizationData) => {
                if (organizationData) {
                    objToSend.organization.name = organizationData.name;
                    objToSend.organization.adminName = organizationData.originAdminId.name;
                    objToSend.organization.adminEmail = organizationData.originAdminId.email;
                    objToSend.organization.adminContactNumber = organizationData.originAdminId.contactNumber;
                    Bot.findOne({ isArchived: false, organizationId: organizationId })
                        .populate({
                            path: 'dialogflow',
                            select: ['projectId', 'dfJson', 'fileName'],
                        })
                        .exec()
                        .then((botData) => {
                            Logger.log.info('Fetched bot details');
                            if (botData) {
                                objToSend.bot.botName = botData.botName;
                                objToSend.bot.dfJson = botData.dialogflow.dfJson;
                                objToSend.bot.fileName = botData.dialogflow.fileName;
                            } else {
                                objToSend.deletedBotName = organizationData.previousBot.name;
                                objToSend.deletedBotDate = organizationData.previousBot.deleteDate;
                            }
                            res.status(200).send(objToSend);
                        })
                        .catch((err) => {
                            Logger.log.error('Error getting bot details.');
                            Logger.log.error(err.message || err);
                            res.status(500).send({ message: 'Error getting details of organization.' });
                        });
                } else {
                    res.status(200).send(objToSend);
                }
            })
            .catch((err) => {
                Logger.log.error('Error getting details of organization.');
                Logger.log.error(err.message || err);
                res.status(500).send({ message: 'Error getting details of organization.' });
            });
    } else {
        Logger.log.error('Organization id not found.');
        res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Get the List of Organization
 */
router.get('/', function(req, res) {
    Logger.log.info('In list organization call');
    let queryFilter = {
        isArchived: false,
    };
    if (req.query.search) queryFilter.name = { $regex: req.query.search, $options: 'i' };
    let option = {
        page: parseInt(req.query.page) || 1,
        limit: parseInt(req.query.limit) || 5,
    };
    option.populate = {
        path: 'originAdminId',
        select: ['name', 'email'],
    };
    option.select = { name: 1, createdAt: 1 };
    option.sort = { createdAt: 'desc' };
    Organization.paginate(queryFilter, option)
        .then((organizationDetails) => {
            res.status(200).send(organizationDetails);
        })
        .catch((err) => {
            Logger.log.error('Error getting list of organizations.');
            Logger.log.error(err.message || err);
            res.status(500).send({ message: 'Error getting list of organizations.' });
        });
});

/**
 * Creates an Organization, with a bot & an Admin
 */
router.post('/', function(req, res) {
    Logger.log.info('In create organization call');
    if (
        !req.body.organization ||
        !req.body.organization.name ||
        !req.body.organization.adminEmail ||
        !req.body.organization.adminName ||
        !req.body.organization.adminContactNumber ||
        !req.body.bot ||
        !req.body.bot.botName ||
        !req.body.bot.dfJson ||
        !req.body.bot.fileName
    ) {
        Logger.log.error('Required field is missing in create organization.');
        return res.status(400).send({ message: 'Required field is missing.' });
    }
    let organization = req.body.organization;
    let bot = req.body.bot;
    if (bot.dfJson && !bot.dfJson.project_id) {
        Logger.log.error('Dialogflow project id not present.');
        return res.status(400).send({ message: 'Dialogflow project_id not present.' });
    }
    UserAgent.findOne({ email: organization.adminEmail, isArchived: false })
        .exec()
        .then((results) => {
            if (results) {
                Logger.log.error('User is already associated with another organization.');
                return res.status(400).send({ message: 'User already exists in another Organisation.' });
            }
            Organization.create({ name: organization.name })
                .then((organizationData) => {
                    Logger.log.info('Organization created successfully.');
                    let promiseArr = [];
                    let userData = {
                        email: organization.adminEmail,
                        name: organization.adminName,
                        contactNumber: organization.adminContactNumber,
                        organizationId: organizationData._id,
                    };
                    if (req.user && req.user._id) {
                        userData.createdBy = req.user._id;
                    }
                    promiseArr.push(UserAgent.create(userData));
                    let dfData = {
                        dfJson: bot.dfJson,
                        projectId: bot.dfJson.project_id,
                        organizationId: organizationData._id,
                        fileName: bot.fileName,
                    };
                    promiseArr.push(Df.create(dfData));
                    Promise.all(promiseArr)
                        .then((results) => {
                            let userData = results[0];
                            let dfData = results[1];
                            Logger.log.info('User created successfully.');
                            Logger.log.info('Dialogflow Object created successfully.');
                            let newPromiseArr = [];
                            let botData = {
                                botName: bot.botName,
                                dialogflow: dfData._id,
                                organizationId: organizationData._id,
                            };
                            botData.widget = BotWidget;
                            botData.messages = BotMessages.configurableMessages;
                            botData.welcomeMessage = BotMessages.welcomeMessage;
                            let signUpToken = jwt.sign(JSON.stringify({ _id: userData._id }), config.jwtSecret);
                            let organizationUpdateObj = {
                                originAdminId: userData._id,
                                daySpecificWorking: OrganizationDefault.daySpecificWorking,
                            };
                            newPromiseArr.push(
                                Organization.findByIdAndUpdate(organizationData._id, organizationUpdateObj, {
                                    new: true,
                                }),
                            );
                            newPromiseArr.push(Bot.create(botData));
                            newPromiseArr.push(
                                UserAgent.findByIdAndUpdate(userData._id, { signUpToken: signUpToken }, { new: true }),
                            );
                            let mailObj = {
                                toAddress: [organization.adminEmail],
                                subject: 'Welcome to Covid-19 Bot by Kevit',
                                text: {
                                    name: organization.adminName ? organization.adminName : '',
                                    setPasswordLink:
                                        config.frontEndURLs.adminPanelBase +
                                        config.frontEndURLs.setPasswordPage +
                                        userData._id +
                                        '?token=' +
                                        signUpToken,
                                },
                                mailFor: 'newUser',
                            };
                            newPromiseArr.push(MailHelper.sendMail(mailObj));
                            Promise.all(newPromiseArr)
                                .then((results) => {
                                    Logger.log.info('Organization updated, binded originAdminId');
                                    Logger.log.info('Bot created');
                                    Logger.log.info('User updated with sign up token');
                                    if (results[3] && results[3].message !== 'SkippedSendMail')
                                        Logger.log.info('Mail to new user sent successfully.');
                                    else Logger.log.info(results[3].description);
                                    res.status(200).send(results[0]);
                                })
                                .catch((err) => {
                                    //If error occurs in creating Bot or binding user to org.,
                                    // then organization is deleted to remove redundancy
                                    Logger.log.error('Error binding user to organization:', err);
                                    let deletePromiseArr = [];
                                    deletePromiseArr.push(Df.deleteOne({ organizationId: organizationData._id }));
                                    deletePromiseArr.push(
                                        UserAgent.deleteOne({ organizationId: organizationData._id }),
                                    );
                                    deletePromiseArr.push(Bot.deleteOne({ organizationId: organizationData._id }));
                                    Promise.all(deletePromiseArr)
                                        .then((deleteResults) => {
                                            Organization.deleteOne({ _id: organizationData._id })
                                                .then((deleteOrganization) => {
                                                    Logger.log.info('Organization deleted successfully.');
                                                    res.status(500).send({
                                                        message: 'Something went wrong, please try again later.',
                                                    });
                                                })
                                                .catch((err) => {
                                                    Logger.log.error(
                                                        'Error creating organization:',
                                                        err.message || err,
                                                    );
                                                    res.status(500).send({ message: 'Error creating Organisation.' });
                                                });
                                        })
                                        .catch((err) => {
                                            Logger.log.error('Error creating organization:', err.message || err);
                                            res.status(500).send({ message: 'Error creating Organisation.' });
                                        });
                                    res.status(500).send({ message: 'Something went wrong, please try again.' });
                                });
                        })
                        .catch((err) => {
                            Logger.log.error('Error creating organization:', err);
                            //If error occurs in creating Bot or binding user to org.,
                            // then organization is deleted to remove redundancy
                            let deletePromiseArr = [];
                            deletePromiseArr.push(Df.deleteOne({ organizationId: organizationData._id }));
                            deletePromiseArr.push(UserAgent.deleteOne({ organizationId: organizationData._id }));
                            Promise.all(deletePromiseArr)
                                .then((deleteResults) => {
                                    Organization.deleteOne({ _id: organizationData._id })
                                        .then((deleteOrganization) => {
                                            Logger.log.info('Organization deleted successfully.');
                                            res.status(500).send({
                                                message: 'Something went wrong, please try again later.',
                                            });
                                        })
                                        .catch((err) => {
                                            Logger.log.error('Error creating organization:', err.message || err);
                                            res.status(500).send({ message: 'Error creating Organisation.' });
                                        });
                                })
                                .catch((err) => {
                                    Logger.log.error('Error creating organization:', err.message || err);
                                    res.status(500).send({ message: 'Error creating Organisation.' });
                                });
                        });
                })
                .catch((err) => {
                    Logger.log.error('Error creating organization:', err.message || err);
                    res.status(500).send({ message: 'Error creating Organisation.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error creating organization:', err.message || err);
            res.status(500).send({ message: 'Error creating Organisation.' });
        });
});

/**
 * Updates an Organization
 */
router.put('/:organizationId', function(req, res) {
    Logger.log.info('In update organization call');
    let updateJson = false;
    if (req.params.organizationId && mongoose.Types.ObjectId.isValid(req.params.organizationId)) {
        let organizationId = req.params.organizationId;
        Bot.findOne({ organizationId: organizationId, isArchived: false })
            .then((botData) => {
                let isNewBot = false;
                let promiseArr = [];
                //Updates Organization name
                if (req.body.organization && req.body.organization.name) {
                    promiseArr.push(
                        Organization.findByIdAndUpdate(
                            organizationId,
                            { name: req.body.organization.name },
                            { new: true },
                        ),
                    );
                }
                //Updates Admin name and/or Contact Number
                if (
                    req.body.organization &&
                    (req.body.organization.hasOwnProperty('adminName') ||
                        req.body.organization.hasOwnProperty('adminContactNumber'))
                ) {
                    let adminObj = {};
                    if (req.body.organization.hasOwnProperty('adminName'))
                        adminObj.name = req.body.organization.adminName;
                    if (req.body.organization.hasOwnProperty('adminContactNumber'))
                        adminObj.contactNumber = req.body.organization.adminContactNumber;
                    promiseArr.push(
                        UserAgent.findOneAndUpdate({ organizationId: organizationId, isArchived: false }, adminObj, {
                            new: true,
                        }),
                    );
                }
                //Updates Bot Data
                if (botData) {
                    if (req.body.bot && req.body.bot.botName) {
                        promiseArr.push(
                            Bot.findOneAndUpdate(
                                {
                                    organizationId: organizationId,
                                    isArchived: false,
                                },
                                { botName: req.body.bot.botName },
                                { new: true },
                            ),
                        );
                    }
                    if (req.body.bot && req.body.bot.dfJson) {
                        updateJson = true;
                        promiseArr.push(Df.findOne({ projectId: req.body.bot.dfJson.project_id, isArchived: false }));
                    }
                } else {
                    if (req.body.bot) {
                        if (!req.body.bot.botName && req.body.bot.dfJson) {
                            Logger.log.error('Bot not present in database, for new bot, bot name not present.');
                            res.status(400).send({ message: 'Please enter bot name.' });
                        } else if (req.body.bot.botName && !req.body.bot.dfJson) {
                            Logger.log.error('Bot not present in database, for new bot, df JSON not present.');
                            res.status(400).send({ message: 'Please enter Dialogflow JSON file.' });
                        } else if (req.body.bot.botName && req.body.bot.dfJson) {
                            isNewBot = true;
                            updateJson = true;
                            promiseArr.push(
                                Df.findOne({ projectId: req.body.bot.dfJson.project_id, isArchived: false }),
                            );
                        }
                    }
                }
                Promise.all(promiseArr)
                    .then((results) => {
                        if (updateJson) {
                            if (
                                results[results.length - 1] &&
                                results[results.length - 1].organizationId.toString() !== organizationId.toString()
                            ) {
                                Logger.log.info(
                                    'Another organization also has the same dialogflow agent: ',
                                    results[results.length - 1].organizationId,
                                );
                            }
                            if (isNewBot) {
                                let dfData = {
                                    dfJson: req.body.bot.dfJson,
                                    projectId: req.body.bot.dfJson.project_id,
                                    organizationId: organizationId,
                                    accessToken: null,
                                };
                                if (req.body.bot.fileName) dfData.fileName = req.body.bot.fileName;
                                Df.create(dfData)
                                    .then((dfDataCreated) => {
                                        let botData = {
                                            botName: req.body.bot.botName,
                                            dialogflow: dfDataCreated._id,
                                            organizationId: organizationId,
                                        };
                                        botData.widget = BotWidget;
                                        botData.messages = BotMessages.configurableMessages;
                                        botData.welcomeMessage = BotMessages.welcomeMessage;
                                        Bot.create(botData)
                                            .then((botData) => {
                                                Logger.log.info('Organization updated successfully.');
                                                res.status(200).send({ message: 'Organisation updated successfully.' });
                                            })
                                            .catch((err) => {
                                                Logger.log.error('Error updating organization:', err.message || err);
                                                let errorMessage = 'Error updating organisation.';
                                                res.status(500).send({ message: errorMessage });
                                            });
                                    })
                                    .catch((err) => {
                                        Logger.log.error('Error updating organization:', err.message || err);
                                        let errorMessage = 'Error updating organisation.';
                                        res.status(500).send({ message: errorMessage });
                                    });
                            } else {
                                let updateObj = {
                                    dfJson: req.body.bot.dfJson,
                                    projectId: req.body.bot.dfJson.project_id,
                                    accessToken: null,
                                };
                                if (req.body.bot.fileName) updateObj.fileName = req.body.bot.fileName;
                                Df.findOneAndUpdate(
                                    {
                                        organizationId: organizationId,
                                        isArchived: false,
                                    },
                                    updateObj,
                                    { new: true },
                                )
                                    .then((updatedDf) => {
                                        Logger.log.info('Organization updated successfully.');
                                        res.status(200).send({ message: 'Organisation updated successfully.' });
                                    })
                                    .catch((err) => {
                                        Logger.log.error('Error updating organization:', err.message || err);
                                        let errorMessage = 'Error updating organisation.';
                                        res.status(500).send({ message: errorMessage });
                                    });
                            }
                        } else {
                            Logger.log.info('Organization updated successfully.');
                            res.status(200).send({ message: 'Organisation updated successfully.' });
                        }
                    })
                    .catch((err) => {
                        Logger.log.error('Error updating organization:', err.message || err);
                        let errorMessage = 'Error updating organisation.';
                        res.status(500).send({ message: errorMessage });
                    });
            })
            .catch((err) => {
                Logger.log.error('Error updating organization:', err.message || err);
                let errorMessage = 'Error updating organisation.';
                res.status(500).send({ message: errorMessage });
            });
    } else {
        Logger.log.error('Organization id not found.');
        res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Deletes an Organization
 */
router.delete('/:organizationId', function(req, res) {
    Logger.log.info('In delete organization call');
    if (req.params.organizationId && mongoose.Types.ObjectId.isValid(req.params.organizationId)) {
        let organizationId = req.params.organizationId;
        let promiseArr = [];
        promiseArr.push(UserAgent.updateMany({ organizationId: organizationId }, { isArchived: true }));
        promiseArr.push(Bot.updateMany({ organizationId: organizationId }, { isArchived: true }));
        promiseArr.push(Df.updateMany({ organizationId: organizationId }, { isArchived: true }));
        promiseArr.push(Organization.findByIdAndUpdate(organizationId, { isArchived: true }));

        Promise.all(promiseArr)
            .then((results) => {
                Logger.log.info('Organization deleted successfully.');
                res.status(200).send({ message: 'Organisation deleted successfully.' });
            })
            .catch((err) => {
                Logger.log.error('Error deleting organization:', err.message || err);
                let errorMessage = 'Error deleting organisation.';
                res.status(500).send({ message: errorMessage });
            });
    } else {
        Logger.log.error('Organization id not found.');
        res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Export Router
 */
module.exports = router;
