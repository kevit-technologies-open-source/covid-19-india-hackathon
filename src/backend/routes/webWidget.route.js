/**
 * 3rd Party APIs and Models
 * */
const express = require('express');
const router = express.Router();
const multer = require('multer');
const fs = require('fs');
const path = require('path');
let mongoose = require('mongoose');
// let Organization = mongoose.model('organization');
let Bot = mongoose.model('bot');
const Organization = mongoose.model('organization');
/**
 * Config
 * */
const config = require('../config');
/**
 * Services
 * */
const Logger = require('./../services/logger');
/**
 * Static Data
 * */
const BotWidget = require('./../shared/bot.widget');

const uploadProfilePath = path.resolve(__dirname, '../upload/' + getBotIconPath());
//Custom Multer storage engine
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, uploadProfilePath);
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
    },
});
const upload = multer({ dest: uploadProfilePath, storage: storage });

/**
 * Gets the message configuration
 */
router.get('/delay', function(req, res) {
    Logger.log.info('In get delay call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOne({
            organizationId: req.query.organizationId,
            isArchived: false,
        })
            .select({ delay: 1 })
            .exec()
            .then((botData) => {
                Logger.log.info('Fetched delay time successfully');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error fetching delay time.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error fetching delay time.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Updates the message Configuration
 */
router.put('/delay', function(req, res) {
    Logger.log.info('In update delay time call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        let updateObj = {};
        if (req.body.delay.hasOwnProperty('messageDelayTime'))
            updateObj['delay.messageDelayTime'] = req.body.delay.messageDelayTime;
        if (req.body.delay.hasOwnProperty('faceAppearDelayTime'))
            updateObj['delay.faceAppearDelayTime'] = req.body.delay.faceAppearDelayTime;
        if (req.body.delay.hasOwnProperty('unFoldTime')) updateObj['delay.unFoldTime'] = req.body.delay.unFoldTime;
        if (req.body.delay.hasOwnProperty('isAutoUnfold'))
            updateObj['delay.isAutoUnfold'] = req.body.delay.isAutoUnfold;
        Bot.findOneAndUpdate(
            {
                organizationId: req.query.organizationId,
                isArchived: false,
            },
            updateObj,
            { new: true },
        )
            .select({
                delay: 1,
            })
            .exec()
            .then((botData) => {
                Logger.log.info('Updated delay time successfully.');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error updating delay time.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error updating delay time.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Gets the welcome messages
 */
router.get('/welcome-messages', function(req, res) {
    Logger.log.info('In get welcome messages call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOne({
            organizationId: req.query.organizationId,
            isArchived: false,
        })
            .select({ welcomeMessage: 1 })
            .exec()
            .then((botData) => {
                Logger.log.info('Fetched welcome messages successfully');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error fetching welcome messages.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error fetching welcome messages.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Updates the welcome message
 */
router.put('/welcome-messages', function(req, res) {
    Logger.log.info('In update welcome message call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        let updateObj = {};
        if (req.body.welcomeMessage.hasOwnProperty('title'))
            updateObj['welcomeMessage.title'] = req.body.welcomeMessage.title;
        if (req.body.welcomeMessage.hasOwnProperty('greetingText'))
            updateObj['welcomeMessage.greetingText'] = req.body.welcomeMessage.greetingText;
        if (req.body.welcomeMessage.requiredFields && req.body.welcomeMessage.requiredFields.hasOwnProperty('name'))
            updateObj['welcomeMessage.requiredFields.name'] = req.body.welcomeMessage.requiredFields.name;
        if (req.body.welcomeMessage.requiredFields && req.body.welcomeMessage.requiredFields.hasOwnProperty('email'))
            updateObj['welcomeMessage.requiredFields.email'] = req.body.welcomeMessage.requiredFields.email;
        if (
            req.body.welcomeMessage.requiredFields &&
            req.body.welcomeMessage.requiredFields.hasOwnProperty('contactNumber')
        )
            updateObj['welcomeMessage.requiredFields.contactNumber'] =
                req.body.welcomeMessage.requiredFields.contactNumber;
        Bot.findOneAndUpdate(
            {
                organizationId: req.query.organizationId,
                isArchived: false,
            },
            updateObj,
            { new: true },
        )
            .select({
                welcomeMessage: 1,
            })
            .exec()
            .then((botData) => {
                Logger.log.info('Updated delay time successfully.');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error updating delay time.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error updating delay time.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Gets the look and feel details
 */
router.get('/look-and-feel', function(req, res) {
    Logger.log.info('In get look and feel widget settings call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOne({
            organizationId: req.query.organizationId,
            isArchived: false,
        })
            .select({ botName: 1, widget: 1 })
            .exec()
            .then((results) => {
                if (results.widget.iconUrl) results.widget.iconUrl = getBotIconUrl(results.widget.iconUrl);
                Logger.log.info('Fetched look and feel widget settings successfully');
                res.status(200).send(results);
            })
            .catch((err) => {
                Logger.log.error('Error fetching look and feel widget settings.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error fetching look and feel widget settings.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Updates the look and feel details
 */
router.put('/look-and-feel', function(req, res) {
    Logger.log.info('In update look and feel widget settings call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        if (!req.body.widget) {
            Logger.log.error('Widget object not passed to update.');
            return res.status(400).send({ message: 'Widget object not passed to update.' });
        }
        let updateObj = {};
        if (req.body.botName) updateObj['botName'] = req.body.botName;
        let widget = req.body.widget;
        if (widget.bot) {
            if (widget.bot.bubbleColour) updateObj['widget.bot.bubbleColour'] = widget.bot.bubbleColour;
            if (widget.bot.fontColour) updateObj['widget.bot.fontColour'] = widget.bot.fontColour;
            if (widget.bot.fontStyle) updateObj['widget.bot.fontStyle'] = widget.bot.fontStyle;
        }
        if (widget.user) {
            if (widget.user.bubbleColour) updateObj['widget.user.bubbleColour'] = widget.user.bubbleColour;
            if (widget.user.fontColour) updateObj['widget.user.fontColour'] = widget.user.fontColour;
            if (widget.user.fontStyle) updateObj['widget.user.fontStyle'] = widget.user.fontStyle;
        }
        if (widget.agent) {
            if (widget.agent.bubbleColour) updateObj['widget.agent.bubbleColour'] = widget.agent.bubbleColour;
            if (widget.agent.fontColour) updateObj['widget.agent.fontColour'] = widget.agent.fontColour;
            if (widget.agent.fontStyle) updateObj['widget.agent.fontStyle'] = widget.agent.fontStyle;
        }
        if (widget.inputBackground) {
            if (widget.inputBackground.bubbleColour)
                updateObj['widget.inputBackground.bubbleColour'] = widget.inputBackground.bubbleColour;
            if (widget.inputBackground.fontColour)
                updateObj['widget.inputBackground.fontColour'] = widget.inputBackground.fontColour;
        }
        if (widget.position) updateObj['widget.position'] = widget.position;
        if (widget.size) updateObj['widget.size'] = widget.size;
        if (widget.chatBackgroundColour) updateObj['widget.chatBackgroundColour'] = widget.chatBackgroundColour;
        if (widget.chatFontColour) updateObj['widget.chatFontColour'] = widget.chatFontColour;
        if (widget.iconUrl) updateObj['widget.iconUrl'] = widget.iconUrl;
        Bot.findOneAndUpdate(
            {
                organizationId: req.query.organizationId,
                isArchived: false,
            },
            updateObj,
            { new: true },
        )
            .select({ widget: 1, botName: 1 })
            .then((results) => {
                if (results.widget.iconUrl) results.widget.iconUrl = getBotIconUrl(results.widget.iconUrl);
                Logger.log.info('Look and feel widget settings updated successfully.');
                res.status(200).send(results);
            })
            .catch((err) => {
                Logger.log.error('Error updating look and feel widget settings.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error updating look and feel widget settings.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Restores look and feel details to default
 */
router.put('/look-and-feel/restore-default', function(req, res) {
    Logger.log.info('In restore to default look and feel widget settings call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOneAndUpdate(
            {
                organizationId: req.query.organizationId,
                isArchived: false,
            },
            { widget: BotWidget },
            { new: true },
        )
            .select({ widget: 1, botName: 1 })
            .then((results) => {
                if (results.widget.iconUrl) results.widget.iconUrl = getBotIconUrl(results.widget.iconUrl);
                Logger.log.info('Look and feel widget settings restored to default successfully.');
                res.status(200).send(results);
            })
            .catch((err) => {
                Logger.log.error('Error restoring look and feel widget settings to default.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error restoring look and feel widget settings to default.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Upload bot icon.
 */
router.post('/upload/bot-icon', upload.single('bot-icon'), (req, res) => {
    Logger.log.info('In upload bot icon call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Bot.findOneAndUpdate(
            {
                organizationId: req.query.organizationId,
                isArchived: false,
            },
            { 'widget.iconUrl': req.file.filename },
            { new: true },
        )
            .exec()
            .then((botData) => {
                Logger.log.info('Bot icon name updated in bot data successfully.');
                responseObj = {
                    file: config.BaseUrl + getBotIconPath() + req.file.filename,
                };
                res.status(200).send(responseObj);
                if (req.query.oldImageName) {
                    Logger.log.info('Old icon name:', req.query.oldImageName);
                    let imagePath = path.resolve(__dirname + '/../upload/' + getBotIconPath() + req.query.oldImageName);
                    fs.unlink(imagePath, (err) => {
                        if (err) {
                            Logger.log.warn(
                                `Error deleting old icon with name: ${req.query.oldImageName} for the organization ${req.query.organizationId}`,
                            );
                            Logger.log.warn(err.message || err);
                        } else Logger.log.info('Successfully deleted old bot icon.');
                    });
                }
            })
            .catch((err) => {
                Logger.log.error('Error updating bot for bot icon');
                Logger.log.error(err.message || err);
                res.status(500).send('Error uploading bot icon.');
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Remove profile-picture of User.
 */
router.delete('/bot-icon', (req, res) => {
    Logger.log.info('In upload bot icon call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        if (!req.query.oldImageName) {
            Logger.log.error('In delete profile picture call, old image name not present for the user:', userId);
            return res.status(400).send({
                status: 'ICON_NAME_NOT_FOUND',
                message: 'Bot icon name not found, unable to remove old icon.',
            });
        }
        Logger.log.info('Old image name:', req.query.oldImageName);
        let imagePath = path.resolve(__dirname + '/../upload/' + getBotIconPath() + req.query.oldImageName);
        fs.unlink(imagePath, (err) => {
            if (err) {
                Logger.log.warn(
                    `Error deleting profile picture with name: ${req.query.oldImageName} for organization ${req.query.organizationId}`,
                );
                Logger.log.warn(err);
                return res.status(500).send('Error removing bot icon.');
            } else {
                Logger.log.info('Successfully deleted old bot icon.');
                Bot.findOneAndUpdate(
                    { organizationId: req.query.organizationId, isArchived: false },
                    { 'widget.iconUrl': null },
                    { new: true },
                )
                    .select({ widget: 1 })
                    .exec()
                    .then((botData) => {
                        res.status(200).send(botData);
                    })
                    .catch((err) => {
                        Logger.log.error('Error deleting bot icon');
                        Logger.log.error(err.message || err);
                        res.status(500).send('Error removing bot icon.');
                    });
            }
        });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
});

/**
 * Helper Functions
 */
function getBotIconPath() {
    return config.uploadLocations.bot.base + config.uploadLocations.bot.icon;
}

function getBotIconUrl(imageName) {
    if (imageName)
        if (imageName.indexOf(config.BaseUrl + getBotIconPath()) !== -1) return imageName;
        else return config.BaseUrl + getBotIconPath() + imageName;
    return '';
}

/**
 * Export Router
 */
module.exports = router;
