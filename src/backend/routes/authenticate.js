const Logger = require('../services/logger');
const mongoose = require('mongoose');
const User = mongoose.model('user-agent');

let authMiddleWare = (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return res.status(200).send();
    }
    let token = req.header('authorization');
    if (token) {
        User.findByToken(token)
            .then((user) => {
                if (user && user.isArchived === false) {
                    req.user = user;
                    req.token = token;
                    Logger.log.info('AUTH - user id:' + user._id);
                    next();
                } else {
                    res.boom.unauthorized('Auth-Token is not valid');
                }
            })
            .catch((err) => {
                console.log('ERROR::', err);
                if (err.status === 'INVALID_TOKEN') res.status(401).send(err);
                else res.status(500).send(err);
            });
    } else {
        Logger.log.warn('JWT - Auth-Token not set in header');
        res.boom.unauthorized('Auth-Token not set in header');
    }
};

let superAdminMiddleWare = (req, res, next) => {
    if (req.user) {
        if (req.user.role === 'superAdmin') {
            next();
        } else {
            Logger.log.warn(`User is ${req.user.role} and trying to access SuperAdmin routes`);
            res.boom.unauthorized('You are unauthorized to access this page.');
        }
    } else {
        Logger.log.warn('User not found, please login again.');
        res.boom.unauthorized('User not found, please login again.');
    }
};

let adminMiddleWare = (req, res, next) => {
    if (req.user) {
        if (req.user.role === 'admin' || req.user.role === 'superAdmin') {
            next();
        } else {
            Logger.log.warn(`User is ${req.user.role} and trying to access Admin routes`);
            res.boom.unauthorized('You are unauthorized to access this page.');
        }
    } else {
        Logger.log.warn('User not found, please login again.');
        res.boom.unauthorized('User not found, please login again.');
    }
};

module.exports = {
    authMiddleWare,
    adminMiddleWare,
    superAdminMiddleWare,
};
