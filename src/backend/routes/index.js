/**
 * Router Configuration Files
 */

/**
 * System and 3rd party libs
 */
const express = require('express');
const router = express.Router();

/**
 * Router Definitions
 */
router.get('/', function(req, res) {
    res.json('Welcome to HP');
});

/**
 * Export Router
 */
module.exports = router;
