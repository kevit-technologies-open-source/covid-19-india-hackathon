/**
 * Router Configuration Files
 */

/**
 * System and 3rd party libs
 */
const express = require('express');
const router = express.Router();
let mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
var User = mongoose.model('user-agent');
const authenticate = require('./authenticate').authMiddleWare;

/**
 * Services
 * */
const Logger = require('./../services/logger');
/**
 * Config
 * */
const config = require('../config');
/**
 * Helper
 * */
const MailHelper = require('./../helper/mailer.helper');

/**
 * Router Definitions
 */

/**
 * Call for Login
 */
router.post('/login', function(req, res) {
    let userId = req.body.userId;
    let password = req.body.password;
    User.findByCredentials(userId, password)
        .then((user) => {
            if (user) {
                let token = user.getAuthToken();
                // .then((token) => {
                // if (req.body.fcmToken) {
                if (req.body.fcmToken && !user.fcmToken.includes(req.body.fcmToken))
                    user.fcmToken.push(req.body.fcmToken);
                user.save()
                    .then(() => {
                        user.profilePicture = getProfileUrl(user.profilePicture);
                        res.send({
                            name: user.name,
                            email: user.email,
                            contactNumber: user.contactNumber,
                            organizationId: user.organizationId,
                            profilePicture: user.profilePicture,
                            organizationId: user.organizationId,
                            _id: user._id,
                            role: user.role,
                            token: token,
                            fcmToken: req.body.fcmToken,
                        });
                    })
                    .catch((err) => {
                        Logger.log.warn('Error is adding FCM token for user : ', userId);
                        Logger.log.warn(err.message || err);
                    });
                // } else {
                //     Logger.log.error("FCM Token is not present ::: ");
                //     Logger.log.error(req.body)
                //     res.boom.badRequest('Please try again with browser that supports sending notifications. Incognito browsers won\'t work. If problem persists, try clearing site data.');
                // }
                // })
                // .catch((e) => {
                //     Logger.log.warn(e);
                //     res.send(e);
                // });
            } else {
                res.send({
                    status: 'USER_NOT_FOUND',
                    message: 'Incorrect email or password.',
                });
            }
        })
        .catch((e) => {
            Logger.log.error(e);
            res.status(400).send(e);
        });
});

/**
 * Call for Logout
 */
router.put('/logout', authenticate, (req, res) => {
    req.user.removeToken(req.token, req.body.fcmToken).then(
        () => {
            res.status(200).send({
                status: 'SUCCESS',
                message: 'Logout Successfully',
            });
        },
        () => {
            res.status(400).send({
                status: 'ERROR',
                message: 'Error in Logout',
            });
        },
    );
});

/**
 * Creating a user
 */
router.post('/user', (req, res) => {
    let u = new User(req.body);
    u.save()
        .then(() => {
            res.send({ status: 'SUCCESS', details: 'User created successfully' });
        })
        .catch((err) => {
            Logger.log.warn('Error : ', err);
        });
});

/**
 * Change Password
 */
router.put('/change-password', authenticate, (req, res) => {
    if (!req.body.oldPassword || !req.body.newPassword) {
        Logger.log.error('Old or new password not present');
        return res.status(400).send({
            message: 'Old or new password not present',
        });
    }
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    if (newPassword.length < 8) {
        res.status(400).send({
            message: 'Password length must be 8 or greater',
        });
        return;
    }
    let user = req.user;
    user.comparePassword(oldPassword)
        .then((isMatch) => {
            if (isMatch) {
                User.findById(user._id).then((u) => {
                    u.password = newPassword;
                    u.save()
                        .then((data) => {
                            if (data) {
                                Logger.log.info('Password changed successfully');
                                res.status(200).json({
                                    message: 'Password changed successfully',
                                });
                            }
                        })
                        .catch((err) => {
                            res.status(500).send({
                                message: 'Error changing password.',
                            });
                        });
                });
            } else {
                res.status(400).send({
                    message: 'Wrong current password.',
                });
            }
        })
        .catch((err) => {
            Logger.log.error('Error changing password.');
            Logger.log.error(err);
            res.status(500).send({ message: 'Error changing password.' });
        });
});

/**
 * Forget Password
 */
//TODO, Mail Sending is remaining
router.post('/forget-password', (req, res) => {
    Logger.log.info('In forget password function call');
    if (!req.body.email) {
        res.status(400).json({
            message: 'Email not found',
        });
        return;
    }
    User.findOne({ email: req.body.email, isArchived: false })
        .exec()
        .then((user) => {
            if (!user) {
                Logger.log.warn('For forget password, user not found in the database with the email:', req.body.email);
                res.status(200).send({
                    message: 'If user exists then mail with reset password link will be sent.',
                });
            } else {
                let token = jwt.sign(JSON.stringify({ _id: user._id, timeStamp: Date.now() }), config.jwtSecret);
                let mailObj = {
                    toAddress: [req.body.email],
                    subject: 'Reset Password Link',
                    text: {
                        name: user.name ? user.name : '',
                    },
                    mailFor: 'forgotPassword',
                };
                if (user.role === 'superAdmin') {
                    mailObj.text.resetPasswordLink =
                        config.frontEndURLs.superAdminPanelBase +
                        config.frontEndURLs.resetPasswordPage +
                        user._id +
                        '?token=' +
                        token;
                    mailObj.text.forgotPasswordLink =
                        config.frontEndURLs.superAdminPanelBase + config.frontEndURLs.forgotPasswordPage;
                } else {
                    mailObj.text.resetPasswordLink =
                        config.frontEndURLs.adminPanelBase +
                        config.frontEndURLs.resetPasswordPage +
                        user._id +
                        '?token=' +
                        token;
                    mailObj.text.forgotPasswordLink =
                        config.frontEndURLs.adminPanelBase + config.frontEndURLs.forgotPasswordPage;
                }
                MailHelper.sendMail(mailObj)
                    .then((mailResult) => {
                        Logger.log.info('Reset Password link:', 'reset/' + user._id + '?token=' + token);
                        res.status(200).send({
                            message: 'If user exists then mail with reset password link will be sent.',
                        });
                    })
                    .catch((err) => {
                        Logger.log.error('Error sending email for forget password');
                        Logger.log.error(err);
                        res.status(500).send({
                            message: 'Something went wrong, please try again later.',
                        });
                    });
            }
        })
        .catch((err) => {
            Logger.log.error('Error while fetching User From Database');
            Logger.log.error(err);
            res.status(500).json({
                status: 'ERROR',
                error: 'DATABASE_ERROR',
                description: 'Error while fetching user list.',
            });
        });
});

/**
 * Reset Password
 */
//TODO, ask if we want to send mail on reset password
router.post('/:id/reset-password', (req, res) => {
    jwt.verify(req.body.token, config.jwtSecret, (err, decoded) => {
        if (err) {
            Logger.log.warn('JWT - Authentication failed. Error in decoding token.');
            return res.status(401).send({ message: 'Authentication failed. Error in decoding token.' });
        } else {
            //TODO:ADD config for hours
            let validTime = decoded.timeStamp + 30 * 60 * 1000;
            if (validTime < Date.now()) {
                res.status(401).send({
                    message:
                        'The link to reset password has expired, please repeat the process by clicking on Forget Password from login page.',
                });
                Logger.log.info('AUTH - token expired. user id:' + decoded._id);
            } else if (decoded._id !== req.params.id) {
                Logger.log.warn('AUTH - Invalid id:' + req.params.id);
                return res.status(401).send({ message: 'Invalid request, please repeat process from beginning.' });
            } else {
                User.findById(decoded._id)
                    .exec()
                    .then((user) => {
                        if (!user) {
                            return res.status(400).send({ message: 'No user for the given mail id found' });
                        } else {
                            user.password = req.body.password;
                            user.save()
                                .then((updatedUser) => {
                                    Logger.log.info('User password updated id:' + updatedUser._id);
                                    res.status(200).send({ message: 'Password changed successfully' });
                                })
                                .catch((err) => {
                                    Logger.log.error('Error in saving user password, id:' + user._id);
                                    Logger.log.error(err);
                                    res.status(500).send({ message: 'Error resetting the password.' });
                                });
                        }
                    })
                    .catch((err) => {
                        Logger.log.error('Fetching user id:' + req.params.id);
                        Logger.log.error(err);
                        res.status(500).send({ message: 'Error resetting the password.' });
                    });
            }
        }
    });
});

/**
 * Set Password (Initially & One time)
 */
//TODO, ask if we want to send mail on reset password
router.post('/:id/set-password', (req, res) => {
    jwt.verify(req.body.signUpToken, config.jwtSecret, (err, decoded) => {
        if (err) {
            Logger.log.warn('JWT - Authentication failed. Error in decoding token.');
            return res.status(401).send({ message: 'Authentication failed. Error in decoding token.' });
        } else {
            if (decoded._id.toString() !== req.params.id.toString()) {
                Logger.log.warn('AUTH - Invalid id:' + req.params.id);
                return res.status(401).send({ message: 'Invalid request, please repeat process from beginning.' });
            } else {
                User.findById(decoded._id)
                    .exec()
                    .then((user) => {
                        if (!user) {
                            return res.status(400).send({ message: 'No user for the given mail id found' });
                        } else if (!user.signUpToken) {
                            Logger.log.warn(
                                'Link to generate password has already been used for user id:' + req.params.id,
                            );
                            return res.status(400).send({
                                message:
                                    'Password has already once set, to recover password, click on Forgot Password from Login Page.',
                            });
                        } else if (!user.signUpToken || user.signUpToken !== req.body.signUpToken) {
                            Logger.log.warn(
                                'AUTH - Invalid signUp token or signUpToken not present in DB for user id:' +
                                    req.params.id,
                            );
                            return res
                                .status(401)
                                .send({ message: 'Invalid request, please repeat process from beginning.' });
                        } else {
                            user.password = req.body.password;
                            user.signUpToken = null;
                            user.save()
                                .then((updatedUser) => {
                                    Logger.log.info('User password set id:' + updatedUser._id);
                                    res.status(200).send({ message: 'Password set successfully' });
                                })
                                .catch((err) => {
                                    Logger.log.error('Error in saving user password, id:' + user._id);
                                    Logger.log.error(err);
                                    res.status(500).send({
                                        message:
                                            'Error setting the password. Please click on Forget Password from Login Page.',
                                    });
                                });
                        }
                    })
                    .catch((err) => {
                        Logger.log.error('Fetching user id:' + req.params.id);
                        Logger.log.error(err);
                        res.status(500).send({
                            message: 'Error setting the password. Please click on Forget Password from Login Page.',
                        });
                    });
            }
        }
    });
});

/**
 * Helper Functions
 */
function getProfileImagePath() {
    return config.uploadLocations.user.base + config.uploadLocations.user.profile;
}

function getProfileUrl(imageName) {
    if (imageName)
        if (imageName.indexOf(config.BaseUrl + getProfileImagePath()) !== -1) return imageName;
        else return config.BaseUrl + getProfileImagePath() + imageName;
    return '';
}

/**
 * Export Router
 */
module.exports = router;
