const express = require('express');
const router = express.Router();
let mongoose = require('mongoose');
let HandOff = mongoose.model('handoff');
let Bot = mongoose.model('bot');
const BotUsers = mongoose.model('user-botuser');
const UserAgent = mongoose.model('user-agent');
let BotUserTranscript = mongoose.model('botuser-transcript');
/**
 * Config
 * */
const config = require('../config');
/**
 * Helpers
 * */
let TranscriptHelper = require('./../helper/transcriptHelper');
/**
 * Services
 * */
const Logger = require('./../services/logger');

/**
 * Get the List of Handoff Pending Users
 */
router.get('/chatWithAgent/:handOffId', (req, res) => {
    let handOffId = req.params.handOffId;
    HandOff.findById(handOffId)
        .populate({
            select: ['name', 'profilePicture'],
            path: 'agentAssigned',
        })
        .then((handOffData) => {
            let agentProfileObj = {};
            handOffData.agentAssigned.forEach((agent) => {
                if (agent.profilePicture)
                    agentProfileObj[agent._id] = {
                        url: getProfileUrl(agent.profilePicture),
                    };
                else
                    agentProfileObj[agent._id] = {
                        name: agent.name,
                    };
            });
            if (Object.keys(agentProfileObj).length === 0) agentProfileObj = null;
            let promiseArr = [];
            promiseArr.push(TranscriptHelper.getTranscriptBasedOnUserId(handOffData.botUserId));
            promiseArr.push(
                Bot.findOne({ organizationId: handOffData.organizationId }).select({
                    'widget.iconUrl': 1,
                    botName: 1,
                }),
            );
            Promise.all(promiseArr)
                .then((results) => {
                    let transcriptData = results[0];
                    let botIconObj = {};
                    if (results[1] && results[1].widget && results[1].widget.iconUrl)
                        botIconObj.url = getBotUrl(results[1].widget.iconUrl);
                    else botIconObj.name = results[1].botName;
                    Logger.log.info('Transcript fetched successfully.', handOffData);
                    let responseData = {
                        transcript: transcriptData.transcript,
                        userId: handOffData.botUserId,
                        handOffId: handOffData._id,
                        agentProfileObj: agentProfileObj,
                        botIconObj: botIconObj,
                        agentAssigned:
                            handOffData.agentAssigned &&
                            handOffData.agentAssigned.length > 0 &&
                            handOffData.agentAssigned[handOffData.agentAssigned.length - 1].name
                                ? handOffData.agentAssigned[handOffData.agentAssigned.length - 1].name
                                : null,
                    };
                    res.status(200).send(responseData);
                })
                .catch((err) => {
                    Logger.log.error('Error fetching the transcript', err.message || err);
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching the transcript', err.message || err);
        });
});

/**
 * Get the List of Handoff Completed Users
 */
router.get('/chatWithBot/:userId', (req, res) => {
    let userId = req.params.userId;
    let promiseArr = [];
    promiseArr.push(TranscriptHelper.getTranscriptBasedOnUserId(userId));
    promiseArr.push(BotUsers.findOne({ userId: userId }).select({ organizationId: 1 }));
    Promise.all(promiseArr)
        .then((results) => {
            let transcriptData = JSON.parse(JSON.stringify(results[0]));
            Logger.log.info('Transcript fetched successfully.');
            let agentArray = [];
            if (!transcriptData || !transcriptData.transcript || transcriptData.transcript.length === 0) {
                Logger.log.info('No messages found for given chat.');
                return res.status(200).send(null);
            }
            transcriptData.transcript.forEach((message) => {
                if (message.isFrom === 'agent' && message.agentId && !agentArray.includes(message.agentId)) {
                    agentArray.push(mongoose.Types.ObjectId(message.agentId));
                }
            });
            Bot.findOne({ organizationId: results[1].organizationId })
                .select({
                    'widget.iconUrl': 1,
                    botName: 1,
                })
                .then((botData) => {
                    let botIconObj = {};
                    if (botData.widget && botData.widget.iconUrl)
                        botIconObj = { url: getBotUrl(botData.widget.iconUrl) };
                    else botIconObj = { name: botData.botName };
                    transcriptData.botIconObj = botIconObj;
                    let agentProfileObj = {};
                    transcriptData.agentProfileObj = agentProfileObj;

                    if (agentArray.length > 0) {
                        UserAgent.find({ _id: { $in: agentArray } })
                            .select({ name: 1, profilePicture: 1 })
                            .then((agents) => {
                                agents.forEach((agent) => {
                                    if (agent.profilePicture)
                                        agentProfileObj[agent._id] = { url: getProfileUrl(agent.profilePicture) };
                                    else agentProfileObj[agent._id] = { name: agent.name };
                                });
                                if (Object.keys(agentProfileObj).length === 0) agentProfileObj = null;
                                transcriptData.agentProfileObj = agentProfileObj;
                                res.status(200).send(transcriptData);
                            })
                            .catch((err) => {
                                Logger.log.error(err.message || err);
                            });
                    } else {
                        res.status(200).send(transcriptData);
                    }
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching the transcript', err.message || err);
        });
});

/**
 * Helper Functions
 */
function getBotImagePath() {
    return config.uploadLocations.bot.base + config.uploadLocations.bot.icon;
}

function getBotUrl(imageName) {
    if (imageName)
        if (imageName.indexOf(config.BaseUrl + getBotImagePath()) !== -1) return imageName;
        else return config.BaseUrl + getBotImagePath() + imageName;
    return '';
}

function getProfileImagePath() {
    return config.uploadLocations.user.base + config.uploadLocations.user.profile;
}

function getProfileUrl(imageName) {
    if (imageName)
        if (imageName.indexOf(config.BaseUrl + getProfileImagePath()) !== -1) return imageName;
        else return config.BaseUrl + getProfileImagePath() + imageName;
    return '';
}

/**
 * Export Router
 */
module.exports = router;
