/**
 * 3rd Party APIs and Models
 * */
const express = require('express');
const router = express.Router();
let mongoose = require('mongoose');
// let Organization = mongoose.model('organization');
let Bot = mongoose.model('bot');
const Organization = mongoose.model('organization');
const HandOff = mongoose.model('handoff');
const BotUserTranscript = mongoose.model('botuser-transcript');
const BotUser = mongoose.model('user-botuser');
const UserAgent = mongoose.model('user-agent');
/**
 * Services
 * */
const Logger = require('./../services/logger');

/**
 * Gets number of request initiated by Agent and by User
 */
router.get('/initiated-by', function(req, res) {
    Logger.log.info('In analytics initiated by call');
    let organizationId = req.query.organizationId;
    //TODO, by default, startDate and endDate have time  00:00:00:000
    let currentTime = new Date();
    let startDate =
        new Date(req.query.startDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    let endDate =
        new Date(req.query.endDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
    if (endDate < startDate) {
        Logger.log.error("End Date can't be lesser than Start Date.", startDate, endDate);
        return res.status(400).send({ message: "End Date can't be lesser than Start Date." });
    }
    Organization.findById(organizationId)
        .select({ timezone: 1, daylightSavings: 1 })
        .exec()
        .then((organization) => {
            if (!organization) {
                Logger.log.error('No Organisation data found for the organization Id.', organizationId);
                return res.status(400).send({ message: 'No Organization data found.' });
            }
            let timezoneOffset = organization.timezone.val;
            if (organization.daylightSavings) timezoneOffset++;
            timezoneOffset = -timezoneOffset;
            startDate.addHours(timezoneOffset);
            endDate.addHours(timezoneOffset);
            endDate.setTime(endDate.getTime() + (24 * 60 * 60 - 1) * 1000);
            HandOff.aggregate([
                {
                    $match: {
                        organizationId: mongoose.Types.ObjectId(organizationId),
                        createdAt: { $gte: startDate, $lte: endDate },
                    },
                },
                {
                    $group: {
                        _id: null,
                        initiatedByAgent: { $sum: { $cond: ['$initiatedByAgent', 1, 0] } },
                        initiatedByUser: { $sum: { $cond: ['$initiatedByAgent', 0, 1] } },
                    },
                },
            ])
                .allowDiskUse(true)
                .then((results) => {
                    let initiatedByAgent = results.length > 0 ? results[0].initiatedByAgent : 0;
                    let initiatedByUser = results.length > 0 ? results[0].initiatedByUser : 0;
                    res.status(200).send({ initiatedByAgent, initiatedByUser });
                })
                .catch((err) => {
                    Logger.log.error('Error fetching analytics for initiated by.');
                    Logger.log.error(err.message || err);
                    return res
                        .status(500)
                        .send({ message: 'Error fetching Analytics for Chat with Customer Support.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching organization details for analytics for initiated by call.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error fetching Analytics for Chat with Customer Support.' });
        });
});

/**
 * Gets number of request initiated by Agent and by User
 */
router.get('/top-agents', function(req, res) {
    Logger.log.info('In analytics top agents call');
    let organizationId = req.query.organizationId;
    //TODO, by default, startDate and endDate have time  00:00:00:000
    let currentTime = new Date();
    let startDate =
        new Date(req.query.startDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    let endDate =
        new Date(req.query.endDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    let topAgentCount = parseInt(req.query.topAgentCount) || 5;
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
    if (endDate < startDate) {
        Logger.log.error("End Date can't be lesser than Start Date.", startDate, endDate);
        return res.status(400).send({ message: "End Date can't be lesser than Start Date." });
    }
    Organization.findById(organizationId)
        .select({ timezone: 1, daylightSavings: 1 })
        .exec()
        .then((organization) => {
            if (!organization) {
                Logger.log.error('No Organization data found for the organization Id.', organizationId);
                return res.status(400).send({ message: 'No Organisation data found.' });
            }
            let timezoneOffset = organization.timezone.val;
            if (organization.daylightSavings) timezoneOffset++;
            timezoneOffset = -timezoneOffset;
            startDate.setTime(startDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + (24 * 60 * 60 - 1) * 1000);
            HandOff.aggregate([
                {
                    $match: {
                        organizationId: mongoose.Types.ObjectId(organizationId),
                        createdAt: { $gte: startDate, $lte: endDate },
                        isWaiting: false,
                    },
                },
                {
                    $unwind: {
                        path: '$agentAssigned',
                    },
                },
                {
                    $sortByCount: '$agentAssigned',
                },
            ])
                .limit(topAgentCount)
                .allowDiskUse(true)
                .then((results) => {
                    let agentIds = [];
                    let countMap = results.reduce((mapObj, element) => {
                        mapObj[element._id] = element.count;
                        agentIds.push(element._id);
                        return mapObj;
                    }, {});
                    let promiseArr = [];
                    promiseArr.push(UserAgent.find({ _id: agentIds }).select({ name: 1 }));
                    promiseArr.push(
                        UserAgent.find({
                            organizationId: organizationId,
                            isArchived: false,
                        }).select({ _id: 1 }),
                    );
                    Promise.all(promiseArr).then((results) => {
                        let agents = results[0];
                        totalAgentCount = results[1] && results[1].length > 0 ? results[1].length : 0;
                        let responseArr = JSON.parse(JSON.stringify(agents));
                        responseArr.forEach((agent) => {
                            agent['count'] = countMap[agent._id];
                        });
                        let responseObj = {
                            topAgents: responseArr,
                            totalAgentCount: totalAgentCount,
                        };
                        Logger.log.info('Agent List fetched successfully.');
                        res.status(200).send(responseObj);
                    });
                })
                .catch((err) => {
                    Logger.log.error('Error fetching analytics for top agents.');
                    Logger.log.error(err.message || err);
                    return res.status(500).send({ message: 'Error fetching analytics of top agents.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching organization details for analytics for initiated by call.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error fetching analytics of top agents.' });
        });
});

/**
 * Gets number of Requests based on Part of Time it Arrived
 */
router.get('/part-of-time', function(req, res) {
    Logger.log.info('In part of time request arrived analytics call');
    let organizationId = req.query.organizationId;
    let currentTime = new Date();
    let startDate =
        new Date(req.query.startDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    let endDate =
        new Date(req.query.endDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
    if (endDate < startDate) {
        Logger.log.error("End Date can't be lesser than Start Date.", startDate, endDate);
        return res.status(400).send({ message: "End Date can't be lesser than Start Date." });
    }
    Organization.findById(organizationId)
        .select({ timezone: 1, daylightSavings: 1 })
        .exec()
        .then((organization) => {
            if (!organization) {
                Logger.log.error('No Organisation data found for the organization Id.', organizationId);
                return res.status(400).send({ message: 'No Organization data found.' });
            }
            let timezoneOffset = organization.timezone.val;
            if (organization.daylightSavings) timezoneOffset++;
            timezoneOffset = -timezoneOffset;
            startDate.setTime(startDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + (24 * 60 * 60 - 1) * 1000);
            HandOff.aggregate([
                {
                    $match: {
                        organizationId: mongoose.Types.ObjectId(organizationId),
                        createdAt: { $gte: startDate, $lte: endDate },
                    },
                },
                {
                    $group: {
                        _id: null,
                        inTeamWorkingHours: {
                            $sum: { $cond: [{ $in: ['$requestArrival', ['inWorkingHours']] }, 1, 0] },
                        },
                        outOfTeamWorkingHours: {
                            $sum: { $cond: [{ $in: ['$requestArrival', ['inWorkingHours']] }, 0, 1] },
                        },
                    },
                },
            ])
                .allowDiskUse(true)
                .then((results) => {
                    Logger.log.info('Fetched part of time analytics successfully.');
                    let inTeamWorkingHours = results.length > 0 ? results[0].inTeamWorkingHours : 0;
                    let outOfTeamWorkingHours = results.length > 0 ? results[0].outOfTeamWorkingHours : 0;
                    res.status(200).send({ inTeamWorkingHours, outOfTeamWorkingHours });
                })
                .catch((err) => {
                    Logger.log.error('Error fetching analytics for initiated by.');
                    Logger.log.error(err.message || err);
                    return res
                        .status(500)
                        .send({ message: 'Error fetching analytics for Requests and Working Hours.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching organization details for analytics for initiated by call.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error fetching analytics for Requests and Working Hours.' });
        });
});

/**
 * Gets number of Requests classified by hours of the day
 */
router.get('/most-comm-hours', function(req, res) {
    Logger.log.info('In most communicating hours analytics call');
    let organizationId = req.query.organizationId;
    //TODO, by default, startDate and endDate have time  00:00:00:000
    let currentTime = new Date();
    let meridiem = req.query.meridiem || 'pm';
    let startDate =
        new Date(req.query.startDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    let endDate =
        new Date(req.query.endDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
    if (endDate < startDate) {
        Logger.log.error("End Date can't be lesser than Start Date.", startDate, endDate);
        return res.status(400).send({ message: "End Date can't be lesser than Start Date." });
    }
    Organization.findById(organizationId)
        .select({ timezone: 1, daylightSavings: 1 })
        .exec()
        .then((organization) => {
            if (!organization) {
                Logger.log.error('No Organization data found for the organization Id.', organizationId);
                return res.status(400).send({ message: 'No Organisation data found.' });
            }
            let timezoneOffset = organization.timezone.val;
            if (organization.daylightSavings) timezoneOffset++;
            let timezoneOffsetStr = '';
            timezoneOffsetStr = timezoneOffset.toString();
            timezoneOffsetStr = Math.abs(timezoneOffsetStr.split('.').shift()).toString();
            if (timezoneOffsetStr.length === 1) timezoneOffsetStr = '0' + timezoneOffsetStr;
            if (timezoneOffset >= 0) timezoneOffsetStr = '+' + timezoneOffsetStr;
            else timezoneOffsetStr = '-' + timezoneOffsetStr;

            let minStr = (Math.abs(timezoneOffset % 1) * 60).toString();
            if (minStr.length === 1) minStr = minStr + '0';
            timezoneOffsetStr += ':' + minStr;
            timezoneOffset = -timezoneOffset;
            startDate.setTime(startDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + (24 * 60 * 60 - 1) * 1000);

            BotUser.aggregate([
                {
                    $match: {
                        organizationId: mongoose.Types.ObjectId(organizationId),
                        // isAppliedForHandOff: true,
                        createdAt: { $gte: startDate },
                        updatedAt: { $lte: endDate },
                    },
                },
                {
                    $group: {
                        _id: null,
                        userId: { $push: '$userId' },
                    },
                },
            ])
                .allowDiskUse(true)
                .then((botUsers) => {
                    if (!botUsers[0] || botUsers[0].userId.length === 0) {
                        Logger.log.info('No user found between given dates.');
                        let emptyResponseObj = {};
                        let emptyArrToSend = [];
                        for (let i = 0; i < 24; i++) {
                            emptyResponseObj[i] = 0;
                        }
                        if (meridiem === 'am') {
                            Object.keys(emptyResponseObj).forEach((hour) => {
                                if (parseInt(hour) < 12)
                                    emptyArrToSend.push({
                                        hour:
                                            (parseInt(hour) === 0 ? 12 : hour) +
                                            'AM - ' +
                                            (parseInt(hour) + 1) +
                                            (parseInt(hour) + 1 === 12 ? 'PM' : 'AM'),
                                        count: emptyResponseObj[hour],
                                    });
                            });
                        } else {
                            Object.keys(emptyResponseObj).forEach((hour) => {
                                if (parseInt(hour) >= 12)
                                    emptyArrToSend.push({
                                        hour:
                                            (parseInt(hour) - 12 === 0 ? 12 : parseInt(hour) - 12) +
                                            'PM - ' +
                                            (parseInt(hour) - 12 + 1) +
                                            (parseInt(hour) - 12 + 1 === 12 ? 'AM' : 'PM'),
                                        count: emptyResponseObj[hour],
                                    });
                            });
                        }
                        return res.status(200).send(emptyArrToSend);
                    }
                    BotUserTranscript.aggregate([
                        {
                            $match: {
                                userId: {
                                    $in: botUsers[0].userId,
                                },
                            },
                        },
                        {
                            $unwind: {
                                path: '$transcript',
                            },
                        },
                        {
                            $match: {
                                $or: [
                                    {
                                        'transcript.isFrom': 'agent',
                                    },
                                    {
                                        'transcript.isFrom': 'bot',
                                    },
                                ],
                            },
                        },
                        {
                            $project: {
                                userId: '$userId',
                                isFrom: '$transcript.isFrom',
                                hourNumber: {
                                    $dateToString: {
                                        format: '%H',
                                        date: '$transcript.timestamp',
                                        timezone: timezoneOffsetStr,
                                    },
                                },
                            },
                        },
                        {
                            $group: {
                                _id: {
                                    hourNumber: '$hourNumber',
                                    chatWith: '$isFrom',
                                },
                                hourUser: {
                                    $addToSet: '$userId',
                                },
                            },
                        },
                        {
                            $project: {
                                count: {
                                    $size: '$hourUser',
                                },
                            },
                        },
                    ])
                        .allowDiskUse(true)
                        .then((hourWiseUsers) => {
                            // console.log('hourWiseUsers::', hourWiseUsers)
                            let arrToSend = [];
                            let responseObj = {};
                            for (let i = 0; i < 24; i++) {
                                responseObj[i] = { chatWithBot: 0, chatWithAgent: 0 };
                            }
                            hourWiseUsers.forEach((data) => {
                                if (data._id.chatWith === 'bot')
                                    responseObj[parseInt(data._id.hourNumber)]['chatWithBot'] = data.count;
                                if (data._id.chatWith === 'agent')
                                    responseObj[parseInt(data._id.hourNumber)]['chatWithAgent'] = data.count;
                            });
                            if (meridiem === 'am') {
                                Object.keys(responseObj).forEach((hour) => {
                                    if (parseInt(hour) < 12)
                                        arrToSend.push({
                                            hour:
                                                (parseInt(hour) === 0 ? 12 : hour) +
                                                'AM - ' +
                                                (parseInt(hour) + 1) +
                                                (parseInt(hour) + 1 === 12 ? 'PM' : 'AM'),
                                            // count: responseObj[hour]['chatWithBot'],
                                            chatWithBot: responseObj[hour]['chatWithBot'],
                                            chatWithAgent: responseObj[hour]['chatWithAgent'],
                                        });
                                });
                            } else {
                                Object.keys(responseObj).forEach((hour) => {
                                    if (parseInt(hour) >= 12)
                                        arrToSend.push({
                                            hour:
                                                (parseInt(hour) - 12 === 0 ? 12 : parseInt(hour) - 12) +
                                                'PM - ' +
                                                (parseInt(hour) - 12 + 1) +
                                                (parseInt(hour) - 12 + 1 === 12 ? 'AM' : 'PM'),
                                            // count: responseObj[hour]['chatWithBot'],
                                            chatWithBot: responseObj[hour]['chatWithBot'],
                                            chatWithAgent: responseObj[hour]['chatWithAgent'],
                                        });
                                });
                            }
                            res.status(200).send(arrToSend);
                        })
                        .catch((err) => {
                            Logger.log.error('Error fetching analytics by aggregate for most communicated hours.');
                            Logger.log.error(err.message || err);
                            return res
                                .status(500)
                                .send({ message: 'Error fetching analytics for most communicated hours.' });
                        });
                })
                .catch((err) => {
                    Logger.log.error('Error fetching bot users for analytics for most communicated hours.');
                    Logger.log.error(err.message || err);
                    return res.status(500).send({ message: 'Error fetching analytics for most communicated hours.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching organization details for analytics for initiated by call.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error fetching analytics for most communicated hours.' });
        });
});

/**
 * Gets number of Requests for given day(s)
 */
router.get('/total-users', function(req, res) {
    Logger.log.info('In most communicating hours analytics call');
    let organizationId = req.query.organizationId;
    let maxColumns = 12;
    //TODO, by default, startDate and endDate have time  00:00:00:000
    let currentTime = new Date();
    let startDate =
        new Date(req.query.startDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    let endDate =
        new Date(req.query.endDate) ||
        new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organisation Id not found in query params.' });
    }
    if (endDate < startDate) {
        Logger.log.error("End Date can't be lesser than Start Date.", startDate, endDate);
        return res.status(400).send({ message: "End Date can't be lesser than Start Date." });
    }
    Organization.findById(organizationId)
        .select({ timezone: 1, daylightSavings: 1 })
        .exec()
        .then((organization) => {
            if (!organization) {
                Logger.log.error('No Organization data found for the organization Id.', organizationId);
                return res.status(400).send({ message: 'No Organisation data found.' });
            }
            let timezoneOffset = organization.timezone.val;
            if (organization.daylightSavings) timezoneOffset++;
            timezoneOffset = -timezoneOffset;
            let timezoneOffsetStr = timezoneOffset.toString();
            if (timezoneOffset % 1 !== 0) {
                timezoneOffsetStr = timezoneOffsetStr.split('.').shift() + (timezoneOffset % 1) * 60;
            }
            startDate.setTime(startDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + timezoneOffset * 60 * 60 * 1000);
            endDate.setTime(endDate.getTime() + (24 * 60 * 60 - 1) * 1000);
            let oneDay = 24 * 60 * 60 * 1000;
            let totalDays = Math.round(Math.abs((endDate.getTime() - startDate.getTime()) / oneDay));
            let groupOf = Math.ceil(totalDays / maxColumns);
            let completeGroups = Math.floor(totalDays / groupOf);
            let remaining = totalDays - completeGroups * groupOf;
            let totalBars = completeGroups;
            if (remaining) totalBars++;
            let responseObj = {};
            for (let i = 0; i < totalBars; i++) {
                let tempDate = new Date(startDate.getTime());
                responseObj[tempDate.setDate(tempDate.getDate() + i * groupOf)] = {
                    withAgent: 0,
                    withBot: 0,
                };
            }
            BotUser.aggregate([
                {
                    $match: {
                        organizationId: mongoose.Types.ObjectId(organizationId),
                        createdAt: { $gte: startDate },
                        updatedAt: { $lte: endDate },
                    },
                },
                {
                    $group: {
                        _id: null,
                        userId: { $push: '$userId' },
                    },
                },
            ])
                .allowDiskUse(true)
                .then((botUsers) => {
                    if (!botUsers[0] || botUsers[0].userId.length === 0) {
                        Logger.log.info('No user found between given dates.');
                        let finalEmptyResponseObj = {};
                        for (let i = 0; i < Object.keys(responseObj).length; i++) {
                            finalEmptyResponseObj[new Date(parseInt(Object.keys(responseObj)[i]))] =
                                responseObj[Object.keys(responseObj)[i]];
                        }
                        return res.status(200).send(finalEmptyResponseObj);
                    }
                    BotUserTranscript.aggregate([
                        {
                            $match: {
                                userId: {
                                    $in: botUsers[0].userId,
                                },
                            },
                        },
                        {
                            $unwind: {
                                path: '$transcript',
                            },
                        },
                        {
                            $match: {
                                $or: [
                                    {
                                        'transcript.isFrom': 'agent',
                                    },
                                    {
                                        'transcript.isFrom': 'bot',
                                    },
                                ],
                            },
                        },
                        {
                            $project: {
                                userId: '$userId',
                                chatDate: '$transcript.timestamp',
                                isFrom: '$transcript.isFrom',
                            },
                        },
                        {
                            $group: {
                                _id: {
                                    month: {
                                        $month: {
                                            date: '$chatDate',
                                            timezone: '+0000',
                                        },
                                    },
                                    day: {
                                        $dayOfMonth: {
                                            date: '$chatDate',
                                            timezone: '+0000',
                                        },
                                    },
                                    year: {
                                        $year: {
                                            date: '$chatDate',
                                            timezone: '+0000',
                                        },
                                    },
                                    chatWith: '$isFrom',
                                },
                                dayUser: {
                                    $addToSet: '$userId',
                                },
                            },
                        },
                        {
                            $project: {
                                count: {
                                    $size: '$dayUser',
                                },
                            },
                        },
                    ])
                        .allowDiskUse(true)
                        .then((results) => {
                            results.forEach((dayRecord) => {
                                let dayDate = new Date(
                                    dayRecord._id.year,
                                    dayRecord._id.month - 1,
                                    dayRecord._id.day,
                                    0,
                                    0,
                                    0,
                                );
                                dayDate.setTime(dayDate.getTime() + -dayDate.getTimezoneOffset() * (60 * 1000));
                                for (let j = 0; j < Object.keys(responseObj).length; j++) {
                                    if (
                                        parseInt(Object.keys(responseObj)[j]) < dayDate.getTime() &&
                                        (j === Object.keys(responseObj).length - 1 ||
                                            parseInt(Object.keys(responseObj)[j + 1]) > dayDate.getTime())
                                    ) {
                                        if (dayRecord._id.chatWith === 'bot') {
                                            // console.log()
                                            responseObj[Object.keys(responseObj)[j]]['withBot'] =
                                                responseObj[Object.keys(responseObj)[j]]['withBot'] + dayRecord.count;
                                        } else {
                                            responseObj[Object.keys(responseObj)[j]]['withAgent'] =
                                                responseObj[Object.keys(responseObj)[j]]['withAgent'] + dayRecord.count;
                                        }
                                    }
                                }
                            });
                            let finalResponseObj = {};
                            for (let i = 0; i < Object.keys(responseObj).length; i++) {
                                finalResponseObj[new Date(parseInt(Object.keys(responseObj)[i]))] =
                                    responseObj[Object.keys(responseObj)[i]];
                            }
                            res.status(200).send(finalResponseObj);
                        })
                        .catch((err) => {
                            Logger.log.error(
                                'Error fetching analytical results by aggregation for analytics for total users.',
                            );
                            Logger.log.error(err.message || err);
                            return res.status(500).send({ message: 'Error fetching analytics for Total Users.' });
                        });
                })
                .catch((err) => {
                    Logger.log.error('Error fetching user list for analytics for total users.');
                    Logger.log.error(err.message || err);
                    return res.status(500).send({ message: 'Error fetching analytics for Total Users.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error fetching organization details for analytics for initiated by call.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error fetching analytics for Total Users.' });
        });
});

/**
 * Export Router
 */
module.exports = router;
