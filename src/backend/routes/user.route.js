/**
 * 3rd Party APIs and Models
 * */
const express = require('express');
const router = express.Router();
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
let mongoose = require('mongoose');
let Organization = mongoose.model('organization');
let Bot = mongoose.model('bot');
let Df = mongoose.model('dialogflow');
const UserAgent = mongoose.model('user-agent');
/**
 * Config
 * */
const config = require('../config');
/**
 * Helper
 * */
const MailHelper = require('./../helper/mailer.helper');
const adminMiddleware = require('./authenticate').adminMiddleWare;
/**
 * Services
 * */
const Logger = require('./../services/logger');

const uploadProfilePath = path.resolve(__dirname, '../upload/' + getProfileImagePath());
//Custom Multer storage engine
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, uploadProfilePath);
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
    },
});
const upload = multer({ dest: uploadProfilePath, storage: storage });

/**
 * Upload for profile-picture of User.
 */
router.post('/upload/profile-picture', upload.single('profile-picture'), (req, res) => {
    let userId = req.user._id;
    if (!userId) {
        Logger.log.error('User id not found in the logged in user');
        return res.status(400).send({
            status: 'USER_NOT_FOUND',
            message: 'User not found, please try by logging in again.',
        });
    }
    UserAgent.findByIdAndUpdate(userId, { profilePicture: req.file.filename }, { new: true })
        .select({
            name: 1,
            email: 1,
            contactNumber: 1,
            profilePicture: 1,
        })
        .exec()
        .then((userData) => {
            userData.profilePicture = getProfileUrl(userData.profilePicture);
            userData.host = config.BaseUrl + getProfileImagePath();
            userData.file = req.file.filename;
            res.status(200).send(userData);
        })
        .catch((err) => {
            Logger.log.error('Error uploading profile picture');
            Logger.log.error(err.message || err);
            res.status(500).send('Error uploading profile picture.');
        });
    if (req.query.oldImageName) {
        Logger.log.info('Old image name:', req.query.oldImageName);
        let imagePath = path.resolve(__dirname + '/../upload/' + getProfileImagePath() + req.query.oldImageName);
        fs.unlink(imagePath, (err) => {
            if (err) {
                Logger.log.warn(
                    `Error deleting profile picture with name: ${req.query.oldImageName} by user ${req.user._id}`,
                );
                Logger.log.warn(err.message || err);
            } else Logger.log.info('Successfully deleted old profile picture.');
        });
    }
});

/**
 * Remove profile-picture of User.
 */
router.delete('/profile-picture', (req, res) => {
    let userId = req.user._id;
    if (!userId) {
        Logger.log.error('User id not found in the logged in user');
        return res.status(400).send({
            status: 'USER_NOT_FOUND',
            message: 'User not found, please try by logging in again.',
        });
    }
    if (!req.query.oldImageName) {
        Logger.log.error('In delete profile picture call, old image name not present for the user:', userId);
        return res.status(400).send({
            status: 'IMAGE_NAME_NOT_FOUND',
            message: 'Image name not found, unable to remove old profile picture.',
        });
    }
    Logger.log.info('Old image name:', req.query.oldImageName);
    let imagePath = path.resolve(__dirname + '/../upload/' + getProfileImagePath() + req.query.oldImageName);
    fs.unlink(imagePath, (err) => {
        if (err) {
            Logger.log.warn(
                `Error deleting profile picture with name: ${req.query.oldImageName} by user ${req.user._id}`,
            );
            Logger.log.warn(err.message || err);
            return res.status(500).send('Error removing profile picture.');
        } else {
            Logger.log.info('Successfully deleted old profile picture.');
            UserAgent.findByIdAndUpdate(userId, { profilePicture: null }, { new: true })
                .select({
                    name: 1,
                    email: 1,
                    contactNumber: 1,
                    profilePicture: 1,
                })
                .exec()
                .then((userData) => {
                    res.status(200).send(userData);
                })
                .catch((err) => {
                    Logger.log.error('Error deleting profile picture');
                    Logger.log.error(err.message || err);
                    res.status(500).send('Error removing profile picture.');
                });
        }
    });
});

/**
 * Gets the Profile
 */
router.get('/profile', function(req, res) {
    Logger.log.info('In get profile call');
    if (!req.user || !req.user._id) {
        Logger.log.error('User data not found in req');
        return res.status(401).send({ message: 'Please first login to update the profile.' });
    }
    UserAgent.findById(req.user._id)
        .select({ name: 1, role: 1, email: 1, contactNumber: 1, profilePicture: 1 })
        .exec()
        .then((userData) => {
            userData.profilePicture = getProfileUrl(userData.profilePicture);
            Logger.log.info('Fetched user details');
            res.status(200).send(userData);
        })
        .catch((err) => {
            Logger.log.error("Error fetching user's profile.");
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: "Error fetching user's profile." });
        });
});

/**
 * Get details of a user
 */
router.get('/:userId', adminMiddleware, function(req, res) {
    Logger.log.info('In get user details call');
    if (req.params.userId && mongoose.Types.ObjectId.isValid(req.params.userId)) {
        let userId = req.params.userId;
        UserAgent.findById(userId)
            .select({ name: 1, email: 1, contactNumber: 1, role: 1 })
            .exec()
            .then((userData) => {
                Logger.log.info('Fetched details of user successfully.');
                res.status(200).send(userData);
            })
            .catch((err) => {
                Logger.log.error('Error getting details of bot.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error getting details of bot.' });
            });
    } else {
        Logger.log.error('User id not found in query params.');
        return res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Get the List of User
 */
router.get('/', function(req, res) {
    Logger.log.info('In list user call');
    if (req.query.organizationId && mongoose.Types.ObjectId.isValid(req.query.organizationId)) {
        Logger.log.info('Organization Id:', req.query.organizationId);
        let queryFilter = {
            isArchived: false,
            organizationId: req.query.organizationId,
        };
        if (req.query.search) queryFilter.name = { $regex: req.query.search, $options: 'i' };
        let option = {
            page: parseInt(req.query.page) || 1,
            limit: parseInt(req.query.limit) || 5,
        };
        option.select = { name: 1, email: 1, role: 1, createdAt: 1, contactNumber: 1, signUpToken: 1 };
        option.sort = { createdAt: 'desc' };
        UserAgent.paginate(queryFilter, option)
            .then((results) => {
                let responseObj = JSON.parse(JSON.stringify(results));
                if (responseObj && responseObj.docs && responseObj.docs.length !== 0) {
                    responseObj.docs.forEach((user) => {
                        if (!user.signUpToken) user.status = 'active';
                        else user.status = 'pending';
                        delete user.signUpToken;
                    });
                }
                res.status(200).send(responseObj);
            })
            .catch((err) => {
                Logger.log.error('Error getting list of users.');
                Logger.log.error(err.message || err);
                return res.status(500).send({ message: 'Error getting list of users.' });
            });
    } else {
        Logger.log.error('Organization Id not found in query params.');
        return res.status(400).send({ message: 'Organization Id not found in query params.' });
    }
});

/**
 * Creates User
 */
router.post('/', adminMiddleware, function(req, res) {
    Logger.log.info('In create user call');
    if (!req.body.organizationId) {
        Logger.log.error('Organization id not found in req');
        return res.status(400).send({ message: 'Organisation id not found in request.' });
    }
    if (!req.body.email) {
        Logger.log.error('Email not present for new user');
        return res.status(400).send({ message: 'Please enter email for new user.' });
    }
    if (!req.body.role || (req.body.role !== 'supportAgent' && req.body.role !== 'admin')) {
        Logger.log.error('Role not present for new user');
        return res.status(400).send({ message: 'Please enter role for new user.' });
    }
    UserAgent.findOne({ email: req.body.email, isArchived: false })
        .exec()
        .then((userData) => {
            if (userData) {
                Logger.log.error('User with provided email already exist.');
                return res.status(400).send({ message: 'User already exists in another organisation.' });
            }
            let objToSave = req.body;
            objToSave.createdBy = req.user._id;
            UserAgent.create(objToSave)
                .then((newUser) => {
                    Logger.log.info('New user created successfully.');
                    let signUpToken = jwt.sign(JSON.stringify({ _id: newUser._id }), config.jwtSecret);
                    // let message = 'Hello';
                    // message += (newUser.name ? ' ' + newUser.name + '' : '') + '!\n';
                    // message += 'Please click on the following button/link to set your password:\n';
                    // message += 'Set Password Link: ' + 'set/' + newUser._id + '?token=' + signUpToken;
                    let mailObj = {
                        toAddress: [newUser.email],
                        subject: 'Welcome to Covid-19 Bot by Kevit',
                        text: {
                            name: newUser.name ? newUser.name : '',
                            setPasswordLink:
                                config.frontEndURLs.adminPanelBase +
                                config.frontEndURLs.setPasswordPage +
                                newUser._id +
                                '?token=' +
                                signUpToken,
                        },
                        mailFor: 'newUser',
                    };
                    UserAgent.findByIdAndUpdate(newUser._id, { signUpToken: signUpToken }, { new: true })
                        .then((results) => {
                            Logger.log.info('User created successfully.');
                            let responseObj = {
                                name: newUser.name,
                                email: newUser.email,
                                contactNumber: newUser.contactNumber,
                                role: newUser.role,
                            };
                            res.status(200).send({ message: 'User created successfully.' });
                            MailHelper.sendMail(mailObj)
                                .then((results) => {
                                    Logger.log.info('Mail sent to new user successfully.');
                                    let responseObj = {
                                        name: newUser.name,
                                        email: newUser.email,
                                        contactNumber: newUser.contactNumber,
                                        role: newUser.role,
                                    };
                                })
                                .catch((err) => {
                                    Logger.log.error('Error sending mail while creating new user.');
                                    Logger.log.error(err.message || err);
                                    return res.status(500).send({ message: 'Error creating new user.' });
                                });
                        })
                        .catch((err) => {
                            Logger.log.error('Error sending mail to new user.');
                            Logger.log.error(err.message || err);
                            UserAgent.findByIdAndDelete(newUser._id)
                                .then((deletedUser) => {
                                    Logger.log.warn('Deleted new user as mail sender is not working.');
                                    return res.status(500).send({ message: 'Error creating new user.' });
                                })
                                .catch((err) => {
                                    Logger.log.error('Error deleting user after creating it.');
                                    Logger.log.error(err.message || err);
                                    return res.status(500).send({ message: 'Error creating new user.' });
                                });
                        });
                })
                .catch((err) => {
                    Logger.log.error('Error creating new user.');
                    Logger.log.error(err.message || err);
                    return res.status(500).send({ message: 'Error creating new user.' });
                });
        })
        .catch((err) => {
            Logger.log.error('Error finding user for creating new one.');
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Error creating new user.' });
        });
});

/**
 * Updates User - Profile
 */
router.put('/profile', function(req, res) {
    Logger.log.info('In user update profile call');
    if (!req.user || !req.user._id) {
        Logger.log.error('User data not found in req');
        return res.status(401).send({ message: 'Please first login to update the profile.' });
    }
    let updateObj = {};
    if (req.body.name) updateObj.name = req.body.name;
    if (req.body.contactNumber) updateObj.contactNumber = req.body.contactNumber;
    UserAgent.findByIdAndUpdate(req.user._id, updateObj, { new: true })
        .select({ name: 1, role: 1, email: 1, contactNumber: 1, profilePicture: 1 })
        .exec()
        .then((userData) => {
            userData.profilePicture = getProfileUrl(userData.profilePicture);
            Logger.log.info('Updated user profile.');
            res.status(200).send(userData);
        })
        .catch((err) => {
            Logger.log.error("Error updating user's profile.");
            Logger.log.error(err.message || err);
            res.status(500).send({ message: "Error updating user's profile." });
        });
});

/**
 * Updates a User
 */
router.put('/:userId/', adminMiddleware, function(req, res) {
    Logger.log.info('In user update call');
    if (req.params.userId && mongoose.Types.ObjectId.isValid(req.params.userId)) {
        let userId = req.params.userId;
        let updateObj = {};
        if (req.body.name) updateObj.name = req.body.name;
        if (req.body.contactNumber) updateObj.contactNumber = req.body.contactNumber;
        if (req.body.role) updateObj.role = req.body.role;
        UserAgent.findByIdAndUpdate(userId, updateObj, { new: true })
            .select({
                name: 1,
                email: 1,
                contactNumber: 1,
                role: 1,
            })
            .exec()
            .then((userData) => {
                Logger.log.info('User Updated successfully.');
                res.status(200).send({ message: 'User updated successfully.' });
                if (req.body.notifyUser && Object.keys(updateObj).length > 0) {
                    let updatedBy = req.user.name;
                    let mailObject = {
                        toAddress: [userData.email],
                        subject: 'Profile Update',
                        text: {
                            name: userData.name,
                            updateFields: updateObj,
                            updatedBy: updatedBy,
                        },
                        mailFor: 'profileUpdate',
                    };
                    MailHelper.sendMail(mailObject)
                        .then((mailResult) => {
                            Logger.log.info('Mail sent successfully to user.');
                        })
                        .catch((err) => {
                            Logger.log.error('Error sending mail to user:', err.message || err);
                            let errorMessage = 'Error sending mail to user.';
                            return res.status(500).send({ message: errorMessage });
                        });
                }
            })
            .catch((err) => {
                Logger.log.error('Error updating the user:', err.message || err);
                let errorMessage = 'Error updating the user.';
                return res.status(500).send({ message: errorMessage });
            });
    } else {
        Logger.log.error('User id not found in request query params.');
        return res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Deletes a user
 */
router.delete('/:userId', adminMiddleware, function(req, res) {
    Logger.log.info('In delete user call');
    if (req.params.userId && mongoose.Types.ObjectId.isValid(req.params.userId)) {
        let userId = req.params.userId;
        //Not added {new: true}, since dialogflow id is used
        UserAgent.findByIdAndUpdate(userId, { isArchived: true, webToken: [], fcmToken: [] })
            .then((userData) => {
                if (!userData) {
                    Logger.log.error('No user found with given id: ', userId);
                    res.status(400).send({ message: 'No user data found.' });
                }
                res.status(200).send({ message: 'User deleted successfully.' });
            })
            .catch((err) => {
                Logger.log.error('Error deleting user:', err.message || err);
                let errorMessage = 'Error deleting user.';
                return res.status(500).send({ message: errorMessage });
            });
    } else {
        Logger.log.error('User id not found.');
        return res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Helper Functions
 */
function getProfileImagePath() {
    return config.uploadLocations.user.base + config.uploadLocations.user.profile;
}

function getProfileUrl(imageName) {
    if (imageName)
        if (imageName.indexOf(config.BaseUrl + getProfileImagePath()) !== -1) return imageName;
        else return config.BaseUrl + getProfileImagePath() + imageName;
    return '';
}

/**
 * Export Router
 */
module.exports = router;
