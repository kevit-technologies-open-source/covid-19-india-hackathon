const express = require('express');
const router = express.Router();
const parseDomain = require('parse-domain');
let mongoose = require('mongoose');
// let webshot = require('webshot');
let Bot = mongoose.model('bot');
let BotUser = mongoose.model('user-botuser');
/**
 * Config
 * */
const config = require('../config');
/**
 * Services
 * */
const Logger = require('./../services/logger');

/**
 * Sends widget configuration by receiving organization's id
 */
router.get('/configuration', function(req, res) {
    let organizationId = req.query.organizationId;
    let hostOrigin = req.headers.referer || req.headers.origin;
    let extractedDomain = parseDomain(hostOrigin);
    let originUrl = '';
    if (extractedDomain) {
        if (extractedDomain.domain) originUrl += extractedDomain.domain;
        if (extractedDomain.subdomain) originUrl = extractedDomain.subdomain + '.' + originUrl;
        if (extractedDomain.tld) originUrl = originUrl + '.' + extractedDomain.tld;
    }
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error(
            'OrganizationId not found or of invalid type while loading bot on website. OrganizationId:',
            organizationId,
        );
        return res.status(400).send({ status: 'invalid_organization_id', message: 'Invalid organisation id found.' });
    }
    Bot.findOne({ isArchived: false, organizationId: organizationId })
        .select({ botName: 1, organizationId: 1, putOnHold: 1, widget: 1, delay: 1, siteUrls: 1, welcomeMessage: 1 })
        .then((bot) => {
            if (!bot) {
                Logger.log.error('No bot data found for the organization:', organizationId);
                return res
                    .status(400)
                    .send({ status: 'bot_not_found', message: 'No bot found for the given organisation.' });
            }
            // if (bot && !bot.siteUrls.includes(originUrl)) {
            //     Logger.log.error('Bot is not hosted for given url, for organization:', organizationId, originUrl);
            //     return res.status(400).send({
            //         status: 'site_not_listed',
            //         message: 'Site is not listed to run the bot, please contact admin.',
            //     });
            // }
            Logger.log.info('Fetched bot details successfully for initial loading.');
            if (bot.widget.iconUrl) bot.widget.iconUrl = getBotIconUrl(bot.widget.iconUrl);
            res.status(200).send(bot);
        })
        .catch((err) => {
            Logger.log.error('Error getting bot details for organization:', organizationId);
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Something went wrong, please try again later.' });
        });
});

/**
 * Binds name, email, contact number of user to bot-user db document
 */
router.put('/contact-form/:userId', function(req, res) {
    let organizationId = req.query.organizationId;
    let userId = req.params.userId;
    if (!organizationId || !mongoose.Types.ObjectId.isValid(organizationId)) {
        Logger.log.error(
            'OrganizationId not found or of invalid type while loading bot on website. OrganizationId:',
            organizationId,
        );
        return res.status(400).send({ status: 'invalid_organization_id', message: 'Invalid organization id found.' });
    }
    BotUser.findOneAndUpdate({ isArchived: false, organizationId: organizationId, userId: userId }, req.body, {
        new: true,
        upsert: true,
    })
        .then((botUser) => {
            if (!botUser) {
                Logger.log.error(`No bot user found for the organization: ${organizationId} and user id: ${userId}`);
                return res.status(400).send({ status: 'user_not_found', message: 'No User found for given details.' });
            }
            Logger.log.info('User form details updated successfully.');
            res.status(200).send({
                status: 'user_update_success',
                message: 'User contact details updated successfully.',
            });
        })
        .catch((err) => {
            Logger.log.error('Error getting bot details for organization:', organizationId);
            Logger.log.error(err.message || err);
            return res.status(500).send({ message: 'Something went wrong, please try again later.' });
        });
});

/**
 * Helper Functions
 */

function getBotIconPath() {
    return config.uploadLocations.bot.base + config.uploadLocations.bot.icon;
}

function getBotIconUrl(imageName) {
    if (imageName)
        if (imageName.indexOf(config.BaseUrl + getBotIconPath()) !== -1) return imageName;
        else return config.BaseUrl + getBotIconPath() + imageName;
    return '';
}

/**
 * Export Router
 */
module.exports = router;
