/**
 * 3rd Party APIs and Models
 * */
const express = require('express');
const router = express.Router();
let mongoose = require('mongoose');
let Bot = mongoose.model('bot');
let Df = mongoose.model('dialogflow');
const Organization = mongoose.model('organization');
/**
 * Services
 * */
const Logger = require('./../services/logger');

/**
 * Get details of a bot
 */
router.get('/:botId', function(req, res) {
    Logger.log.info('In get bot details call');
    if (req.params.botId && mongoose.Types.ObjectId.isValid(req.params.botId)) {
        let botId = req.params.botId;
        Bot.findById(botId)
            .select({ botName: 1, siteUrls: 1 })
            .populate({
                path: 'dialogflow',
                select: ['projectId'],
            })
            .then((botData) => {
                Logger.log.info('Fetched details of bot successfully.');
                res.status(200).send(botData);
            })
            .catch((err) => {
                Logger.log.error('Error getting details of bot.');
                Logger.log.error(err.message || err);
                res.status(500).send({ message: 'Error getting details of bot.' });
            });
    } else {
        Logger.log.error('Bot id not found.');
        res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Get the List of Bot
 */
router.get('/', function(req, res) {
    Logger.log.info('In list bot call');
    let queryFilter = {
        isArchived: false,
    };
    if (req.query.search) queryFilter.botName = { $regex: req.query.search, $options: 'i' };
    let option = {
        page: parseInt(req.query.page) || 1,
        limit: parseInt(req.query.limit) || 5,
    };
    option.populate = {
        path: 'organizationId',
        select: ['name', 'originAdminId'],
        populate: {
            path: 'originAdminId',
            select: ['name'],
        },
    };
    option.select = { botName: 1, createdAt: 1, organizationId: 1, putOnHold: 1 };
    option.sort = { createdAt: 'desc' };
    Bot.paginate(queryFilter, option)
        .then((results) => {
            let responseArr = [];
            if (results.docs && results.docs.length > 0) {
                let botDetails = results.docs;
                botDetails.forEach((bot) => {
                    let botObj = {
                        botName: bot.botName,
                        organizationName: bot.organizationId.name,
                        adminName: bot.organizationId.originAdminId.name,
                        createdDate: bot.createdAt,
                        _id: bot._id,
                        putOnHold: bot.putOnHold,
                    };
                    responseArr.push(botObj);
                });
            }
            results.docs = responseArr;
            res.status(200).send(results);
        })
        .catch((err) => {
            Logger.log.error('Error getting list of bots.');
            Logger.log.error(err.message || err);
            res.status(500).send({ message: 'Error getting list of bots.' });
        });
});

/**
 * Puts a bot on hold
 */
router.put('/:botId/putonhold/', function(req, res) {
    Logger.log.info('In bot put on hold call');
    if (req.params.botId && mongoose.Types.ObjectId.isValid(req.params.botId)) {
        let botId = req.params.botId;
        let status = req.body.status;
        Bot.findByIdAndUpdate(botId, { putOnHold: status }, { new: true })
            .then((results) => {
                Logger.log.info('Bot put on-hold successfully.');
                let responseMessage = '';
                if (status) responseMessage = 'Bot put on-hold successfully.';
                else responseMessage = 'Bot released from hold successfully.';
                res.status(200).send({ message: responseMessage });
            })
            .catch((err) => {
                Logger.log.error('Error putting bot on hold:', err.message || err);
                let errorMessage = 'Error putting bot on hold.';
                res.status(500).send({ message: errorMessage });
            });
    } else {
        Logger.log.error('Bot id not found.');
        res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Deletes bot
 */
router.delete('/:botId', function(req, res) {
    Logger.log.info('In delete bot call');
    if (req.params.botId && mongoose.Types.ObjectId.isValid(req.params.botId)) {
        let botId = req.params.botId;
        //Not added {new: true}, since dialogflow id is used
        Bot.findByIdAndUpdate(botId, { isArchived: true, dialogflow: null })
            .then((botData) => {
                if (!botData) {
                    Logger.log.error('No bot found with given id: ', botId);
                    res.status(400).send({ message: 'No bot data found.' });
                }
                let promiseArr = [];
                promiseArr.push(Df.findByIdAndUpdate(botData.dialogflow, { isArchived: true }, { new: true }));
                promiseArr.push(
                    Organization.findByIdAndUpdate(
                        botData.organizationId,
                        { previousBot: { name: botData.botName, deleteDate: new Date() } },
                        { new: true },
                    ),
                );
                Promise.all(promiseArr)
                    .then((botData) => {
                        Logger.log.info('Bot deleted successfully.');
                        res.status(200).send({ message: 'Bot deleted successfully.' });
                    })
                    .catch((err) => {
                        Logger.log.error('Error deleting organization:', err.message || err);
                        let errorMessage = 'Error deleting organization.';
                        res.status(500).send({ message: errorMessage });
                    });
            })
            .catch((err) => {
                Logger.log.error('Error deleting organization:', err.message || err);
                let errorMessage = 'Error deleting organization.';
                res.status(500).send({ message: errorMessage });
            });
    } else {
        Logger.log.error('Organization id not found.');
        res.status(400).send({ message: 'Something went wrong, please try again.' });
    }
});

/**
 * Export Router
 */
module.exports = router;
