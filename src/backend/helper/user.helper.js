const botUser = require('../models/user-botuser.model');

let getBotUserNameById = (botUserId) => {
    return new Promise((resolve, reject) => {
        botUser.findOne({ userId: botUserId }).then((userName) => {
            if (userName) {
                resolve(userName.name);
            }
        });
    });
};
module.exports = {
    getBotUserNameById,
};
