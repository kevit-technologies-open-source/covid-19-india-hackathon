const mongoose = require('mongoose');
const UserAgent = mongoose.model('user-agent');

let createSuperAdmin = () => {
    return new Promise((resolve, reject) => {
        UserAgent.findOne({ isArchived: false, email: 'admin@kevit.io' }).then(superAdmin => {
            if (superAdmin) {
                return resolve();
            }
            UserAgent.create({
                'role': 'superAdmin',
                'isArchived': false,
                'name': 'Kevit Technologies',
                'email': 'admin@kevit.io',
                'password': 'admin123',
                'contactNumber': '9099904257',
                '__v': 1022,
                'fcmToken': [],
                'profilePicture': null,
                'organizationForSession': [],
            }).then(newSuperAdmin => {
                return resolve();
            }).catch(err => {
                console.log('Error creating superadmin', err.message);
                return reject(err.message);
            });

        }).catch(err => {
            console.log('Error creating superadmin', err.message);
            return reject(err.message);
        });
    });
};

module.exports = {
    createSuperAdmin
}
