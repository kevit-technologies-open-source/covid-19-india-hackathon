const admin = require('firebase-admin');
const Logger = require('./../services/logger');
const serviceAccount = require('../keys/fireBaseKey');
const mongoose = require('mongoose');
const UserAgent = mongoose.model('user-agent');
/**
 * Config
 * */
const config = require('../config');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: config.firebaseDbUrl,
});

/**
 * Function sends json data in form of notification, whch can be received by frontend
 * */
const sendNotification = (tokens, data) => {
    return new Promise((resolve, reject) => {
        tokens.forEach((token) => {
            const payload = {
                token: token,
                data: {
                    payload: JSON.stringify(data),
                },
                webpush: {
                    headers: {
                        TTL: '20',
                    },
                },
            };
            admin
                .messaging()
                .send(payload)
                .then((response) => {
                    Logger.log.debug('NOTIFICATIONS - Sent successfully');
                    resolve(response);
                })
                .catch((err) => {
                    Logger.log.error('NOTIFICATIONS ERROR : ', err.errorInfo.message);
                    if (err.errorInfo.message === 'Requested entity was not found.') {
                        UserAgent.findOneAndUpdate({ fcmToken: token }, { $pull: { fcmToken: token } }, { new: true })
                            .then((data) => {
                                Logger.log.info('Token removed successfully', token);
                                return resolve();
                            })
                            .catch((err) => {
                                Logger.log.error('Error in removing token');
                                Logger.log.error(err.message || err);
                            });
                    }
                    reject(err);
                });
        });
        return resolve();
    });
};

module.exports = {
    sendNotification: sendNotification,
};
