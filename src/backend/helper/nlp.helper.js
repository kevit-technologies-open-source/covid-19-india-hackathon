const axios = require('axios');
const googleAuth = require('google-oauth-jwt');
const mongoose = require('mongoose');
// const fs = require('fs');
// const path = require('path');
const Logger = require('./../services/logger');
/**
 * Models
 * */
const Df = mongoose.model('dialogflow');
// let fileName = 'dialogflow-Agent.json';
// let pathToCredentialFile = path.join(__dirname, '../keys/', fileName);
// let credentials = fs.readFileSync(pathToCredentialFile).toString();
// credentials = JSON.parse(credentials);
// const projectId = credentials.project_id;
// const clientEmail = credentials.client_email;
// const privateKey = credentials.private_key;

/**
 * Helper function for identification process of the intent from the dialogflow
 * */
let identifyIntent = (msg, sessionId, organizationId, context) => {
    return new Promise((resolve, reject) => {
        return getGoogleToken(organizationId)
            .then((df) => {
                Logger.log.info('Token received for identifying intent');
                return getDialogFlowResponse(df.projectId, df.accessToken, msg, sessionId, context);
            })
            .then((dialogFlowResponse) => {
                Logger.log.info('Response received in identify intent method');
                return resolve(dialogFlowResponse);
            })
            .catch((err) => {
                Logger.log.error('Error in identify intent');
                Logger.log.error(err.message || err);
                return reject(err);
            });
    });
};

/**
 * Helper function for fetching intent list from the dialogflow
 * */
let getIntentList = (language, organizationId) => {
    return new Promise((resolve, reject) => {
        return getGoogleToken(organizationId)
            .then((df) => {
                Logger.log.info('Token received for identifying intent');
                return getIntentsFromDialogflow(df.projectId, df.accessToken, language, null);
            })
            .then((dialogFlowResponse) => {
                Logger.log.info('Intent list fetched from dialogflow', dialogFlowResponse);
                return resolve(dialogFlowResponse);
            })
            .catch((err) => {
                Logger.log.error('Error in identify intent');
                Logger.log.error(err.message || err);
                return reject(err);
            });
    });
};

/**
 * Helper function for fetching intent by id from the dialogflow
 * */
let getIntentDetails = (language, intentId, organizationId) => {
    return new Promise((resolve, reject) => {
        return getGoogleToken(organizationId)
            .then((df) => {
                Logger.log.info('Token received for identifying intent');
                return getIntentByIdFromDialogflow(df.projectId, df.accessToken, language, intentId);
            })
            .then((intent) => {
                Logger.log.info('Intent details fetched from dialogflow');
                return resolve(intent);
            })
            .catch((err) => {
                Logger.log.error('Error in identify intent');
                Logger.log.error(err.message || err);
                return reject(err);
            });
    });
};

/**
 * Creates a handoff entry in DB, sends notification to agent, and a proactive message to the bot user
 * */
let getDialogFlowResponse = (projectId, token, msg, sessionId, context) => {
    return new Promise((resolve, reject) => {
        let url =
            'https://dialogflow.googleapis.com/v2/projects/' +
            projectId +
            '/agent/sessions/' +
            sessionId +
            ':detectIntent';
        let dialogflowtoken = token;
        try {
            let DialogflowRequestOptions = {
                method: 'POST',
                url: url,
                // json: true,
                data: {
                    queryInput: {
                        text: {
                            text: msg,
                            languageCode: 'en-us',
                        },
                    },
                },
                headers: {
                    Authorization: 'Bearer ' + dialogflowtoken,
                },
            };

            if (context && context.length !== 0) DialogflowRequestOptions.data.contexts = context;
            axios(DialogflowRequestOptions)
                .then(({ data }) => {
                    return resolve(data);
                })
                .catch((err) => {
                    Logger.log.error('Error in getting dialogflow response');
                    Logger.log.error(err.message || err);
                    return reject(err);
                });
        } catch (err) {
            Logger.log.error('Error in getting dialogflow response');
            Logger.log.error(err.message || err);
            return reject(err);
        }
    });
};

/**
 * Helper function for identification process of the intent from the dialogflow
 * */
let getIntentsFromDialogflow = (projectId, token, language, pageToken) => {
    let output = [];
    let options = {
        method: 'get',
        url: `https://dialogflow.googleapis.com/v2/projects/${projectId}/agent/intents`,
        params: {
            languageCode: language,
            access_token: token,
            pageSize: 1000,
            intentView: 'INTENT_VIEW_UNSPECIFIED',
        },
        headers: { 'Content-Type': 'application/json' },
        responseType: 'json',
    };
    if (pageToken) {
        options.params.pageToken = pageToken;
    }
    Logger.log.info(`Creating a request for listing dialogflow intents at ${new Date()}`);
    return axios(options)
        .then(({ data }) => {
            let result = data;
            let isEmpty = Object.entries(result).length === 0 && result.constructor === Object;
            if (result && !isEmpty) {
                output = output.concat(result.intents);
                if (result.nextPageToken) {
                    return getIntentsFromDialogflow(projectId, token, language, result.nextPageToken).then(
                        (results) => {
                            return output.concat(results);
                        },
                    );
                } else {
                    return output;
                }
            } else {
                return [];
            }
        })
        .catch((err) => {
            Logger.log.error('Error in getting dialogflow response');
            Logger.log.error(err.message || err);
            return Promise.reject(err);
        });
};

/**
 * Helper function for identification process of the intent from the dialogflow
 * */
let getIntentByIdFromDialogflow = (projectId, token, language, intentId) => {
    // let output = [];
    return new Promise((resolve, reject) => {
        let options = {
            method: 'GET',
            url: `https://dialogflow.googleapis.com/v2/projects/${projectId}/agent/intents/${intentId}`,
            params: {
                languageCode: language,
                access_token: token,
                intentView: 'INTENT_VIEW_UNSPECIFIED',
            },
            headers: { 'Content-Type': 'application/json' },
            // json: true,
        };
        Logger.log.info(`Creating a request for getting intent data at ${new Date()}`);
        axios(options)
            .then(({ data }) => {
                let intent = data;
                Logger.log.info(`Received intent details from DF at ${new Date()}`);
                Logger.log.info(intent);
                return resolve(intent);
            })
            .catch((err) => {
                Logger.log.error('Error fetching intent details.');
                Logger.log.error(err.message || err);
                return reject(err);
            });
    });
};

/**
 * Finds token from DB, if not found, fetches new token and stores to DB
 * */
let getGoogleToken = (organizationId) => {
    return new Promise((resolve, reject) => {
        Logger.log.info('Fetching a google token');
        Df.findOne({ isArchived: false, organizationId: organizationId })
            .then((df) => {
                if (df && df.accessToken) {
                    Logger.log.info('Token found from db');
                    return resolve(df);
                } else {
                    Logger.log.warn('Token not found from db');
                    generateDfToken(df.dfJson.client_email, df.dfJson.private_key)
                        .then((token) => {
                            Logger.log.info('New token generated.');
                            Df.findOneAndUpdate(
                                { isArchived: false, organizationId: organizationId },
                                { accessToken: token },
                                { upsert: true, new: true },
                            )
                                .then((updatedDf) => {
                                    Logger.log.warn('Token was not present in database ' + Date.now());
                                    Logger.log.info('Token successfully saved to database at ' + Date.now());
                                    return resolve(updatedDf);
                                })
                                .catch((err) => {
                                    Logger.log.error(err.message || err);
                                    return reject(err);
                                });
                        })
                        .catch((err) => {
                            Logger.log.error(err.message || err);
                            return reject(err);
                        });
                }
            })
            .catch((err) => {
                Logger.log.error(err.message || err);
                return reject(err);
            });
    });
};

/**
 * Requests dialogflow for new token
 * */
let generateDfToken = (email, key) => {
    return new Promise((resolve, reject) => {
        googleAuth.authenticate(
            {
                email: email,
                key: key,
                scopes: ['https://www.googleapis.com/auth/cloud-platform'],
            },
            (err, token) => {
                if (err) {
                    return reject(err);
                }
                return resolve(token);
            },
        );
    });
};

let cronForUpdatingToken = () => {
    return new Promise((resolve, reject) => {
        Df.find({ isArchived: false })
            .then((dfs) => {
                let promiseArr = [];
                dfs.forEach((df) => {
                    promiseArr.push(generateDfToken(df.dfJson.client_email, df.dfJson.private_key));
                });
                Promise.all(promiseArr)
                    .then((tokenResults) => {
                        Logger.log.info('New token generated for all projects.');
                        let updatePromiseArr = [];
                        for (let i = 0; i < tokenResults.length; i++) {
                            updatePromiseArr.push(
                                Df.findOneAndUpdate(
                                    { _id: dfs[i]._id },
                                    { accessToken: tokenResults[i] },
                                    { upsert: true, new: true },
                                ),
                            );
                        }
                        Promise.all(updatePromiseArr)
                            // Df.findOneAndUpdate({ projectId: projectId }, { accessToken: token }, { upsert: true, new: true })
                            .then((updatedDfToken) => {
                                Logger.log.info('Tokens successfully saved to database at' + Date.now());
                                return resolve(updatePromiseArr);
                            })
                            .catch((err) => {
                                Logger.log.error('Error updating Df in database while running cron.');
                                Logger.log.error(err.message || err);
                                return reject(err);
                            });
                    })
                    .catch((err) => {
                        Logger.log.error('Error generating token while running cron.');
                        Logger.log.error(err.message || err);
                        return reject(err);
                    });
            })
            .catch((err) => {
                Logger.log.error('Error finding Df in database while running cron.');
                Logger.log.error(err.message || err);
                return reject(err);
            });
    });
};

module.exports = {
    identifyIntent,
    cronForUpdatingToken,
    getIntentList,
    getIntentDetails,
};
