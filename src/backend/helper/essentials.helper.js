const _ = require('lodash');
const axios = require('axios');
let getStateName = () => {
    return new Promise((resolve, reject) => {
        const stateList = [];
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/resources/resources.json',
        };
        axios(option)
            .then((resourceData) => {
                for (let i = 0; i < resourceData.data.resources.length; i++) {
                    stateList.push(resourceData.data.resources[i].state);
                }
                resolve(
                    _.uniqBy(stateList, function(e) {
                        return e;
                    }),
                );
            })
            .catch((e) => {
                reject(e);
            });
    });
};

let getCityNameByState = (stateName) => {
    return new Promise((resolve, reject) => {
        const cityList = [];
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/resources/resources.json',
        };
        axios(option)
            .then((resourceData) => {
                for (let i = 0; i < resourceData.data.resources.length; i++) {
                    if (resourceData.data.resources[i].state == stateName) {
                        cityList.push(resourceData.data.resources[i].city);
                    }
                }

                resolve(
                    _.uniqBy(cityList, function(e) {
                        return e;
                    }),
                );
            })
            .catch((e) => {
                reject(e);
            });
    });
};
let getEssentialsCategoryByCity = (cityName) => {
    return new Promise((resolve, reject) => {
        let essentialsCategory = '';
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/resources/resources.json',
        };
        axios(option)
            .then((resourceData) => {
                for (let i = 0; i < resourceData.data.resources.length; i++) {
                    if (resourceData.data.resources[i].city == cityName) {
                        essentialsCategory = essentialsCategory + resourceData.data.resources[i].category + '@';
                    }
                }

                resolve(essentialsCategory);
            })
            .catch((e) => {
                reject(e);
            });
    });
};
let getEssentialsDetailsByCaregory = (categoryName, cityName) => {
    console.log(categoryName, cityName, 'daasd0-----------------------');
    return new Promise((resolve, reject) => {
        const essentialsCategory = [];
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/resources/resources.json',
        };
        axios(option)
            .then((resourceData) => {
                for (let i = 0; i < resourceData.data.resources.length; i++) {
                    if (
                        resourceData.data.resources[i].city == cityName &&
                        resourceData.data.resources[i].category == categoryName
                    ) {
                        essentialsCategory.push(resourceData.data.resources[i]);
                    }
                }
                console.log('city------------', essentialsCategory);
                resolve(essentialsCategory);
            })
            .catch((e) => {
                reject(e);
            });
    });
};
module.exports = {
    getStateName: getStateName,
    getCityNameByState: getCityNameByState,
    getEssentialsCategoryByCity: getEssentialsCategoryByCity,
    getEssentialsDetailsByCaregory: getEssentialsDetailsByCaregory,
};
