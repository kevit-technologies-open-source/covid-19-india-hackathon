const { ConsoleTranscriptLogger } = require('botbuilder');
const mongoose = require('mongoose');
const BotUser = mongoose.model('user-botuser');
const BotUserTranscript = mongoose.model('botuser-transcript');
const Logger = require('../services/logger');

class Transcript extends ConsoleTranscriptLogger {
    constructor() {
        super();
    }
    /**
     * Generates transcript object and saves to database
     * */
    logActivity(activity) {
        // console.log("activity in transcript::", JSON.stringify(activity, null, 3));
        let messageNotToStore = false;
        if (activity && activity.text && activity.text.indexOf('00') !== -1) {
            switch (activity.text) {
                case '00DISABLETEXTBOX00':
                    activity.text = 'Yes';
                    break;
                case '00CONTINUEWITHBOT00':
                    activity.text = 'No';
                    break;
                case '00HANDOFFOVER00':
                    activity.text = 'Yes';
                    break;
                case '00CONTINUEWITHAGENT00':
                    activity.text = 'No';
                    break;
                case '00HANDOFFOVERBYUSER00':
                    activity.text = 'Yes';
                    break;
                case '00HANDOFFDENYBYUSER00':
                    activity.text = 'No';
                    break;
                case '00ENABLETEXTBOX00':
                    messageNotToStore = true;
                    break;
                case '00ASKUSERABOUTCOMPLETE00':
                    messageNotToStore = true;
                    break;
            }
        }
        let obj;
        let userId;
        if (activity.from.role === 'user') {
            userId = activity.from.id;
            if (activity.text === '00CLOSE00') {
                BotUser.findOneAndUpdate({ userId: activity.from.id }, { $set: { isConversionOver: true } })
                    .then(() => {
                        Logger.log.info(activity.recipient.id, ' left the conversation');
                    })
                    .catch((err) => {
                        Logger.log.error('Unable to complete a conversation of user..');
                        Logger.log.error(err.message || err);
                    });
                activity.text = 'Closed the conversation';
            }
            if (messageNotToStore) {
                Logger.log.info('Message not to be stored is:', activity.text);
                //this message will not be stored
            } else if (activity.text) {
                obj = {
                    isFromBot: false,
                    text: activity.text,
                    timestamp: new Date(),
                };
            }
        } else {
            userId = activity.recipient.id;
            if (messageNotToStore) {
                Logger.log.info('Message not to be stored is:', activity.text);
                //this message will not be stored
            } else if (activity.text) {
                obj = {
                    isFromBot: true,
                    text: activity.text,
                    timestamp: new Date(),
                };
            } else if (activity.attachments && activity.attachments.length > 0) {
                obj = {
                    isFromBot: true,
                    type: 'attachment',
                    text: activity.attachments[0].content.text,
                    buttons: JSON.stringify(activity.attachments[0].content.buttons),
                    timestamp: new Date(),
                };
                // console.log('Attachment to store::', activity.attachments[0].content);
            }
        }
        if (obj) {
            BotUserTranscript.findOneAndUpdate({ userId: userId }, { $push: { transcript: obj } }, { upsert: true })
                .then((data) => {
                    Logger.log.info('Transcript Updated');
                })
                .catch((err) => {
                    BotUserTranscript.findOneAndUpdate(
                        { userId: userId },
                        { $push: { transcript: obj } },
                        { upsert: true },
                    )
                        .then((data) => {
                            Logger.log.info('Transcript Updated');
                        })
                        .catch((err) => {
                            Logger.log.error('Error in updating transcript');
                            Logger.log.error(err.message || err);
                        });
                });
            BotUser.findOneAndUpdate({ userId: userId }, { $set: { lastInteraction: new Date() } })
                .then(() => {})
                .catch((err) => {});
        }
    }
}
/**
 * Note: This Helper is not now used to store the transcript and is only for reference.
 *  */
/**
 * Returns transcript from the user Id passed of the user
 * */
function getTranscriptBasedOnUserId(id) {
    return new Promise((resolve, reject) => {
        BotUserTranscript.findOne({ userId: id }, { transcript: 1 })
            .then((userTrans) => {
                // console.log(userTrans);
                resolve(userTrans);
            })
            .catch((err) => {
                Logger.log.error('Error in getting Transcript of User');
                Logger.log.error(err.message || err);
            });
    });
}

module.exports = {
    Transcript: Transcript,
    getTranscriptBasedOnUserId: getTranscriptBasedOnUserId,
};
