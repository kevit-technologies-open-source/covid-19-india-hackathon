const moment = require('moment');
const axios = require('axios');
let getLatestNews = () => {
    return new Promise(async (resolve, reject) => {
        let months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/updatelog/log.json',
        };
        let latestNews = await axios(option);
        let messagetmp = `${
            months[new Date().getMonth()]
        }-${new Date().getDate()},${new Date().getFullYear()}</br></br>`;
        for (let i = latestNews.data.length; i > latestNews.data.length - 6; i--) {
            if (latestNews.data[i]) {
                messagetmp =
                    messagetmp +
                    `<b>${new moment(latestNews.data[i].timestamp * 1000)
                        .fromNow()
                        .toString()
                        .replace('an', 'An')}</b></br>${latestNews.data[i].update}</br></br>`;
            }
        }
        resolve(messagetmp);
    });
};
module.exports = {
    getLatestNews: getLatestNews,
};
