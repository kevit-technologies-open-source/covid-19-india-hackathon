const mongoose = require('mongoose');
const BotUser = mongoose.model('user-botuser');
const UserAgent = mongoose.model('user-agent');
const HandOff = mongoose.model('handoff');
const Bot = mongoose.model('bot');
const { ActivityTypes, CardFactory, ActionTypes } = require('botbuilder');

/**
 * Helpers
 * */
let ProactiveHelper;
const FirebaseHelper = require('./firebaseNotification');
const NlpHelper = require('./../helper/nlp.helper');
const TimezoneHelper = require('./timezone.helper');
/**
 * Services
 * */
const Logger = require('./../services/logger');

/**
 * Global objects to maintain 5 min timers
 * */
let timerArr = [];
let warnTimerArr = [];
let totalTimeOutMin = 5;
let warningTimeoutInMin = 1;

/**
 * Send Proactive message to the bot user
 * */
let sendMessageToBotUser = ({ agentId, message, value, agentUserId }) => {
    return new Promise((resolve, reject) => {
        ProactiveHelper = require('./../bot/index');
        let handOffId = agentId.split('agent-').pop();
        HandOff.findById(handOffId)
            .then((handOffData) => {
                return BotUser.findOne({ userId: handOffData.botUserId })
                    .then((userData) => {
                        if (!userData) {
                            Logger.log.error('No user data found with the given user id.');
                            return reject();
                        }
                        let fromId = userData.userId;
                        if (handOffData && (handOffData.isWaiting || handOffData.isFinished)) {
                            Logger.log.info(
                                `Chat with HandoffId: ${handOffId} is either completed or not started. isWaiting: ${handOffData.isWaiting}, isFinished: ${handOffData.isFinished}`,
                            );
                            let messageToSend = {
                                text: JSON.stringify({
                                    message: '00NOCONVERSATIONIDENTIFIED00',
                                    handOffId: handOffData._id,
                                }),
                            };
                            return ProactiveHelper.sendProactiveMessages({
                                message: messageToSend,
                                address: handOffData.agentAddress[handOffData.agentAddress.length - 1],
                            });
                        }
                        if (
                            agentUserId &&
                            handOffData.agentAssigned[handOffData.agentAssigned.length - 1].toString() !==
                                agentUserId.toString()
                        ) {
                            Logger.log.info(
                                `Chat with HandoffId: ${handOffId} is not going with the agent with Id: ${agentUserId}`,
                            );
                            let messageToSend = {
                                text: JSON.stringify({
                                    message: '00CHATACCEPTEDBYANOTHERAGENT00',
                                    handOffId: handOffData._id,
                                }),
                            };
                            return ProactiveHelper.sendProactiveMessages({
                                message: messageToSend,
                                address: handOffData.agentAddress[handOffData.agentAddress.length - 2],
                            });
                        }
                        if (message === '00ASKUSERABOUTCOMPLETE00') {
                            Logger.log.info('Message to end conversation identified.');
                            let promiseArr = [];
                            promiseArr.push(
                                changeHandOffStatus({
                                    botUserId: userData.userId,
                                    organizationId: userData.organizationId,
                                }),
                            );
                            promiseArr.push(
                                Bot.findOne({
                                    isArchived: false,
                                    organizationId: userData.organizationId,
                                }),
                            );
                            promiseArr.push(
                                UserAgent.findById(handOffData.agentAssigned[handOffData.agentAssigned.length - 1]),
                            );
                            return Promise.all(promiseArr)
                                .then((results) => {
                                    let handOffEndMessage = results[1].messages.handOffEnd
                                        ? results[1].messages.handOffEnd
                                        : 'Our agent marked the chat as resolved, you can continue chatting with me.';
                                    let agentName = results[2].name ? results[2].name : '';
                                    message = resolveDynamicMessage(handOffEndMessage, {
                                        'agent-name': agentName,
                                    });
                                    Logger.log.info('Handoff status changed in database successfully.');
                                    let messageObj = {
                                        text: message,
                                        value: 'detailText',
                                        inputHint: fromId,
                                    };
                                    let handOffObject = {
                                        text: 'end',
                                        value: 'handOffStatus',
                                        inputHint: fromId,
                                    };
                                    let newPromiseArr = [];
                                    newPromiseArr.push(
                                        ProactiveHelper.sendProactiveMessages({
                                            message: handOffObject,
                                            address: userData.address,
                                        }),
                                    );
                                    let messageToSend = {
                                        text: JSON.stringify({
                                            message: '00CHATMARKEDASCOMPLETED00',
                                            handOffId: handOffData._id,
                                        }),
                                    };
                                    newPromiseArr.push(
                                        ProactiveHelper.sendProactiveMessages({
                                            message: messageToSend,
                                            address: handOffData.agentAddress[handOffData.agentAddress.length - 1],
                                        }),
                                    );
                                    newPromiseArr.push(
                                        ProactiveHelper.sendProactiveMessages({
                                            message: messageObj,
                                            address: userData.address,
                                            handOffOver: true,
                                        }),
                                    );
                                    return Promise.all(newPromiseArr);
                                })
                                .catch((err) => {
                                    Logger.log.error(
                                        `Error updating isFinished true in database for user ${userData.userId}`,
                                    );
                                    Logger.log.error(err.message || err);
                                    return reject();
                                });
                        } else if (message.indexOf('00INTENTID##') !== -1) {
                            let intentId = message.split('##')[1];
                            return NlpHelper.getIntentDetails('en', intentId, userData.organizationId)
                                .then(async (intent) => {
                                    if (!intent) {
                                        Logger.log.error(`No intent details found for the intent id ${intentId}.`);
                                        return reject({
                                            message: `No intent details found for the intent id ${intentId}.`,
                                        });
                                    }
                                    Logger.log.info('Intent received in Handoff Helper');
                                    let atleastOneText = false;
                                    if (intent && intent.messages && intent.messages.length > 0) {
                                        for (let i = 0; i < intent.messages.length; i++) {
                                            if (intent.messages[i].hasOwnProperty('text')) {
                                                atleastOneText = true;
                                                let textResponses = intent.messages[i].text.text;
                                                let messageObj = {
                                                    text:
                                                        textResponses[Math.floor(Math.random() * textResponses.length)],
                                                    value: 'fromAgent',
                                                    inputHint: fromId,
                                                };
                                                try {
                                                    await ProactiveHelper.sendProactiveMessages({
                                                        message: messageObj,
                                                        address: userData.address,
                                                    });
                                                } catch (err) {
                                                    Logger.log.error('Error sending response of intent.');
                                                    Logger.log.error(err.message || err);
                                                    return reject(err);
                                                }
                                            }
                                        }
                                        let promiseArr = [];
                                        promiseArr.push(
                                            changeHandOffStatus({
                                                botUserId: userData.userId,
                                                organizationId: userData.organizationId,
                                                lastResponseAsIntent: true,
                                            }),
                                        );
                                        promiseArr.push(
                                            Bot.findOne({
                                                isArchived: false,
                                                organizationId: userData.organizationId,
                                            }),
                                        );
                                        promiseArr.push(
                                            UserAgent.findById(
                                                handOffData.agentAssigned[handOffData.agentAssigned.length - 1],
                                            ),
                                        );
                                        return Promise.all(promiseArr)
                                            .then((results) => {
                                                let handOffEndMessage = results[1].messages.handOffEnd
                                                    ? results[1].messages.handOffEnd
                                                    : 'Our agent marked the chat as resolved, you can continue chatting with me.';
                                                let agentName = results[2].name ? results[2].name : '';
                                                message = resolveDynamicMessage(handOffEndMessage, {
                                                    'agent-name': agentName,
                                                });
                                                Logger.log.info('Handoff status changed in database successfully.');
                                                let messageObj = {
                                                    text: message,
                                                    value: 'detailText',
                                                    inputHint: fromId,
                                                };
                                                let newPromiseArr = [];
                                                newPromiseArr.push(
                                                    ProactiveHelper.sendProactiveMessages({
                                                        message: messageObj,
                                                        address: userData.address,
                                                        handOffOver: true,
                                                    }),
                                                );
                                                Logger.log.info(
                                                    'Text response present in df response and sent to user, notifying agent.',
                                                );
                                                let messageToSend = {
                                                    text: JSON.stringify({
                                                        message: '00RESPONSEINDIALOGFLOW00',
                                                        handOffId: handOffData._id,
                                                    }),
                                                };
                                                newPromiseArr.push(
                                                    ProactiveHelper.sendProactiveMessages({
                                                        message: messageToSend,
                                                        address:
                                                            handOffData.agentAddress[
                                                                handOffData.agentAddress.length - 1
                                                            ],
                                                    }),
                                                );
                                                return Promise.all(newPromiseArr);
                                            })
                                            .catch((err) => {
                                                Logger.log.error(
                                                    `Error updating isFinished true in database for user ${userData.userId}`,
                                                );
                                                Logger.log.error(err.message || err);
                                                return reject();
                                            });
                                    } else if (!atleastOneText) {
                                        Logger.log.info('No text response present in df response, notifying agent.');
                                        let messageToSend = {
                                            text: JSON.stringify({
                                                message: '00NORESPONSEINDIALOGFLOW00',
                                                handOffId: handOffData._id,
                                            }),
                                        };
                                        return ProactiveHelper.sendProactiveMessages({
                                            message: messageToSend,
                                            address: handOffData.agentAddress[handOffData.agentAddress.length - 1],
                                        });
                                    }
                                })
                                .catch((err) => {
                                    Logger.log.error('Error getting intent details based on the intent id.');
                                    Logger.log.error(err.message || err);
                                    return reject(err);
                                });
                        } else {
                            let messageObj = {
                                text: message,
                                value: 'fromAgent',
                                inputHint: fromId,
                            };
                            if (value) messageObj.value = value;
                            return ProactiveHelper.sendProactiveMessages({
                                message: messageObj,
                                address: userData.address,
                            });
                        }
                    })
                    .catch((err) => {
                        Logger.log.error('Error fetching user details.', err.message || err);
                        return reject();
                    });
            })
            .then((results) => {
                Logger.log.info('Proactive message sent successfully to the bot user.');
                return resolve();
            })
            .catch((err) => {
                Logger.log.error('Error fetching user details.', err.message || err);
                return reject();
            });
    });
};

/**
 * Send Proactive message to the agent by finding agent's address from handoff's object
 * */
let sendMessageToAgent = ({ botUserId, message, value }) => {
    return new Promise((resolve, reject) => {
        ProactiveHelper = require('./../bot/index');
        HandOff.findOne({ botUserId: botUserId, isFinished: false }, {})
            .then((handOffData) => {
                if (!handOffData) {
                    Logger.log.error('No handoff data found with the given user id.');
                    return reject();
                }
                if (handOffData && handOffData.agentAddress.length === 0) {
                    Logger.log.error('No agent address found with the given user id.');
                    return reject();
                }
                if (message.indexOf('00') !== -1) {
                    message = extractMessageFromCode(message);
                }
                let messageToSend = { text: JSON.stringify({ message: message, handOffId: handOffData._id }) };
                if (value) {
                    messageToSend.value = value;
                }
                return ProactiveHelper.sendProactiveMessages({
                    message: messageToSend,
                    address: handOffData.agentAddress[handOffData.agentAddress.length - 1],
                });
            })
            .then((results) => {
                Logger.log.info('Proactive message sent successfully to the agent.');
                return resolve();
            })
            .catch((err) => {
                Logger.log.error('Error fetching handoff details', err.message || err);
                return reject();
            });
    });
};

/**
 * Creates a handoff entry in DB, sends notification to agent, and a proactive message to the bot user
 * */
let initializeHandOffProcess = ({ botUserId, organizationId, messageToUser, requestArrival, fromId }) => {
    return new Promise((resolve, reject) => {
        ProactiveHelper = require('./../bot/index');
        BotUser.findOneAndUpdate({ userId: botUserId }, { isAppliedForHandOff: true }, { new: true })
            .then((userData) => {
                if (!userData) {
                    Logger.log.error('No user data found with the given user id.');
                    return reject();
                }
                let handOff = new HandOff({ botUserId: botUserId, organizationId: organizationId });
                if (requestArrival === 'inWorkingHours') handOff.requestArrival = 'inWorkingHours';
                else if (requestArrival === 'outOfWorkingHours') handOff.requestArrival = 'outOfWorkingHours';
                handOff
                    .save()
                    .then((handOffData) => {
                        UserAgent.find({ isArchived: false, organizationId: organizationId })
                            .select({ fcmToken: 1 })
                            .then((userAgents) => {
                                let fcmTokenArr = [];
                                userAgents.forEach((agent) => {
                                    fcmTokenArr = [...fcmTokenArr, ...agent.fcmToken];
                                });
                                fcmTokenArr = [...new Set(fcmTokenArr)];
                                let userObj = JSON.parse(JSON.stringify(userData));
                                userObj.handOffId = handOffData._id;
                                let dataToSend = {
                                    userData: userObj,
                                    isWaiting: true,
                                };
                                FirebaseHelper.sendNotification(fcmTokenArr, dataToSend)
                                    .then((results) => {
                                        Logger.log.info('Notification to add new pending request sent successfully.');
                                        let proactiveMessageToBot =
                                            messageToUser ||
                                            'I have sent an request to our agent, they will connect to you shortly.';
                                        let messageObj = {
                                            text: proactiveMessageToBot,
                                            value: 'detailText',
                                            inputHint: fromId,
                                        };
                                        ProactiveHelper.sendProactiveMessages({
                                            message: messageObj,
                                            address: userData.address,
                                        })
                                            .then(() => {
                                                Logger.log.info(
                                                    'Proactive message sent to bot for waiting of the agent.',
                                                );
                                                return resolve(handOffData);
                                            })
                                            .catch((err) => {
                                                Logger.log.error(
                                                    'Error sending proacive message to bot for waiting of the agent.',
                                                    err.message || err,
                                                );
                                            });
                                    })
                                    .catch((err) => {
                                        Logger.log.error('Error occurred in sending notification.', err.message || err);
                                    });
                            })
                            .catch((err) => {
                                Logger.log.error('Error occurred in finding User Agent', err.message || err);
                            });
                    })
                    .catch((err) => {
                        Logger.log.error('Error fetching handoff details.', err.message || err);
                        return reject();
                    });
            })
            .catch((err) => {
                Logger.log.error('Error fetching user details.', err.message || err);
                return reject();
            });
    });
};

/**
 * when agent accepts the request to communicate with user, its address gets binded to handoff object,
 * proactive message to bot user, notifying frontend to enable the bot user's text box
 * */
let bindAgentAddressToHandOff = ({ agentId, agentAddress }) => {
    return new Promise((resolve, reject) => {
        ProactiveHelper = require('./../bot/index');
        let handOffId = agentId.split('agent-').pop();
        if (!agentAddress) {
            Logger.log.info('Agent address for binding to Handoff not found');
            return reject();
        }
        HandOff.findByIdAndUpdate(handOffId, { $push: { agentAddress: agentAddress } }, { new: true })
            .then((updatedHandOff) => {
                Logger.log.info('Agent address binded to handoff object.');
                return resolve();
            })
            .catch((err) => {
                Logger.log.error('Error binding the agent address to the handoff object.', err.message || err);
                return reject();
            });
    });
};

/**
 * Changing of the handoff status when conversation with agent gets completed
 * */
let changeHandOffStatus = ({ botUserId, organizationId, lastResponseAsIntent = false }) => {
    return new Promise((resolve, reject) => {
        ProactiveHelper = require('./../bot/index');
        let promiseArr = [];
        promiseArr.push(BotUser.findOneAndUpdate({ userId: botUserId }, { isHandOffGoingOn: false }, { new: true }));
        promiseArr.push(
            HandOff.findOneAndUpdate(
                {
                    botUserId: botUserId,
                    isWaiting: false,
                    isFinished: false,
                },
                { isFinished: true },
                { new: true },
            ),
        );
        Promise.all(promiseArr)
            .then(async (results) => {
                let updatedHandOff = results[1];
                Logger.log.info('Changed the status of isFinished flag to true.');
                await notifyConversationIsOver({ handOffId: updatedHandOff._id, organizationId, lastResponseAsIntent });
                if (updatedHandOff.agentAddress && updatedHandOff.agentAddress.length !== 0) {
                    ProactiveHelper.sendProactiveMessages({
                        message: '00HANDOFFOVERBYUSER00',
                        address: updatedHandOff.agentAddress[updatedHandOff.agentAddress.length - 1],
                    })
                        .then(() => {
                            Logger.log.info('Proactive message sent to bot agent to close the chat.');
                            return resolve();
                        })
                        .catch((err) => {
                            Logger.log.error(
                                'Error sending proactive message to agent informing chat closed by user.',
                                err.message || err,
                            );
                        });
                } else {
                    return resolve();
                }
            })
            .catch((err) => {
                Logger.log.error('Error changing isFinished status in handOff object.', err.message || err);
                return reject();
            });
    });
};

/**
 * Sends firebase notification to other agents for acceptance of request by an agent
 * */
let notifyAcceptRequestToOtherUsers = ({ agentId, handOffId, organizationId, handOffObj }) => {
    return new Promise((resolve, reject) => {
        UserAgent.find({ isArchived: false, organizationId: organizationId })
            .select({ fcmToken: 1 })
            .then((userAgents) => {
                if (userAgents.length === 0) {
                    Logger.log.warn(
                        'No agent found for given organization for sending notification on acceptance of the request.',
                    );
                    Logger.log.warn('OrganizationId:', organizationId);
                    return resolve();
                }
                Logger.log.info('List of user-agents for notification sending retrieved', userAgents.length);
                let fcmTokenArr = [];
                userAgents.forEach((agent) => {
                    if (agent._id.toString() !== agentId.toString()) {
                        fcmTokenArr = [...fcmTokenArr, ...agent.fcmToken];
                    }
                });
                fcmTokenArr = [...new Set(fcmTokenArr)];
                let dataToSend = {
                    isAlreadyAssigned: true,
                };
                if (handOffId) dataToSend.handOffId = handOffId;
                if (handOffObj) dataToSend.handOffObj = handOffObj;
                FirebaseHelper.sendNotification(fcmTokenArr, dataToSend)
                    .then((results) => {
                        Logger.log.info('Notification for acceptance sent successfully.');
                        return resolve();
                    })
                    .catch((err) => {
                        Logger.log.error('Error sending notification.', err.message || err);
                        return reject();
                    });
            })
            .catch((err) => {
                Logger.log.error('Error fetching agent details.', err.message || err);
                return reject();
            });
    });
};

/**
 * Sends firebase notification to all agents for completion of the conversation,
 * so entry can be moved from pending to completed in the web panel
 * */

let notifyConversationIsOver = ({ handOffId, organizationId, lastResponseAsIntent }) => {
    return new Promise((resolve, reject) => {
        UserAgent.find({ isArchived: false, organizationId: organizationId })
            .then((userAgents) => {
                let fcmTokenArr = [];
                userAgents.forEach((agent) => {
                    fcmTokenArr = [...fcmTokenArr, ...agent.fcmToken];
                });
                fcmTokenArr = [...new Set(fcmTokenArr)];
                let dataToSend = {
                    handOffId: handOffId,
                    isCompleted: true,
                    message: 'Chat marked as resolved.',
                };
                if (lastResponseAsIntent) dataToSend.message = 'Response of the selected intent sent to user.';
                FirebaseHelper.sendNotification(fcmTokenArr, dataToSend)
                    .then(() => {
                        Logger.log.info('Notification for completed sent successfully.');
                        return resolve();
                    })
                    .catch((err) => {
                        Logger.log.error('Error sending notification.', err.message || err);
                        return reject();
                    });
            })
            .catch((err) => {
                Logger.log.error('Error fetching agent details.', err.message || err);
                return reject();
            });
    });
};

/**
 * Sends firebase notification to other agents for acceptance of request by an agent
 * */
let notifyAcceptRequestToOldAgent = ({ agentId, acceptanceMessage, handOffId }) => {
    return new Promise((resolve, reject) => {
        UserAgent.findById(agentId)
            .select({ fcmToken: 1 })
            .then((userAgent) => {
                if (!userAgent) {
                    Logger.log.warn(
                        'No agent found for sending notification on acceptance of the request to other agent.',
                    );
                    return resolve();
                }
                let dataToSend = {
                    handOffId: handOffId,
                    message: acceptanceMessage,
                    isCompleted: true,
                };
                FirebaseHelper.sendNotification(userAgent.fcmToken, dataToSend)
                    .then((results) => {
                        Logger.log.info('Notification for acceptance sent successfully.');
                        return resolve();
                    })
                    .catch((err) => {
                        Logger.log.error('Error sending notification.', err.message || err);
                        return reject();
                    });
            })
            .catch((err) => {
                Logger.log.error('Error fetching agent details.', err.message || err);
                return reject();
            });
    });
};

/**
 * Checks whether  bot is not putOnHold
 * */

let checkBotStatus = ({ organizationId }) => {
    return new Promise((resolve, reject) => {
        let objToReturn = {};
        if (!organizationId) {
            Logger.log.error('Organization id not found in checkBotStatus', organizationId);
        }
        Bot.findOne({ isArchived: false, organizationId: organizationId })
            .select({ organizationId: 1, messages: 1, botName: 1, putOnHold: 1 })
            .then((botData) => {
                if (botData.putOnHold) {
                    objToReturn.isActive = false;
                    objToReturn.message = botData.messages.putOnHold;
                } else if (!botData.putOnHold) {
                    objToReturn.isActive = true;
                }
                return resolve(objToReturn);
            })
            .catch((err) => {
                Logger.log.error('Error fetching agent details.', err.message || err);
                return reject();
            });
    });
};

/**
 * Checks whether to enable handoff or not
 * */
let checkSystemHandOffStatus = ({ organizationId, botUserId }) => {
    return new Promise((resolve, reject) => {
        if (!organizationId) {
            Logger.log.error('No organization id found.');
            return reject;
        }
        let objToReturn = {};
        Bot.findOne({ isArchived: false, organizationId: organizationId })
            .select({ organizationId: 1, messages: 1, botName: 1 })
            .populate({
                path: 'organizationId',
                select: {
                    timezone: 1,
                    daylightSavings: 1,
                    work24by7: 1,
                    daySpecificWorking: 1,
                    isHandOffShutDown: 1,
                },
            })
            .then((botData) => {
                if (!botData.organizationId) {
                    Logger.log.error('No organization id found.');
                    return reject();
                }
                let organization = botData.organizationId;
                if (organization.isHandOffShutDown) {
                    objToReturn.allowHandOff = false;
                    objToReturn.message = botData.messages.handOffShutDown;
                    objToReturn.requestArrival = 'handOffShutdown';
                    // return resolve(objToReturn);
                } else if (organization.work24by7) {
                    objToReturn.allowHandOff = true;
                    objToReturn.message = botData.messages.handOffStart;
                    objToReturn.requestArrival = 'inWorkingHours';
                } else {
                    let teamOffSet = TimezoneHelper.extractValue(organization.timezone.str) * 60;
                    let currentTime = new Date();
                    let timeStamp = currentTime.getTime() + (currentTime.getTimezoneOffset() + teamOffSet) * 60 * 1000;
                    //TODO Ask someone for the knowledge of daylight
                    if (organization.daylightSavings) timeStamp += 60 * 60 * 1000;
                    let localDate = new Date(timeStamp);
                    let timeInMinutes = localDate.getHours() * 60 + localDate.getMinutes();

                    let day = TimezoneHelper.extractDay(organization.daySpecificWorking, localDate.getDay());
                    if (!day.isEnabled) {
                        objToReturn.allowHandOff = false;
                        if (day.isPublicHoliday) {
                            objToReturn.message = botData.messages.publicHoliday;
                            objToReturn.requestArrival = 'onPublicHoliday';
                        } else {
                            objToReturn.message = generateDayAndTimeForMessage(
                                botData.messages.handOffUnavailable,
                                organization.daySpecificWorking,
                                day,
                            );
                            objToReturn.requestArrival = 'onOffDay';
                        }
                    } else {
                        objToReturn.allowHandOff = true;
                        if (timeInMinutes >= day.startTime && timeInMinutes <= day.endTime) {
                            objToReturn.message = botData.messages.handOffStart;
                            objToReturn.requestArrival = 'inWorkingHours';
                        } else {
                            objToReturn.message = generateDayAndTimeForMessage(
                                botData.messages.handOffUnavailable,
                                organization.daySpecificWorking,
                                day,
                            );
                            objToReturn.requestArrival = 'outOfWorkingHours';
                        }
                    }
                }
                if (!objToReturn.allowHandOff) {
                    HandOff.create({
                        botUserId: botUserId,
                        organizationId: organizationId,
                        requestArrival: objToReturn.requestArrival,
                    })
                        .then((handOff) => {
                            Logger.log.info('Handoff Request not to be sent to panel is generated.');
                            return resolve(objToReturn);
                        })
                        .catch((err) => {
                            Logger.log.error(
                                `Error creating handoff request for not to be sent to panel, for organization: ${organizationId} and bot-user-id: ${botUserId}`,
                            );
                            Logger.log.error(err.message || err);
                            return reject();
                        });
                } else {
                    return resolve(objToReturn);
                }
            })
            .catch((err) => {
                Logger.log.error('Error fetching bot details to check team timings, for organization:', organizationId);
                Logger.log.error(err.message || err);
                return reject();
            });
    });
};

/**
 * Turn on handoff in bot's Conversation Data
 * */
let turnOnHandOffStatusInDirectline = ({ agentId }) => {
    return new Promise((resolve, reject) => {
        Logger.log.info('Agent accepted request for handoff', agentId);
        ProactiveHelper = require('./../bot/index');
        let handOffId = agentId.split('agent-').pop();
        HandOff.findById(handOffId)
            .then((handOffData) => {
                return BotUser.findOneAndUpdate(
                    { userId: handOffData.botUserId },
                    { isHandOffGoingOn: true, lastInteraction: Date.now() },
                    { new: true },
                )
                    .then((userData) => {
                        if (!userData) {
                            Logger.log.error('No user data found with the given user id.');
                            return reject();
                        }
                        ProactiveHelper.sendProactiveMessages({
                            address: userData.address,
                            handOffInitialize: true,
                            sendMessage: false,
                        })
                            .then((proactiveResult) => {
                                Logger.log.info(
                                    'Handoff status in conversation data changed to true for bot user id:',
                                    userData.userId,
                                );
                                return resolve();
                            })
                            .catch((err) => {
                                Logger.log.error(
                                    `Error updating handoff status in conversation state for user id: ${userData.userId}`,
                                );
                                Logger.log.error(err.message || err);
                                return reject();
                            });
                    })
                    .catch((err) => {
                        Logger.log.error(`Error finding user for agent id: ${agentId}`);
                        Logger.log.error(err.message || err);
                        return reject();
                    });
            })
            .catch((err) => {
                Logger.log.error(`Error finding handoff data for agent id: ${agentId}`);
                Logger.log.error(err.message || err);
                return reject();
            });
    });
};

/**
 * Sends config message to user for transfer of agent
 * */
let configMessageToUser = ({ userAddress, message }) => {
    return new Promise((resolve, reject) => {
        Logger.log.info('Sending configurable message to user', message);
        ProactiveHelper = require('./../bot/index');
        ProactiveHelper.sendProactiveMessages({
            address: userAddress,
            message: message,
            handOffInitialize: true,
        })
            .then((proactiveResult) => {
                Logger.log.info('Configurable message sent successfully to user.');
                return resolve();
            })
            .catch((err) => {
                Logger.log.error(`Error sending configurable message for user address: ${userAddress}`);
                Logger.log.error(err.message || err);
                return reject();
            });
    });
};

/**
 * Checks whether to enable handoff or not
 * */
let getMessageDelay = ({ organizationId }) => {
    return new Promise((resolve, reject) => {
        if (!organizationId) {
            Logger.log.error('No organization id found.');
            return reject;
        }
        let objToReturn = {};
        Bot.findOne({ isArchived: false, organizationId: organizationId })
            .select({ delay: 1 })
            .then((botData) => {
                if (!botData.delay || !botData.delay.hasOwnProperty('messageDelayTime')) {
                    Logger.log.warn('No delay between messages found for the organization:', organizationId);
                    return resolve(0);
                }
                return resolve(botData.delay.messageDelayTime);
            })
            .catch((err) => {
                Logger.log.error('Error fetching bot details to check team timings, for organization:', organizationId);
                Logger.log.error(err.message || err);
                return reject();
            });
    });
};

/**
 * Replaces fields with dynamic values
 * */
let resolveDynamicMessage = (message, dynamicObject) => {
    let generatedMessage = '';
    msgArr = message.split('{{');
    if (msgArr[0]) generatedMessage += msgArr[0];
    for (let i = 1; i < msgArr.length; i++) {
        let msgSubArr = msgArr[i].split('}}');
        generatedMessage += dynamicObject[msgSubArr[0].trim()];
        generatedMessage += msgSubArr[1];
    }
    return generatedMessage;
};

let extractMessageFromCode = (messageCode) => {
    switch (messageCode) {
        case '00ALLOWFORSCREENSHOT00':
            return 'Allow';
        case '00DENYFORSCREENSHOT00':
            return 'Deny';
    }
};

/**
 * Add or Update Timeouts for Handoff
 * */
let addOrUpdateTimeout = ({ userId, agentId }) => {
    if (agentId) {
        let handOffId = agentId.split('agent-').pop();
        HandOff.findById(handOffId)
            .select({ botUserId: 1 })
            .exec()
            .then((handOff) => {
                let userId = handOff.botUserId;
                if (!timerArr[userId] && !warnTimerArr[userId]) {
                    Logger.log.trace(`Initially Setting timeout for ${userId} at ${new Date()}`);
                    timerArr[userId] = setTimeout(function() {
                        Logger.log.trace(
                            `4 minutes passed for ${userId} at ${new Date()}, and in next few seconds, we'll clear out.`,
                        );
                        delete timerArr[userId];
                        warnTimerArr[userId] = setTimeout(function() {
                            if (warnTimerArr[userId]) {
                                Logger.log.trace(`Removing after warning for ${userId} at ${new Date()}`);
                                delete warnTimerArr[userId];
                                completeAuoHandOff({ userId });
                            }
                        }, warningTimeoutInMin * 60 * 1000); // Should be of 1 min
                    }, (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000); // Should be of 4 min
                } else {
                    Logger.log.trace(`Updating timeout for ${userId} at ${new Date()}`);
                    clearTimeout(timerArr[userId]);
                    clearTimeout(warnTimerArr[userId]);
                    timerArr[userId] = setTimeout(function() {
                        Logger.log.trace(
                            `4 minutes passed for ${userId} at ${new Date()}, , and in next few seconds, we'll clear out.`,
                        );
                        delete timerArr[userId];
                        warnTimerArr[userId] = setTimeout(function() {
                            if (warnTimerArr[userId]) {
                                Logger.log.trace(`Removing after warning for ${userId} at ${new Date()}`);
                                delete warnTimerArr[userId];
                                completeAuoHandOff({ userId });
                            }
                        }, warningTimeoutInMin * 60 * 1000); // Should be of 1 min
                    }, (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000); // Should be of 4 min
                }
            })
            .catch((err) => {
                Logger.log.error('Unable to find handOff data for updating timer');
                Logger.log.error(err.message || err);
                return reject();
            });
    } else {
        if (!timerArr[userId] && !warnTimerArr[userId]) {
            Logger.log.trace(`Initially Setting timeout for ${userId} at ${new Date()}`);
            timerArr[userId] = setTimeout(function() {
                Logger.log.trace(
                    `4 minutes passed for ${userId} at ${new Date()}, and in next few seconds, we'll clear out.`,
                );
                delete timerArr[userId];
                warnTimerArr[userId] = setTimeout(function() {
                    if (warnTimerArr[userId]) {
                        Logger.log.trace(`Removing after warning for ${userId} at ${new Date()}`);
                        delete warnTimerArr[userId];
                        completeAuoHandOff({ userId });
                    }
                }, warningTimeoutInMin * 60 * 1000); // Should be of 1 min
            }, (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000); // Should be of 4 min
        } else {
            Logger.log.trace(`Updating timeout for ${userId} at ${new Date()}`);
            clearTimeout(timerArr[userId]);
            clearTimeout(warnTimerArr[userId]);
            timerArr[userId] = setTimeout(function() {
                Logger.log.trace(
                    `4 minutes passed for ${userId} at ${new Date()}, , and in next few seconds, we'll clear out.`,
                );
                delete timerArr[userId];
                warnTimerArr[userId] = setTimeout(function() {
                    if (warnTimerArr[userId]) {
                        Logger.log.trace(`Removing after warning for ${userId} at ${new Date()}`);
                        completeAuoHandOff({ userId });
                        delete warnTimerArr[userId];
                    }
                }, warningTimeoutInMin * 60 * 1000); // Should be of 1 min
            }, (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000); // Should be of 4 min
        }
    }
};

/**
 * Function to change Handoff status in DB after few minutes
 * */
let completeAuoHandOff = ({ userId }) => {
    return new Promise((resolve, reject) => {
        ProactiveHelper = require('./../bot/index');
        BotUser.findOne({ userId: userId })
            .then((userData) => {
                let promiseArr = [];
                promiseArr.push(
                    changeHandOffStatus({
                        botUserId: userId,
                        organizationId: userData.organizationId,
                    }),
                );
                // console.log('Userdata:', userData);
                promiseArr.push(
                    Bot.findOne({
                        isArchived: false,
                        organizationId: userData.organizationId,
                    }),
                );
                Promise.all(promiseArr)
                    .then((results) => {
                        let handOffFiveMinMessage = results[1].messages.fiveMinMessage
                            ? results[1].messages.fiveMinMessage
                            : 'As no conversation was there with agent, I marked your as resolved.';
                        Logger.log.info('Handoff status changed in database successfully.');
                        let messageObj = {
                            text: handOffFiveMinMessage,
                            value: 'detailText',
                            inputHint: userId,
                        };
                        let handOffObject = {
                            text: 'end',
                            value: 'handOffStatus',
                            inputHint: userId,
                        };
                        let newPromiseArr = [];
                        newPromiseArr.push(
                            ProactiveHelper.sendProactiveMessages({
                                message: handOffObject,
                                address: userData.address,
                            }),
                        );
                        newPromiseArr.push(
                            ProactiveHelper.sendProactiveMessages({
                                message: messageObj,
                                address: userData.address,
                                handOffOver: true,
                            }),
                        );
                        Promise.all(newPromiseArr)
                            .then((results) => {
                                Logger.log.info(
                                    'Handoff completed automatically after 5 mins of no interaction, for user: ',
                                    userId,
                                );
                            })
                            .catch((err) => {
                                Logger.log.error(
                                    `Error sending configurable message to user on auto-completion for userId: ${userData.userId}`,
                                );
                                Logger.log.error(err.message || err);
                                return reject();
                            });
                    })
                    .catch((err) => {
                        Logger.log.error(
                            `Error finding bot or changing handoff status during hndoff auto completion for userI ${userData.userId}`,
                        );
                        Logger.log.error(err.message || err);
                        return reject();
                    });
            })
            .catch((err) => {
                Logger.log.error(`Error finding user during handoff auto completion for userId: ${userData.userId}`);
                Logger.log.error(err.message || err);
                return reject();
            });
    });
};

/**
 * Removes timeout from array, when completed by Agent or bot-closed by user
 * */
let removeTimeout = ({ userId }) => {
    clearTimeout(warnTimerArr[userId]);
    clearTimeout(timerArr[userId]);
    delete warnTimerArr[userId];
    delete timerArr[userId];
};

/**
 * Function to be run on Server start to start timeout for ongoing Handoffs
 * */
let initializeTimeoutArray = () => {
    return new Promise((resolve, reject) => {
        Logger.log.info('In initializeTimeoutArray function to start timeout for ongoing Handoffs.');
        let currentTime = new Date().getTime();
        BotUser.find({ isHandOffGoingOn: true })
            .select({ userId: 1, lastInteraction: 1 })
            .exec()
            .then((users) => {
                users.forEach((user) => {
                    if (user.lastInteraction) {
                        let timeDifference = currentTime - user.lastInteraction.getTime();
                        if (timeDifference < (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000) {
                            Logger.log.info('Last message was inside the normal duration for user', user.userId);
                            timerArr[user.userId] = setTimeout(function() {
                                Logger.log.trace(
                                    `4 minutes passed for ${
                                        user.userId
                                    } at ${new Date()}, and in next few seconds, we'll clear out.`,
                                );
                                delete timerArr[user.userId];
                                warnTimerArr[user.userId] = setTimeout(function() {
                                    if (warnTimerArr[user.userId]) {
                                        Logger.log.trace(`Removing after warning for ${user.userId} at ${new Date()}`);
                                        delete warnTimerArr[user.userId];
                                        completeAuoHandOff({ userId: user.userId });
                                    }
                                }, warningTimeoutInMin * 60 * 1000);
                            }, (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000 - timeDifference);
                            return resolve();
                        } else if (
                            timeDifference >= (totalTimeOutMin - warningTimeoutInMin) * 60 * 1000 &&
                            timeDifference < totalTimeOutMin * 60 * 1000
                        ) {
                            Logger.log.info('Last message was inside the warning duration for user', user.userId);
                            warnTimerArr[user.userId] = setTimeout(function() {
                                if (warnTimerArr[user.userId]) {
                                    Logger.log.trace(`Removing after warning for ${user.userId} at ${new Date()}`);
                                    delete warnTimerArr[user.userId];
                                    completeAuoHandOff({ userId: user.userId });
                                }
                            }, totalTimeOutMin * 60 * 1000 - timeDifference);
                            return resolve();
                        } else {
                            completeAuoHandOff({ userId: user.userId });
                            return resolve();
                        }
                    }
                });
            })
            .catch((err) => {
                Logger.log.error('Error finding users having handoff going on for initializing timeout array.');
                Logger.log.error(err.message || err);
                return reject();
            });
    });
};

/**
 * Replaces fields with dynamic values
 * */
let generateDayAndTimeForMessage = (message, daySpecificWorking, day) => {
    // console.log(message);
    if (message.indexOf('{{') === -1) return message;
    let immediateNextDayStartTime = -1;
    let startTime = 'morning';
    // console.log('in generate day time function');
    // console.log(message, daySpecificWorking, day);
    // console.log('in generate day time function');
    // console.log('in generate day time function');
    let nextDay = '';
    // if(message.indexOf('{{next-day}}') !== -1){
    // console.log('in if...');
    nextDay = 'next day';
    let nextEnabledDay = -1;
    let immediateNextDay;
    let count = 0;
    while (count < 6) {
        // console.log('Day to be checked::', daySpecificWorking[(day['dayNumber'] + count) % 7]);
        if (daySpecificWorking[(day['dayNumber'] + count) % 7].isEnabled) {
            nextEnabledDay = daySpecificWorking[(day['dayNumber'] + count) % 7].dayNumber;
            immediateNextDayStartTime = daySpecificWorking[(day['dayNumber'] + count) % 7].startTime;
            if (count === 0) {
                immediateNextDay = true;
            }
            break;
        } else {
            count++;
        }
    }
    if (immediateNextDay) {
        nextDay = 'tomorrow';
    } else if (!immediateNextDay && nextEnabledDay !== -1) {
        // console.log('nextEnabledDay::', nextEnabledDay);
        switch (nextEnabledDay.toString()) {
            case '0':
                nextDay = 'Sunday';
                break;
            case '1':
                nextDay = 'Monday';
                break;
            case '2':
                nextDay = 'Tuesday';
                break;
            case '3':
                nextDay = 'Wednesday';
                break;
            case '4':
                nextDay = 'Thursday';
                break;
            case '5':
                nextDay = 'Friday';
                break;
            case '6':
                nextDay = 'Saturday';
                break;
        }
    }
    // }
    if (message.indexOf('{{start-time}}') !== -1 && immediateNextDayStartTime !== -1) {
        let hrs = Math.floor(immediateNextDayStartTime / 60);
        let mins = 0;
        let meridiem = 'AM';
        // console.log('StartTime in mins::', immediateNextDayStartTime, hrs);
        if (immediateNextDayStartTime % 60 !== 60) {
            mins = immediateNextDayStartTime % 60;
        }
        if (hrs > 12) {
            meridiem = 'PM';
            hrs = hrs - 12;
        }
        // console.log('hrs, mins, meridiem::', hrs, mins, meridiem);
        startTime = hrs.toString() + ':' + (mins.toString().length === 1 ? '0' : '') + mins.toString() + ' ' + meridiem;
        // console.log('StartTime::', startTime);
    }

    let dynamicObject = {};
    if (startTime) dynamicObject['start-time'] = startTime;
    if (nextDay) dynamicObject['next-day'] = nextDay;
    return resolveDynamicMessage(message, dynamicObject);
};

module.exports = {
    sendMessageToBotUser: sendMessageToBotUser,
    sendMessageToAgent: sendMessageToAgent,
    initializeHandOffProcess: initializeHandOffProcess,
    bindAgentAddressToHandOff: bindAgentAddressToHandOff,
    changeHandOffStatus: changeHandOffStatus,
    notifyAcceptRequestToOtherUsers: notifyAcceptRequestToOtherUsers,
    notifyAcceptRequestToOldAgent: notifyAcceptRequestToOldAgent,
    checkBotStatus: checkBotStatus,
    checkSystemHandOffStatus: checkSystemHandOffStatus,
    getMessageDelay: getMessageDelay,
    turnOnHandOffStatusInDirectline: turnOnHandOffStatusInDirectline,
    resolveDynamicMessage: resolveDynamicMessage,
    configMessageToUser: configMessageToUser,
    addOrUpdateTimeout: addOrUpdateTimeout,
    removeTimeout: removeTimeout,
    initializeTimeoutArray: initializeTimeoutArray,
};
