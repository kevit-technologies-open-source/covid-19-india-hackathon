let extractValue = (str) => {
    let val;
    switch (str) {
        case '(UTC +14) LINT - Kiritimati':
            val = 14;
            break;
        case '(UTC +13:45) CHADT - Chatham Islands':
            val = 13.75;
            break;
        case '(UTC +13) NZDT - Auckland':
            val = 13;
            break;
        case '(UTC +12) ANAT - Anadyr':
            val = 12;
            break;
        case '(UTC +11) AEDT - Melbourne':
            val = 11;
            break;
        case '(UTC +10:30)	ACDT - Adelaide':
            val = 10.5;
            break;
        case '(UTC +10) AEST - Brisbane':
            val = 10;
            break;
        case '(UTC +9:30) ACST - Darwin':
            val = 9.5;
            break;
        case '(UTC +9) JST - Tokyo':
            val = 9;
            break;
        case '(UTC +8:45) ACWST - Eucla':
            val = 8.75;
            break;
        case '(UTC +8) CST - Beijing':
            val = 8;
            break;
        case '(UTC +7) WIB - Jakarta':
            val = 7;
            break;
        case '(UTC +6:30) MMT - Yangon':
            val = 6.5;
            break;
        case '(UTC +6) BST - Dhaka':
            val = 6;
            break;
        case '(UTC +5:45) NPT - Kathmandu':
            val = 5.75;
            break;
        case '(UTC +5:30) IST - New Delhi':
            val = 5.5;
            break;
        case '(UTC +5) UZT - Tashkent':
            val = 5;
            break;
        case '(UTC +4:30) AFT - Kabul':
            val = 4.5;
            break;
        case '(UTC +4) GST - Dubai':
            val = 4;
            break;
        case '(UTC +3:30) IRST - Tehran':
            val = 3.5;
            break;
        case '(UTC +3) MSK - Moscow':
            val = 3;
            break;
        case '(UTC +2) EET - Cairo':
            val = 2;
            break;
        case '(UTC +1) CET - Brussels':
            val = 1;
            break;
        case '(UTC +0) GMT - London':
            val = 0;
            break;
        case '(UTC -1) CVT - Praia':
            val = -1;
            break;
        case '(UTC -2) GST - King Edward Point':
            val = -2;
            break;
        case '(UTC -3) ART - Buenos Aires':
            val = -3;
            break;
        case "(UTC -3:30) NST - St. John's":
            val = -3.5;
            break;
        case '(UTC -4) VET - Caracas':
            val = -4;
            break;
        case '(UTC -5) EST - New York':
            val = -5;
            break;
        case '(UTC -6) CST - Mexico City':
            val = -6;
            break;
        case '(UTC -7) MST - Calgary':
            val = -7;
            break;
        case '(UTC -8) PST - Los Angeles':
            val = -8;
            break;
        case '(UTC -9) AKST - Anchorage':
            val = -9;
            break;
        case '(UTC -9:30) MART - Taiohae':
            val = -9.5;
            break;
        case '(UTC -10) HST - Honolulu':
            val = -10;
            break;
        case '(UTC -11) NUT - Alofi':
            val = -11;
            break;
        case '(UTC -12) AoE - Baker Island':
            val = -12;
            break;
    }
    return val;
};

extractDay = (days, dayNumber) => {
    for (let i = 0; i < days.length; i++) {
        if (days[i].dayNumber === dayNumber) {
            return days[i];
        }
    }
};

module.exports = {
    extractValue: extractValue,
    extractDay: extractDay,
};
