const axios = require('axios');
let getIndaiReport = (is24hours) => {
    return new Promise(async (resolve, reject) => {
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/data.json',
        };
        let stateData = await axios(option);
        if (is24hours) {
            resolve(
                'Today in India, it has crossed in 24 hours </br>' +
                    '<li>confirmed:- ' +
                    stateData.data.statewise[0]['deltaconfirmed'] +
                    '</li>' +
                    '<li>Deaths:- ' +
                    stateData.data.statewise[0]['deltadeaths'] +
                    '</li>' +
                    '<li>Recovered:- ' +
                    stateData.data.statewise[0]['deltarecovered'] +
                    '</li>',
            );
        } else {
            resolve(
                'Today in India, it has crossed </br>' +
                    '<li>confirmed:- ' +
                    stateData.data.statewise[0]['confirmed'] +
                    '</li>' +
                    '<li>active:- ' +
                    stateData.data.statewise[0]['active'] +
                    '</li>' +
                    '<li>deaths:- ' +
                    stateData.data.statewise[0]['deaths'] +
                    '</li>',
            );
        }
    });
};
let getCityReport = (cityName, is24Hourd) => {
    return new Promise(async (resolve, reject) => {
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/state_district_wise.json',
        };
        let cityData = await axios(option);
        let keys = Object.keys(cityData.data);
        for (let i = 0; i < keys.length; i++) {
            let citykey = Object.keys(cityData.data[keys[i]].districtData);
            for (let j = 0; j < citykey.length; j++) {
                if (citykey[j].toLowerCase() == cityName.toLowerCase()) {
                    if (is24Hourd) {
                        resolve(
                            'Here is the situation of COVID-19 Contagion in <b>' +
                                cityName +
                                '</b> total confirmed in last 24 hour ' +
                                cityData.data[keys[i]].districtData[cityName].delta.confirmed,
                        );
                    } else {
                        resolve(
                            'Here is the situation of COVID-19 Contagion in <b>' +
                                cityName +
                                '</b> total confirmed :-' +
                                cityData.data[keys[i]].districtData[cityName].confirmed,
                        );
                    }
                }
            }
        }
    });
};
let getStateReport = (stateName, is24Hors) => {
    return new Promise(async (resolve, reject) => {
        let option = {
            method: 'GET',
            url: 'https://api.covid19india.org/data.json',
        };
        let stateData = await axios(option);
        for (let i = 0; i < stateData.data.statewise.length; i++) {
            if (stateData.data.statewise[i].state.toLowerCase() == stateName.toLowerCase()) {
                if (is24Hors) {
                    resolve(
                        'Here is the situation of COVID-19 Contagion in <b>' +
                            stateName +
                            '</b> last 24 hour' +
                            '<li>Confirmed:-' +
                            stateData.data.statewise[i]['deltaconfirmed'] +
                            '</li>' +
                            '<li>Deaths:-' +
                            stateData.data.statewise[i]['deltadeaths'] +
                            '</li>' +
                            '<li>Recovered:-' +
                            stateData.data.statewise[i]['deltarecovered'] +
                            '</li>',
                    );
                } else {
                    resolve(
                        'Here is the situation of COVID-19 Contagion in <b>' +
                            stateName +
                            '</b>' +
                            '<li>Active:-' +
                            stateData.data.statewise[i]['active'] +
                            '</li>' +
                            '<li>Deaths:-' +
                            stateData.data.statewise[i]['deaths'] +
                            '</li>' +
                            '<li>Confirmed:-' +
                            stateData.data.statewise[i]['confirmed'] +
                            '</li>' +
                            '<li>Recovered:-' +
                            stateData.data.statewise[i]['recovered'] +
                            '</li>',
                    );
                }
            }
        }
    });
};
module.exports = {
    getStateReport: getStateReport,
    getCityReport: getCityReport,
    getIndaiReport: getIndaiReport,
};
