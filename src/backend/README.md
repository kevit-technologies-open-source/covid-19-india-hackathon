# HACKATHON AGAINST COVID 19

### AI CHATBOT FOR HEALTHCARE & MEDICATION

## Introduction

-   Purpose of project
    -   Today the whole world is fighting against COVID-19. As an organization, we tried to serve the nation by giving a small contribution.
    -   This project is a combination of symptom diagnosis, FAQs, remote access with Medical Representatives.
-   What we achieved with this project
    -   Any individual can have self assessment COVID-19 test by a set of clicks

## Modules

-   Backend Server
-   Super Admin Panel
-   Admin Panel

### 1 Backend Server

#### 1.1 Purpose of the module

-   This is the main core of the project. It connects NLU engine, Bot Framework, Database, third party API services, Mailer service, etc.
-   All the API calls from other two modules are pointed here.

#### 1.2 Module features

-   Main Bot logic is developed here.
    A received message is to be redirected to NLU engine, third party APIs or Human handover is decided here.
-   NLU engine is connected here. Session to NLU engine is maintained here.
-   Also the database and transcript logging are done here.

#### 1.3 Module technical stack

-   **Server runtime** : NodeJS v10.x
-   **Database** : MongoDB v4.x
-   **Bot framework**: MS Botframework 4.5
-   **NLU Engine**: Dialogflow
-   **Notification service** : Firebase
-   **Mail service** : Sendgrid

#### 1.4 Module configuration

-   Import DialogFlow agent
    -   Check this [guide](https://miningbusinessdata.com/how-to-import-an-agent-zip-file-into-api-ai/) to know how you can import the DialogFlow agent.
    -   The zip file to restore agent is in the source code with name `Hackathon-COVID-19.zip`
-   Bot channel registration
    -   Check the guide on [Azure](https://docs.microsoft.com/en-us/azure/bot-service/bot-service-quickstart-registration?view=azure-bot-service-3.0&viewFallbackFrom=azure-bot-service-4.0) on how to create bot channel registration.
    -   Copy the Bot Handle's value as `botHandle` while creating the bot, as it'll be needed in Admin Panel Module.
    -   Please note that you would need Microsoft App id & Microsoft App password
    -   Put Microsoft App id in the file `.env` in: `MICROSOFT_APP_ID`
    -   Put Microsoft App password in the file `.env` in: `MICROSOFT_APP_PASSWORD` ![app-id-password.png](./imgs/app-id-password.png)
-   Firebase Notification setup
    -   After logging into Google account, [create firebase project](https://console.firebase.google.com/).
    -   Click on Add Project ![screenshot-console.firebase.google.com-2020.04.13-10_56_56.png](./imgs/screenshot-console.firebase.google.com-2020.04.13-10_56_56.png)
    -   In step 1, add project name(for ex. _covid-hackathon-project_)
    -   In step 2, click on continue
    -   In step 3, select the location of organization (our case, India) and accept the T&C to create Project
    -   Click on Continue once the processing gets completed
    -   Select the above created project from dropdown and add a Web App by clicking on below shown icon ![screenshot-console.firebase.google.com-2020.04.13-12_24_25.png](./imgs/screenshot-console.firebase.google.com-2020.04.13-12_24_25.png)
    -   Register Web App by adding nickname, and click on **Register app**
    -   Copy the `firebaseConfig` object generated in the script. It'll be needed while configuring the **Admin Panel Module** ![screenshot-console.firebase.google.com-2020.04.13-12_37_02.png](./imgs/screenshot-console.firebase.google.com-2020.04.13-12_37_02.png)
    -   After clicking on **Continue to Console**, click on the Web App, recently created and click on **Settings** icon ![screenshot-console.firebase.google.com-2020.04.13-12_40_19.png](./imgs/screenshot-console.firebase.google.com-2020.04.13-12_40_19.png)
    -   Select the **Service accounts** tab, copy `databaseURL` and paste it to the file `.env` in: `FIREBASE_DB_URL`
    -   Generate private key by clicking on below **Generate new private key** button, paste the json credentials to file `fireBaseKey.json` in _keys folder_
-   Configure SendGrid account
    -   Signup to [SendGrid](https://signup.sendgrid.com/) and fill the required information and proceed further.
    -   Login in to the account and goto _Settings_ => _[API Keys](https://app.sendgrid.com/settings/api_keys)_. Confirm on the prompted **Confirm Email Address** button. Click on the link received in the mail inbox to verify the email. ![sendgrid-mail-confirm.png](./imgs/sendgrid-mail-confirm.png)
    -   Refresh the [API key page](https://app.sendgrid.com/settings/api_keys) and click on Create API key and paste it to the file `.env` in: `SENDGRID_API_KEY`
    -   For verification, follow the steps for Single Sender Verification from SendGrid [guide](https://sendgrid.com/docs/ui/sending-email/sender-verification)
    -   Paste sender email address to the file `.env` in: `FROM_EMAIL_ADDRESS`

#### 1.5 Get up and running

-   Install requirements
    -   Go to `source/backend` and open the terminal from that folder
    -   Run `npm i` to install all the dependencies
    -   Currently, the backend server will start on PORT 3555, which is configurable
    -   Once done, run `npm start`, and the server starts running on port 3555 (specified in env file)
    -   For making our port publicly available, install tunnelling software like [ngrok]() and from terminal generate URL for backend server port (Here, by running `ngrok http 3555`).
    -   URL pointing to port 3555 should look something like https://bb5938c3.ngrok.io
    -   Now, go to our Bot channel registration that we created in Azure Portal, and go to Settings.
    -   Insert the URL in Messaging endpoint. Now append /bot/messages in the URL.

        -   ex. our URL is https://bb5938c3.ngrok.io so, before pasting in the box, it will become https://bb5938c3.ngrok.io/bot/messages![screenshot-portal.azure.com-2020.04.13-15_41_43.png](./imgs/screenshot-portal.azure.com-2020.04.13-15_41_43.png)

### 2 Super Admin Panel

#### 2.1 Purpose of the module

-   As the name suggests, it is managed by the super-admin to manage the organizations
-   All the organizations can be controlled from here.

#### 2.2 Module features

-   Super admin can create and control all the organizations from this panel.
-   Super admin can login into any of the organization's Admin Panel

#### 2.3 Module technical stack

-   **Frontend framework**: Angular 8.x

#### 2.4 Module configuration

-   Configure the backend server URL
    -   In the `source/super-admin-panel/src/environments`, in environment.ts and environment.prod.ts file,
        -   replace the `apiUrl` to the public URL generated pointing to backend (i.e. https://bb5938c3.ngrok.io(This is ex. URL))
        -   replace the `adminUrl` to the public URL generated pointing to port 4600 (port 4600 is the default port of Admin Panel)

#### 2.5 Get up and running

-   Install requirements
    -   Go to `source/super-admin-panel` and open the terminal from that folder
    -   Run `npm i` to install the dependencies
    -   Once done, run `npm start`, and the superadmin panel loads onto port 4700
    -   Login into panel from browser at `localhost:4700` with the credentials:
        -   Email: admin@kevit.io
        -   Password: admin123
    -   Create organization and upload the dialpgflow json file, generated by following steps:
        -   Get service account credentials for Dialogflow:
            -   To get the service account credentials, go to "Settings" > "General Settings" > click on "Service Account" name. ![screenshot-dialogflow.cloud.google.com-2020.04.13-17_26_24.png](./imgs/screenshot-dialogflow.cloud.google.com-2020.04.13-17_26_24.png)
            -   It will open Google Cloud Platform screen. Click on the 3 dot menu on right, and click on "Create key"![screenshot-console.cloud.google.com-2020.04.13-17_30_24.png](./imgs/screenshot-console.cloud.google.com-2020.04.13-17_30_24.png)
            -   In the popup, click on JSON type, and click on "Create". It will download a JSON file.![Screenshot 2019-09-28 at 6.44.16 PM.png](./imgs/Screenshot 2019-09-28 at 6.44.16 PM.png)
            -   Now, upload this JSON file.
        -   Set Password Link would have been sent to the **Admin Email** specified while creating the organization.

### 3 Admin Panel

#### 3.1 Purpose of the module

-   Human Handover is performed from this panel. Medical representatives' account can be created. From this panel, they can reach people. This is great feature that overcomes the drawback of NLP engines. As and when bot fails, human can takeover the control removing the blockage to users for their answers.
-   Moreover widget can be configured from here.

#### 3.2 Module features

-   Analytics, to observe the traffic in the system
-   Human Handover, to get guidance and sympathy from medical representatives
-   Web Widget Settings, for the bot appearance and various message delay
-   General Settings like Team hours, Handover settings, etc.
-   User (Medical Representative) related operations , to add new, update/delete existing ones

#### 3.3 Module technical stack

-   **Frontend framework**: Angular 8.x

#### 3.4 Module configuration

-   Fetch Directline secret from Azure Portal
    -   Please follow this guide to fetch directline secret.
    -   Once you have the secret, consider it as `directlineSecret` to put
-   Configure the backend server URL & firebase configurations
    -   In the `source/super-admin-panel/src/environments`, in environment.ts and environment.prod.ts file,
        -   replace the `apiUrl` to the public URL generated pointing to backend (i.e. https://bb5938c3.ngrok.io(This is ex. URL))
        -   replace `secret` with the above generated `directlineSecret`.
        -   replace `directlineBotName` object with the `botHandle` generated from **Step 1.4**
        -   replace `firebase` object with the `firebaseConfig` generated from **Step 1.4** ![admin-env.png](./imgs/admin-env.png)

#### 3.5 Get up and running

-   Install requirements
-   Go to `source/admin-panel` and open the terminal from that folder
    -   Run `npm i` to install the dependencies
    -   Once done, run `npm start`, and the admin panel loads onto port 4600
    -   Login into panel from browser at `localhost:4600` with the email address created from _SuperAdmin Panel_, and password set from _Set Password_ mail received.
    -   From the "Bot Settings" => "Integrations", add the Site URL on which bot is to be run and **Save the Changes**.
    -   Paste the **Bot Script** into the websites HTML code (Note. For development purpose, it can be set into console of browser).
    -   Bot should get loaded in the Web page.
