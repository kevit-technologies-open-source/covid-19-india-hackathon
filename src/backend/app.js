/**
 * System and 3rd party libs
 */
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const mongoose = require('mongoose');
const cors = require('cors');
const boom = require('express-boom');
/**
 * Required Services
 */
const Logger = require('./services/logger');
/**
 * Config
 * */
const config = require('./config');
/**
 *
 * Global declarations
 */
const models = path.join(__dirname, 'models');
let dbURL = config.server.mongoDBConnectionUrl || 'mongodb://127.0.0.1:27017/HP';
if (process.env.NODE_ENV === 'test') {
    dbURL = 'mongodb://127.0.0.1:27017/HP-test';
}

/**
 * Bootstrap Models
 */
fs.readdirSync(models).forEach((file) => require(path.join(models, file)));

/**
 * Bootstrap App
 */
const app = express();
app.use(Logger.morgan);
app.use(bodyParser.json({ limit: '50mb' }));
app.use(
    bodyParser.urlencoded({
        limit: '50mb',
        extended: false,
    }),
);
app.use(boom());
app.use(cookieParser());
Logger.log.warn('Server Started');
// Allow CORS

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

app.use(
    cors({
        origin: true,
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
        credentials: true,
    }),
);
app.use(express.static(path.join(__dirname, 'upload')));

/**
 * Import and Register Routes
 */
const index = require('./routes/index');
const auth = require('./routes/auth.route.js');
const supportPanel = require('./routes/supportPanel.route');
const trans = require('./routes/transcript.route');
const handOffRequest = require('./routes/handOffRequest');
const organization = require('./routes/organization.route');
const botRoute = require('./routes/bot.route');
const user = require('./routes/user.route');
const botSettings = require('./routes/botSettings.route');
const webWidget = require('./routes/webWidget.route');
const botWidget = require('./routes/botWidget.route');
const analytics = require('./routes/analytics.route');

const bot = require('./bot/index');
const authenticate = require('./routes/authenticate').authMiddleWare;
const superAdminMiddleware = require('./routes/authenticate').superAdminMiddleWare;
const adminMiddleware = require('./routes/authenticate').adminMiddleWare;

app.use('/', index);
app.use('/auth', auth);
app.use('/supportPanel', authenticate, supportPanel);
app.use('/transcript', authenticate, trans);
app.use('/bot', bot.router);
app.use('/handoff', authenticate, handOffRequest);
app.use('/organization', authenticate, superAdminMiddleware, organization);
app.use('/bot-route', authenticate, superAdminMiddleware, botRoute);
app.use('/user', authenticate, user);
app.use('/bot-settings', authenticate, adminMiddleware, botSettings);
app.use('/web-widget', authenticate, adminMiddleware, webWidget);
app.use('/bot-widget', botWidget);
app.use('/analytics', authenticate, adminMiddleware, analytics);

/**
 * Mongoose Configuration
 */
mongoose.Promise = global.Promise;

mongoose.connection.on('connected', () => {
    Logger.log.info('DATABASE - Connected');
});

mongoose.connection.on('error', (err) => {
    Logger.log.error('DATABASE - Error:' + err);
});

mongoose.connection.on('disconnected', () => {
    Logger.log.warn('DATABASE - disconnected  Retrying....');
});

let connectDb = function() {
    const dbOptions = {
        poolSize: 5,
        reconnectTries: Number.MAX_SAFE_INTEGER,
        reconnectInterval: 500,
        useNewUrlParser: true,
        useFindAndModify: false,
    };
    mongoose.connect(dbURL, dbOptions).catch((err) => {
        Logger.log.fatal('DATABASE - Error:' + err);
    });
};

connectDb();

const schedulerLocal = require('./services/schedule-cron');

// console.log(config.environment);
if (config.environment !== 'local' && config.environment !== 'test') {
    schedulerLocal.scheduleCrons();
}

//Checks whether SuperAdmin present
const DbInitialization = require('./helper/serverInitialization');
DbInitialization.createSuperAdmin();

//Chat with Agent Timeout Initialization

const HandOffHelper = require('./helper/handoff.helper');
HandOffHelper.initializeTimeoutArray();

module.exports = app;
