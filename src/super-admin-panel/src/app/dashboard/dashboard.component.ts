import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild
} from "@angular/core";
import { MatDialog } from "@angular/material";
import { DialogComponent } from "../components/dialog/dialog.component";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AuthService } from "../services/auth.service";
import { UserProfileService } from "../services/user-profile.service";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

declare var $: any;

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  isMenuToggle = false;
  message;
  dialogRef;
  userProfile;
  changePasswordForm;
  updatePasswordForm;
  userName;
  initialLetter;
  photoUrl;
  isImageLoad = false;
  unSubscribe = new Subject();
  headerWidth;
  @ViewChild("changePass", { static: false }) changePasswordTemp: ElementRef;
  @ViewChild("updateProfile", { static: false }) updateProfileTemp: ElementRef;
  constructor(
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private _authService: AuthService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private userProfileService: UserProfileService,
    private _eref: ElementRef
  ) {}

  ngOnInit() {
    this.userName = this._authService.getUserName();
    this.photoUrl = this._authService.getUserProfile();
    this.initialLetter = this.userName.substr(0, 1);
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl("", Validators.required),
      newPassword: new FormControl("", Validators.required),
      confirmPassword: new FormControl("", Validators.required)
    });

    this.updatePasswordForm = new FormGroup({
      name: new FormControl("", Validators.required),
      email: new FormControl(""),
      contactNumber: new FormControl("", Validators.required)
    });
    this.headerWidth = document.getElementById("header").offsetWidth;
  }

  openChangePassDialog(template) {
    this.changePasswordForm.reset();
    this.isMenuToggle = false;
    this.dialogRef = this.dialog.open(DialogComponent, {
      data: { title: "Change Password", template: template, isShowSave: true },
      height: "50vh",
      width: "40vw"
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result.isSave) {
        this.changePassword();
      }
    });
  }

  changePassword() {
    if (this.changePasswordForm.valid) {
      if (
        !this.changePasswordForm.controls["oldPassword"].value.trim().length
      ) {
        this.snackBar.open("Please enter valid old password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
        !this.changePasswordForm.controls["newPassword"].value.trim().length
      ) {
        this.snackBar.open("Please enter valid new password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
        !this.changePasswordForm.controls["confirmPassword"].value.trim().length
      ) {
        this.snackBar.open("Please enter valid confirm password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
        this.changePasswordForm.controls["newPassword"].value !==
        this.changePasswordForm.controls["confirmPassword"].value
      ) {
        this.snackBar.open(
          "New password and confirm password does not match.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
        return;
      }
      const data = {
        oldPassword: this.changePasswordForm.controls["oldPassword"].value,
        newPassword: this.changePasswordForm.controls["newPassword"].value
      };
      this._authService
        .changePassword(data)
        .then((res: any) => {
          this.dialog.closeAll();
          this.snackBar.open("Password changed successfully.", "", {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-success"]
          });
        })
        .catch(err => {
          console.log(err);
          this.snackBar.open(
            err.error && err.error.message
              ? err.error.message
              : "Something went wrong.",
            "",
            {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-error"]
            }
          );
        });
    } else {
      this.snackBar.open("Please enter all fields.", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
    }
  }

  userLogout() {
    this.isMenuToggle = false;
    this._authService
      .logout()
      .then(res => {
        this.router.navigate(["login"]);
      })
      .catch(err => {
        this.snackBar.open(
          err.error.message ? err.error.message : "Something went wrong",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      });
  }

  onUpdateProfile() {
    let data = {
      name: this.updatePasswordForm.controls["name"].value,
      contactNumber: this.updatePasswordForm.controls["contactNumber"].value,
      profilePicture: this.photoUrl
    };
    if (this.updatePasswordForm.controls["name"].value.trim() === "") {
      this.snackBar.open("Please enter valid name", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
      return;
    }
    if (this.updatePasswordForm.controls["contactNumber"].value.trim() !== "") {
      if (
        this.updatePasswordForm.controls["contactNumber"].value.length < 6 ||
        this.updatePasswordForm.controls["contactNumber"].value.length > 10
      ) {
        this.snackBar.open(
          "Please enter contact number of length 6 to 10.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
        return;
      }
    } else if (
      this.updatePasswordForm.controls["contactNumber"].value &&
      this.updatePasswordForm.controls["contactNumber"].value.trim() === ""
    ) {
      this.snackBar.open("Please enter valid Contact Number", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
      return;
    }

    this.userProfileService
      .updateUser(data)
      .then((res: any) => {
        this.dialog.closeAll();
        this._authService.setUserName(res.name);
        this.userName = res.name;
        this.snackBar.open("Profile Updated successfully.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-success"]
        });
      })
      .catch(err => {
        this.snackBar.open(
          err.error.message ? err.error.message : "Something went wrong",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      });
  }

  restrictNumberInput(e, value?) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(e.charCode);
    if (e.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if ((value === "phone" && e.target.value.length >= 9) || e.keyCode === 46) {
      event.preventDefault();
    }
  }

  openUpdateProfileDialog(updateProfile) {
    this.isMenuToggle = false;
    this.userProfileService.getUser().then((res: any) => {
      this.userProfile = res;
      this.updatePasswordForm.controls.name.setValue(res.name);
      this.updatePasswordForm.controls.email.setValue(res.email);
      this.updatePasswordForm.controls.contactNumber.setValue(
        res.contactNumber
      );
      this.photoUrl = res.profilePicture;
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: "Update Profile",
          template: updateProfile,
          isShowSave: true
        },
        width: "40vw"
      });
    });
  }

  uploadDisplayPicture(events: any) {
    let pictureName;
    if (this.photoUrl) {
      pictureName = this.photoUrl.substring(this.photoUrl.lastIndexOf("/") + 1);
    }

    if (events.target.files && events.target.files[0]) {
      const fd = new FormData();
      fd.append(
        "profile-picture",
        events.target.files[0],
        events.target.files[0].name
      );

      this.userProfileService
        .updateProfilePicture(pictureName ? pictureName : "", fd)
        .then((res: any) => {
          this.photoUrl = res.profilePicture;
          console.log(this.photoUrl);
          this._authService.setUserProfile(this.photoUrl);
        });
    }
  }

  handleMenuClick(event) {
    if (event === "changePass") {
      this.openChangePassDialog(this.changePasswordTemp);
    } else if (event === "updateProfile") {
      this.openUpdateProfileDialog(this.updateProfileTemp);
    } else if (event === "logout") {
      this.userLogout();
    } else {
      this.isMenuToggle = false;
    }
  }
  ngOnDestroy(): void {
    this.unSubscribeAllObserver();
  }

  unSubscribeAllObserver() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  deleteProfilePic() {
    let oldImage = this.photoUrl.substr(this.photoUrl.lastIndexOf("/") + 1);
    this.userProfileService
      .deleteProfilePic(oldImage)
      .then(res => {
        this.photoUrl = null;
      })
      .catch(err => {
        console.log(err);
      });
  }

  toggleMenu() {
    this.isMenuToggle = !this.isMenuToggle;
    this.headerWidth = document.getElementById("header").offsetWidth;
  }
}
