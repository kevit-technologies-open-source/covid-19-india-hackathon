import { Component, OnDestroy, OnInit } from "@angular/core";
import { DialogComponent } from "../../components/dialog/dialog.component";
import { MatDialog } from "@angular/material";
import { BotsService } from "../../services/bots.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
  selector: "app-bots",
  templateUrl: "./bots.component.html",
  styleUrls: ["./bots.component.scss"]
})
export class BotsComponent implements OnInit, OnDestroy {
  search: any = "";
  isPagination = true;
  disablePrev = false;
  disableNext = true;
  currentPage: number = 1;
  totalpage: number = 0;
  totalPages = [];
  dialogRef;
  selectedBot;
  isBotLoaded = false;
  bots: any[] = [];
  unSubscribe = new Subject();
  constructor(
    public dialog: MatDialog,
    private _botService: BotsService,
    private snackBar: MatSnackBar,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activeRoute.queryParams
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(value => (this.currentPage = value.page));
    this.getAllBots(this.currentPage ? this.currentPage : 1, 10);
  }
  getAllBots(page, limit) {
    this._botService
      .getBotList(page, limit, this.search)
      .then((res: any) => {
        this.bots = res.docs;
        this.isBotLoaded = true;
        this.currentPage = res.page;
        this.totalpage = res.pages;
        this.isPagination = res.total > 1;
        if (res.page > this.totalpage) {
          this.currentPage = 1;
        }
        this.disablePrev = this.currentPage === 1;
        this.disableNext = this.currentPage === this.totalpage;
        let temp = parseInt(this.currentPage / 5 + "", 10);
        if (this.currentPage % 5 === 0) {
          temp--;
        }
        let count = 0;
        this.totalPages = [];
        if (this.totalpage > 5) {
          // @ts-ignore
          for (
            let i =
              this.currentPage + 5 <= this.totalpage
                ? this.currentPage === 1
                  ? this.currentPage
                  : this.currentPage
                : this.totalpage - 4;
            i <=
            (this.currentPage + 5 <= res.pages
              ? this.currentPage + 5
              : this.totalpage);
            i++
          ) {
            count++;
            if (count > 5 || i > this.totalpage) {
              break;
            }
            this.totalPages.push({
              no: i,
              status: i === res["page"]
            });
          }
        } else {
          for (let i = 1; i <= res["pages"]; i++) {
            this.totalPages.push({
              no: i,
              status: i === res["page"]
            });
          }
        }
      })
      .catch(err => {
        console.log(err);
        this.snackBar.open(
          err.error && err.error.message
            ? err.error.message
            : "Something went wrong.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      });
  }
  changePage(page) {
    this.currentPage = page;
    this.router.navigate(["dashboard", "bots"], {
      queryParams: {
        page: this.currentPage
      }
    });
    this.getAllBots(this.currentPage, 10);
  }
  /*
      Function for going to next page
   */
  nextPage() {
    if (!this.disableNext) {
      this.changePage(++this.currentPage);
    }
  }

  /*
     Function for going to prev page
  */
  prevPage() {
    if (!this.disablePrev) {
      this.changePage(--this.currentPage);
    }
  }

  getBotDetails(index, template) {
    this._botService
      .getBotDetails(this.bots[index]._id)
      .then((res: any) => {
        this.selectedBot = res;
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            title: "More Details",
            template: template,
            isShowSave: false
          },
          width: "60vw"
        });
      })
      .catch(err => {
        console.log(err);
        this.snackBar.open(
          err.error && err.error.message
            ? err.error.message
            : "Something went wrong.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      });
  }
  changeBotOnHold(index, template) {
    this.selectedBot = this.bots[index];

    this.dialogRef = this.dialog.open(DialogComponent, {
      panelClass: "addUser",
      data: { title: "Put on hold", template: template, isShowSave: false },
      height: "auto",
      width: "500px"
    });
  }

  setBotConfig() {
    const data = {
      status: this.selectedBot.putOnHold
    };
    this._botService
      .setBotOnHold(this.selectedBot._id, data)
      .then((res: any) => {
        this.snackBar.open(res.message, "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-success"]
        });
      })
      .catch(err => {
        console.log(err);
        this.snackBar.open(
          err.error && err.error.message
            ? err.error.message
            : "Something went wrong.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      });
  }

  openDeleteDialog(index, template) {
    this.selectedBot = this.bots[index];
    this.dialogRef = this.dialog.open(DialogComponent, {
      panelClass: "addUser",
      data: { title: "Delete Bot", template: template, isShowSave: false },
      height: "auto",
      width: "500px"
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._botService
          .deleteBot(this.bots[index]._id)
          .then((res: any) => {
            this.getAllBots(this.currentPage, 10);
            this.snackBar.open("Bot deleted successfully!", "", {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-success"]
            });
            this.selectedBot.putOnHold = !this.selectedBot.putOnHold;
          })
          .catch(err => {
            console.log(err);
            this.snackBar.open(
              err.error && err.error.message
                ? err.error.message
                : "Something went wrong.",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          });
      }
    });
  }

  getSearchList(event) {
    if (event.keyCode === 8 || event.keyCode === 46) {
      if (this.search.length === 1) {
        this.search = "";
        this.getAllBots(1, 10);
      }
    }
    if (event.keyCode === 13) {
      this.getAllBots(1, 10);
    }
  }
  ngOnDestroy(): void {
    this.unSubscribeAllObserver();
  }

  unSubscribeAllObserver() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }
  setChecked() {
    this.selectedBot.putOnHold = !this.selectedBot.putOnHold;
    this.setBotConfig();
    this.dialogRef.close();
  }
  unChecked() {
    this.dialogRef.close();
  }
}
