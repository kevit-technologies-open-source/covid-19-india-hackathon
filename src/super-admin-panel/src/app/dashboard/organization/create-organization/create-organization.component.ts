import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { DialogComponent } from "../../../components/dialog/dialog.component";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { OrganizationService } from "../../../services/organization.service";
import { DomSanitizer } from "@angular/platform-browser";
import * as FileSaver from "file-saver";

@Component({
  selector: "app-create-organization",
  templateUrl: "./create-organization.component.html",
  styleUrls: ["./create-organization.component.scss"]
})
export class CreateOrganizationComponent implements OnInit {
  editID;
  organizationForm: FormGroup;
  testJson = {
    botId: "",
    userId: "",
    agentName: "",
    type: "",
    project_id: "",
    private_key_id: "",
    private_key: "",
    client_email: "",
    client_id: "",
    auth_uri: "",
    token_uri: "",
    auth_provider_x509_cert_url: "",
    client_x509_cert_url: ""
  };
  isJsonUpload = false;
  isJsonValid = false;
  isFirstUpload = false;
  isSaveOrg = false;
  fileUrl;
  jsonFile;
  jsonFileName;
  isShowUpload = true;
  organizationData;
  @ViewChild("jsonUpload", { static: false }) jsonUploadFile: ElementRef;
  constructor(
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _organizationService: OrganizationService,
    private sanitizer: DomSanitizer,
    private router: Router
  ) {}

  ngOnInit() {
    this.initForm();

    if (this.activeRoute.snapshot.params.id) {
      this.editID = this.activeRoute.snapshot.params.id;
      if (this.editID) {
        this._organizationService
          .getOrganizationById(this.editID)
          .then((res: any) => {
            this.initForm(res);
            this.organizationData = res;
            this.jsonFile = res.bot.dfJson;
            this.isShowUpload = !res.bot.botName;
            this.isJsonValid = true;
          })
          .catch(err => {
            console.log(err);
            this.snackBar.open(
              err.error && err.error.message
                ? err.error.message
                : "Something went wrong.",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          });
      }
    }
  }

  initForm(data?) {
    this.organizationForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      adminName: new FormControl(null, [Validators.required]),
      adminEmail: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      adminContactNumber: new FormControl(null, [Validators.required]),
      bot: new FormGroup({
        botName: new FormControl(null, [Validators.required]),
        dfJson: new FormControl(null)
      })
    });
    if (data) {
      this.organizationForm.controls["name"].setValue(data.organization.name);
      this.organizationForm.controls["adminName"].setValue(
        data.organization.adminName ? data.organization.adminName : ""
      );
      this.organizationForm.controls["adminEmail"].setValue(
        data.organization.adminEmail
      );
      this.organizationForm.controls["adminContactNumber"].setValue(
        data.organization.adminContactNumber
          ? data.organization.adminContactNumber
          : ""
      );
      if (data.bot.hasOwnProperty("botName")) {
        this.organizationForm.controls["bot"]["controls"]["botName"].setValue(
          data.bot.botName
        );
        this.organizationForm.controls["bot"]["controls"]["dfJson"].setValue(
          data.bot.dfJson
        );
        this.jsonFileName = data.bot.fileName;
      }
    }
  }

  openDialog(templateRef) {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: "700px",
      data: { title: "DialogFlow Instructions", template: templateRef }
    });
  }

  saveOrganization() {
    if (this.organizationForm.valid) {
      if (!this.organizationForm.controls["name"].value.trim().length) {
        this.snackBar.open("Please enter valid Organization name.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (!this.organizationForm.controls["adminName"].value.trim().length) {
        this.snackBar.open("Please enter valid Admin name.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
        !this.organizationForm.controls["adminContactNumber"].value.trim()
          .length
      ) {
        this.snackBar.open("Please enter valid Admin contact number.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
        this.organizationForm.controls["adminContactNumber"].value.trim()
          .length > 15
      ) {
        this.snackBar.open(
          "Contact number should not be more than 15 digits.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
        return;
      }
      if (
        !this.organizationForm.controls["bot"]["controls"][
          "botName"
        ].value.trim().length
      ) {
        this.snackBar.open("Please enter valid Bot name.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (!this.organizationForm.controls["bot"]["controls"]["dfJson"].value) {
        this.jsonUploadFile.nativeElement.value = "";
        this.snackBar.open(
          "Please upload json file by following informed steps",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
        return;
      }
      if (!this.isJsonValid) {
        this.jsonUploadFile.nativeElement.value = "";
        console.log("Please upload json file by following informed steps");
        return;
      }

      const data = {
        organization: {
          name: this.organizationForm.controls["name"].value,
          adminName: this.organizationForm.controls["adminName"].value,
          adminEmail: this.organizationForm.controls["adminEmail"].value,
          adminContactNumber: this.organizationForm.controls[
            "adminContactNumber"
          ].value
        },
        bot: {
          botName: this.organizationForm.controls["bot"]["controls"]["botName"]
            .value,
          dfJson: this.organizationForm.controls["bot"]["controls"]["dfJson"]
            .value,
          fileName: this.jsonFileName
        }
      };
      this.isSaveOrg = true;
      if (this.editID) {
        this._organizationService
          .updateOrganization(this.editID, data)
          .then((res: any) => {
            this.isSaveOrg = false;
            this.snackBar.open("Organization saved successfully.", "", {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-success"]
            });
            this.router.navigate(["dashboard", "organizations"]);
          })
          .catch(err => {
            this.isSaveOrg = false;
            this.snackBar.open(
              err.error && err.error.message
                ? err.error.message
                : "Something went wrong.",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          });
      } else {
        this._organizationService
          .createOrganization(data)
          .then((res: any) => {
            this.isSaveOrg = false;
            console.log(res);
            this.snackBar.open("Organization added successfully.", "", {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-success"]
            });
            this.router.navigate(["dashboard", "organizations"]);
          })
          .catch(err => {
            this.isSaveOrg = false;
            console.log(err);
            this.snackBar.open(
              err.error && err.error.message
                ? err.error.message
                : "Something went wrong.",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          });
      }
    }
  }
  uploadJSON() {
    this.jsonUploadFile.nativeElement.click();
  }
  checkAndUploadJSON(event) {
    this.isJsonUpload = false;
    this.isFirstUpload = false;
    const file = new FileReader();
    let fileName,
      type = "";
    file.onload = e => {
      fileName = event.target.files[0].name;
      type = event.target.files[0].name.split(".");
      if (type[type.length - 1] === "json") {
        this.isJsonUpload = true;
        this.isFirstUpload = true;
        this.isShowUpload = !this.isFirstUpload;
        this.processJSON(file.result, fileName);
      } else {
        this.jsonUploadFile.nativeElement.value = "";
        this.snackBar.open(
          "Please upload json file by following informed steps",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      }
    };
    file.readAsText(event.target.files[0]);
  }
  processJSON(result, fileName) {
    const entries = Object.keys(this.testJson);
    try {
      const json = Object.keys(JSON.parse(result));
      const isJSONOKay = json.every(key => {
        return entries.includes(key);
      });
      if (isJSONOKay) {
        result = JSON.parse(result);
        this.organizationForm.controls["bot"]["controls"]["dfJson"].setValue(
          result
        );
        this.snackBar.open("File Uploaded successfully!", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-success"]
        });
        this.isJsonUpload = false;
        this.jsonFile = this.organizationForm.controls["bot"]["controls"][
          "dfJson"
        ].value;
        this.jsonFileName = fileName;
        this.isJsonValid = true;
      } else {
        this.jsonUploadFile.nativeElement.value = "";
        this.snackBar.open(
          "Please upload json file by following informed steps",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
        this.isJsonUpload = false;
        this.isJsonValid = false;
      }
    } catch (e) {
      this.jsonUploadFile.nativeElement.value = "";
      this.snackBar.open(
        "Please upload json file by following informed steps",
        "",
        {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        }
      );
      this.isJsonUpload = false;
      this.isJsonValid = false;
      return;
    }
  }

  downloadJSON() {
    const data = JSON.stringify(
      this.organizationForm.controls["bot"]["controls"]["dfJson"].value
    );
    const blob = new Blob([data], { type: "text/json;charset=utf-8" });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      window.URL.createObjectURL(blob)
    );
    FileSaver.saveAs(blob, this.jsonFileName);
  }

  openJSONDialog(viewDetailsTemp) {
    if (this.jsonFile) {
      let dialogRef = this.dialog.open(DialogComponent, {
        width: "700px",
        height: "500px",
        data: { title: "DialogFlow json", template: viewDetailsTemp }
      });
    }
  }
  restrictNumberInput(e, value?) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(e.charCode);
    if (e.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if ((value === "phone" && e.target.value.length >= 9) || e.keyCode === 46) {
      event.preventDefault();
    }
  }
}
