import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OrganizationModel } from "../../models/organization.model";
import { OrganizationService } from "../../services/organization.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { DialogComponent } from "../../components/dialog/dialog.component";
import { MatDialog } from "@angular/material";
import { environment } from "../../../environments/environment.prod";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-organization",
  templateUrl: "./organization.component.html",
  styleUrls: ["./organization.component.scss"]
})
export class OrganizationComponent implements OnInit, OnDestroy {
  search: any = "";
  isPagination = true;
  disablePrev = false;
  dialogRef;
  disableNext = true;
  currentPage: number = 1;
  totalpage: number = 0;
  totalPages = [];
  isOrganizationLoad = false;
  organizations: OrganizationModel[] = [];
  unSubscribe = new Subject();
  adminurl;
  deleteIndex;
  userName = this._authService.getUserName();
  token = this._authService.getToken();
  userId = this._authService.getUserId();
  adminUrl = environment.adminUrl;
  constructor(
    private _organizationService: OrganizationService,
    private _authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.adminurl = environment.adminUrl;
    this.activeRoute.queryParams
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(value => (this.currentPage = value.page));

    this.getOrganization(this.currentPage ? this.currentPage : 1, 10);
  }

  getOrganization(page, limit) {
    this._organizationService
      .getOrganizations(page, limit, this.search)
      .then((res: any) => {
        this.isOrganizationLoad = true;
        this.organizations = res.docs;
        this.currentPage = res.page;
        this.totalpage = res.pages;
        this.isPagination = res.total > 1;
        if (res.page > this.totalpage) {
          this.currentPage = 1;
        }
        this.disablePrev = this.currentPage === 1;
        this.disableNext = this.currentPage === this.totalpage;
        let temp = parseInt(this.currentPage / 5 + "", 10);
        if (this.currentPage % 5 === 0) {
          temp--;
        }
        let count = 0;
        this.totalPages = [];
        if (this.totalpage > 5) {
          // @ts-ignore
          for (
            let i =
              this.currentPage + 5 <= this.totalpage
                ? this.currentPage === 1
                  ? this.currentPage
                  : this.currentPage
                : this.totalpage - 4;
            i <=
            (this.currentPage + 5 <= res.pages
              ? this.currentPage + 5
              : this.totalpage);
            i++
          ) {
            count++;
            if (count > 5 || i > this.totalpage) {
              break;
            }
            this.totalPages.push({
              no: i,
              status: i === res["page"]
            });
          }
        } else {
          for (let i = 1; i <= res["pages"]; i++) {
            this.totalPages.push({
              no: i,
              status: i === res["page"]
            });
          }
        }
      });
  }

  /*
  Function for changing Page
  @Params {number} - Argument is the page you want to go to
  Can be next or previous
*/
  changePage(page) {
    this.currentPage = page;
    this.getOrganization(page, 10);
    this.router.navigate(["dashboard", "organizations"], {
      queryParams: {
        page: this.currentPage
      }
    });
  }
  /*
      Function for going to next page
   */
  nextPage() {
    if (!this.disableNext) {
      this.changePage(++this.currentPage);
    }
  }

  /*
     Function for going to prev page
  */
  prevPage() {
    if (!this.disablePrev) {
      this.changePage(--this.currentPage);
    }
  }

  getSearchList(event) {
    if (event.keyCode === 8 || event.keyCode === 46) {
      if (this.search.length === 1) {
        this.search = "";
        this.getOrganization(1, 10);
      }
    }
    if (event.keyCode === 13) {
      this.getOrganization(1, 10);
    }
  }

  deleteOrganization(organizationID) {
    this._organizationService
      .deleteOrganization(organizationID)
      .then((res: any) => {
        this.snackBar.open("Organization deleted successfully.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-success"]
        });
        this.getOrganization(this.currentPage, 10);
      })
      .catch(err => {
        console.log(err);
        this.snackBar.open(
          err.error && err.error.message
            ? err.error.message
            : "Something went wrong.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
      });
  }
  ngOnDestroy(): void {
    this.unSubscribeAllObserver();
  }

  unSubscribeAllObserver() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  openDeleteDialog(deleteTemplate, index) {
    this.deleteIndex = index;
    this.dialogRef = this.dialog.open(DialogComponent, {
      panelClass: "addUser",
      data: {
        title: "Delete Organization",
        template: deleteTemplate,
        isShowSave: false
      },
      height: "auto",
      width: "500px"
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteOrganization(this.organizations[this.deleteIndex]._id);
      }
    });
  }
}
