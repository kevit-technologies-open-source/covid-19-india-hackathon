import { Routes } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";
import { OrganizationComponent } from "./organization/organization.component";
import { CreateOrganizationComponent } from "./organization/create-organization/create-organization.component";
import { BotsComponent } from "./bots/bots.component";

export const dashboardRoutes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    children: [
      {
        path: "",
        redirectTo: "organizations",
        pathMatch: "full"
      },
      {
        path: "organizations",
        component: OrganizationComponent
      },
      {
        path: "organizations/createOrganization",
        component: CreateOrganizationComponent
      },
      {
        path: "organizations/edit/:id",
        component: CreateOrganizationComponent
      },
      {
        path: "bots",
        component: BotsComponent
      }
    ]
  }
];
