import { NgModule } from "@angular/core";
import { OrganizationComponent } from "./organization/organization.component";
import { MaterialModule } from "../material.module";
import { SharedModule } from "../utils/shared.module";
import { RouterModule } from "@angular/router";
import { dashboardRoutes } from "./dashboard.route";
import { DashboardComponent } from "./dashboard.component";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogComponent } from "../components/dialog/dialog.component";
import { CreateOrganizationComponent } from "./organization/create-organization/create-organization.component";
import { BotsComponent } from "./bots/bots.component";
import { OrganizationService } from "../services/organization.service";
import { BotsService } from "../services/bots.service";
import { HeaderDropdownComponent } from "../components/header-dropdown/header-dropdown.component";
import { ClickOutsideDirective } from "../utils/click-outside.directive";

@NgModule({
  imports: [
    MaterialModule,
    SharedModule,
    RouterModule.forChild(dashboardRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    OrganizationComponent,
    DashboardComponent,
    DialogComponent,
    CreateOrganizationComponent,
    BotsComponent,
    HeaderDropdownComponent,
    ClickOutsideDirective
  ],
  entryComponents: [DialogComponent],
  providers: [OrganizationService, BotsService]
})
export class DashboardModule {}
