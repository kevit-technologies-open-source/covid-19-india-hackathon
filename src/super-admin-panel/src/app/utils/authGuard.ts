import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { AuthService } from "../services/auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
  _userActionOccured: Subject<void> = new Subject();
  get userActionOccured(): Observable<void> {
    return this._userActionOccured.asObservable();
  }

  constructor(private router: Router, private _authService: AuthService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const token = this._authService.getToken();
    const user = this._authService.getUserName();
    if (token && user) {
      return true;
    } else {
      this._authService
        .logout()
        .then(res => {
          this.router.navigate(["login"], {
            queryParams: { returnUrl: state.url }
          });
        })
        .catch(err => {
          this.router.navigate(["login"], {
            queryParams: { returnUrl: state.url }
          });
        });

      return false;
    }
  }

  notifyUserAction() {
    this._userActionOccured.next();
  }
}
