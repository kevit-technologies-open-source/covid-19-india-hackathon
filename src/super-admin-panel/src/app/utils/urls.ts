import { environment } from "../../environments/environment";

export class Urls {
  static baseUrl = environment.apiUrl;
  static login = Urls.baseUrl + "auth/login";
  static logout = Urls.baseUrl + "auth/logout";
  static forgetPassword = Urls.baseUrl + "auth/forget-password";
  static changePassword = Urls.baseUrl + "auth/change-password";
  static organization = Urls.baseUrl + "organization/";
  static resetPassword = Urls.baseUrl + "auth/";
  static bot = Urls.baseUrl + "bot-route/";
  static userProfile = Urls.baseUrl + "user/profile";
  static profilePicture = Urls.baseUrl + "user/upload/profile-picture";
}
