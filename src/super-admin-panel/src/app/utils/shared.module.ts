import { LoaderComponent } from "../components/loader/loader.component";
import { NgModule } from "@angular/core";
@NgModule({
  declarations: [LoaderComponent],
  exports: [LoaderComponent]
})
export class SharedModule {}
