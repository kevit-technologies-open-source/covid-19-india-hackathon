import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AuthService } from "../../services/auth.service";
import { LocationStrategy } from "@angular/common";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

declare var $;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  forgetPassForm: FormGroup;
  resetPassForm: FormGroup;
  remember;
  id;
  token;
  isLoadingMain = false;
  formStatus = "login";
  hide = true;

  constructor(
    private formBuild: FormBuilder,
    private _authService: AuthService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.loginForm = this.formBuild.group({
      userId: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
      rememberMe: false
    });
  }

  ngOnInit() {
    this.forgetPassForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email])
    });

    this.resetPassForm = new FormGroup({
      newPassword: new FormControl("", Validators.required),
      confirmPassword: new FormControl("", Validators.required)
    });
    if (this._authService.getUserName() && this._authService.getToken()) {
      this.isLoadingMain = true;
      this.router.navigate(["dashboard"]);
    }
    if (this.router.url.includes("/reset")) {
      this.formStatus = "resetPassword";
    }
    if (this.router.url.includes("/forgot")) {
      this.formStatus = "forgotPassword";
    }
  }

  doLogin() {
    if (this.loginForm.valid) {
      if (!this.loginForm.value.password.trim().length) {
        this.snackBar.open("Please enter valid password.", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      const data = {
        userId: this.loginForm.value.userId.toLowerCase(),
        password: this.loginForm.value.password
      };
      this._authService.setRememberMe(this.loginForm.value.rememberMe);
      this._authService
        .login(data)
        .then((res: any) => {
          if (res.role === "superAdmin") {
            this.isLoadingMain = true;
            this.router.navigate(["dashboard"]);
          } else {
            this.snackBar.open(
              "You don't have rights to login into the system.",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          }
        })
        .catch(err => {
          if (err.error.status === "USER_NOT_FOUND") {
            this.snackBar.open(
              "You have entered wrong email address or password.",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          } else {
            this.snackBar.open(
              err.error.message ? err.error.message : "Something went wrong",
              "",
              {
                horizontalPosition: "right",
                duration: 3000,
                panelClass: ["snack-error"]
              }
            );
          }
        });
    } else {
      this.snackBar.open("Please enter all the required details.", "", {
        horizontalPosition: "right",
        duration: 3000
      });
    }
  }

  forgotPass() {
    this.formStatus = "forgotPassword";
    if ((this.formStatus = "forgotPassword")) {
      this.forgetPassForm.controls["email"].setValue(
        this.loginForm.controls["userId"].value
      );
    }
  }

  backToLogin() {
    this.formStatus = "login";
  }

  sendEmail() {
    if (this.forgetPassForm.valid) {
      let data = {
        email: this.forgetPassForm.controls["email"].value.toLowerCase()
      };
      this._authService
        .forgetPassword(data)
        .then((res: any) => {
          this.snackBar.open(
            "Reset password link has been sent to your email address.",
            "",
            {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-success"]
            }
          );
        })
        .catch(err => {
          this.snackBar.open(
            err.error.message ? err.error.message : "Something went wrong.",
            "",
            {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-error"]
            }
          );
        });
    } else {
      this.snackBar.open("Please enter email address.", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
    }
    this.formStatus = "login";
  }

  doResetPassword() {
    if (this.resetPassForm.valid) {
      if (!this.resetPassForm.controls["newPassword"].value.trim().length) {
        this.snackBar.open("Please enter valid new password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (!this.resetPassForm.controls["confirmPassword"].value.trim().length) {
        this.snackBar.open("Please enter valid confirm password", "", {
          horizontalPosition: "right",
          duration: 3000,
          panelClass: ["snack-error"]
        });
        return;
      }
      if (
        this.resetPassForm.controls["newPassword"].value !==
        this.resetPassForm.controls["confirmPassword"].value
      ) {
        this.snackBar.open(
          "New password and confirm password does not match.",
          "",
          {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-error"]
          }
        );
        return;
      }
      this.id = this.route.snapshot.params.id;
      this.route.queryParams.subscribe(value => (this.token = value.token));
      console.log(this.token);
      const data = {
        token: this.token,
        password: this.resetPassForm.controls["newPassword"].value
      };
      this._authService
        .resetPassword(data, this.id)
        .then((res: any) => {
          this.snackBar.open("Password reset successfully.", "", {
            horizontalPosition: "right",
            duration: 3000,
            panelClass: ["snack-success"]
          });
        })
        .catch(err => {
          this.snackBar.open(
            err.error && err.error.message
              ? err.error.message
              : "Something went wrong.",
            "",
            {
              horizontalPosition: "right",
              duration: 3000,
              panelClass: ["snack-error"]
            }
          );
        });
    } else {
      this.snackBar.open("Please enter all fields.", "", {
        horizontalPosition: "right",
        duration: 3000,
        panelClass: ["snack-error"]
      });
    }
    this.formStatus = "login";
  }

  handleEnter($event: any, save?) {
    this.hide = !this.hide;
    if (save === "setPassword") {
      this.doResetPassword();
    } else if (save === "doLogin") {
      this.doLogin();
    }
  }
}
