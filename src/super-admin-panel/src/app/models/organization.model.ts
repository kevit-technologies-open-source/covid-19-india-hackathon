export class OrganizationModel {
  _id: string;
  name: string;
  createdAt: Date;
  originAdminId: {
    _id: string;
    email: string;
    name: string;
  };
}
