import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Urls } from "../utils/urls";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  user: any;
  token: string;
  isAdminLogin = false;
  profilePicture;
  remember;

  constructor(private http: HttpClient, private router: Router) {
    const u = localStorage.getItem("user");
    this.user = u ? u : null;
    const t = localStorage.getItem("token");
    this.token = t ? t : null;
    const r = localStorage.getItem("remember");
    this.remember = r ? r : false;
    const p = localStorage.getItem("profile");
    this.remember = p ? p : false;
  }

  login(creds) {
    return new Promise((resolve, reject) => {
      return this.http.post(Urls.login, creds).subscribe(
        (res: any) => {
          this.token = res.token;
          this.user = res.name;
          this.profilePicture = res.profilePicture;
          if (res.role === "superAdmin") {
            if (this.getRememberMe()) {
              localStorage.setItem("userId", res._id);
              localStorage.setItem("token", this.token);
              localStorage.setItem("user", this.user);
              localStorage.setItem("profile", this.profilePicture);
            } else {
              sessionStorage.setItem("userId", res._id);
              sessionStorage.setItem("token", this.token);
              sessionStorage.setItem("user", this.user);
              sessionStorage.setItem("profile", this.profilePicture);
            }
          }

          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  forgetPassword(data) {
    return new Promise((resolve, reject) => {
      return this.http.post(Urls.forgetPassword, data).subscribe(
        (res: any) => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  changePassword(data) {
    return new Promise((resolve, reject) => {
      return this.http.put(Urls.changePassword, data).subscribe(
        (res: any) => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  resetPassword(data, id) {
    return new Promise((resolve, reject) => {
      return this.http
        .post(Urls.resetPassword + id + "/reset-password", data)
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  logout() {
    return new Promise((resolve, reject) => {
      this.http
        .put(Urls.logout, { headers: { authorization: this.getToken() } })
        .subscribe(
          res => {
            if (this.getRememberMe()) {
              localStorage.removeItem("userId");
              localStorage.removeItem("token");
              localStorage.removeItem("user");
              localStorage.removeItem("profile");
            } else {
              sessionStorage.removeItem("userId");
              sessionStorage.removeItem("token");
              sessionStorage.removeItem("user");
              sessionStorage.removeItem("profile");
            }
            this.token = "";
            resolve(res);
          },
          err => {
            if (err.hasOwnProperty("error")) {
              console.log(err);
              if (err.error.statusCode === 401) {
                if (this.getRememberMe()) {
                  localStorage.removeItem("token");
                  localStorage.removeItem("user");
                  localStorage.removeItem("profile");
                } else {
                  sessionStorage.removeItem("token");
                  sessionStorage.removeItem("user");
                  sessionStorage.removeItem("profile");
                }
                this.router.navigate(["login"]);
                this.token = "";
              }
            }
            reject(err);
          }
        );
    });
  }
  getToken() {
    if (this.getRememberMe()) {
      return localStorage.getItem("token");
    } else {
      return sessionStorage.getItem("token");
    }
  }

  getUserName() {
    if (this.getRememberMe()) {
      return localStorage.getItem("user");
    } else {
      return sessionStorage.getItem("user");
    }
  }
  getUserId() {
    if (this.getRememberMe()) {
      return localStorage.getItem("userId");
    } else {
      return sessionStorage.getItem("userId");
    }
  }

  getUserProfile() {
    if (this.getRememberMe()) {
      return localStorage.getItem("profile");
    } else {
      return sessionStorage.getItem("profile");
    }
  }

  setUserName(name) {
    if (this.getRememberMe()) {
      localStorage.setItem("user", name);
    } else {
      sessionStorage.setItem("user", name);
    }
  }
  setUserProfile(profile) {
    if (this.getRememberMe()) {
      localStorage.setItem("profile", profile);
    } else {
      sessionStorage.setItem("profile", profile);
    }
  }
  setRememberMe(val) {
    localStorage.setItem("remember", val);
  }

  getRememberMe() {
    return JSON.parse(localStorage.getItem("remember"));
  }
}
