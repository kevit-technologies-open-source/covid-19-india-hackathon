import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Urls } from "../utils/urls";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: "root"
})
export class UserProfileService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private _authService: AuthService
  ) {}

  getUser() {
    return new Promise((resolve, reject) => {
      return this.http.get(Urls.userProfile).subscribe(
        (res: any) => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  updateUser(data) {
    return new Promise((resolve, reject) => {
      return this.http.put(Urls.userProfile, data).subscribe(
        (res: any) => {
          resolve(res);
          if (this._authService.getRememberMe()) {
            localStorage.setItem("user", res.name);
          } else {
            sessionStorage.setItem("user", res.name);
          }
        },
        err => {
          reject(err);
        }
      );
    });
  }

  updateProfilePicture(oldFileName, fd) {
    return new Promise((resolve, reject) => {
      return this.http
        .post(Urls.profilePicture + "?oldImageName=" + oldFileName, fd)
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  deleteProfilePic(imageName) {
    return new Promise((resolve, reject) => {
      return this.http
        .delete(Urls.baseUrl + "user/profile-picture?oldImageName=" + imageName)
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
}
