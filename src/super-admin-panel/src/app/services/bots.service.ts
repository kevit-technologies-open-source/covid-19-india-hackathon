import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Urls } from "../utils/urls";

@Injectable()
export class BotsService {
  constructor(private http: HttpClient) {}

  getBotList(page, limit, search) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          Urls.bot + "?page=" + page + "&limit=" + limit + "&search=" + search
        )
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
  getBotDetails(botId) {
    return new Promise((resolve, reject) => {
      this.http.get(Urls.bot + botId).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  setBotOnHold(botId, data) {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.bot + botId + "/putonhold", data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  deleteBot(botId) {
    return new Promise((resolve, reject) => {
      this.http.delete(Urls.bot + botId).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
