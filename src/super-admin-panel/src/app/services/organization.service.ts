import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Urls } from "../utils/urls";

@Injectable()
export class OrganizationService {
  constructor(private http: HttpClient) {}

  getOrganizations(page, limit, search) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          Urls.organization +
            "?page=" +
            page +
            "&limit=" +
            limit +
            "&search=" +
            search
        )
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
  createOrganization(data) {
    return new Promise((resolve, reject) => {
      this.http.post(Urls.organization, data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  getOrganizationById(organizationID) {
    return new Promise((resolve, reject) => {
      this.http.get(Urls.organization + organizationID).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  updateOrganization(organizationID, data) {
    return new Promise((resolve, reject) => {
      this.http.put(Urls.organization + organizationID, data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
  deleteOrganization(organizationID) {
    return new Promise((resolve, reject) => {
      this.http.delete(Urls.organization + organizationID).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
