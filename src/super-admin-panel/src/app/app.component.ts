import { Component, OnInit } from "@angular/core";
import { AuthGuard } from "./utils/authGuard";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  loading;
  constructor(private authGuard: AuthGuard) {
    this.loading = false;
  }
}
